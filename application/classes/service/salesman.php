<?php defined('SYSPATH') or die('No direct script access.');


class Service_Salesman
{

    public static function get_salesman($name)
    {
        return ORM::factory('salesman')
            ->where('LOWER("name")', "=", mb_strtolower($name))
            ->find();
    }

    public static function create_salesman($name, $language_id = 1)
    {
        $salesman = ORM::factory('salesman');

        $salesman->name = $name;
        $salesman->language($language_id);
        $salesman->save();

        self::set_last_order($salesman);

        return $salesman;
    }

    public static function set_last_order(Model_Salesman $salesman)
    {
        $last = ORM::factory('salesman')
            ->order_by('poradi', 'desc')
            ->find();

        $salesman->poradi = $last->poradi + 1;
        $salesman->save();
    }
}