<?php defined('SYSPATH') or die('No direct script access.');

class Service_Gallery extends Service_Page
{

	public static function get_gallery_by_route_id($route_id) {
		$result_array = array();

		$gallery_orm = orm::factory("gallery")
				->where("route_id","=",$route_id)
				->find();
		$result_array = $gallery_orm->as_array();
		
		return $result_array;
	}

	public static function get_gallery_photos($language_id, $gallery_id, $limit = 100, $offset = 0) {
		$result_array = array();

		$photos = orm::factory("gallery_photo")
						->where("gallery_id","=",$gallery_id)
						->language($language_id)
						->limit($limit)
						->offset($offset)
						->order_by("poradi","DESC")
						->find_all();
		foreach ($photos as $photo) {
			$result_array[$photo->id] = $photo->as_array();
			$result_array[$photo->id]["photo"] = self::_photo_way_generator($photo->photo_src,"media/photos/gallery/item/gallery/images-".$photo->gallery->id."/",array("ad"=>"jpg","small"=>"jpg"));
		}
		return $result_array;
	}

	public static function get_galleries($language_id) {
		$result_array = array();

		$years_orm = orm::factory("gallery")
				->select(array(DB::expr('YEAR(`date`)'),"year"))
				->language($language_id)
				->group_by(DB::expr('YEAR(`date`)'))
				->order_by(DB::expr('YEAR(`date`)'),"desc")
				->find_all();

		foreach ($years_orm as $year) {
			$res_year = array();
			$galeries_orm = orm::factory("gallery")
					->where(DB::expr('YEAR(`date`)'),"=",$year->year)
					->language($language_id)
					->order_by("poradi","ASC")
					->find_all();
			foreach ($galeries_orm as $gallery) {
				$res_year[$gallery->id] = $gallery->as_array();
				$res_year[$gallery->id]["nazev_seo"] = $gallery->route->nazev_seo;

				$photo = $gallery->gallery_photos
						->language($language_id)
						->order_by("poradi","ASC")
						->find();
				$res_year[$gallery->id]["photo"] = self::_photo_way_generator($photo->photo_src,"media/photos/gallery/item/gallery/images-".$photo->gallery->id."/",array("small"=>"jpg"));

				$res_year[$gallery->id]["count"] = self::get_gallery_total_photo_count($language_id,$gallery->id); 
			}
			$result_array[$year->year] = $res_year; 
		}
		return $result_array;
	}

	public static function get_gallery_total_photo_count($language_id,$gallery_id) {
		$gallery_photos = orm::factory("gallery",$gallery_id)->gallery_photos
						->language($language_id)
						->find_all();
				return $gallery_photos->count();
	}
	public static function get_last_photos($laguage_id,$limit = 3) {
		$result_array = array();
		$gallery_orm = orm::factory("gallery_photo")
				->language($laguage_id)
				->order_by("gallery_photos.id","DESC")
				->limit($limit)
				->find_all();

		foreach ($gallery_orm as $photo) {
			$result_array[$photo->id] = $photo->as_array();
			$result_array[$photo->id]["photo"] = self::_photo_way_generator($photo->photo_src,"media/photos/gallery/item/gallery/images-".$photo->gallery->id."/",array("small"=>"jpg"));
			$result_array[$photo->id]["gallery"] =$photo->gallery->as_array();
			$result_array[$photo->id]["gallery"]["nazev_seo"] =$photo->gallery->route->nazev_seo;
		}

		return $result_array;
	}

	public static function get_last_galleries($limit = 3) {
		$result_array = array();
		$query = DB::query(Database::SELECT, 'SELECT galleries.id, galleries.date, gallery_data.route_id, gallery_data.nazev, routes.nazev_seo, gallery_photos.photo_src FROM galleries INNER JOIN gallery_data ON galleries.id = gallery_data.gallery_id INNER JOIN routes ON routes.id = gallery_data.route_id INNER JOIN gallery_photos ON gallery_photos.gallery_id = galleries.id GROUP BY gallery_photos.gallery_id ORDER BY date DESC LIMIT :limit');
		$query->param(':limit', $limit);
		$results = $query->execute()->as_array();
		foreach ($results as $result) {
			$result_array[$result["id"]]["photo_src"] = "media/photos/gallery/item/gallery/images-".$result["id"]."/".$result["photo_src"]."-small.jpg";
			$result_array[$result["id"]]["nazev"] = $result["nazev"];
			$result_array[$result["id"]]["nazev_seo"] = $result["nazev_seo"];
		}
		return $result_array;
	}
}

