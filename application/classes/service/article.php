<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * Servisa pro obsluhu clanku.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Article extends Service_Hana_Module_Base
{
    public static $navigation_module = "article";
    public static $order_by = "date";
    public static $order_direction = "desc";

    private static $date_format = "Y-00-00";


    public static $photos_resources_dir = "media/photos/";
    public static $photos_resources_subdir = "";
    protected static $thumbs = array('small' => 't1', 'big' => 'ad');

    public static $months = array("Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec");

    /**
     * Nacte clanek dle route_id
     * @param int $id
     * @return array
     */
    public static function get_article_by_route_id($id)
    {
        $article = orm::factory(self::$navigation_module)->where("route_id", "=", $id)->find();

        $result_data = array();
        $result_data = $article->as_array();
        $result_data["nazev_seo"] = $article->route->nazev_seo;

        $date = new DateTime($article->date);
        $result_data["month"] = self::$months[$date->format("m") - 1];

        $result_data["photo"] = Service_Page::_photo_way_generator($article->photo_src, self::$photos_resources_dir . self::$navigation_module . "/item/" . self::$photos_resources_subdir . "images-" . $article->id . "/", array("ad"=>"jpg"));
        // $filename = self::$photos_resources_dir . self::$navigation_module . "/item/" . self::$photos_resources_subdir . "images-" . $article->id . "/" . $article->photo_src . "-ad.jpg";
        // if (file_exists(str_replace('\\', '/', DOCROOT) . $filename)) {
        //     $result_data["photo"] = url::base() . $filename;
        // } else {
        //     $result_data["photo"] = false;
        // }

        $result_data['photos'] = array();
        $photos_orm = $article->article_photos
            ->where('zobrazit', '=', 1)
            ->where('language_id', '=', $article->language_id)
            ->order_by('poradi', 'asc')
            ->find_all();

        foreach ($photos_orm as $photo) {
            $result_data['photos'][$photo->id] = $photo->as_array();
            foreach (self::$thumbs as $key => $thumb) {

                $dirname = self::$photos_resources_dir . self::$navigation_module . "/item/gallery/images-" . $article->id . '/' . $photo->photo_src . '-' . $thumb . '.jpg';

                if ($photo->photo_src && file_exists(str_replace('\\', '/', DOCROOT) . $dirname)) {

                    $result_data['photos'][$photo->id][$key] = $dirname;
                }
            }
        }


        return $result_data;
    }

    public static function get_article_total_items_list($language_id, $category = 0)
    {
        return DB::select(db::expr("COUNT(articles.id) as pocet"))->from("articles")->join("article_data")->on("articles.id", "=", "article_data.article_id")->join("routes")->on("article_data.route_id", "=", "routes.id")->where("routes.zobrazit", "=", 1)->where("routes.language_id", "=", $language_id)->execute()->get("pocet");
    }

    /**
     * Nacte sadu clanku podle kategorie a jazykove verze
     * @param type $language_id
     * @return boolean
     */
    public static function get_article_list($language_id = 0, $category = 0, $limit = 100, $offset = 0, $without = 0)
    {
        $articles = orm::factory("article")
            ->join("routes")->on("article_data.route_id", "=", "routes.id")
            ->language($language_id)
            //->where("article_category_id","=",db::expr($category))
            ->where("routes.zobrazit", "=", 1)
            ->where("articles.id", "!=", $without)
            ->order_by(self::$order_by, self::$order_direction)
            ->limit($limit)
            ->offset($offset)
            ->find_all();

        $result_data = array();
        foreach ($articles as $article) {
            $date = new DateTime($article->date);
            $result_data[$article->id] = $article->as_array();
            $result_data[$article->id]["nazev_seo"] = $article->route->nazev_seo;
            $result_data[$article->id]["month"] = self::$months[$date->format("m") - 1];
            
            foreach ($article->article_categories->find_all() as $key => $acat) {
                $result_data[$article->id]["categories"][$acat->id] = $acat->as_array();
            }

            
            $result_data[$article->id]['photo'] = Service_Page::_photo_way_generator($article->photo_src,self::$photos_resources_dir . self::$navigation_module . "/item/" . self::$photos_resources_subdir . "images-" . $article->id . "/",array("home"=>"jpg"));

        }

        return $result_data;
    }

    /**
     * Nacte sadu clanku podle kategorie a jazykove verze
     * @param type $language_id
     * @param type $category
     * @return boolean
     */
    public static function get_article_banner_list($language_id = 0, $limit = 2)
    {
        $articles = orm::factory("article")
            ->join("routes")->on("routes.id", "=", "article_data.route_id")
            ->where("routes.zobrazit", "=", 1)
            ->language($language_id)
            ->order_by(self::$order_by, self::$order_direction)
            ->limit($limit)
            ->find_all();

        $result_data = array();
        foreach ($articles as $article) {
            $date = new DateTime($article->date);
            $result_data[$article->id] = $article->as_array();
            $result_data[$article->id]["nazev_seo"] = $article->route->nazev_seo;
            $result_data[$article->id]["month"] = self::$months[$date->format("m") - 1];

            $filename = self::$photos_resources_dir . self::$navigation_module . "/item/" . self::$photos_resources_subdir . "images-" . $article->id . "/" . $article->photo_src . "-t2.jpg";
            if (file_exists(str_replace('\\', '/', DOCROOT) . $filename)) {
                $result_data[$article->id]["photo"] = url::base() . $filename;
            } else {
                $result_data[$article->id]["photo"] = false;
            }

        }
        //die(print_r($result_data));
        return $result_data;
    }

    /**
     * Returns categories for this module
     * @param int $route_id
     * @param int|null $category_id vehicle category id
     * @return array
     */
    public static function get_articles_by_route_id($route_id, $category_id = null)
    {
        $return = array();
        $route = ORM::factory('route', $route_id);
        $module = $route->module->kod;
        if ($route->module_action != 'index' && $route->module_action != 'detail') {
            $module .= '_' . $route->module_action;
        }

        if (!class_exists('Model_' . $module)) {
            if ($module == 'service' && $route->module_action == 'detail')
                $module = 'vehicle_service';
            else
                $module = 'page';

        }

        $model = ORM::factory($module)
            ->where('route_id', '=', $route->id)
            ->find();

        if ($module == 'article_category')
            return self::get_articles_from_category($model);

        if (!is_null($category_id))
            $model = ORM::factory('vehicle_category')
                ->where('vehicle_categories.id', '=', $category_id)
                ->language($route->language_id)
                ->find();

        $article_categories = $model
            ->article_categories
            ->join('routes')->on('routes.id', '=', 'article_category_data.route_id')
            ->where('routes.zobrazit', '=', 1)
            ->where('language_id', '=', $route->language_id)
            ->order_by('poradi')
            ->find_all();

        $i = 0;
        foreach ($article_categories as $category) {
            $return[$i] = $category->as_array();
            $return[$i]['nazev_seo'] = $category->route->nazev_seo;
            $return[$i]['articles'] = self::get_articles_from_category($category);
            $i++;
        }

        return $return;
    }

    /**
     * Returns all articles in category
     * @param Model_Article_Category $category
     * @return array
     */
    public static function get_articles_from_category(Model_Article_Category $category)
    {
        $return = array();

        $articles = $category
            ->articles
            ->join('routes')->on('routes.id', '=', 'article_data.route_id')
            ->where('language_id', '=', $category->language_id)
            ->where('routes.zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();

        $i = 0;
        foreach ($articles as $article) {
            $return[$i] = $article->as_array();
            $return[$i]['nazev_seo'] = $article->route->nazev_seo;
            $i++;
        }

        return $return;
    }

    /**
     * Return background for header
     * @param Model_Route $route
     * @return string
     */
    public static function get_header(Model_Route $route)
    {
        $bg_template = new View('header/simple');

        if ($route->module_action == 'index') {
            $page = Service_Page::get_page_by_route_id($route->id);
            $bg_template->heading = $page['nadpis'];

            return $bg_template->render();
        } else {
            $article = self::get_article_by_route_id($route->id);
            $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();

            $bg_template->heading = $article['nadpis'];
            $bg_template->back_link = current($sel_links);

            return $bg_template->render();
        }
    }

    /**
     * Search configuration
     * @return array
     */
    public static function search_config()
    {
        return array(
            "title" => "Výsledky v článcích",
            "display_title" => "article_data.nazev",
            "display_text" => "article_data.uvodni_popis",
            "search_columns" => array("article_data.nazev", "article_data.popis", "article_data.uvodni_popis"),
        );
    }


    /**
     * Returns articles in the year
     * @param DateTime $date
     * @param int $language_id
     * @return array
     */
    public static function get_articles_by_date(DateTime $date, $language_id = 0)
    {
        $return = array();

        $next_year = clone $date;
        $next_year->add(new DateInterval("P1Y"));

        $articles = ORM::factory("article")
            ->join("routes")->on("article_data.route_id", "=", "routes.id")
            ->language($language_id)
            ->where("routes.zobrazit", "=", 1)
            ->where("date", ">=", $date->format(self::$date_format))
            ->where("date", "<", $next_year->format(self::$date_format))
            ->order_by("date")
            ->order_by("poradi")
            ->find_all();

        $i = 0;
        foreach ($articles as $article) {
            $return[$i] = $article->as_array();
            $return[$i]["route"] = $article->route->as_array();
            $return[$i]["photo"] = Service_Page::_photo_way_generator($article->photo_src, "media/photos/article/item/images-".$article->id."/", array("home"=>"jpg"));
            $i++;
        }

        return $return;
    }

    /**
     * Returns the year of the oldest article
     * @param int $language_id
     * @return DateTime
     */
    public static function get_the_oldest_year($language_id = 0)
    {
        return self::get_year();
    }

    /**
     * Returns the year of the youngest article
     * @param int $language_id
     * @return DateTime
     */
    public static function get_the_latest_year($language_id = 0)
    {
        return self::get_year("DESC");
    }

    /**
     * @param string $order
     * @param int $language_id
     * @return DateTime
     */
    public static function get_year($order = "ASC", $language_id = 0)
    {
        return new DateTime(ORM::factory("article")
            ->language($language_id)
            ->order_by("date", $order)
            ->find()
            ->date);
    }

}

?>

