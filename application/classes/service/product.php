<?php defined('SYSPATH') or die('No direct script access.');

class Service_Product extends Service_Hana_Product
{

    public static $photo_thumb_suffix = "t2";
    /**
     * Získá náhodné produkty v dané kategorie.
     * @param $cat_id
     * @param $limit
     * @param int $language_id
     * @param array $without
     * @return array
     */
    public static function get_random_products_by_category($cat_id, $limit, $language_id = 0, $without = array())
    {
        $return = array();
        $products = ORM::factory("product_category", $cat_id)
            ->products
            ->join("routes")->on("product_data.route_id", "=", "routes.id")
            ->language($language_id)
            ->where("routes.zobrazit", "=", 1)
            ->where("products.smazano", "!=", 1)
            ->where("parent_id","=",0)
            ->order_by(DB::expr('RAND()') );
        if (!empty($without))
            $products = $products->where("products.id", "not in", $without);
        $products = $products
            ->limit($limit)
            ->find_all();

            //->where("pocet_na_sklade", ">", 0)

        $index = 0;
        foreach ($products as $product)
        {
            $return[$index] = $product->as_array();
            $return[$index]["cena_s_dph"] = $product->prices()->get_total_price_with_tax();
            $return[$index]["cena_bez_dph"] = $product->prices()->get_total_price_without_tax();
            $return[$index]["cena_s_dph_bez_slevy"] = $product->prices()->get_raw_price_with_tax();
            $return[$index]["nazev_seo"] = $product->route->nazev_seo;
            $filename = self::$photos_resources_dir."product/item/images-".$product->id."/".$product->photo_src."-at.jpg";
            if($product->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$filename))
            {
                $return[$index]["photo"]=url::base().$filename;
            } else {
                $return[$index]["photo"]=url::base().self::$no_photo;
            }
            $index++;
        }


        return $return;
    }

    public static function get_random_product_by_product($route_id, $limit) {
        $return_data =  array();
        $without = array();
        $product = Service_Product::get_product_detail_by_route_id($route_id);
        $product_orm = orm::factory('product',$product["id"]);

        $without[] = $product["id"];
        $category_orm = $product_orm->product_categories->limit(1)->order_by("product_categories_products.id", "DESC")->find();
        //die(var_dump($categories->id));
        $return_data = self::get_random_products_by_category($category_orm->id,3,$product_orm->language_id,$without);
        /*if($categories!=null){
            while(sizeof($return_data)<$limit) {
                foreach ($categories as $key => $cat) {
                    if(sizeof($return_data)<$limit){
                    $rand_product = self::get_random_products_by_category($cat->id,1,$product_orm->language_id,$without);
                    $rand_product = array_pop($rand_product);
                    $without[] = $rand_product["id"];
                    $return_data[$rand_product["id"]] = $rand_product;
                }
                }
            }
        }*/

        return $return_data;

    }
    public static function update_rating($product_id)
    {
        $product = ORM::factory("product", $product_id);
        $ratings = Service_Product_Rating::get_rating($product_id);
        $average = Service_Product_Rating::calculare_rating($ratings);
        $product->rating = $average;
        $product->rating_count = count($ratings);
        $product->save();
    }
    public static function get_favourite_cat($limit=12, $language_id = 0, $favorite=1, $category=false){
        $categories_data = array();

        $categories_orm = ORM::factory("product_category")
                        ->where("zobrazit","=",1);
        if($favorite)
         $categories_orm=$categories_orm ->where("zobrazit_carousel","=",$favorite);
        if(!is_null($category))
        $categories_orm=$categories_orm ->where("parent_id","=",$category);

        $categories_orm =$categories_orm->language($language_id)
                        ->limit($limit)
                        ->order_by("poradi", "ASC")
                        ->find_all();

        foreach ($categories_orm as $key => $category) {
            $categories_data[$category->id] = $category->as_array();


                $categories_data[$category->id]["nazev_seo"] = $category->route->nazev_seo;
                $categories_data[$category->id]["photo"] = "";

                $filename=self::$photos_resources_dir."product/category/images-".$category->id."/". $category->photo_src."-t2.jpg";
                if( $category->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$filename))
                {
                    $categories_data[$category->id]["photo"]=url::base().$filename;
                }
            }

        return $categories_data;
    }

    public static function get_favourite($limit=4, $skladem = false, $language_id = 0)
    {
        $product_data=array();
            $products_orm = ORM::factory('product')
                ->join("routes")->on('product_data.route_id',"=","routes.id")
                ->where("routes.zobrazit", "=", 1)
                ->where("products.smazano", "!=", 1)
                ->where("zobrazit_carousel","=",1)
                ->language($language_id)
                ->where("k_prodeji", "=", 1);
            if ($skladem)
                 $products_orm->where("pocet_na_sklade", ">", 0);
            $products_orm = $products_orm->order_by("rating", "DESC")
                ->order_by("rating_count", "DESC")
                ->order_by("imported", "DESC")
                ->limit($limit)
                ->find_all();



            foreach($products_orm as $item)
            {
                $product_data[$item->id]=$item->as_array();
                $product_data[$item->id]["nazev_seo"]=$item->route->nazev_seo;

                // ke kazdemu produktu ziskam podle cenove skupiny i hodnotu ceny
                $product_data[$item->id]["cena_s_dph"] = orm::factory("product")->where("products.id","=",$item->id)->find()->prices()->get_total_price_with_tax();
                $product_data[$item->id]["cena_bez_dph"] = orm::factory("product")->where("products.id","=",$item->id)->find()->prices()->get_total_price_without_tax();


                // cesta k obrazku
                $filename=self::$photos_resources_dir."product/item/images-".$item->id."/". $item->photo_src."-at.jpg";
                if( $item->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$filename))
                {
                    $product_data[$item->id]["photo"]=url::base().$filename;
                } else {
                    $product_data[$item->id]["photo"]=url::base().self::$no_photo;
                }

            }

        return $product_data;
    }

    public static function get_all_products($language_id = 1, $limit = 0)
    {
        $return = array();
        $base_url = (isset($_SERVER["REQUEST_SCHEME"]) ? $_SERVER["REQUEST_SCHEME"] : "http")."://".$_SERVER["SERVER_NAME"]."/";
        $products = ORM::factory("product")
            ->join("routes")->on("product_data.route_id", "=", "routes.id")
            ->language($language_id)
            ->where("routes.zobrazit", "=", 1)
            ->where("products.smazano","=",0)
            ->where("products.parent_id","=",0)
            ->order_by("poradi");
        if ($limit > 0)
            $products->limit($limit);
        $products = $products->find_all();

        $i = 0;
        foreach ($products as $product) {

            $variants = Service_Product::get_product_variant_detail_by($product->id); 
            
            foreach ($variants as $variant) {
                $return[] = $variant;
                $return[$i]["full_address"] = $base_url.$product->route->nazev_seo;
                $return[$i]["full_address_photo"] = $base_url.self::$photos_resources_dir."/product/item/images-".$product->id."/".$product->photo_src."-t1.jpg";

                $i++;
            }    
            
        }

        return $return;

    }

    public static function get_favourite_items($limit=4)

    {

//$products = DB::select('*',array(DB::expr('COUNT(`product_id`)'), 'total'))->from('order_items')->order_by('total') ->as_object()->execute();;;

//$products = DB::select('product_id')->from('order_items') ->execute()->as_array();;;
        $products= orm::factory("order_item")->order_by('product_id')->find_all();
        foreach($products as $item){
            // $productss[]['pocet']=0;
            $productss[]=$item['product_id'];

            //print_r($item['product_id']."''");

        }



        /*
          $product_data=array();

          $products_orm=orm::factory("product")->join("routes")->on("product_data.route_id","=","routes.id")->where("routes.zobrazit","=",1)->where("zobrazit_carousel","=",1)->limit($limit)->find_all();
          foreach($products_orm as $item)
          {
                 $product_data[$item->id]=$item->as_array();
                 $product_data[$item->id]["nazev_seo"]=$item->route->nazev_seo;

                     // ke kazdemu produktu ziskam podle cenove skupiny i hodnotu ceny
          $product_data[$item->id]["cena_s_dph"] = orm::factory("product")->where("products.id","=",$item->id)->find()->prices()->get_total_price_with_tax();
          $product_data[$item->id]["cena_bez_dph"] = orm::factory("product")->where("products.id","=",$item->id)->find()->prices()->get_total_price_without_tax();


                  // cesta k obrazku
             $filename=self::$photos_resources_dir."product/item/images-".$item->id."/". $item->photo_src."-t2.jpg";
             if( $item->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$filename))
             {
                  $product_data[$item->id]["photo"]=url::base().$filename;
             }

          }

          return $product_data;*/
    }

    public static function insert_parent_product($products) {
        $result_data = array();
        foreach ($products as $product) {
            $product_orm = orm::factory("product",$product["id"]);
            $parent_product_orm = orm::factory("product",$product_orm->parent_id);

            $result_data[$product_orm->id] = $product;
            $result_data[$product_orm->id]["parent"] = $parent_product_orm->as_array();
            $result_data[$product_orm->id]["parent"]["nazev_seo"] = $parent_product_orm->route->nazev_seo;

            $filename=self::$photos_resources_dir."product/item/images-".$parent_product_orm->id."/". $parent_product_orm->photo_src."-ad.jpg";
                if( $parent_product_orm->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$filename))
                {
                    $result_data[$product_orm->id]["parent"]["photo"]=url::base().$filename;
                } else {
                    $result_data[$product_orm->id]["parent"]["photo"]=url::base().self::$no_photo;
                }
        }

        return $result_data;
    }

    public static function searchProducts($products)
    {
        $results = array();
        foreach ($products as $product) {
            $res = orm::factory("product")
                ->where('products.id','=', $product['id'])
                ->find();
            array_push($results, $res);
        }

        $return = array();
        $index = 0;
        foreach ($results as $product)
        {
            $return[$index] = $product->as_array();
            $return[$index]["cena_s_dph"] = $product->prices()->get_total_price_with_tax();
            $return[$index]["cena_bez_dph"] = $product->prices()->get_total_price_without_tax();
            $return[$index]["cena_s_dph_bez_slevy"] = $product->prices()->get_raw_price_with_tax();
            $return[$index]["nazev_seo"] = $product->route->nazev_seo;
            $filename = self::$photos_resources_dir."product/item/images-".$product->id."/".$product->photo_src."-at.jpg";
            if($product->photo_src && file_exists(str_replace('\\', '/',DOCROOT).$filename))
            {
                $return[$index]["photo"]=url::base().$filename;
            } else {
                $return[$index]["photo"]=url::base().self::$no_photo;
            }
            $index++;
        }

        return $return;
    }

    public static function search_config()
    {
        return array(
            "title" => "Produkty",
            "display_title" => "product_data.nazev",
            "display_text" => "product_data.uvodni_popis",
            "search_columns" => array("product_data.nazev", "product_data.uvodni_popis", "product_data.popis")
        );
    }
}

?>