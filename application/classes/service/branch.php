<?php defined('SYSPATH') or die('No direct script access.');


class Service_Branch
{

    public static function get_branch($name)
    {
        return ORM::factory('branch')
            ->where('LOWER("nazev")', "like", "%" . mb_strtolower($name) . "%")
            ->find();
    }

    public static function create_branch($name, $language_id = 1)
    {
        $branch = ORM::factory('branch');

        $branch->nazev = $name;
        $branch->language($language_id);
        $branch->save();

        self::set_last_order($branch);

        return $branch;
    }

    public static function set_last_order(Model_Branch $branch)
    {
        $last = ORM::factory('salesman')
            ->order_by('poradi', 'desc')
            ->find();

        $branch->poradi = $last->poradi + 1;
        $branch->save();
    }
}