<?php defined('SYSPATH') or die('No direct script access.');

class Service_Watchdog {

    /**
     * Vytvoří nový záznam pokud již neexistuje, jinak pouze aktualizuje
     * @param $product_id
     * @param $shopper_id
     * @param $price
     * @param $in_stock
     * @param $language_id
     * @return ORM
     */
    public static function save($product_id, $shopper_id, $price, $in_stock = false, $language_id = 1)
    {
        if (self::check($product_id, $shopper_id)) {
            $watchdog = self::get_watchdog($product_id, $shopper_id);
        } else {
            $watchdog = ORM::factory("watchdog");
            $watchdog->product_id = $product_id;
            $watchdog->shopper_id = $shopper_id;
            $watchdog->language_id = $language_id;
            $watchdog->in_stock = $in_stock;
        }

        $watchdog->price = $price;
        $watchdog->in_stock = $in_stock;
        return $watchdog->save();
    }

    /**
     * Zkontroluje, zda již neexistuje záznam
     * @param $product_id
     * @param $user_id
     * @return boolean
     */
    public static function check($product_id, $user_id)
    {
        return self::get_watchdog($product_id, $user_id)->loaded();
    }

    /**
     * Získá psa podle ... níže uvedené
     * @param $product_id
     * @param $user_id
     * @return mixed
     */
    public static function get_watchdog($product_id, $user_id)
    {
        return ORM::factory("watchdog")->where("product_id", "=", $product_id)->where("shopper_id", "=", $user_id)->find();
    }

    /**
     * Získá všechny psy k uživateli
     * @param $user_id
     * @return mixed
     */
    public static function get_watchdogs($user_id)
    {
        $return = array();
        $dogs = ORM::factory("watchdog")->where("shopper_id", "=", $user_id)->order_by("edited")->find_all();

        $i = 0;
        foreach ($dogs as $dog)
        {
            $return[$i] = $dog->as_array();
            $return[$i]["product"] = Service_Product::get_product_detail_by_id($dog->product->id);
            $return[$i]["product"]["nazev_seo"] = $dog->product->route->nazev_seo;
            $i++;
        }

        return $return;
    }

    /**
     * Získá všechny psy k produktu
     * @param $product_id
     * @return mixed
     */
    public static function get_watchdogs_by_product_id($product_id)
    {
        $return = array();
        $dogs = ORM::factory("watchdog")->where("product_id", "=", $product_id)->order_by("edited")->find_all();

        $i = 0;
        foreach ($dogs as $dog)
        {
            $return[$i] = $dog->as_array();
            $return[$i]["product"] = Service_Product::get_product_detail_by_id($dog->product->id);
            $return[$i]["product"]["nazev_seo"] = $dog->product->route->nazev_seo;
            $i++;
        }

        return $return;
    }

    /**
     * Smaže psa podle product a user id
     * @param $product_id
     * @param $user_id
     */
    public static function delete_watchdog_simple($product_id, $user_id)
    {
        if (self::check($product_id, $user_id))
        {
            $watchdog = self::get_watchdog($product_id, $user_id);
            $watchdog->delete();
        }
    }

    /**
     * Smaže psa
     * @param $watchdog_id
     */
    public static function delete_watchdog($watchdog_id)
    {
        ORM::factory("watchdog", $watchdog_id)->delete();
    }

    /**
     * Zkontroluje psy jestli odpovídají změně a vrátí je
     * @param $product_id
     * @param $price
     * @return ORM array
     */
    public static function check_watchdogs_by_price($product_id, $price)
    {
        return ORM::factory("watchdog")->where("product_id", "=", $product_id)->where("price", ">=", $price)->find_all();
    }

    /**
     * Zkontroluje psy jestli odpovídají změně a vrátí je
     * @param $product_id
     * @param $in_stock
     * @return ORM array
     */
    public static function check_watchdogs_by_stock($product_id)
    {
        return ORM::factory("watchdog")->where("product_id", "=", $product_id)->where("in_stock", "=", true)->find_all();
    }

    /**
     * Zkontroluje psy a následně na to zareaguje.
     * Metoda je spuštěná při změně produktu
     * @param $product
     */
    public static function check_watchdogs($product)
    {
        $product_dph_price = $product->prices()->get_total_price_with_tax();
        $in_stock = ($product->pocet_na_sklade > 0) ? true : false;

        $watchdogs_price = self::check_watchdogs_by_price($product->id, $product_dph_price);
        $watchdogs_stock = self::check_watchdogs_by_stock($product->id);
        if (count($watchdogs_price) > 0)
            Service_Watchdog_Email::send_price($watchdogs_price);

        if ($in_stock && count($watchdogs_stock) > 0)
            Service_Watchdog_Email::send_stock($watchdogs_stock);
    }

}

?>