<?php defined('SYSPATH') or die('No direct script access.');


class Service_Revir extends Service_Page
{
	public static function get_revirs($language_id,$parent_id = 0) {
		$result_data = array();
		$reviry_orm = orm::factory("revir")
					->language($language_id)
					->where("zobrazit","=",1)
					->where("parent_id","=",$parent_id)
					->order_by("poradi","ASC")
					->find_all();
		foreach ($reviry_orm as $revir_orm) {
			$result_data[$revir_orm->id] = $revir_orm->as_array();
			$result_data[$revir_orm->id]["nazev_seo"] = $revir_orm->nav_id;
			$result_data[$revir_orm->id]["photo"] = self::_photo_way_generator($revir_orm->photo_src,"media/photos/revir/item/images-".$revir_orm->id."/",array("ad"=>"jpg"));
			$result_data[$revir_orm->id]["childs"] = self::get_revirs($language_id,$revir_orm->id);
		}
		return $result_data;

	}

}