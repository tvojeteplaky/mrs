<?php defined('SYSPATH') or die('No direct script access.');

class Service_Watchdog_Email {

    /**
     * Odešle email o snížení ceny
     * @param $watchdogs
     */
    public static function send_price($watchdogs)
    {
        $template = new View("emails/watchdog/price");
        foreach ($watchdogs as $watch)
        {
            $user = $watch->shopper;
            $template->user = $user;
            $template->product = Service_Product::get_product_detail_by_id($watch->product->id);
            Service_Email::process_email($template->render(), "watchdog_price", $user->email, $user->nazev);
        }
    }

    /**
     * Odešle email o dostupnosti
     * @param $watchdogs
     */
    public static function send_stock($watchdogs)
    {
        $template = new View("emails/watchdog/stock");
        foreach ($watchdogs as $watch)
        {
            $user = $watch->shopper;
            $template->user = $user;
            $template->product = Service_Product::get_product_detail_by_id($watch->product->id);
            Service_Email::process_email($template->render(), "watchdog_stock", $user->email, $user->nazev);
        }
    }

}

?>