<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 24.9.2014
 * Time: 17:02
 */

require_once dirname(__FILE__) . '/../../../../modules/gpwebpay/classes/GpwebpayAutoload.php';

class Service_Gpwebpay_Impl {

    /**
     * @param Model_Order $order
     */
    public static function redirectToPaymentGate(Model_Order $order) {
        $gpOrder = new Gpwebpay_Create_Order($order->order_code, $order->order_total_CZK, null, 203, 1);
        $url = Service_Gpwebpay::getUrlForNewPayment($gpOrder);
        Request::instance()->redirect($url);
    }

    /**
     * For decoding prCode, srCode you can user Gpwebpay_Primary_Return_Code, Gpwebpay_Secondary_Return_Code
     * @param Gpwebpay_Create_Order $createOrder empty create order, it will be filled
     * @return bool if response it's ok
     */
    public static function checkCreateOrderResponse($createOrder) {
        $createOrder->setOrderNumber(isset($_GET['ORDERNUMBER']) ? (int)$_GET['ORDERNUMBER'] : null);
        $createOrder->setOperation(isset($_GET['OPERATION']) ? $_GET['OPERATION'] : null);
        $createOrder->setMerOrderNum(isset($_GET['MERORDERNUM']) ? (int)$_GET['MERORDERNUM'] : null);
        $createOrder->setMd(isset($_GET['MD']) ? $_GET['MD'] : null);  // podle googlu odstraneno (pry na radu primo GP)???
        $createOrder->setPrCode(isset($_GET['PRCODE']) ? $_GET['PRCODE'] : null);
        $createOrder->setSrCode(isset($_GET['SRCODE']) ? $_GET['SRCODE'] : null);
        $createOrder->setResultText(isset($_GET['RESULTTEXT']) ? $_GET['RESULTTEXT'] : null);

        $digest = isset($_GET['DIGEST']) ? $_GET['DIGEST'] : '';

        return Service_Gpwebpay::verifyCreateOrderResponse($createOrder, $digest);
    }

    /**
     * You can use Gpwebpay_Order_State to get message for order state code.
     * @param $order
     * @return GpwebpayStructOrderStateResponse|null
     */
    public static function getOrderStatus($order) {
        return Service_Gpwebpay::getQueryStatus($order->order_code);
    }
} 