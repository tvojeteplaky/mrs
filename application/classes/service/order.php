<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 *
 *
 * @package    Hana
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */
class Service_Order extends Service_Hana_Order {
    
//    protected static function on_change_order_state($order)
//    {
//        
//    }
    
    protected static function calculate_order_discount($prices, Model_Cart $shopping_cart, $raw_discount=0)
    {
        // sleva pro prihlaseneho uzivatele - prvni nakup
        $shopper = Shauth::instance()->get_user();
        
        if($shopper && $shopper->action_first_purchase==1)
        {
            $owner_data=orm::factory("product_setting",1);
            $prices["order_discount"]=$owner_data->first_purchase_discount;
        }
        else
        {
            $prices["order_discount"]=$raw_discount;
        }
        
        
        return $prices;
    }

    /**
     * Add columns to database for gp_webpay info
     * @param $order
     * @param $gpwebpay_order
     */
    public static function update_order_for_gpwebpay(Gpwebpay_Create_Order $gpwebpay_order, Model_Order $order)
    {
        $order->gp_webpay_prcode = $gpwebpay_order->getPrCode();
        $order->gp_webpay_srcode = $gpwebpay_order->getSrCode();
        $order->gp_webpay_status = Service_Gpwebpay_Impl::getOrderStatus($order)->getState();
        $order->update_gp_text_status();
        $order->save();
    }

    /**
     * Update gp webpay payment information
     * @param $order_id
     */
    public static function update_gpwebpay_status($order_id)
    {
        $order = ORM::factory("order", $order_id);
        $order->gp_webpay_status = Service_Gpwebpay_Impl::getOrderStatus($order)->getState();
        $order->update_gp_text_status();
        $order->save();
    }
}