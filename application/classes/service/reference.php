<?php defined('SYSPATH') or die('No direct script access.');


class Service_Reference
{

    /**
     * Returns categories with theirs references
     * @param int $language_id
     * @return array
     */
    public static function get_categories($language_id = 0)
    {
        $return = array();
        $categories = ORM::factory('reference_category')
            ->join("routes")->on("routes.id", "=", "reference_category_data.route_id")
            ->language($language_id)
            ->where('routes.zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();

        $i = 0;
        foreach ($categories as $category) {
            $return[$i] = $category->as_array();
            $return[$i]["route"] = $category->route->as_array();
            $return[$i]["nazev_seo"] = $category->route->nazev_seo;
        //  $return[$i]['references'] = self::get_references($category);
            $i++;
        }

        return $return;
    }

    /**
     * Returns references
     * @param Model_Reference_Category $category
     * @param int $language_id
     * @return array
     */
    public static function get_references(Model_Reference_Category $category = null, $language_id = 0)
    {
        $return = array();
        if (!$category) {
            $references = ORM::factory('reference')
                ->join('routes')->on('reference_data.route_id', '=', 'routes.id')
                ->language($language_id)
                ->where('routes.zobrazit', '=', 1)
                ->where('homepage', '=', 1)
                ->order_by('poradi')
                ->find_all();
        } else {
            $references = $category
                ->references
                ->join('routes')->on('reference_data.route_id', '=', 'routes.id')
                ->where('language_id', '=', $category->language_id)
                ->where('routes.zobrazit', '=', 1)
                ->order_by('poradi')
                ->find_all();
        }

        $i = 0;
        foreach ($references as $reference) {
            $return[$i] = $reference->as_array();
            $return[$i]["route"] = $reference->route->as_array();
            $i++;
        }

        return $return;
    }

    /**
     * Returns all about the reference
     * @param int $id
     * @param int $language_id
     * @return array
     */
    public static function get_reference($id, $language_id = 0)
    {
        $return = array();
        $reference = ORM::factory('reference')
            ->where('references.id', '=', $id)
            ->language($language_id)
            ->find();

        $return = $reference->as_array();
        $return['nazev_seo'] = $reference->route->nazev_seo;

        $return['gallery'] = array();
        $photos = $reference->reference_photos
            ->where('zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();

        foreach ($photos as $photo)
            $return['gallery'][] = $photo->as_array();

        return $return;
    }

    /**
     * Returns reference category based on route
     * @param Model_Route $route
     * @return Model_Reference_Category
     */
    public static function get_category_by_route(Model_Route $route)
    {
        return ORM::factory("reference_category")
            ->where("route_id", "=", $route->id)
            ->find();
    }

    /**
     * Returns reference based on route
     * @param Model_Route $route
     * @return Model_Reference
     */
    public static function get_reference_by_route(Model_Route $route)
    {
        return ORM::factory("reference")
            ->where("route_id", "=", $route->id)
            ->find();
    }

    /**
     * @param int $language_id
     * @return Model_Reference_Category
     */
    public static function get_first_category($language_id = 0)
    {
        return ORM::factory("reference_category")
            ->join("routes")->on("reference_category_data.route_id", "=", "routes.id")
            ->language($language_id)
            ->where("routes.zobrazit", "=", 1)
            ->order_by("poradi")
            ->find();
    }

    public static function get_reference_page_route($language_id = 1)
    {
        $module = ORM::factory("module")
            ->where("kod", "=", "reference")
            ->find();

        $route = ORM::factory("route")
            ->where("language_id", "=", $language_id)
            ->where("module_id", "=", $module->id)
            ->where("module_action", "=", "index")
            ->find();

        return array(
            $route->nazev_seo => Service_Page::get_page_by_route_id($route->id),
        );
    }

    /**
     * Returns breadcrumbs to reference or its category based on route in string form
     * @param $nazev_seo
     * @return array
     */
    public static function get_navigation_breadcrumbs($nazev_seo)
    {
        $return = array();
        $route = ORM::factory("route")
            ->where("nazev_seo", "=", $nazev_seo)
            ->find();

        $reference = self::get_reference_by_route($route);
        $category = NULL;
        if ($reference->loaded()) {
            $return = array_merge($return, array($reference->route->nazev_seo => $reference->as_array()));
            $return[$reference->route->nazev_seo]["nazev_seo"] = $reference->route->nazev_seo;
            $category = $reference->reference_category->where("language_id", "=", $route->language_id);
        } else {
            $category = self::get_category_by_route($route);
        }
        $return = array_merge($return, array($category->route->nazev_seo => $category->as_array()));
        $return[$category->route->nazev_seo]["nazev_seo"] = $category->route->nazev_seo;

        $return = array_merge(self::get_reference_page_route($route->language_id), $return);

        return $return;
    }
}