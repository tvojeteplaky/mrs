<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Slider extends ORM
{

    // Validation rules
    protected $_rules = array(
        'nazev' => array(
            'not_empty' => NULL,
        )
    );

    protected $_has_many = array(
      'slides' => array()
    );

}

?>