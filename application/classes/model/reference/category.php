<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Reference_Category extends ORM_Language
{

    protected $_join_on_routes = true;

    protected $_has_many = array(
        'references' => array()
    );
}