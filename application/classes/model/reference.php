<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Reference extends ORM_Language {

    protected $_join_on_routes = true;

    protected $_has_many = array(
        'reference_photos' => array()
    );

    protected $_belongs_to = array(
        'reference_category' => array()
    );

    // Validation rules
    protected $_rules = array(
        'nazev' => array(
            'not_empty'  => NULL,
        ),
    );

}