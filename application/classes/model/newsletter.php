<?php defined('SYSPATH') or die('No direct script access.');

class Model_Newsletter extends ORM_Language
{
    protected $_belongs_to = array(
        'article' => array(),
        'newsletter_type' => array()
    );
}