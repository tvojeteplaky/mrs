<?php defined('SYSPATH') or die('No direct script access.');

class Model_Watchdog extends ORM {

    protected $_belongs_to = array(
        'shopper' => array(),
        'product' => array(),
    );

    protected $_rules = array(
        'price' => array(
            'not_empty' => NULL
        ),
    );

}

?>