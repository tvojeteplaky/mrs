<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Page extends ORM_Language {

    protected $_join_on_routes = true;

    protected $_has_many = array(
        'page_photos' => array(),
		'page_files' => array(),
        'product_action' => array()
    );

    protected $_belongs_to = array(
        'product_category' => array()
    );

    // Validation rules
    protected $_rules = array(
        'nazev' => array(
            'not_empty'  => NULL,
        ),
    );

    public function get_relatives_many() {
        return $this->_has_many;
    }

    public function get_relatives_belongs_to() {
        return $this->_belongs_to;
    }

}
?>
