<?php defined('SYSPATH') or die('No direct script access.');

class Model_Gallery extends ORM_Language
{
    protected $_join_on_routes = true;

    protected $_has_many = array(
        'gallery_photos' => array(),
        );

}