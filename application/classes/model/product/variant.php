<?php defined('SYSPATH') or die('No direct script access.');

class Model_Product_Variant extends ORM_Language {
	protected $_join_on_routes = false;

	protected $_product_priceholder=null;
	 /**
     * Vrati objekt s vypoctenymi cenami.
     * @param type $price_category cenova kategorie - defaultne 0 = vezme se od prihlaseneho uzivatele/defaultni
     * @return Model_Product_Priceholder 
     */
    public function prices($price_category=false, $voucher=false)
    {
        if(!$price_category)
        {
            $user=Service_User::instance()->get_user();
            if($user && $user->price_category_id)
            {
                $price_category=$user->price_category->kod;
            }
        }
        
        if(empty($this->_product_priceholder[$price_category])) 
        {
            $this->_product_priceholder[$price_category]=new Model_Product_Priceholder($this, $price_category, $voucher);
        }
        
        return isset($this->_product_priceholder[$price_category])?$this->_product_priceholder[$price_category]:"";
    }

    protected $_belongs_to = array(
        'product' => array(),
    );


    protected $_has_many = array(
    	'price_categories' => array('through' => 'price_categories_products'),
    );

    protected $_callbacks = array(
            'code' => array('code_available')
    );

}