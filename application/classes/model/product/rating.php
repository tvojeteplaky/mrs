<?php defined('SYSPATH') or die('No direct script access.');

class Model_Product_Rating extends ORM {

    protected $_has_one = array(
        'products' => array()
    );

    protected $_rules = array(
        'name' => array(
            'not_empty' => NULL
        ),
    );

}