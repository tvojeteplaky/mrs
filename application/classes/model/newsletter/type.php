<?php defined('SYSPATH') or die('No direct script access.');


class Model_Newsletter_Type extends ORM_Language
{

    protected $_has_many = array(
        'newsletter_recipients' => array(),
        'newsletters' => array()
    );
}
