<?php defined('SYSPATH') or die('No direct script access.');

class Model_Newsletter_Recipient extends ORM
{

    // Validation rules
    protected $_rules = array(
        'email' => array(
            'not_empty' => NULL,
            'email' => array(),
        ),
        'newsletter_type_id' => array(
            'not_empty' => NULL
        )
    );

    protected $_belongs_to = array(
        'newsletter_type' => array()
    );
}

?>
