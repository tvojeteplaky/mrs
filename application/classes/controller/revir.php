<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Revir extends Controller
{


	public function action_index()
	{
		$template = new View("revir/list");
		$template->reviry = Service_Revir::get_revirs($this->application_context->get_actual_language_id());
		$this->request->response = $template->render();
	}

	public function action_subnav() {
		$subnav =new View("page/subnav");
		$subnav->items =  Service_Revir::get_revirs($this->application_context->get_actual_language_id());
		$this->request->response = $subnav->render();
	}
}