<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani stranek s vypisem a detailem produktu + stranky s nakupnim kosikem.
 */
class Controller_User extends Controller
{
    /**
     *
     * @var Service_User
     */
    private $user_service;

    public function before()
    {
        parent::before();
        $this->user_service = Service_User::instance();

    }

    /**
     * Akce pro obsluhu uzivatelskeho boxiku (widgetu) vlozeneho do stranek.
     */
    public function action_widget()
    {
        $user_widget = new View("user/user_widget");

//        // registrace do newsletteru - rychla
//        if(isset($_POST["newsletter_text"]) && $_POST["newsletter_text"]){
//            $result=$this->user_service->signup_newsletter($_POST["newsletter_text"]);
//            Request::instance()->redirect(url::base()."?newsletterreg=".$result);
//        }
//        
//        if(isset($_GET["newsletterreg"]) && $_GET["newsletterreg"]){
//            $rt=Service_Route::instance();
//            if($_GET["newsletterreg"]=="ok")
//            {
//                $rt->set_message(__("Registrace do newsletteru proběhla úspěšně."));
//                $rt->set_message_type("flash_ok");
//            }
//            else
//            {
//                $rt->set_message(__("Registrace do newsletteru byla neúspěšná. <br /> Neplatný, nebo již existující email."));
//                $rt->set_message_type("flash_error");
//            }
//   
//        }


        if ($this->user_service->logged_in()) {
            $user_widget->logged_in = true;
            $user = $this->user_service->get_user();
            $price_category_id = (int)$user->price_category_id;
            //$user_widget->price_category=@DB::select("popis")->from("price_categories")->where("price_categories.id","=",$price_category_id)->as_object()->execute()->current()->popis;

            $user_widget->user = $user;
        } else {
            $user_widget->logged_in = false;
        }
        $this->request->response = $user_widget->render();
    }

    public function action_show_login_form($on_success_redirect_to = false, $included = false)
    {
        $user_login_template = new View("user/user_login");
        $user_login_template->redirect = $on_success_redirect_to;
        $user_login_template->included = $included;
        $this->request->response = $user_login_template->render();
    }

    public function action_show_login_form_popup($on_success_redirect_to = false, $included = false)
    {
        $user_login_template = new View("user/user_login_popup");
        $user_login_template->show_popup = !empty($_POST["show_login_popup"]) ? true : false;
        $user_login_template->redirect = $on_success_redirect_to;
        $user_login_template->included = $included;
        $this->request->response = $user_login_template->render();
    }


    /**
     * Akce pro obsluhu prihlaseni uzivatele.
     * @param string $on_success_redirect_to volitelne url na kterou se ma beh programu presmerovat po uspesnem prihlaseni
     * @param boolean $included priznak zda jde o samostatny formular, nebo je vlozeny do sirsi struktury formularu
     */
    public function action_login($on_success_redirect_to = false)
    {
        $result = $this->user_service->login(isset($_POST["username"]) ? $_POST["username"] : null, isset($_POST["password"]) ? $_POST["password"] : null);
        if ($result) {
            // pokud byl ulozen docasny uzivatel, smazu ho
            $this->user_service->delete_temporarily_stored_user();

            $redirect = true;
            if ($on_success_redirect_to) $redirect = (url::base() . $on_success_redirect_to);
            $this->response_object->set_message(__(I18n::get("Uživatel byl přihlášen")));
            $this->response_object->set_redirect($redirect);
        } else {
            $this->response_object->set_redirect($result);
            //$this->response_object->set_data($_POST["user-registration"]);
            $this->response_object->set_message(__(I18n::get("Přihlášení uživatele se nezdařilo")));
        }

    }

    public function action_logout()
    {
        // obecna sablona textove stranky
        $this->user_service->logout();
        $this->response_object->set_message(__(I18n::get("Uživatel byl odhlášen")));
        $this->response_object->set_status(true);
        Request::instance()->redirect(url::base());

    }

    /**
     * Akce pro zobrazeni prihlasovaciho formulare.
     * @param string $on_success_redirect_to volitelne url na kterou se ma beh programu presmerovat po uspesnem prihlaseni
     * @param boolean $included priznak zda jde o samostatny formular, nebo je vlozeny do sirsi struktury formularu
     */
    public function action_show_registration_form($on_success_redirect_to = false, $included = false)
    {
        // pripadne nacteni dat z ARESu
        if (isset($_GET["ic"])) {
            try {
                $ares = Service_Ares::getAresBasicInformation(trim($_GET["ic"]));
                $a_arr = $ares->as_array();
                foreach ($a_arr as $a_key => $a_value) {
                    if ($a_value === null) {
                        $ares_array[$a_key] = "";
                    } else {
                        $ares_array[$a_key] = $a_value;
                    }
                }
                exit(json_encode($ares_array));
            } catch (Exception $e) {
                throw($e);
                exit(json_encode(array("ares_error" => "Chyba, data z ARESu nemohla být načtena, zkontrolute prosím IČ.")));
            }
        }

        $user_registration_template = new View("user/user_registration");
        $user_registration_template->data = $this->response_object->get_data();
        $user_registration_template->errors = $this->response_object->get_errors();
        $user_registration_template->redirect = $on_success_redirect_to;
        $user_registration_template->included = $included;

        if ($this->user_service->logged_in()) {
            $shopper = $this->user_service->get_user();
            // pokud zobrazujeme registracni formuler prihlasenemu uzivateli jde o zmenu udaju 
            $user_registration_template->editmode = true;
            $branch_list_template = new View("user/user_account_branch_list");
            $branch_list_template->branches = $this->user_service->get_all_user_branches($shopper);
            $user_registration_template->branch_list = $branch_list_template;
            $shopper = $this->user_service->get_user();

            if (!$this->response_object->get_data()) $user_registration_template->data = $shopper;
        }
        $this->request->response = $user_registration_template->render();
    }

    public function action_show_registration_form_popup($on_success_redirect_to = false, $included = false)
    {
        // pripadne nacteni dat z ARESu
        if (isset($_GET["ic"])) {
            try {
                $ares = Service_Ares::getAresBasicInformation(trim($_GET["ic"]));
                exit(json_encode($ares->as_array()));
            } catch (Exception $e) {
                exit(json_encode(array("ares_error" => "Chyba, data z ARESu nemohla být načtena, zkontrolute prosím IČ.")));
            }
        }

        $user_registration_template = new View("user/user_registration_popup");
        $user_registration_template->data = $this->response_object->get_data();
        $user_registration_template->errors = $this->response_object->get_errors();
        $user_registration_template->show_popup = !empty($_POST["show_registration_popup"]) ? true : false;


        // AKCE- REGISTRACE: predvyplneni formulare z popupu akcni registrace na uvodce
        $registration_action_email = Input::post("registration-action-email");
        if (!empty($registration_action_email)) {
            $validation = new Validate(array("email" => $registration_action_email));
            $validation->rule("email", "email");

            if ($validation->check()) {
                $user_registration_template->show_popup = true;
                $user_registration_template->show_action_popup = false;
                $user_registration_template->data["email"] = $registration_action_email;

                $result = Service_Newsletter::subscribe_to_newsletter($registration_action_email);

                Session::instance()->set("action_first_purchase", 1);
            } else {
                $user_registration_template->show_action_popup = true;
                $user_registration_template->email = $registration_action_email;
                $user_registration_template->errors = true;
            }
        } else {
            // zobrazeni popupu v zavislosti na cookies
            if (!Cookie::get("action_popup_show") && $this->application_context->get_actual_seo() == "index" && $user_registration_template->show_popup == false) {
                $user_registration_template->show_action_popup = true;
            } else {
                $user_registration_template->show_action_popup = false;
            }

            Cookie::set("action_popup_show", 1);
        }

        $user_registration_template->redirect = $on_success_redirect_to;

        $this->request->response = $user_registration_template->render();
    }


    /**
     * Zpracovava registracni formular. Pokud jde o noveho uzivatele, dojde zaroven k prihlaseni.
     * @param type $on_success_redirect_to
     */
    public function action_registration($on_success_redirect_to = false)
    {

        $data = $_POST["user-registration"];
        $data["username"] = $_POST["user-registration"]["email"]; // username - deprecated
        if ($this->user_service->logged_in()) {
            $result = $this->user_service->change_registration_data($data, $this->response_object);
            $newuser = false;
        } else {
            $result = $this->user_service->register_user($_POST["user-registration"], $this->response_object);
            $newuser = true;
        }

        $this->response_object->set_status($result);

        if ($result) {
            if ($newuser) {
                Shauth::instance()->force_login($_POST["user-registration"]["email"]);

                // pokud byl ulozen docasny uzivatel, smazu ho
                $this->user_service->delete_temporarily_stored_user();

                $shopper = $this->user_service->get_user();
                // zaslani mailu s registraci
                if ($shopper->id) {
                    $this->response_object->set_message(__("Registrace byla dokončena."));

                    $shopper_registration_tpl = new View("emails/shopper/new_user");
                    $shopper_registration_tpl->owner_data = orm::factory("product_setting", 1);
                    $message = $shopper_registration_tpl->render();

                    Service_Email::process_email($message, "new_user", $shopper->email, $shopper->nazev);

                }


            } else {
                $this->response_object->set_message(__("Změna údajů registrace proběhla úspěšně."));
            }

            // presmeruju
            if (Shauth::instance()->logged_in()) {

                $redirect = true;
                if ($on_success_redirect_to) $redirect = (url::base() . $on_success_redirect_to);
                $this->response_object->set_redirect($redirect);
            } else {
                // TODO chyba zalogovat! - registrace probehla uspesne ale uzivatele nebylo mozne prihlasit
            }
        } else {
            // registrace se nezdarila
            $this->response_object->set_redirect($result);
            $this->response_object->set_data($_POST["user-registration"]);

            if ($newuser) {
                $this->response_object->set_message(__("Registrace se nezdařila, zkontrolujte prosím zadané údaje"));
            } else {
                $this->response_object->set_message(__("Změna údajů se nezdařila, zkontrolujte prosím zadané údaje"));
            }

        }

    }

    /**
     * Akce pro obsluhu hlavni stranky (rozcestniku), ktera se zobrazi po prihlaseni.
     */
    public function action_show_main_page()
    {
        $shopper = $this->user_service->get_user();
        if (!$shopper) Request::instance()->redirect(url::base());

        $user_account_template = new View("user/user_account_index");
        $user_account_template->shopper = $shopper;
        $user_account_template->item = Service_Page::get_page_by_route_id($this->application_context->get_route_id());

        $branch_list_template = new View("user/user_account_branch_list");
        $branch_list_template->branches = $this->user_service->get_all_user_branches($shopper);
        $user_account_template->branch_list = $branch_list_template;

        $this->request->response = $user_account_template->render();
    }

    /**
     * Zobrazení formuláře pro změnu nastavení účtu uživatele.
     * @param type $nazev_seo
     */
    public function action_show_account_setup()
    {
        $shopper = $this->user_service->get_user();
        if (!$shopper) Request::instance()->redirect(url::base());

        $user_account_setup_template = new View("user/user_account_setup");
        $user_account_setup_template->user_registration = Request::factory("internal/user/show_registration_form/ucet-uzivatele/true")->execute()->response;

        $user_account_setup_template->item = Service_Page::get_page_by_route_id($this->application_context->get_route_id());

        $this->request->response = $user_account_setup_template->render();
    }

    /**
     * Zobrazeni formulare pro editaci dorucovaci adresy (pobocky uzivatele)
     * @param string $on_success_redirect_to
     */
    public function action_show_branch_edit_form($on_success_redirect_to = false)
    {
        if (!$this->user_service->logged_in()) Request::instance()->redirect(url::base());
        $user = $this->user_service->get_user();

        $user_branchedit_template = new View("user/user_account_branch_edit");
        $user_branchedit_form_template = new View("user/user_account_branch_edit_form");
        $user_branchedit_form_template->redirect = $on_success_redirect_to;

        $editmode = (isset($_GET["branch_id"]) && $_GET["branch_id"]) ? true : ((isset($_POST["branchedit"]["branch_id"]) && $_POST["branchedit"]["branch_id"]) ? true : false);

        $user_branchedit_form_template->editmode = $editmode;

        if ($this->response_object->get_status() === false) {
            $data = $this->response_object->get_data();
        } elseif ($editmode) {
            $data = $this->user_service->get_shopper_branch($_GET["branch_id"], $user);
        } else {
            $data = array();
        }

        $user_branchedit_form_template->data = $data;
        $user_branchedit_form_template->errors = $this->response_object->get_errors();
        $user_branchedit_form_template->redirect = $on_success_redirect_to;
        $user_branchedit_template->user_branch_form = $user_branchedit_form_template;

        $this->request->response = $user_branchedit_template->render();
    }

    /**
     * Zpracovani a ulozeni datdorucovaci adresy (pobocky uzivatele).
     * @param type $on_success_redirect_to
     */
    public function action_branch_edit($on_success_redirect_to = false)
    {
        if (!$this->user_service->logged_in()) Request::instance()->redirect(url::base());
        $user = $this->user_service->get_user();
        $editmode = ((isset($_POST["branchedit"]["id"]) && $_POST["branchedit"]["id"]) ? true : false);

        $result = $this->user_service->save_branch_data($_POST["branchedit"], $user->id);

        $this->response_object->set_status($result);

        if ($result) {
            $redirect = true;
            if ($on_success_redirect_to) $redirect = (url::base() . $on_success_redirect_to);
            $this->response_object->set_redirect($redirect);

            if ($editmode) {
                $this->response_object->set_message(__("Dodací adresa byla změněna."));
            } else {
                $this->response_object->set_message(__("Dodací adresa byla přidána."));
            }
        } else {
            if ($editmode) {
                $this->response_object->set_message(__("Editace dodací adresy se nezdařila, zkontrolujte prosím zadané údaje."));
            } else {
                $this->response_object->set_message(__("Vložení dodací adresy se nezdařilo, zkontrolujte prosím zadané údaje."));
            }

            $this->response_object->set_errors($this->user_service->errors);
            $this->response_object->set_data($_POST["branchedit"]);
        }
    }

    /**
     * Smazani dorucovaci adresy (pobocky uzivatele).
     * @param type $on_success_redirect_to
     */
    public function action_branch_delete($on_success_redirect_to = false)
    {
        if (!$this->user_service->logged_in()) Request::instance()->redirect(url::base());
        $user = $this->user_service->get_user();

        $result = $this->user_service->delete_user_branch($_GET["branch_id"], $user->id);

        if ($result) {
            $redirect = true;
            if ($on_success_redirect_to) $redirect = (url::base() . $on_success_redirect_to);
            $this->response_object->set_redirect($redirect);
            $this->response_object->set_message(__("Dodací adresa byla smazána."));
        } else {
            $this->response_object->set_message(__("Nepodařilo se smazat dodací adresu."));
            $this->response_object->set_errors($this->user_service->errors);
            $this->response_object->set_data($_POST["branchedit"]);
        }


    }


    public function action_show_orders_list()
    {

        $shopper = $this->user_service->get_user();
        if (!$shopper) Request::instance()->redirect(url::base());

        $user_account_orders_list_template = new View("user/user_account_orders_list");

        $user_account_orders_list_template->unfinished_orders = Service_Order::get_orders_list($shopper->id, "nothing", "<>");
        $user_account_orders_list_template->finished_orders = Service_Order::get_orders_list($shopper->id, "done", "=");

        //die(print_r(Service_Order::get_orders_list($shopper->id,"completed","<>")));
        //$user_account_orders_list_template->shopper=$shopper;

        $this->request->response = $user_account_orders_list_template->render();
    }

    public function action_order_detail($order_id)
    {

        $shopper = $this->user_service->get_user();
        if (!$shopper) Request::instance()->redirect(url::base());

        $user_account_order_detail = new View("user/user_account_order_detail");


        $order = orm::factory("order")->where("id", "=", $order_id)->where("order_shopper_id", "=", $shopper->id)->find();

        $user_account_order_detail->owner_data = $this->application_context->get_web_owner_data();

        $user_account_order_detail->order_code = $order->order_code;

        $customer["nazev"] = $order->order_shopper_name;
        $customer["ulice"] = $order->order_shopper_street;
        $customer["mesto"] = $order->order_shopper_city;
        $customer["psc"] = $order->order_shopper_zip;
        $customer["email"] = $order->order_shopper_email;
        $customer["telefon"] = $order->order_shopper_phone;
        $customer["poznamka"] = $order->order_shopper_note;

        $user_account_order_detail->billing_data = $customer;

        $customer_branch["nazev"] = $order->order_branch_name;
        $customer_branch["ulice"] = $order->order_branch_street;
        $customer_branch["mesto"] = $order->order_branch_city;
        $customer_branch["psc"] = $order->order_branch_zip;
        $customer_branch["email"] = $order->order_branch_email;
        $customer_branch["telefon"] = $order->order_branch_phone;

        $user_account_order_detail->branch_data = $customer_branch;

        $user_account_order_detail->doprava = $order->shipping->nazev;
        $user_account_order_detail->platba = $order->payment->nazev;
        $user_account_order_detail->cart_products = orm::factory("order_item")->where("order_id", "=", $order->id)->find_all();

        //$user_account_order_detail->shopping_cart=Request::factory("internal/cart/detail_table/read_only/".$order->id."/true")->execute()->response;
        //$order_price_summary_table->summary_total=true;

        if ($order->order_voucher_discount) ;
        {
            // sleva na kupon
            $price_values["voucher_discount"] = $order->order_voucher_discount;
        }

        // prirazeni ceny kosiku
        $price_values["payment_price"] = $order->order_shipping_price;
        $price_values["shipping_price"] = $order->order_payment_price;

        $price_values["total"] = $order->order_total_CZK;
        $price_values["total_wo_dph"] = $order->order_total_without_vat;


        $user_account_order_detail->price_values = $price_values;


        //$user_account_order_detail->order_price_summary_table=Request::factory("internal/order/order_price_summary_table/true")->execute()->response;


        //$user_account_orders_list_template->shopper=$shopper;

        $this->request->response = $user_account_order_detail->render();
    }


    /**
     * Widget uzivatelske subnavigace
     */
    public function action_subnav()
    {
        $subnav = new View("user/subnav");
        $parent_navigation_item_id = 12; // vygenerovat vetev pouze pro polozku daneho id: ucet-uzivatele (viz admin "systemove stranky")
        $links = $temp_links = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), 1, $parent_navigation_item_id, true, false);
        $sel_links = $sel_links_temp = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $subnav->recomended = Service_Product::get_favourite(3);;
        // nadrazena polozka "ucet uzivatele" bude soucasti submenu, ale aktivni bude pouze pokud bude jedinna zvolena 

        // 
        if (count($sel_links) > 1) {
            array_pop($sel_links);
        }


//        $sel_links=array_shift($sel_links_temp);
//        $sel_links=array($sel_link["nazev_seo"]=>$sel_link);

        //$subnav->title=$sel_link["nazev"];

        //die(print_r($links));
        //$links = !empty($temp_links[$sel_link["nazev_seo"]]["children"][$sel_link2["nazev_seo"]]["children"])?$temp_links[$sel_link["nazev_seo"]]["children"][$sel_link2["nazev_seo"]]["children"]:array();
        foreach ($sel_links as $key => $sel_link) {

            if (empty($links)) $links = !empty($temp_links[$sel_link["nazev_seo"]]["children"]) ? $temp_links[$sel_link["nazev_seo"]]["children"] : array();
        }

        //$subnav->links=$links;

        $links = array_shift($links);
        $subnav->links = $links["children"];
        $subnav->title = I18n::get('Můj účet');

        $subnav->sel_links = $sel_links;
        $this->request->response = $subnav->render();
    }

    public function action_show_send_new_password_form()
    {
        $new_password_template = new View("user/user_send_new_password_form");

        if (!empty($_GET["newpass_code"]) && strpos($_GET["newpass_code"], "-")) {
            $new_pass_arr = explode("-", $_GET["newpass_code"]);
            $user_id = $new_pass_arr[0];
            $hash = $new_pass_arr[1];

            $shopper = orm::factory("shopper")->where("id", "=", $user_id)->where("password", "=", $hash)->find();

            if ($shopper !== null && $shopper->id) {
                $new_passsword = substr(md5(rand()), 0, 7);
                $password = array("password" => $new_passsword, "password_confirm" => $new_passsword);
                $response = $shopper->change_password($password, false);
                //die(var_dump($response));
                $new_password_email = new View("emails/shopper/new_password");
                $new_password_email->new_password = $new_passsword;
                $new_password_email->owner_data = orm::factory("product_setting", 1);
                Service_Email::process_email($new_password_email->render(), "new_password", $shopper->email, $shopper->nazev);
                Request::instance()->redirect(url::base() . "zapomenute-heslo?newpass_code=changed");
            } else {
                $new_password_template->status_message = "Změna hesla nemohla být provedena. Pokuste se ho prosím vygenerovat znovu a v případě neúspěchu kontaktujte správce e-shopu.";
                $new_password_template->show_form = true;
            }
        } elseif (!empty($_GET["newpass_code"]) && $_GET["newpass_code"] == "changed") {
            $new_password_template->status_message = "Bylo vám zasláno nové heslo na zadaný e-mail.";
            $new_password_template->show_form = false;
        } elseif (Service_User::instance()->logged_in()) {
            $new_password_template->status_message = "Jste přihlášen(a). Pro změnu hesla prosím použijte nastavení uživatelského účtu.";
            $new_password_template->show_form = false;
        } else {
            $new_password_template->status_message = "";
            $new_password_template->show_form = true;
        }

        $this->request->response = $new_password_template->render();
    }

    public function action_send_new_password_link()
    {
        $email = $_POST["email"];
        $fake_string = $_POST["kontrolni_retezec"];

        if (!empty($fake_string)) Request::instance()->redirect(url::base());

        $shopper = orm::factory("shopper")->where("email", "=", $email)->where("smazano", "=", 0)->find();

        if ($shopper !== null && $shopper->id) {
            $new_password_question_email = new View("emails/shopper/new_password_question");
            $new_password_question_email->owner_data = orm::factory("product_setting", 1);
            $new_password_question_email->new_pass_link = url::base() . "zapomenute-heslo?newpass_code=" . $shopper->id . "-" . $shopper->password;
            Service_Email::process_email($new_password_question_email->render(), "new_password_question", $shopper->email, $shopper->nazev);

            $this->response_object->set_status(true);
            $this->response_object->set_message(__("Na Váš e-mail byly zaslány informace pro vygenerování nového hesla."));
        } else {
            if (empty($email)) {
                $this->response_object->set_status(false);
                $this->response_object->set_message(__("Zadejte prosím e-mailovou adresu."));
            } else {
                $this->response_object->set_status(false);
                $this->response_object->set_message(__("Chyba - neexistující uživatel."));
            }

        }

    }

    public function action_show_registration_action_popup()
    {
        Cookie::delete("action_popup_show");
        $this->request->redirect(url::base());
    }

    public function action_watchdogs()
    {
        $shopper = $this->user_service->get_user();
        if (!$shopper) Request::instance()->redirect(url::base());

        $template = new View("user/watchdogs");
        $template->dogs = Service_Watchdog::get_watchdogs($shopper->id);
        $template->shopper = $shopper;
        $this->request->response = $template->render();
    }

}

?>
