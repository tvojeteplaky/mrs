<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Reference extends Controller
{

    public function action_index()
    {
        $category = Service_Reference::get_first_category();
        $this->request->redirect(url::base() . $category->route->nazev_seo);
    }

    public function action_widget()
    {
        $template = new View('reference/widget');


        $template->items = Service_Reference::get_references();
        $this->request->response = $template->render();
    }

    public function action_category()
    {
        $template = new View("reference/category");

        $category = Service_Reference::get_category_by_route($this->application_context->get_actual_route());
        $template->item = $category;
        $template->items = Service_Reference::get_references($category);

        $this->request->response = $template->render();
    }

    public function action_detail()
    {
        $template = new View("reference/detail");

        $reference = Service_Reference::get_reference_by_route($this->application_context->get_actual_route());
        $template->item = $reference;

        $this->request->response = $template->render();
    }

    public function action_subnav($nazev_seo)
    {
        $subnav = new View("navigation/subnav");
        $links = Service_Reference::get_categories();

        $breadcrumbs = Hana_Navigation::instance()->get_navigation_breadcrumbs();

        $subnav->items = $links;
        $subnav->breadcrumbs = $breadcrumbs;
        $subnav->parent = array_pop($breadcrumbs);
        $this->request->response = $subnav->render();
    }

}