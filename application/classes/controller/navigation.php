<?php defined('SYSPATH') or die('No direct script access.');

/**
 * navigation widgets
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Navigation extends Controller
{
    public function action_main()
    {
        //die(print_r(Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id())));
        $nav = new View("navigation/main");
        $links = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id());
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $nav->links = $links;
        $nav->sel_links = $sel_links;
        $this->request->response = $nav->render();
    }

    public function action_category()
    {
        $nav = new View("navigation/category");

        $links = Service_Catalog_Category::get_categories_by_parent_id(0, $this->application_context->get_actual_language_id(), 2, 5);
        $nav->links = $links;
        //$nav->sel_link = array_shift(Hana_Navigation::instance()->get_navigation_breadcrumbs());

        $this->request->response = $nav->render();
    }

    public function action_breadcrumbs()
    {
        // mimo uvodku
        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $breadcrumbs = new View("navigation/breadcrumbs");
        $navigation_items = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $breadcrumbs->items = array_reverse($navigation_items, true);
        $this->request->response = $breadcrumbs->render();
    }

    public function action_side()
    {
        $template = new View('navigation/side');

        $links = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id());
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $template->links = $links;
        $template->sel_links = $sel_links;
        $this->request->response = $template->render();
    }

    public function action_site_index()
    {
        $site_index = new View("navigation/site_index");
        
        $raw_links = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(),3,0,false,false);
        
        $site_index->links= $raw_links;

        $this->request->response = $site_index->render();
    }
}

?>
