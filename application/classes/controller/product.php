<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Sobrazeni vypisu a u produktu
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Product extends Controller
{

    private $items_on_page = 15;

    /**
     * uvodka eshopu - vypsane produkty
     * @param type $page
     */
    public function action_index($page = 1)
    {
        $route_id = $this->application_context->get_route_id();
        $nazev_seo = $this->application_context->get_actual_seo();

        Session::instance()->set("last_product_category", $nazev_seo);

        $categories = new View("product/product_carousel");
        $pc_template_outer = new View("product/product_category");
        $pc_template_inner = new View("product/product_category_items");
        $pc_template_inner->logged_in = Service_User::instance()->logged_in();

        // ziskani vsech parametru z filtru a z razeni
        $parameters = array();

        // upraveno na radiobutton
        if (isset($_GET["sort"])) {
            $sort_array = explode(":", $_GET["sort"]);
            $parameters["order_by"] = $sort_array[0];
            $parameters["order_direction"] = $sort_array[1];
        }

        if (isset($_GET["products_filter_action"]))
            $parameters["products_filter_action"] = $_GET["products_filter_action"];
        if (isset($_GET["price_selected_max"]))
            $parameters["price_selected_max"] = $_GET["price_selected_max"];
        if (isset($_GET["price_selected_min"]))
            $parameters["price_selected_min"] = $_GET["price_selected_min"];

//
        if (isset($_GET["search_string"]) && ($_GET["search_string"]))
            $parameters["search_string"] = $_GET["search_string"];
        if (isset($_POST["search_string"]) && ($_POST["search_string"]))
            $parameters["search_string"] = $_POST["search_string"];

        // skklad akce novinky

        if (isset($_GET["new"]) && !empty($_GET["new"])) {
            //  $parameters["where"][1] ='products.product_action_type=2"';
            $parameters["where"][0][0] = 'new';
            $parameters["where"][0][1] = '=';
            $parameters["where"][0][2] = '1';

            $pc_template_outer->new = $_GET["new"];
        }
        if (isset($_GET["action"]) && !empty($_GET["action"])) {
            //  $parameters["where"][2] ='products.pocet_na_sklade>0';
            $parameters["where"][1][0] = 'action';
            $parameters["where"][1][1] = '=';
            $parameters["where"][1][2] = '1';


            $pc_template_outer->action = $_GET["action"];
        }
        if (isset($_GET["gift"]) && !empty($_GET["gift"])) {
            //  $parameters["where"][2] ='products.pocet_na_sklade>0';
            $parameters["where"][2][0] = 'gift';
            $parameters["where"][2][1] = '=';
            $parameters["where"][2][2] = '1';


            $pc_template_outer->gift = $_GET["gift"];
        }
        if (isset($_GET["sale_off"]) && !empty($_GET["sale_off"])) {
            //  $parameters["where"][2] ='products.pocet_na_sklade>0';
            $parameters["where"][3][0] = 'sale_off';
            $parameters["where"][3][1] = '=';
            $parameters["where"][3][2] = '1';

            $pc_template_outer->sale_off = $_GET["sale_off"];
        }
        if (isset($_GET["stock"]) && !empty($_GET["stock"])) {
            $parameters["where"][4][0] = 'in_stock';
            $parameters["where"][4][1] = '=';
            $parameters["where"][4][2] = '1';
            $pc_template_outer->stock = $_GET["stock"];
        }
        if (isset($_GET["top"]) && !empty($_GET["top"])) {
            //  $parameters["where"][2] ='products.pocet_na_sklade>0';
            $parameters["where"][5][0] = 'top';
            $parameters["where"][5][1] = '=';
            $parameters["where"][5][2] = '1';


            $pc_template_outer->top = $_GET["top"];
        }

        $parameters["where"][6][0] = "smazano";
        $parameters["where"][6][1] = "=";
        $parameters["where"][6][2] = "0";

        // prepocteni cen z price slideru podle toho zda je dana sleva procentem
        /*        $price_category_value=$this->product_service->get_price_category_percentual_value();
        //
                if($price_category_value)
                {
                    if(isset($parameters["price_selected_max"]))$parameters["price_selected_max"]=$parameters["price_selected_max"] * (100/(100-$price_category_value));
                    if(isset($parameters["price_selected_min"]))$parameters["price_selected_min"]=$parameters["price_selected_min"] * (100/(100-$price_category_value));
                }*/
        // sestrojeni pole podminek a parametru pro vybrani orm objektu
        // inicializace strankovani
        $product_show_pages = isset($_GET["products_show_pages"]) ? $_GET["products_show_pages"] : $this->items_on_page; //"all";
        if ($product_show_pages == "all") {
            $product_show_pages = 10000;
            $page = 1;
        }
        if (isset($_GET["gotopage"])) {
            $page = $_GET["gotopage"];
            unset($_GET["gotopage"]);
        }

        $total_items = Service_Product_Category::get_category_total_items(false, $parameters);
        $pc_template_outer->total_items = $total_items;
        $pagination = Pagination::factory(array(
            'current_page' => array('source' => $nazev_seo, 'value' => $page),
            'total_items' => $total_items,
            'items_per_page' => $product_show_pages,
            'view' => 'pagination/basic_hana',
            'auto_hide' => TRUE
        ));
        $pc_template_outer->items_per_page = $pagination->items_per_page;

        $parameters["limit"] = $pagination->items_per_page;
        $parameters["offset"] = $pagination->offset;
        //echo("<br />limit:".$pagination->items_per_page.", offset:".$pagination->offset);
        // vygenerovani vypisu dane kategorie
        //$parameters["search_string"] = $search_string;
        $products = array();
        $category = Service_Product_Category::get_product_category_by_route_id(false, $parameters);
        //$child_products = Service_Product_Category::get_children_category_products($category["category_data"]["id"],$parameters);
        //$products = array_merge($products,$child_products);
        $page_data = Service_Page::get_page_by_route_id($route_id);

        if (isset($category["products"])) {
            $products = array_merge($products, $category["products"]);
        }
        if (!empty($category["category_data"])) {
            $pc_template_outer->category = $category["category_data"];
            $categories->categories = Service_Product::get_favourite_cat(12, $this->application_context->get_actual_language_id(), 0, $category["category_data"]["id"]);
            $pc_template_outer->carousel = $categories;
        } else {
            $pc_template_outer->page = $page_data;
        }
        // vygenerovani vypisu obsazenych produktu
        //if (/* $category["category_data"]["priorita"]>0 && */!Service_Product_Category::is_empty_category($route_id)) { // kategorie bez aplikace filtrovani obsahuje produkty
        // naplneni sablony produky
        if (isset($products)) {
            //var_dump($products);
            $pc_template_inner->products = $products;
        } else {
            // zadne produkty pro dane filtrovani
            $pc_template_outer->empty_category_by_filter = true;
        }

        $pc_template_outer->current_query = htmlentities(url::query());

        // zjisteni hodnot a naplneni filtrovacich a tridicich prvku
        //$pc_filters->manufacturers = Service_Product_Category::get_category_manufacturers($route_id); //($nazev_seo, $search_string, $parameters["products_filter_manufacturers"], $parameters["products_filter_colors"], $parameters["products_filter_sizes"]);//get_category_manufacturers($nazev_seo);
        // rozmezi cen
        $range = Service_Product_Category::get_category_price_ranges(0);
        if ($range["price_min"] > $range["price_max"])
            $range = array("price_min" => 0, "price_max" => 0);
        $pc_template_outer->prices = $range;

        // zachovani kontextu - vsechny zvolene promenne z predchoziho pozadavku musim opet vyplnit
        //// vyhledavani
        if (isset($parameters["search_string"]))
            $pc_template_outer->search_string = $parameters["search_string"];

        //// polozka razeni
        $pc_template_outer->products_order = !empty($parameters["order_by"]) ? $parameters["order_by"] : Service_Product_Category::$default_order_by; //upraveno na radio $_GET["products_order"];
        //// smer razeni
        $pc_template_outer->sort = !empty($parameters["order_direction"]) ? $parameters["order_direction"] : Service_Product_Category::$default_order_direction; //upraveno na radio$_GET["sort"];
        //upraveno na radio$pc_template_outer->current_query_sort=(url::query()?htmlentities(url::query())."&amp;":"?");
        //// pocet na stranku
        if (isset($_GET["products_show_pages"]))
            $pc_template_outer->products_show_pages = $_GET["products_show_pages"];
        else
            $pc_template_outer->products_show_pages = 20;

        //// akce checkbox
        /* if (isset($_GET["products_filter_action"]))
             $pc_filters->products_filter_action = 1;
         else
             $pc_filters->products_filter_action = 0;*/

        //// checkboxy vyrobcu
        /*if (isset($_GET["pfm"]) && $_GET["pfm"]) {
            $manchbids = array_combine($_GET["pfm"], $_GET["pfm"]);
            if (count($manchbids)) {
                $pc_filters->manchbids = $manchbids;
            }
        } else {
            $pc_filters->manchbids = array();
        }*/

        /*>$pc_filters->price_selected_min = $pc_template_outer->price_selected_min = (isset($_GET["price_selected_min"]) ? $_GET["price_selected_min"] : $prices["price_min"]);
        $pc_filters->price_selected_max = $pc_template_outer->price_selected_max = (isset($_GET["price_selected_max"]) ? $_GET["price_selected_max"] : $prices["price_max"]);
        $pc_filters->price_min = $prices["price_min"];
        $pc_filters->price_max = $prices["price_max"];*/
        $pc_template_outer->price_selected_max = (isset($_GET["price_selected_max"]) ? $_GET["price_selected_max"] : $range["price_max"]);
        $pc_template_outer->price_selected_min = (isset($_GET["price_selected_min"]) ? $_GET["price_selected_min"] : $range["price_min"]);

        // zaslani dodatecnych dat a vlozeni produktu do vnejsi sablony
        $pc_template_outer->pagination = $pagination->render();

        //$pc_template_outer->filters = $pc_filters->render();
        $pc_template_outer->current_page = url::base() . $nazev_seo . (($page > 1) ? "/" . $page : "");
        $pc_template_outer->product_category_items = $pc_template_inner;

        // ulozeni aktualniho GETu do sesny
        Session::instance()->set("actual_query", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
        $pc_template_outer->empty_category = false;
        //} else {
        // prazdna kategorie
        //$pc_template_outer->empty_category = true;
        //}


        $this->request->response = $pc_template_outer->render();
        /*$language_id = $this->application_context->get_actual_language_id();
        $category_orm = orm::factory("product_category")
                            ->join("routes")->on("product_category_data.route_id","=","routes.id")
                            ->where("product_categories.parent_id","=",0)
                            ->where("routes.zobrazit","=",1)
                            ->where("routes.deleted", "=", 0)
                            ->order_by("poradi", "ASC")
                            ->find();

            Request::instance()->redirect(url::base() . $category_orm->route->nazev_seo);*/
        /*$redirect_seo = DB::select("nazev_seo")->from("routes")
            ->join("product_category_data")->on("routes.id", "=", "product_category_data.route_id")
            ->join("product_categories")->on("product_category_data.product_category_id", "=", "product_categories.id")
            ->where("routes.language_id", "=", $language_id)
            ->where("product_categories.parent_id", "=", 0)
            ->where("routes.zobrazit", "=", 1)
            ->where("routes.deleted", "=", 0)
            ->order_by("poradi", "ASC")
            ->limit(1)
            ->execute()->get("nazev_seo");*/


        /* $route_id = $this->application_context->get_route_id();
         $nazev_seo = $this->application_context->get_actual_seo();
         //$template=new View("page");
         $page_data = Service_Page::get_page_by_route_id($route_id);

         // inicializace strankovani
         $product_show_pages = $this->items_on_page;


         $total_items = Service_Product_Category::get_category_total_items();

         $pagination = Pagination::factory(array(
             'current_page' => array('source' => $nazev_seo, 'value' => $page),
             'total_items' => $total_items,
             'items_per_page' => $product_show_pages,
             'view' => 'pagination/basic_hana',
             'auto_hide' => TRUE
         ));
         $parameters["limit"] = $pagination->items_per_page;
         $parameters["offset"] = $pagination->offset;

         $category = Service_Product_Category::get_product_category_by_route_id(false, $parameters);
         $pc_template_outer = new View("product/product_category_index");
         $pc_template_inner = new View("product/product_category_items");

         $pc_template_outer->category = $page_data;
         $pc_template_outer->pagination = $pagination->render();
         $pc_template_inner->products = $category["products"];
         $pc_template_outer->product_category_items = $pc_template_inner;

         $this->request->response = $pc_template_outer->render();
         //$this->request->response=$template->render();*/
    }

    /**
     * Kategorie produktu
     * @param type $page
     */
    public function action_category($page = 1)
    {
        $route_id = $this->application_context->get_route_id();
        $nazev_seo = $this->application_context->get_actual_seo();

        Session::instance()->set("last_product_category", $nazev_seo);

        $search_string = (isset($_GET["search_string"]) ? $_GET["search_string"] : (isset($_POST["search_string"]) ? $_POST["search_string"] : ""));

        $categories = new View("product/product_carousel");
        $pc_template_outer = new View("product/product_category");
        $pc_template_inner = new View("product/product_category_items");
        $pc_template_inner->logged_in = Service_User::instance()->logged_in();
        $pc_filters = new View("product/product_category_filters");
        $pc_filters->search_string = $search_string;

        // ziskani vsech parametru z filtru a z razeni
        $parameters = array();

        // upraveno na radiobutton
        //if(isset($_GET["products_order"])) $parameters["order_by"]=$_GET["products_order"];
        //if(isset($_GET["sort"])) $parameters["order_direction"]=$_GET["sort"];
        if (isset($_GET["sort"])) {
            $sort_array = explode(":", $_GET["sort"]);
            $parameters["order_by"] = $sort_array[0];
            $parameters["order_direction"] = $sort_array[1];
        }

        if (isset($_GET["products_filter_action"]))
            $parameters["products_filter_action"] = $_GET["products_filter_action"];
        if (isset($_GET["price_selected_max"]))
            $parameters["price_selected_max"] = $_GET["price_selected_max"];
        if (isset($_GET["price_selected_min"]))
            $parameters["price_selected_min"] = $_GET["price_selected_min"];

        if (isset($_GET["pfm"]) && ($_GET["pfm"]))
            $parameters["products_filter_manufacturers"] = $_GET["pfm"];
        else
            $parameters["products_filter_manufacturers"] = null;
//        if(isset($_GET["pfc"]) && ($_GET["pfc"])) $parameters["products_filter_colors"]=$_GET["pfc"]; else $parameters["products_filter_colors"]=null;
//        if(isset($_GET["pfs"]) && ($_GET["pfs"])) $parameters["products_filter_sizes"]=$_GET["pfs"]; else $parameters["products_filter_sizes"]=null;
//
        if (isset($_GET["search_string"]) && ($_GET["search_string"]))
            $parameters["search_string"] = $_GET["search_string"];
        if (isset($_POST["search_string"]) && ($_POST["search_string"]))
            $parameters["search_string"] = $_POST["search_string"];

        // skklad akce novinkz

        if (isset($_GET["new"]) && !empty($_GET["new"])) {
            //  $parameters["where"][1] ='products.product_action_type=2"';
            $parameters["where"][0][0] = 'new';
            $parameters["where"][0][1] = '=';
            $parameters["where"][0][2] = '1';

            $pc_template_outer->new = $_GET["new"];
        }
        if (isset($_GET["action"]) && !empty($_GET["action"])) {
            //  $parameters["where"][2] ='products.pocet_na_sklade>0';
            $parameters["where"][1][0] = 'action';
            $parameters["where"][1][1] = '=';
            $parameters["where"][1][2] = '1';


            $pc_template_outer->action = $_GET["action"];
        }
        if (isset($_GET["gift"]) && !empty($_GET["gift"])) {
            //  $parameters["where"][2] ='products.pocet_na_sklade>0';
            $parameters["where"][2][0] = 'gift';
            $parameters["where"][2][1] = '=';
            $parameters["where"][2][2] = '1';


            $pc_template_outer->gift = $_GET["gift"];
        }
        if (isset($_GET["sale_off"]) && !empty($_GET["sale_off"])) {
            //  $parameters["where"][2] ='products.pocet_na_sklade>0';
            $parameters["where"][3][0] = 'sale_off';
            $parameters["where"][3][1] = '=';
            $parameters["where"][3][2] = '1';


            $pc_template_outer->sale_off = $_GET["sale_off"];
        }
        if (isset($_GET["stock"]) && !empty($_GET["stock"])) {
            $parameters["where"][4][0] = 'in_stock';
            $parameters["where"][4][1] = '=';
            $parameters["where"][4][2] = '1';
            $pc_template_outer->stock = $_GET["stock"];
        }

        $parameters["where"][6][0] = "smazano";
        $parameters["where"][6][1] = "!=";
        $parameters["where"][6][2] = "1";
        // prepocteni cen z price slideru podle toho zda je dana sleva procentem
//        $price_category_value=$this->product_service->get_price_category_percentual_value();
//
//        if($price_category_value)
//        {
//            if(isset($parameters["price_selected_max"]))$parameters["price_selected_max"]=$parameters["price_selected_max"] * (100/(100-$price_category_value));
//            if(isset($parameters["price_selected_min"]))$parameters["price_selected_min"]=$parameters["price_selected_min"] * (100/(100-$price_category_value));
//        }
        // sestrojeni pole podminek a parametru pro vybrani orm objektu
        // inicializace strankovani
        $product_show_pages = isset($_GET["products_show_pages"]) ? $_GET["products_show_pages"] : $this->items_on_page; //"all";
        if ($product_show_pages == "all") {
            $product_show_pages = 10000;
            $page = 1;
        }
        if (isset($_GET["gotopage"])) {
            $page = $_GET["gotopage"];
            unset($_GET["gotopage"]);
        }

        $total_items = Service_Product_Category::get_category_total_items($route_id, $parameters);
        $pc_template_outer->total_items = $total_items;
        $pagination = Pagination::factory(array(
            'current_page' => array('source' => $nazev_seo, 'value' => $page),
            'total_items' => $total_items,
            'items_per_page' => $product_show_pages,
            'view' => 'pagination/basic_hana',
            'auto_hide' => TRUE
        ));
        $pc_template_outer->items_per_page = $pagination->items_per_page;

        $parameters["limit"] = $pagination->items_per_page;
        $parameters["offset"] = $pagination->offset;
        //echo("<br />limit:".$pagination->items_per_page.", offset:".$pagination->offset);
        // vygenerovani vypisu dane kategorie
        $parameters["search_string"] = $search_string;
        $products = array();
        $category = Service_Product_Category::get_product_category_by_route_id($route_id, $parameters);
        //$child_products = Service_Product_Category::get_children_category_products($category["category_data"]["id"],$parameters);
        //$products = array_merge($products,$child_products);

        if (isset($category["products"])) {
            $products = array_merge($products, $category["products"]);
        }


        if (!empty($category["category_data"]))
            $pc_template_outer->category = $category["category_data"];
        $categories->categories = Service_Product::get_favourite_cat(12, $this->application_context->get_actual_language_id(), 0, $category["category_data"]["id"]);
        $pc_template_outer->carousel = $categories;
        // vygenerovani vypisu obsazenych produktu
        //if (/* $category["category_data"]["priorita"]>0 && */!Service_Product_Category::is_empty_category($route_id)) { // kategorie bez aplikace filtrovani obsahuje produkty
        // naplneni sablony produky
        if (isset($products)) {
            $pc_template_inner->products = $products;
        } else {
            // zadne produkty pro dane filtrovani
            $pc_template_outer->empty_category_by_filter = true;
        }

        $pc_template_outer->current_query = htmlentities(url::query());

        // zjisteni hodnot a naplneni filtrovacich a tridicich prvku
        $pc_filters->manufacturers = Service_Product_Category::get_category_manufacturers($route_id); //($nazev_seo, $search_string, $parameters["products_filter_manufacturers"], $parameters["products_filter_colors"], $parameters["products_filter_sizes"]);//get_category_manufacturers($nazev_seo);
        // rozmezi cen
        $range = Service_Product_Category::get_category_price_ranges($route_id);
        if ($range["price_min"] > $range["price_max"])
            $range = array("price_min" => 0, "price_max" => 0);
        $pc_template_outer->prices = $range;

        // zachovani kontextu - vsechny zvolene promenne z predchoziho pozadavku musim opet vyplnit
        //// vyhledavani
        if (isset($parameters["search_string"]))
            $pc_template_outer->search_string = $parameters["search_string"];

        //// polozka razeni
        $pc_template_outer->products_order = !empty($parameters["order_by"]) ? $parameters["order_by"] : Service_Product_Category::$default_order_by; //upraveno na radio $_GET["products_order"];
        //// smer razeni
        $pc_template_outer->sort = !empty($parameters["order_direction"]) ? $parameters["order_direction"] : Service_Product_Category::$default_order_direction; //upraveno na radio$_GET["sort"];
        //upraveno na radio$pc_template_outer->current_query_sort=(url::query()?htmlentities(url::query())."&amp;":"?");
        //// pocet na stranku
        if (isset($_GET["products_show_pages"]))
            $pc_template_outer->products_show_pages = $_GET["products_show_pages"];
        else
            $pc_template_outer->products_show_pages = 20;

        //// akce checkbox
        /* if (isset($_GET["products_filter_action"]))
             $pc_filters->products_filter_action = 1;
         else
             $pc_filters->products_filter_action = 0;

         //// checkboxy vyrobcu
         if (isset($_GET["pfm"]) && $_GET["pfm"]) {
             $manchbids = array_combine($_GET["pfm"], $_GET["pfm"]);
             if (count($manchbids)) {
                 $pc_filters->manchbids = $manchbids;
             }
         } else {
             $pc_filters->manchbids = array();
         }

         $pc_filters->price_selected_min = $pc_template_outer->price_selected_min = (isset($_GET["price_selected_min"]) ? $_GET["price_selected_min"] : $prices["price_min"]);
         $pc_filters->price_selected_max = $pc_template_outer->price_selected_max = (isset($_GET["price_selected_max"]) ? $_GET["price_selected_max"] : $prices["price_max"]);
         $pc_filters->price_min = $prices["price_min"];
         $pc_filters->price_max = $prices["price_max"];*/
        $pc_template_outer->price_selected_max = (isset($_GET["price_selected_max"]) ? $_GET["price_selected_max"] : $range["price_max"]);
        $pc_template_outer->price_selected_min = (isset($_GET["price_selected_min"]) ? $_GET["price_selected_min"] : $range["price_min"]);

        // zaslani dodatecnych dat a vlozeni produktu do vnejsi sablony
        $pc_template_outer->pagination = $pagination->render();

        $pc_template_outer->filters = $pc_filters->render();
        $pc_template_outer->current_page = url::base() . $nazev_seo . (($page > 1) ? "/" . $page : "");
        $pc_template_outer->product_category_items = $pc_template_inner;

        // ulozeni aktualniho GETu do sesny
        Session::instance()->set("actual_query", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
        $pc_template_outer->empty_category = false;
        //} else {
        // prazdna kategorie
        //    $pc_template_outer->empty_category = true;
        //}


        $this->request->response = $pc_template_outer->render();
    }


    public function action_random()
    {
        $rand_products = new View("product/product_rand");

        $route_id = $this->application_context->get_actual_route();

        $rand_products->products = Service_Product::get_random_product_by_product($route_id, 3);

        $this->request->response = $rand_products->render();
    }

    /**
     *  produktu.
     */
    public function action_detail()
    {


        $route_id = $this->application_context->get_route_id();
        $product_detail = new View("product/product_detail");
        $item = Service_Product::get_product_detail_by_route_id($route_id);
        $product_detail->variants = Service_Product_Variant::get_product_variants($item["id"]);
        //die(var_dump($product_detail->variants));
        $youtube_parts = explode('/', $item["youtube_code"]);
        $item["youtube"] = isset($youtube_parts[3]) ? $youtube_parts[3] : "";
        $product_detail->item = $item;
        $product_detail->shopper = Service_User::instance()->get_user();
        //$product_detail->essoxCalculatorUrl = Service_Essox::getEssoxCalculatorUrl($item['cena_s_dph']);
        $this->request->response = $product_detail->render();
        //print_r($item);
    }


    public function action_detail_form()
    {
        $varianta_id = arr::get($_POST, 'varianta_id', '');

        //$route_id = $this->application_context->get_route_id();
        $product_detail = new View("product/product_detail_form");
        $item = Service_Product::get_product_detail_by_variant_id($varianta_id);
        $this->response_object->set_message('Varianta byla změněna!');  // nastaveni vysledne zpravy

        //$youtube_parts = explode('/', $item["youtube_code"]);
        //$item["youtube"] = isset($youtube_parts[3]) ? $youtube_parts[3] : "";
        $product_detail->item = $item;
        //$product_detail->shopper = Service_User::instance()->get_user();
        $this->request->response = $product_detail->render();
        //print_r($item);
    }

    /**
     * Detail produktu.
     */
    public function action_popup_detail()
    {
        $route_id = $this->application_context->get_route_id();
        $product_detail = new View("product/product_detail_popup");
        $product_detail->item = Service_Product::get_product_detail_by_route_id(null, $_GET["detailConf"]);
        $product_detail->shopper = Service_User::instance()->get_user();
        $this->request->response = $product_detail->render();
    }

    public function action_category_widget($seo, $param = "")
    {
        $product_carousel = new View("product/product_carousel");
        $categories = Service_Product::get_favourite_cat(24, $this->application_context->get_actual_language_id(), 0, $param);

        //die(print_r($categories));

        $product_carousel->categories = $categories;
        $this->request->response = $product_carousel->render();
        //$this->request->response=$template->render();
    }

    /**
     * Karousel s vybranymi kategoriemi
     */
    public function action_carousel_widget($seo, $param = "")
    {
        $product_carousel = new View("product/product_carousel");

        //$category=Service_Product_Category::get_carousel_product_categories($param, $this->application_context->get_actual_language_id());
        $categories = Service_Product::get_favourite_cat(24, $this->application_context->get_actual_language_id());

        //  die(print_r($priducts));

        $product_carousel->categories = $categories;
        $this->request->response = $product_carousel->render();
        //$this->request->response=$template->render();
    }

    public function action_product_widget($seo, $param = "")
    {
        $product_carousel = new View("product/top");
        $parameters = array();

        //$category=Service_Product_Category::get_carousel_product_categories($param, $this->application_context->get_actual_language_id());
        $products = Service_Product::get_favourite(6, false, $this->application_context->get_actual_language_id());

        //  die(print_r($priducts));

        $product_carousel->products = $products;
        $this->request->response = $product_carousel->render();
        //$this->request->response=$template->render();
    }


    /**
     * Widget produktove subnavigace
     */
    public function action_category_subnav()
    {
        $subnav = new View("product/subnav");
        $links = $temp_links = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), 3, 0, false, false);
        $sel_links = $sel_links_temp = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        //$sel_links = Service_product::get_navigation_breadcrumbs($this->application_context->get_actual_seo());
        //die(print_r($sel_links));
        $sel_link = array_pop($sel_links_temp);

        $sel_link2 = array_pop($sel_links_temp);

        $links = array();
        //$links = !empty($temp_links[$sel_link["nazev_seo"]]["children"][$sel_link2["nazev_seo"]]["children"])?$temp_links[$sel_link["nazev_seo"]]["children"][$sel_link2["nazev_seo"]]["children"]:array();
        if (empty($links))
            $links = !empty($temp_links[$sel_link["nazev_seo"]]["children"]) ? $temp_links[$sel_link["nazev_seo"]]["children"] : array();
        //die(print_r($sel_links));
        //die(print_r($links));
        $subnav->links = $links;
        //$subnav->recomended=Service_Product::get_favourite(3);;
        $subnav->sel_links = $sel_links;

        $this->request->response = $subnav->render();
    }

    /**
     * Widget pro podobné produkty
     */
    public function action_similar_widget()
    {
        $template = new View("product/similar_widget");
        $template_products = new View("product/product_category_items");
        $route = Service_Route::get_page_route_by_nazev_seo(Session::instance()->get("last_product_category"));
        $category = Service_Product_Category::get_product_category_by_route_id($route->id);
        $product = Service_Product::get_product_detail_by_route_id($this->application_context->get_route_id());
        //die(print_r(Service_Product::get_random_products_by_category($category["category_data"]["id"], 6, 0, array($product["id"]))));
        if (isset($category["category_data"]["id"]))
            $products = Service_Product::get_random_products_by_category($category["category_data"]["id"], 6, 0, array($product["id"]));
        else
            $products = array();
        $template_products->products = $products;
        $template->product_template = $template_products->render();
        $this->request->response = $template->render();
    }

    public function action_favourite_widget()
    {
        $template_products = new View("product/product_category_items");
        $template_products->products = Service_Product::get_favourite(3);

        $this->request->response = $template_products->render();
    }


}

?>
