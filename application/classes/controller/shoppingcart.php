<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Obsluha nakupniho kosiku.
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_ShoppingCart extends Controller
{
   
    /**
    * Metoda generujici maly nahledovy nakupni kosik.
    */
    public function action_widget()
    {
        // vykresleni mini-kosiku
        $mini_cart_template=new View("shoppingcart/cart_widget");
        
        /*@var $cart Model_Cart */
        $cart=  Service_ShoppingCart::get_cart();
        //$mini_cart_template->items=$cart->get_cart_products();
        
        $mini_cart_template->total_items=$cart->total_items;
        $mini_cart_template->cena_celkem_bez_dph=$cart->total_cart_price_without_tax;
        $mini_cart_template->cena_celkem_s_dph=$cart->total_cart_price_with_tax;
        
        $this->request->response=$mini_cart_template->render();
    }

    /**
     * Metoda generujici detail kosiku i s obsluznyma tlacitkama a hornima navigacnima ikonama (kompletni 1. krok objednavky).
     * @param string $mode - priznak (editable/read_only) zda se ma zobrazit editovatelny/needitovatelny kosik
     */
    public function action_detail($mode="editable")
    {
        $order_container_template=new View("order/order_container");
        $cart_detail_container_template=new View("shoppingcart/cart_detail_container");
        
        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $order_container_template->page = $page;

        $cart=Service_Hana_ShoppingCart::get_cart();
        //die(var_dump($cart->get_cart_products()));
        $owner_data=orm::factory("product_setting",1);
        $shipping_free_threshold=$owner_data->shipping_free_threshold;
        $cart_detail_container_template->priceToFreeShipping = $shipping_free_threshold-$cart->total_cart_price_with_tax; 
        
        $cart_detail_template=Request::factory("internal/shoppingcart/detail_table/".$mode)->execute()->response;
        $cart_detail_container_template->cart_detail_table=$cart_detail_template;
        $cart_detail_container_template->total_products=$cart->total_items;
        
//        // doprava
//        $cart_detail_container_template->shipping=Request::factory("internal/order/shipping")->execute()->response;
//        
//        // platba
//        $cart_detail_container_template->payment=Request::factory("internal/order/payment")->execute()->response;
//        
        // tabulka souctu cen
        $cart_detail_container_template->order_price_summary_table=Request::factory("internal/order/order_price_summary_table/")->execute()->response;

        $order_container_template->content=$cart_detail_container_template;
        $this->request->response=$order_container_template->render();
    }

    /**
     * Tato metoda zobrazi pouze tabulku s kosikem - bez obsluznych tlacitek a ikon.
     * @param string $mode - priznak (editable/read_only) zda se ma zobrazit editovatelny/needitovatelny kosik
     */
    public function action_detail_table($mode=false)
    {
        $shopping_cart=Service_Hana_ShoppingCart::get_cart();

        if($shopping_cart->total_cart_price_with_tax>0)
        {
            $cart_detail_template=new View("shoppingcart/cart_detail_table");
            $cart_detail_template->read_only=($mode=="editable")?false:true;
	 
            //die(var_dump($shopping_cart->get_cart_products()));
            $cart_detail_template->cart_products=Service_Product::insert_parent_product($shopping_cart->get_cart_products());
            $cart_price_with_tax=$shopping_cart->total_cart_price_with_tax;
            $cart_detail_template->presents=Service_Product::get_presents($cart_price_with_tax);
            $cart_detail_template->selected_present=Session::instance()->get("selected_gift", false);
            $cart_detail_template->price_total_products=$cart_price_with_tax;
            
            $cart_detail_template->voucher = Service_Voucher::get_voucher();
                    
            //zmeneno $cart_detail_template->price_total=$this->cart_service->get_total_order_price_with_tax();

            $this->request->response=$cart_detail_template->render();
        }
        else
        {
            $shopping_cart->flush();
        }

    }
    
    public function action_show_gift_boxes()
    {
        $template=new View("shoppingcart/order_gift_box");
        //$language_id=$this->application_context->get_actual_language_id();
        $gift_boxes = orm::factory("gift_box")->where("zobrazit","=",1)->find_all();
        $template->gift_boxes = $gift_boxes;
        $template->selected_gift_box=Session::instance()->get("gift_box", null);
        
        $this->request->response=$template->render();
    }
    
    
    //////////////////////////////////////// metody pro manipulaci s obsahem kosiku
    
    /**
     * Obsluha pridavani do kosiku.
     */
    public function action_add_item()
    {
        // zpracovani dat
        $product_id=Input::post("product_id");
        $quantity=trim(Input::post("quantity"));
        
        $response=Service_ShoppingCart::add_to_cart($product_id, $quantity);
	//$response_item=  Service_Product::get_product_detail_by_id($product_id);
        //die(var_dump(Service_ShoppingCart::generate_cart_content_with_prices(Service_ShoppingCart::get_cart())));
        // nastaveni Hana_Response objektu na zaklade vysledku dat
        $this->response_object->set_status($response);                                          // true/false - probehlo ok?
        $this->response_object->set_redirect($response);                                        // true/false - presmerovat? 
        $this->response_object->set_message(Service_ShoppingCart::get_cart()->get_messages());  // nastaveni vysledne zpravy
        
        // nutno "zaregistrovat ke zpracovani" vsechny widgety, ktery je nutno pri ajaxovym pozadavku pregenerovat
        Hana_Widgets::instance()->add_to_process("minicart", "internal/shoppingcart/widget"); // Widget "minicart"
        
        $response=Request::factory("internal/shoppingcart/detail_table/editable")->execute()->response;
        
        $this->request->response=$response;
    }
    
    /**
     * Odstrani zbozi z kosiku.
     * @return type 
     */
    public function action_remove_item()
    {
        $product_id=Input::get("item_id");
        if(!$product_id) return;
        
        $response=Service_ShoppingCart::remove_from_cart($product_id);
        
        $this->response_object->set_status($response);                                          // true/false - probehlo ok?
        $this->response_object->set_redirect($response);                                        // true/false - presmerovat? 
        $this->response_object->set_message(Service_ShoppingCart::get_cart()->get_messages());  // nastaveni vysledne zpravy
        
        if($this->application_context->is_ajax())
        {
            Hana_Widgets::instance()->add_to_process("minicart", "internal/shoppingcart/widget"); // Widget "minicart"
        
            $response=Request::factory("internal/shoppingcart/detail_table/editable")->execute()->response;
            $this->request->response=$response;
        }
    }
    
    /**
     * 
     */
    public function action_refresh()
    {
        $status=true;
        
        
        $cart_products=Input::post("product_quantity");
        if(!empty($cart_products))
        {
            $shopping_cart=Service_ShoppingCart::get_cart();
            foreach($cart_products as $product_id=>$quantity)
            {
                if($quantity<0) $quantity=1;
                
                $st=$shopping_cart->set_item($product_id, $quantity, false, true);
                if(!$st) $status=false;
            }
            
            // pridani darku
            $gift_id=Input::post("gift_item");
            
            if(!empty($gift_id))
            { 
                // pri pridani darku je nutne nejprv vsechny odebrat
                $presents=  Service_Product::get_presents(false, true);
                foreach($presents as $present)
                {
                    $shopping_cart->remove_item($present["id"]);
                }

                if(Service_Product::is_free_present($gift_id, Service_Hana_ShoppingCart::get_cart()->total_cart_price_with_tax))
                {
                    Session::instance()->set("selected_gift", $gift_id);
                    $st=$shopping_cart->set_item($gift_id, 1, false, false);
                    if(!$st) $status=false;
                }
                
            }
        }
        
        // odebrani pripadnych darku z kosiku - pokud doslo k poklesu ceny
//        if(!Service_Product::is_free_present(Service_Hana_ShoppingCart::get_cart()->total_cart_price_with_tax))

//        $presents=  Service_Product::get_presents(false, true);
//        foreach($presents as $present)
//        {
//            if($present["gift_threshold_price"] > Service_Hana_ShoppingCart::get_cart()->total_cart_price_with_tax)
//            {
//                $shopping_cart->remove_item($present["id"]);
//            }
//        }

        
        $this->response_object->set_status($status);                                          // true/false - probehlo ok?
        if($status)
        {
            $this->response_object->set_redirect(true);                                        // true/false - presmerovat? 
            $this->response_object->set_message(array(Hana_Response::MSG_PROCESSED=>__("Obsah košíku byl přepočítán")));  // nastaveni vysledne zpravy
        }
        else
        {
            $this->response_object->set_message(array(Hana_Response::MSG_ERROR=>__("Bylo zadáno neplatné množství")));
        }
        
        if($this->application_context->is_ajax())
        {
            Hana_Widgets::instance()->add_to_process("minicart", "internal/shoppingcart/widget"); 
        
            $response=Request::factory("internal/shoppingcart/detail_table/editable")->execute()->response;
            $this->request->response=$response;
        }
        
        return $status;
        
    }
    
    /**
     * Ulozeni kosiku a redirect na prvni krok objednavky
     */
    public function action_go_to_order()
    {
        $status = $this->action_refresh();
        $this->response_object->delete_messages(); // zpravu z action_refresh budu ignorovat
        
        // ulozeni slevoveho kuponu
        $voucher_code=Input::post("voucher");
        
        if($voucher_code)
        {
            if(Service_Voucher::pre_use_voucher($voucher_code))
            {
                $this->action_refresh();
                $this->response_object->set_message(array(Hana_Response::MSG_PROCESSED=>__("Slevový kupón byl započítán")));
            }
            else
            {
                $this->response_object->set_message(array(Hana_Response::MSG_ERROR=>__("Slevový kupón není platný")));
                $status=false;
            }
        }
        
        // ulozeni darkove krabicky
        $gift_box=Input::post("gift_box");
        Session::instance()->set("gift_box", $gift_box);


        $this->response_object->set_status($status);
 
        if($status)
        {
            $this->response_object->set_redirect(url::base().__("uzivatelske-udaje"));
            
        }
        else
        {
            
            
        }
    }   
    
    
}

?>
