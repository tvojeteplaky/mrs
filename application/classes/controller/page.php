<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Page extends Controller
{
    /**
     * Staticka promenna kvuli urceni typu subnavigace v navaznosti na hlavni stranku.
     * @var type
     */
    public static $current_page_category_id = 3;

    /**
     * Metoda generujici obsah uvodni stranku.
     */
    public function action_index()
    {
        $route_id = $this->application_context->get_actual_route();
        $template = new View("page/homepage");
        $template->item = Service_Page::get_page_by_route_id($route_id);
        $template->owner = orm::factory("owner",1);
        $this->request->response = $template->render();
    }

    /**
     * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
     */
    public function action_detail($page_get = 1)
    {
        $route_id = $this->application_context->get_actual_route();
        $page = Service_Page::get_page_by_route_id($route_id, true);
        // $top_page = Service_Page::get_top_parent_page($page);

        if ($page["indexpage"]) {
            return $this->action_index();
		} elseif ($page["nazev_seo"] == 'ulovky') {
			$template = new View("ulovky/galerie");

			$query = DB::query(Database::SELECT, 'SELECT COUNT(id) AS photos_count FROM page_photos WHERE page_id = :page_id');
			$query->param(':page_id', $page['id']);
			$photos_count = $query->execute()->get('photos_count');
			$template->photos_count = $photos_count;

      		$items_per_page = 30;
      		$pagination = Pagination::factory(array(
        		'current_page'   => array('source' => $this->application_context->get_actual_seo(), 'value'=>$page_get),
        		'total_items'    => $photos_count,
        		'items_per_page' => $items_per_page,
        		'view'              => 'pagination/basic',
        		'auto_hide'      => TRUE
        	));

			$query2 = DB::query(Database::SELECT, 'SELECT * FROM page_photos INNER JOIN page_photo_data ON page_photos.id = page_photo_data.page_photo_id WHERE page_id = :page_id LIMIT :limit OFFSET :offset');
			$query2->param(':page_id', $page['id']);
			$query2->param(':limit', $pagination->items_per_page);
			$query2->param(':offset', $pagination->offset);
			$photos = $query2->execute()->as_array();
			$template->photos = $photos;
      	    $template->pagination=$pagination->render();

        } else {
            $template = new View("page/detail");
        }

        $template->item = $page;
        // $template->top_item = $top_page;
        // $sublinks = (Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), self::$current_page_category_id));
        // $template->sublinks = isset($sublinks[$this->application_context->get_actual_seo()]["children"]) ? $sublinks[$this->application_context->get_actual_seo()]["children"] : array();


        self::$current_page_category_id = $page["page_category_id"];

        $template = $template->render();

        $this->request->response = $template;
    }

    public function action_structured() {
        $page = new View("page/structured");
        $route_id = $this->application_context->get_actual_route();
        $page->item = Service_Page::get_page_by_route_id($route_id, true);
        $page->childs = Service_Page::get_pages_with_parent($page->item);
        $this->request->response = $page->render();
    }

    public function action_static($kod)
    {
        $language_id = $this->application_context->get_actual_language_id();
        $this->request->response = Service_Page::get_static_content_by_code($kod, $language_id)->popis;
    }

    public function action_subnav($nazev_seo)
    {
        $subnav = new View("navigation/subnav");
        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $links = Service_Page::get_pages_with_parent(Service_Page::get_top_parent_page($page));

        $breadcrumbs = Hana_Navigation::instance()->get_navigation_breadcrumbs();

        $subnav->items = $links;
        $subnav->breadcrumbs = $breadcrumbs;
        $subnav->parent = array_pop($breadcrumbs);
        $this->request->response = $subnav->render();
    }

    public function action_structured_subnav($nazev_seo) {
        $subnav = new View("page/subnav");
        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $links = Service_Page::get_pages_with_parent(Service_Page::get_top_parent_page($page));

        $breadcrumbs = Hana_Navigation::instance()->get_navigation_breadcrumbs();

        $subnav->items = $links;
        $subnav->breadcrumbs = $breadcrumbs;
        $subnav->parent = array_pop($breadcrumbs);
        $this->request->response = $subnav->render();
    }
    public function action_widget()
    {
        $widget = new View('page/widget');
        $widget->items = Service_Page::get_pages_with_parent($this->application_context->get_actual_language_id(), 3, 3);
        $this->request->response = $widget->render();
    }

    public function action_unrelated_widget()
    {
        $widget = new View('page/unrelated_widget');
        $widget->item = Service_Page::get_unrelated_page_to_homepage($this->application_context->get_actual_language_id());
        $this->request->response = $widget->render();
    }



    public function action_sitemap()
    {
        $template = new View("page/detail");
        $route_id = $this->application_context->get_actual_route();
        $page = Service_Page::get_page_by_route_id($route_id);

        // vylistovani vsech viditelnych "struktur"
        $sitemap = new View("page/sitemap");
        $sitemap->zarazene = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), 3);
        $sitemap->nezarazene = Hana_Navigation::instance()->get_navigation($this->application_context->get_actual_language_id(), 2);

        $page["popis"] = $sitemap->render();

        $template->item = $page;
        $this->request->response = $template->render();
    }


}

?>
