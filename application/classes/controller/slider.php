<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Slider extends Controller
{

    public function action_widget()
    {
        $template = new View('slider/widget');
        $template->sliders = Service_Slider::get_sliders(1);
        $this->request->response = $template->render();
    }

}