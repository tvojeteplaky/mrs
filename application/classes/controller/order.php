<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani stranek a podstranek patricich k objednavkam.
 */
class Controller_Order extends Controller
{

    /**
     *
     * @var Service_User
     */
    private $user_service;

    private $eshop_homepage_cz_seo = "nabidka"; // uvodka eshopu pro presmerovani z neuspesnych kroku objednavky

    public function before()
    {
        parent::before();
        // vsechny metody jsou platne pouze v rozsahu internich requestu, zadne externi volani

        $this->user_service = Service_User::instance();

    }

    /** zakladni metody kontrolery modulu **/

    /********* 1) Akce pro obsluhu dopravy a platby **********/

    /**
     * Metoda pro zobrazeni 1. kroku objednavky (0.krok = kosik)
     */
    public function action_show_shipping_payment()
    {
        $total_items = Service_Hana_ShoppingCart::get_cart()->total_items;
        if ($total_items == 0) Request::instance()->redirect(__($this->eshop_homepage_cz_seo));

        // zpracovani dat a zaslani vysledku ze submetod
        $order_payment = Request::factory("internal/order/payment")->execute()->response;
        $order_shipping = Request::factory("internal/order/shipping")->execute()->response;

        $eshop_setting = orm::factory("product_setting", 1)->find()->as_array();
        //die(var_dump($eshop_setting));
        if (!$this->user_service->logged_in()) $this->user_service->need_insert_user_data(true);

        $order_container_template = new View("order/order_container");
        $order_shipping_payment_template = new View("order/order_shipping_payment");

        //Montáž vypočítání ceny
        $order = Service_Order::get_populated_order();
        if (ceil($order->order_total_without_vat) <= $eshop_setting["instalation_price_limit"]) {
            $montaz_price = $eshop_setting["instalation_price_under_limit"];
        } else {
            $montaz_price = ceil($order->order_total_without_vat / 100 * $eshop_setting["instalation_percentage_after_limit"]);
        }


        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $order_container_template->page = $page;
        /* Informace o objednávce - tabulka*/
        $cart_detail_template = Request::factory("internal/shoppingcart/detail_table/" . $mode = false)->execute()->response;


        $order_shipping_payment_template->cart_detail_table = Request::factory("internal/shoppingcart/detail_table/read_only")->execute()->response;

        // doprava
        $order_shipping_payment_template->order_shipping = $order_shipping;

        // platba
        $order_shipping_payment_template->order_payment = $order_payment;

        //Nastavení eshopu(obsahuje data o montáži)
        $order_shipping_payment_template->setting = $eshop_setting;
        $order_shipping_payment_template->montaz_price = $montaz_price;
        $order_shipping_payment_template->montaz_selected = Session::instance()->get("montaz");

        // prehled cen
        $order_shipping_payment_template->order_price_summary_table = Request::factory("internal/order/order_price_summary_table/")->execute()->response;

        $order_container_template->content = $order_shipping_payment_template;
        $order_container_template->cart_table = $cart_detail_template;
        $order_container_template->favourite = Service_Product::get_favourite(100);;

        $order_container_template->order_step = 2;
        $this->request->response = $order_container_template->render();

    }

    /**
     * Metoda pro ajaxove prepocitani dopravy a platby na zaklade zvolenych parmetru.
     */
    public function action_refresh_shipping_payment()
    {
        // ulozeni dopravy a platby
        $shipping_id = (isset($_POST["shipping_id"])) ? $_POST["shipping_id"] : false;
        $payment_id = (isset($_POST["payment_id"])) ? $_POST["payment_id"] : false;

        Service_Shipping::set_shipping_id($shipping_id);
        Service_Payment::set_payment_id($payment_id);

        // presmerovani na znovuvygenerovani contentu
        $this->action_show_shipping_payment();
    }

    /**
     * Metoda pro ulozeni zvolene dopravy a platby (zpracovani 1. kroku objednavky a prechod na 2. krok)
     */
    public function action_save_shipping_payment()
    {
        $next_step_seo = __("potvrzeni-objednavky");
        // TODO doimplementovat kontrolu i v servisni metode

        $result = Service_Order::process_selected_shipping_payment_types(isset($_POST["shipping_id"]) ? $_POST["shipping_id"] : false, isset($_POST["payment_id"]) ? $_POST["payment_id"] : false, isset($_POST["montaz"]) ? true : false);
        $this->response_object->set_status($result);

        if ($result) {
            // vyber platby a dopravy probehl uspesne, pokracuji k dalsimu kroku
            $this->response_object->set_redirect($next_step_seo);
        } else {

        }
    }

    /********* 2) Akce pro obsluhu dodacich udaju **********/

    /**
     * "Mezikrok" mezi 1-2 (prihlaseni/registrace+prihlaseni/ziskani dat o uzivateli bez registrace)
     */
    public function action_show_order_user_data_source()
    {
        $total_items = Service_Hana_ShoppingCart::get_cart()->total_items;

        if ($total_items == 0) Request::instance()->redirect(__($this->eshop_homepage_cz_seo));

        $next_step_seo = __("dodaci-udaje");
        $next_step_seo_unregistered = __("doprava-a-platba");

        $expand_form = "";

        if ($this->user_service->logged_in()) {
            Request::instance()->redirect(url::base() . $next_step_seo);
        }


        $order_container_template = new View("order/order_container");
        $order_user_data_source_template = new View("order/order_user_data_source");

        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $order_container_template->page = $page;
        // rozdeleni prace do subkontroleru ;-) TODO - umoznit nastaveni techto trech moznosti v adminu
        // 1. zjistit prihlaseni uzivatele a pripadne ho presmerovat

        // 2.1 zavolat kontroler pro prihlaseni
        $user_login = Request::factory("internal/user/show_login_form/" . $next_step_seo . "/true/")->execute()->response;

        // 2.2 zavolat kontroler pro registraci s prihlasenim
        $user_registration = Request::factory("internal/user/show_registration_form/" . $next_step_seo . "/true/")->execute()->response;

        // 2.2 zavolat kontroler pro pokracovani bez prihlaseni
        $order_unregistered_user = Request::factory("internal/order/show_unregistered_user_form/" . $next_step_seo_unregistered)->execute()->response;

        if ($this->user_service->is_temporarily_stored()) {
            $order_user_data_source_template->action = "add_unregistered_user";
        } else {
            if (isset($_POST["h_action"])) $order_user_data_source_template->action = $_POST["h_action"];
        }

        /* Informace o objednávce - tabulka*/
        $cart_detail_template = Request::factory("internal/shoppingcart/detail_table/" . $mode = false)->execute()->response;

        $order_user_data_source_template->user_login = $user_login;
        $order_user_data_source_template->user_registration = $user_registration;
        $order_user_data_source_template->order_unregistered_user = $order_unregistered_user;
        $order_user_data_source_template->expand_form = $expand_form;

        $order_container_template->content = $order_user_data_source_template;
        $order_container_template->cart_table = $cart_detail_template;
        $order_container_template->favourite = Service_Product::get_favourite(100);;
        $order_container_template->order_step = 1;
        $this->request->response = $order_container_template->render();
    }


    /**
     * Metoda pro zobrazeni 2. krok objednavky (vyber dodaci adresy)
     */
    public function action_show_order_delivery_data($msg = false)
    {
        $total_items = Service_Hana_ShoppingCart::get_cart()->total_items;
        if ($total_items == 0) Request::instance()->redirect(__($this->eshop_homepage_cz_seo));

        $next_step_seo = __("doprava-a-platba");

        // pokud neni uzivatel prihlasen, jdu na "mezikrok"
        if (!$this->user_service->logged_in() /*&& $this->user_service->need_insert_user_data()*/) {
            Request::instance()->redirect(url::base() . __("uzivatelske-udaje"));
        }


        $order_container_template = new View("order/order_container");
        $order_user_delivery_data_template = new View("order/order_user_delivery_data");


        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $order_container_template->page = $page;

        // ziskani dat o zakaznikovi
        $shopper = $this->user_service->get_user();
        $order_user_delivery_data_template->data = $shopper;


        /* Informace o objednávce - tabulka*/
        $cart_detail_template = Request::factory("internal/shoppingcart/detail_table/" . $mode = false)->execute()->response;


        // ziskani dat o zakaznikovych adresach
        $order_user_delivery_data_template->branches = $this->user_service->get_order_shopper_branches($shopper);

        $order_container_template->content = $order_user_delivery_data_template;
        $order_container_template->favourite = Service_Product::get_favourite(100);;
        $order_container_template->cart_table = $cart_detail_template;
        $order_container_template->order_step = 1;
        $this->request->response = $order_container_template->render();
    }

    /**
     * Metoda pro zpracovani 2. kroku objednavky a prechod na 3. krok
     */
    public function action_save_shopper_branch()
    {
        $next_step_seo = __("doprava-a-platba");
        $selected_branch_id = (isset($_POST["branch_id"])) ? $_POST["branch_id"] : false;
        $result = $this->user_service->store_shopper_branch($selected_branch_id);

        $this->response_object->set_status($result);
        if ($result) {
            $this->response_object->set_redirect($next_step_seo);
        }
    }

    /********* 2) Akce pro prehledu objednavky a vyplneni zaverecnych dat **********/


    /**
     * Metoda pro 3. krok objednavky (zobrazeni prehledu objednavky).
     */
    public function action_show_order_confirmation()
    {
        $shopping_cart = Service_Hana_ShoppingCart::get_cart();
        $total_items = $shopping_cart->total_items;
        if ($total_items == 0) Request::instance()->redirect(__($this->eshop_homepage_cz_seo));

        // pokud neni uzivatel prihlasen, jdu na "mezikrok"
        if (!$this->user_service->logged_in() && !$this->user_service->is_temporarily_stored()) {
            Request::instance()->redirect(url::base() . __("uzivatelske-udaje"));
        }

        $shopper = $this->user_service->get_user();

        $order_container_template = new View("order/order_container");
        $order_confirmation_template = new View("order/order_confirmation");
        $order_summary_table_template = new View("order/order_summary_table");
        $order_summary_table_template->summary_total = true;


        $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
        $order_container_template->page = $page;

        $order = Service_Order::get_populated_order();


        // kosik s dopravou a platbou
        //$order_confirmation_template->shopping_cart=Request::factory("internal/shoppingcart/detail_table/read_only")->execute()->response;
        $order_confirmation_template->cart_products = Service_Product::insert_parent_product($shopping_cart->get_cart_products());
        $cart_price_with_tax = $shopping_cart->total_cart_price_with_tax;
        $order_confirmation_template->presents = Service_Product::get_presents($cart_price_with_tax);
        $order_confirmation_template->selected_present = Session::instance()->get("selected_gift", false);
        $order_confirmation_template->price_total_products = $cart_price_with_tax;
        $order_confirmation_template->voucher = Service_Voucher::get_voucher();

        $order_confirmation_template->gift_box_code = orm::factory("gift_box")->where("id", "=", Session::instance()->get("gift_box"))->find()->code;

        //$order_confirmation_template->order_price_summary_table=Request::factory("internal/order/order_price_summary_table/true")->execute()->response;
        $price_values = array();

        if ($shopper && $shopper->action_first_purchase == 1) {
            $owner_data = orm::factory("product_setting", 1);
            //$price_values["order_discount"]=array("name"=>"Sleva za první nákup","value"=>-1*$owner_data->first_purchase_discount);
            //$price_values["order_total_with_discount"]=array("name"=>"Cena zboží po odečtení slevy","value"=>$order->order_total_with_vat-1*$owner_data->first_purchase_discount);
        }

        // prirazeni ceny kosiku
        $price_values["shipping_price"] = array("name" => "Dopravné a balné", "value" => $order->order_shipping_price);
        $price_values["payment_price"] = array("name" => "Platební metoda", "value" => $order->order_payment_price, "type" => $order->order_payment_nazev);

        $price_values["montaz"] = array("name" => "Montáž s DPH", "value" => $order->order_montaz_price);


        $price_values["total"] = array("name" => "Cena celkem s DPH", "value" => $order->order_total_CZK, "class" => "lastRow");
        $price_values["total_wo_dph"] = array("name" => "Cena zboží celkem bez DPH", "value" => $order->order_total_without_vat);


        $order_confirmation_template->price_values = $price_values;


        $order_summary_table_template->billing_data = $shopper;
        $order_summary_table_template->owner_data = orm::factory("product_setting", 1);//$this->application_context->get_web_owner_data();
        $order_summary_table_template->branch_data = $this->user_service->get_shopper_branch(isset($_SESSION["branch_id"]) ? $_SESSION["branch_id"] : false, $this->user_service->get_user());
        //die(print_r($this->user_service->get_shopper_branch(isset($_SESSION["branch_id"])?$_SESSION["branch_id"]:false,$this->user_service->get_user())));
        $order_summary_table_template->doprava = $order->order_shipping_nazev;
        $order_summary_table_template->platba = $order->order_payment_nazev;
        $order_confirmation_template->order_summary_table = $order_summary_table_template;

        $data = $this->response_object->get_data();
        $order_confirmation_template->data = $data;
        $order_confirmation_template->errors = $this->response_object->get_errors();

        $order_container_template->content = $order_confirmation_template;
        $order_container_template->order_step = 3;
        $this->request->response = $order_container_template->render();
    }

    /**
     * Metoda pro zpracovani 3 kroku objednavky - vlastni zpracovani a odeslani objednavky!
     */
    public function action_send_order()
    {
        $total_items = Service_Hana_ShoppingCart::get_cart()->total_items;
        if ($total_items == 0) Request::instance()->redirect(__($this->eshop_homepage_cz_seo));

        $next_step_seo = __("objednavka-odeslana");

        // pokud neni uzivatel prihlasen, jdu na "mezikrok"
        if (!$this->user_service->logged_in() && !$this->user_service->is_temporarily_stored()) {
            Request::instance()->redirect(url::base() . __("uzivatelske-udaje"));
        }
        $additional_data = array();


        if (false)//!Input::post("obchodni_podminky",false)) // obchodni podminky na poslednim kroku nebudou
        {
            $this->response_object->set_status(false);
            //die(print_r($_POST));
            $this->response_object->set_data($_POST);
            $this->response_object->set_errors(array("obchodni_podminky" => "Je nutno souhlasit s obchodními podmínkami."));
            $this->response_object->set_message("Je nutno souhlasit s obchodními podmínkami", Hana_Response::MSG_ERROR);
            $this->response_object->set_redirect(false);
        } else {

            $order = Service_Order::get_populated_order(); // ziskame objekt objednavky a nechame vypocist ceny na zaklade kosiku
            $shopper = $this->user_service->get_user();
            $shopper_branch = $this->user_service->get_shopper_branch(isset($_SESSION["branch_id"]) ? $_SESSION["branch_id"] : false, $this->user_service->get_user());

            //die(var_dump(Input::post("wantactions")));
            if (Input::post("wantactions") == "agree") {
                $additional_data["actions"] = 1;
                //die(var_dump($additional_data));
            }


            $additional_data["shopper_note"] = Input::post("poznamka");
            $additional_data["customer_order_code"] = Input::post("customer_order_code");
            $additional_data["gift_box_code"] = orm::factory("gift_box")->where("id", "=", Session::instance()->get("gift_box"))->find()->code;


            $saved_order = Service_Order::save_order($order, $shopper, $shopper_branch, $additional_data);
            if ($saved_order->id) {
                // objednavka byla uspesne ulozena

                // prvni nakup byl vyuzit
                $shopper->action_first_purchase = 0;
                $shopper->save();


                $payment = $saved_order->payment;
                if (Service_Payment::check_payment($payment)) {
                    Service_Payment::proceed_special_payment($payment, $saved_order, function (Model_Order $order) {
                        Session::instance()->set("saved_order_id", $order->id);
                    });
                }
                //
                $order_data = Service_Order::dump_order($saved_order);

                $payment = Service_Payment::get_selected_payment_orm();
                if ($payment->predem && empty($additional_data["customer_order_code"])) {
                    $attachment = Service_Hana_Order_Pdf::create_order_pdf($order_data);
                } else {
                    $attachment = false;
                }

                // odeslani e-mailu
                //die(print_r($order_data));
                Service_Order_Email::send_order($order_data, $attachment);


                //$order_complete_template = new View("emails/mail_order_container");

                //$order_complete_template=$this->_create_order_confirmation_template($order, $order_complete_template);

                //Service_Email::process_email($order_complete_template->render(), "order_email", $shopper->email, $shopper->name, array("order_code"=>$order->order_code));

                // die("--mail vygenerovan--"); // testovaci breakpoint
                // odstranim pripadne udaje zadane uzivatelem bez registrace
                $this->user_service->delete_temporarily_stored_user();

                $_SESSION["order_saved"] = "345SDFG";
                $_SESSION["order_id"] = $order->id;

                $this->response_object->set_status(true);
                $this->response_object->set_redirect($next_step_seo);
            } else {

            }
        }
    }


    /********* 4) Akce pro obsluhu zaverecneho oznameni o odeslani objednavky **********/

    /**
     * Zobrazeni zaverecneho okna "objednavka odeslana".
     */
    public function action_show_order_complete()
    {
        if (!isset($_SESSION["order_saved"]) || $_SESSION["order_saved"] != "345SDFG" || !isset($_SESSION["order_id"]) || !$_SESSION["order_id"]) throw new Kohana_Exception("Požadovaná stránka nebyla nalezena");

        $order_id = $_SESSION["order_id"];

        $order = orm::factory("order")->find($order_id); // TODO odkontrolovat zda objednavka patri k zakaznikovi

        $order_complete_template = new View("order/order_complete");
        //$order_complete_template=$this->_create_order_confirmation_template($order, $order_complete_template);

        // GoogleEcommerce a Heureka
        //die(var_dump($order));
        Googleanalytics::createTransaction($order->order_code, ($order->order_total_CZK - $order->order_shipping_price - $order->order_payment_price), "", ($order->order_lower_vat + $order->order_higher_vat), ($order->order_shipping_price + $order->order_payment_price), "Zlín", "", "Česká republika");
        //($orderId, $total, $affiliation="", $tax="", $shipping="", $city="", $state="", $country="")
//        
//        $heureka_code=orm::factory("owner")->get_heureka_code();
//
//        if($heureka_code)
//        {
//            try {
//              $overeno = new Heureka_Overeno($heureka_code);
//              $overeno->setEmail($order->order_shopper_email);  
//            } catch (Exception $e) {
//              // nepodarilo se inicializovat heureka overeno 
//              Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
//              $heureka_code=false;  
//            }
//
//            
//        }
//        
        foreach (orm::factory("order_item")->where("order_id", "=", $order->id)->find_all() as $item) {
//            if($heureka_code) $overeno->addProduct($item->nazev);
            Googleanalytics::addItem($item->product_id, $item->nazev, "", $item->price_with_tax, $item->units);
        }
//        
//        if($heureka_code){
//            try {
//                $overeno->send();
//            } catch (Exception $e) {
//                Kohana::$log->add(Kohana::ERROR, Kohana::exception_text($e));
//                // nepodarilo se navazat spojeni s heurekou
//                
//            }
//
//            
//        }

        // odstranim obsah kosiku
        Service_ShoppingCart::flush_cart();

        $order_complete_template->order_total = $order->order_total_CZK - $order->order_shipping_price - $order->order_payment_price;

        $_SESSION["order_id"] = 0;

        $this->request->response = $order_complete_template->render();
        /*
        // obecna sablona textove stranky
        $page=new View("message");
        $page->page=array("popis"=>__("Děkujeme za vaši objednávku, v nejbližší době bude zpracována."));
        $this->request->response=$page->render();
         */
    }


    /** submetody kontroleru modulu **/

    /**
     * Submetoda pro generovani a prezentaci cenovych souctu. (Mimo kosiku, ten si vlastni cenu pocita a zobrazi sam)
     */
    public function action_order_price_summary_table($summary_total = false)
    {
        $order = Service_Order::get_populated_order();
        if ($order->get_cart()->total_cart_price_with_tax > 0) {
            $price_values = array();
            $order_price_summary_table = new View("order/order_price_summary_table");
            $order_price_summary_table->summary_total = $summary_total;
            // prirazeni ceny kosiku
            $price_values["shipping_price"] = array("name" => "Dopravné a balné", "value" => $order->order_shipping_price);
            $price_values["payment_price"] = array("name" => "Příplatek (sleva) za platební metodu", "value" => $order->order_payment_price, "type" => $order->order_payment_nazev);

            $shopper = Shauth::instance()->get_user();
            if ($shopper && $shopper->action_first_purchase == 1) {
                $owner_data = orm::factory("product_setting", 1);
                $price_values["order_discount"] = array("name" => "Sleva za první nákup", "value" => -1 * $owner_data->first_purchase_discount);

            }
            $price_values["total"] = array("name" => "Cena celkem", "value" => $order->order_total_CZK, "class" => "bold total");
            $price_values["total_wo_dph"] = array("name" => "Cena zboží celkem bez DPH", "value" => $order->order_total_without_vat);
            $order_price_summary_table->price_values = $price_values;
            $this->request->response = $order_price_summary_table->render();
        }
    }

    /*
     * Submetoda kontroleru pro zobrazeni nabidky dopravy.
     */
    public function action_shipping()
    {
        $order_shipping_template = new View("order/order_shipping");
        $order = Service_Order::get_populated_order();
        $order_shipping_price = $order->order_shipping_price;
        $order_shipping_template->shipping_types = Service_Shipping::get_shipping_types(false, $order->order_total_with_vat);
        $order_shipping_template->current_shipping_price = $order_shipping_price;
        $this->request->response = $order_shipping_template->render();
    }

    /**
     * Submetoda kontroleru pro zobrazeni nabidky platby.
     */
    public function action_payment()
    {
        $order_payment_template = new View("order/order_payment");
        $order_total = Service_Order::get_populated_order()->order_total_with_vat;
        $order_payment_template->payment_types = Service_Payment::get_shipping_payment_types();
        $order_payment_template->current_payment_price = Service_Order::get_populated_order()->order_payment_price;
        $order_payment_template->order_total = $order_total;
        //$order_payment_template->essoxCalculatorUrl = Service_Essox::getEssoxCalculatorUrl($order_total);
        $this->request->response = $order_payment_template->render();
    }


    public function action_show_unregistered_user_form($on_success_redirect_to = false)
    {
        $order_unregistered_user_template = new View("order/order_unregistered_user");

        if ($this->response_object->get_status() === false) {
            // doslo k odeslani formulare s negativnim vysledkem - vyplnime posledni zadana data
            $data = $this->response_object->get_data();
        } elseif ($this->user_service->is_temporarily_stored()) {
            // docasna registrace uz byla provedena, vyplnime data
            $data["unregistered-user"] = $this->user_service->get_user();
            $data["unregistered-user-branch"] = $this->user_service->get_shopper_branch();
        } else {
            $data["unregistered-user"] = $data["unregistered-user-branch"] = array();
        }

        $order_unregistered_user_template->unregistered_user_data = !empty($data["unregistered-user"]) ? $data["unregistered-user"] : array();
        $order_unregistered_user_template->unregistered_user_data_branch = !empty($data["unregistered-user"]) ? $data["unregistered-user-branch"] : array();

        $errors = $this->response_object->get_errors();
        if (isset($errors["unregistered_user"])) $errors = $errors["unregistered_user"]; else $errors = array();

        if (!isset($errors["branch_errors"])) $errors["branch_errors"] = array();
        $order_unregistered_user_template->errors = $errors;
        $order_unregistered_user_template->redirect = $on_success_redirect_to;
        $this->request->response = $order_unregistered_user_template->render();
    }

    public function action_add_unregistered_user($on_success_redirect_to = false)
    {
        $result = $this->user_service->temporarily_store_user($_POST);

        $this->response_object->set_data($_POST);
        $this->response_object->set_status($result);
        if ($result) {
            $this->user_service->need_insert_user_data(false);
            $redirect = true;
            if ($on_success_redirect_to) $redirect = (url::base() . $on_success_redirect_to);
            $this->response_object->set_message(__("Údaje o uživateli jsou kompletní, pokračujte prosím v objednávce"));
            $this->response_object->set_redirect($redirect);
        } else {
            $this->response_object->set_redirect(false);
            //$this->response_object->set_data($_POST["user-registration"]);
            $this->response_object->set_errors(array("unregistered_user" => $this->user_service->errors));
            $this->response_object->set_message(__("Zadané údaje nejsou kompletní, zkontrolujte je prosím"));
        }
    }


    private function _create_order_confirmation_template($order, $order_complete_template)
    {

        $order_complete_template->owner_data = $this->order_service->get_owner();

        $order_complete_template->order_code = $order->order_code;

        $customer["nazev"] = $order->order_shopper_name;
        $customer["ulice"] = $order->order_shopper_street;
        $customer["mesto"] = $order->order_shopper_city;
        $customer["psc"] = $order->order_shopper_zip;
        $customer["email"] = $order->order_shopper_email;
        $customer["telefon"] = $order->order_shopper_phone;
        $customer["poznamka"] = $order->order_shopper_note;

        // cenova skupina prihlaseneho uzivatele
        $user = $this->user_service->get_user();
        if (is_object($user) && $user->id) {
            $price_category_id = (int)$user->price_category_id;
            $user_price_category = @DB::select("kod", "popis")->from("price_categories")->where("price_categories.id", "=", $price_category_id)->as_object()->execute()->current();
            $customer["kod_cenove_skupiny"] = $user_price_category->kod;
            $customer["popis_cenove_skupiny"] = $user_price_category->popis;
        } else {
            $conf = Kohana::config('hana');
            $customer["kod_cenove_skupiny"] = $conf["default_price_code_D0"];
            $customer["popis_cenove_skupiny"] = "";
        }

        $order_complete_template->billing_data = $customer;

        $customer_branch["nazev"] = $order->order_branch_name;
        $customer_branch["ulice"] = $order->order_branch_street;
        $customer_branch["mesto"] = $order->order_branch_city;
        $customer_branch["psc"] = $order->order_branch_zip;
        $customer_branch["email"] = $order->order_branch_email;
        $customer_branch["telefon"] = $order->order_branch_phone;

        $order_complete_template->branch_data = $customer_branch;

        $order_complete_template->doprava = $order->shipping->nazev;
        $order_complete_template->platba = $order->payment->nazev;
        $order_complete_template->products = $this->cart_service->get_cart_content(true, $order->id, true);

        $order_complete_template->shopping_cart = Request::factory("internal/cart/detail_table/read_only/" . $order->id . "/true")->execute()->response;
        //$order_price_summary_table->summary_total=true;

        if ($this->cart_service->get_voucher_discount_with_tax()) ;
        {
            // sleva na kupon
            $price_values["voucher_discount"] = array("name" => "Sleva na kupón", "value" => $this->cart_service->get_voucher_discount_with_tax());
        }

        // prirazeni ceny kosiku
        $price_values["payment_price"] = array("name" => "Příplatek (sleva) za platební metodu", "value" => $this->order_service->get_payment_price_with_tax(), "type" => $this->order_service->get_payment_type());
        $price_values["shipping_price"] = array("name" => "Dopravné a balné", "value" => $this->order_service->get_shipping_price_with_tax());

        $price_values["total"] = array("name" => "Cena celkem", "value" => $this->order_service->get_total_order_price_with_tax(), "class" => "strong");
        $price_values["total_wo_dph"] = array("name" => "Cena zboží celkem bez DPH", "value" => $this->cart_service->get_cart_price_without_tax(), "class" => "strong");


        $order_complete_template->price_values = $price_values;


        $order_complete_template->order_price_summary_table = Request::factory("internal/order/order_price_summary_table/true")->execute()->response;

        return $order_complete_template;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    // GPWebPay //

    public function action_gpwebpay_response()
    {
        $saved_order_id = Session::instance()->get("saved_order_id");
        $order = orm::factory("order", $saved_order_id);
        $dumped_order = Service_Order::dump_order($order);
        $gp_order = new Gpwebpay_Create_Order($order->order_code, $order->order_total);
        $result = Service_Gpwebpay_Impl::checkCreateOrderResponse($gp_order);
        if (!$result)
            $result = ($gp_order->getPrCode() == 0) ? true : false;

        $order->order_state_id = 8;
        if ($result) {
            Service_Order_Email::send_order($dumped_order, false);
            $order->order_state_id = 9;
        }
        $order->save();

        Service_Order::update_order_for_gpwebpay($gp_order, $order);

        Service_ShoppingCart::flush_cart();
        $this->user_service->delete_temporarily_stored_user();

        $template = new View("order/order_complete_gp");
        $template->order = Service_Order::dump_order($order);
        $template->gp_order = $gp_order;
        $template->result = $result;
        $template->order_total = $order->order_total_CZK - $order->order_shipping_price - $order->order_payment_price;
        $this->request->response = $template->render();
    }

    // PayU //

    public function action_show_payu_form()
    {
        $saved_order_id = Session::instance()->get("saved_order_id");
        if (!$saved_order_id) die("chyba - platbu objednávky nelze dokončit");

        //Session::instance()->delete("saved_order_id");

        $model_order = orm::factory("order")->where("id", "=", $saved_order_id)->find();
        //die(print_r($model_order));

        $payU = new PayU();
        // pozor castka v halerich
        $payInfo = new PayUInfo(($model_order->order_total_CZK * 100), "Platba z eshopu noventis.cz", $saved_order_id, $model_order->order_shopper_name, $model_order->order_shopper_name, $model_order->order_shopper_email, "123.123.123.123");
        // vygeneruje unikatni session  id, ktere je potreba ulozit do objednavky
        $session_id = $payInfo->getSessionId();
        $model_order->payu_session_code = $session_id;
        $model_order->is_payu = 1;
        $model_order->save();

        $head_template = $payU->createHead();
        $form_template = $payU->createForm($payInfo);

        $head_text = $head_template->render();
        $form_text = $form_template->render();

        $this->request->response = $head_text . "<br /><br />" . $form_text;

    }

    public function action_show_payu_ok()
    {
        $pos_id = (isset($_GET["pos_id"])) ? $_GET["pos_id"] : false;
        $pay_type = (isset($_GET["pay_type"])) ? $_GET["pay_type"] : false;
        $session_id = (isset($_GET["session_id"])) ? $_GET["session_id"] : false;
        $order_id = (isset($_GET["order_id"])) ? $_GET["order_id"] : false;

        if ($order_id) {
            $model_order = orm::factory("order", $order_id)->where("payu_session_code", "=", $session_id)->find();
            //die(print_r($model_order));

            $payU = new PayU();

            try {
                $status = $payU->checkStatus($_GET);
                $model_order->payu_status_code = $status->getStatusCode();
                $model_order->payu_status_message = $status->getStatusMessage();
                $model_order->payu_error_message = $status->getErrorMessage();
                $model_order->save();

                $order_data = Service_Order::dump_order($model_order);

                Service_Order_Email::send_order($order_data, null);
                $_SESSION["order_saved"] = "345SDFG";
                $_SESSION["order_id"] = $model_order->id;
                // redirect na order-complete
                Request::instance()->redirect(url::base() . __("objednavka-odeslana"));

                //echo "status code: ".$status->getStatusCode();
                //echo "status: ".$status->getStatusMessage();
                //echo "chyba: ".$status->getErrorMessage();
            } catch (Exception $ex) {
                echo "chyba:" . $ex->getMessage();
            }
        }

    }

    public function action_show_payu_error()
    {
        $pos_id = (isset($_GET["pos_id"])) ? $_GET["pos_id"] : false;
        $pay_type = (isset($_GET["pay_type"])) ? $_GET["pay_type"] : false;
        $session_id = (isset($_GET["session_id"])) ? $_GET["session_id"] : false;
        $order_id = (isset($_GET["order_id"])) ? $_GET["order_id"] : false;
        $error = (isset($_GET["error"])) ? $_GET["error"] : false;

        if ($order_id) {
            $model_order = orm::factory("order")->where("id", "=", $order_id)->where("payu_session_code", "=", $session_id)->find();
            //die(print_r($model_order));

            $payU = new PayU();

            try {
                $status = $payU->checkStatus($_GET);
                $model_order->payu_status_code = $status->getStatusCode();
                $model_order->payu_status_message = $status->getStatusMessage();
                $model_order->payu_error_message = $status->getErrorMessage();
                $model_order->save();

                $payu_error_template = new View("order/order_payu_error");
                $payu_error_template->payu_status_code = $status->getStatusCode();
                $payu_error_template->payu_status_message = $status->getStatusMessage();
                $payu_error_template->payu_error_message = $status->getErrorMessage();
                $this->request->response = $payu_error_template->render();

                //echo "status code: ".$status->getStatusCode();
                //echo "status: ".$status->getStatusMessage();
                //echo "chyba: ".$status->getErrorMessage();
            } catch (Exception $ex) {
                echo "chyba";
            }
        }

    }

    public function action_show_payu_change_status()
    {
        // nesmi se vypsat nic jinyho nez OK pro ukonceni posilani oznameni o urcite zmene na platbe
        $pos_id = (isset($_POST["pos_id"])) ? $_POST["pos_id"] : false;
        $session_id = (isset($_POST["session_id"])) ? $_POST["session_id"] : false;
        $ts = (isset($_POST["ts"])) ? $_POST["ts"] : false;
        $sig = (isset($_POST["sig"])) ? $_POST["sig"] : false;

        $payU = new PayU();

        try {
            $status = $payU->checkStatus($_POST, true);
            if ($status->getStatusCode() === PayUStatus::STATUS_PAYMENT_RECEIVED) {
                // uloz uspesnou platbu
            } else {
                // ruzne typy stavu
                // platba v procesu nebo chyba pri platbe
            }
            $model_order = orm::factory("order")->where("id", "=", $order_id)->where("payu_session_code", "=", $session_id)->find();
            $model_order->payu_status_code = $status->getStatusCode();
            $model_order->payu_status_message = $status->getStatusMessage();
            $model_order->payu_error_message = $status->getErrorMessage();
            $model_order->save();

            //echo "status code: ".$status->getStatusCode();
            //echo "status: ".$status->getStatusMessage();
            //echo "chyba: ".$status->getErrorMessage();
        } catch (Exception $ex) {
            //echo "chyba";
        }

        die("OK");
    }


}

?>
