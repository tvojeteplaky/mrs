<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Slider_Slider_Edit extends Controller_Hana_Edit
{

    protected $back_link = false;

    public function before()
    {
        $this->action_buttons = array("ulozit" => array("name" => "ulozit", "value" => "odeslat a zůstat zde"));
        $this->orm = new Model_Slider();
        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden" => true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();
    }
}