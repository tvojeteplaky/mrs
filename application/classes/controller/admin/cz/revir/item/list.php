<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Revir_Item_List extends Controller_Hana_List
{

	protected $with_route=false;

    protected $default_order_by = "poradi";
    protected $default_order_direction= "asc";

    public function before() {
        $this->orm=new Model_Revir();

        parent::before();
    }
    protected function _column_definitions()
    {
        $this->auto_list_table->column("id")->label("# ID")->width(50)->set();
        
        $this->auto_list_table->column("nazev")->type("link")->label("Název")->item_settings(array("hrefid" => $this->base_path_to_edit, "orm_tree_level_indicator" => true))->css_class("txtLeft")->filterable()->sequenceable()->width(250)->set();
        $this->auto_list_table->column("popis")->label("Popis")->css_class("txtLeft")->item_settings(array("maxlenght" => 100))->filterable()->set();
        //$this->auto_list_table->column("slideshow")->type("relatedDetail")->label("Prezentace")->data_src(array("db_query"=>db::select(array(db::expr("count(id)"),"count"))->from(strtolower($this->orm->class_name)."_slideshows"),"db_query_where_colid"=>"page_id"))->item_settings(array("hrefid"=>$this->base_path_to_slideshow,"alt"=>"editovat prezentaci","alt_empty"=>"vložit nové fotky do prezentace","image"=>"images.png","image_empty"=>"images_empty.png"))->width(40)->exportable(false)->set();
        if (Kohana::config("languages")->get("enabled"))
            $this->auto_list_table->column("available_languages")->type("languages")->item_settings(array("hrefid" => $this->base_path_to_edit))->width(60)->set();
        $this->auto_list_table->column("poradi")->type("changeOrderShifts")->label("")->sequenceable()->width(32)->exportable(false)->printable(false)->set();
        $this->auto_list_table->column("zobrazit")->type("switch")->item_settings(array("action" => "change_visibility", "states" => array(0 => array("image" => "lightbulb_off.png", "label" => "neaktivní"), 1 => array("image" => "lightbulb.png", "label" => "aktivní"))))->sequenceable()->filterable(array("col_name" => "routes.zobrazit"))->label("")->width(32)->set();
        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    }

}
