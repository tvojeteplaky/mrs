<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Newsletter_Item_List extends Controller_Hana_List
{
    protected $with_route = false;

    protected $default_order_by = "date";
    protected $default_order_direction = "desc";
    protected $default_buttons = array(
        "update" => array("type" => "submit", "nazev" => "Odeslat zaškrtnuté", "action" => "send", 'onclick' => 'Opravdu chcete odeslat?')
    );

    public function before()
    {
        $this->orm = new Model_Newsletter();

        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_list_table->column("id")->label("# ID")->width(30)->set();
        $this->auto_list_table->column("nazev")->type("link")->label("Název")->item_settings(array("hrefid" => $this->base_path_to_edit))->css_class("txtLeft")->filterable()->sequenceable()->width(300)->set();
        //$this->auto_list_table->column("date")->type("text")->label("Datum")->item_settings(array("special_format"=>"cz_date"))->css_class("txtLeft")->sequenceable()->filterable(array("type"=>"daterangepicker"))->width(150)->set();
        $this->auto_list_table->column("newsletter_type_id")->label("Pohlaví")->data_src(array("related_table_1" => "newsletter_type", "column_name" => "nazev"))->css_class("txtLeft")->filterable()->sequenceable()->set();
        $this->auto_list_table->column("popis")->label("Text")->css_class("txtLeft")->item_settings(array("maxlenght" => 100))->filterable()->set();
//        if(Kohana::config("languages")->get("enabled"))
//        $this->auto_list_table->column("available_languages")->type("languages")->item_settings(array("hrefid"=>$this->base_path_to_edit))->width(58)->set();
        $this->auto_list_table->column("sent")->type("indicator")->item_settings(array("states" => array(0 => array("label" => "<span class='glyphicon glyphicon-envelope inactive'></span>"), 1 => array("label" => "<span class='glyphicon glyphicon-envelope active'></span>"))))->sequenceable()->filterable(array("col_name" => "sent"))->label("")->width(32)->set();
        $this->auto_list_table->column("zobrazit")->type("switch")->item_settings(array("action" => "change_visibility", "states" => array(0 => array("image" => "lightbulb_off.png", "label" => "neaktivní"), 1 => array("image" => "lightbulb.png", "label" => "aktivní"))))->sequenceable()->filterable(array("col_name" => "routes.zobrazit"))->label("")->width(32)->set();

        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    }

    protected function _orm_setup()
    {
        parent::_orm_setup();

        $this->orm
            ->join("newsletter_types")->on("newsletter_types.id", "=", "newsletters.newsletter_type_id");
    }

    protected function _form_action_send($data)
    {
        if ($data['delete'])
            foreach ($data['delete'] as $newsletter => $is) {
                if (!$is)
                    continue;
                $newsletter = ORM::factory('newsletter')
                    ->where('newsletter_id', '=', $newsletter)
                    ->language(0)
                    ->find();
                Service_Newsletter::send_newsletter($newsletter);
            }
    }

}

?>
