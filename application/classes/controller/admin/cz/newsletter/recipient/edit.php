<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Newsletter_Recipient_Edit extends Controller_Hana_Edit
{
    protected $with_route=true;
    protected $item_name_property=array("email"=>"s názvem");
	protected static $secure_code = "sdfsdfe";
    

    public function before() {
        $this->orm=new Model_Newsletter_Recipient();
        
        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        $this->auto_edit_table->row("email")->type("edit")->label("Email")->condition("Položka musí mít minimálně 3 znaky.")->set();
		$this->auto_edit_table->row("newsletter_type_id")->type("selectbox")->data_src(array("related_table_1" => "newsletter_type", "column_name" => "nazev"))->label("Pohlaví")->set();
        $this->auto_edit_table->row("allowed")->type("checkbox")->default_value(1)->label("Povolen")->set();

        
    }

    protected function _form_action_main_prevalidate($data)
    {
        parent::_form_action_main_prevalidate($data);

		$query = DB::query(Database::SELECT, 'SELECT COUNT(id) AS recipients_count FROM newsletter_recipients WHERE email = :email');
		$query->param(':email', $data['email']);
		$recipients_count = $query->execute()->get('recipients_count');

		if($recipients_count != '0') {
			$query = DB::query(Database::UPDATE, 'UPDATE newsletter_recipients SET newsletter_type_id = :newsletter_type_id WHERE email = :email');
			$query->param(':newsletter_type_id', $data['newsletter_type_id']);
			$query->param(':email', $data['email']);
			$query->execute();
		}

        return $data;
    }

	protected function _form_action_main_postvalidate($data)
    {
        parent::_form_action_main_postvalidate($data);

		$hash = md5(static::$secure_code . $data['email']);

		$query = DB::query(Database::UPDATE, 'UPDATE newsletter_recipients SET hash = :hash WHERE email = :email');
		$query->param(':hash', $hash);
		$query->param(':email', $data['email']);
		$query->execute();
    }
    
}
