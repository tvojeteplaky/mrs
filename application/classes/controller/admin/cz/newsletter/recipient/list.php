<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Newsletter_Recipient_List extends Controller_Hana_List
{
    protected $with_route=false;
protected $table_template = "admin/recipient_list";
    protected $default_order_by = "email";
    protected $default_order_direction= "asc";
    protected $default_buttons = array(
        "import" => array(
            "nazev" => "Import příjemců",
            "type" => "submit",
            "action" => "recipient_import",
            "onclick" => null
        ),
    );

    public function before() {
        $this->orm=new Model_Newsletter_Recipient();

        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_list_table->column("id")->label("# ID")->width(30)->set();
        $this->auto_list_table->column("email")->type("link")->label("Název")->item_settings(array("hrefid"=>$this->base_path_to_edit))->css_class("txtLeft")->filterable()->sequenceable()->width(300)->set();
		$this->auto_list_table->column("newsletter_type_id")->label("Pohlaví")->data_src(array("related_table_1" => "newsletter_type", "column_name" => "nazev"))->filterable()->sequenceable()->set();

        $this->auto_list_table->column("allowed")->type("switch")->item_settings(array("action"=>"change_visibility","states"=>array(0=>array("image"=>"lightbulb_off.png","label"=>"neaktivní"),1=>array("image"=>"lightbulb.png","label"=>"aktivní"))))->sequenceable()->filterable()->label("")->width(32)->set();

        
        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    }

	protected function _orm_setup()
    {
        parent::_orm_setup();

        $this->orm
            ->join("newsletter_types")->on("newsletter_types.id", "=", "newsletter_recipients.newsletter_type_id");
    }
    
   
    public function _form_action_recipient_import()
    {
        if(isset($_FILES["recipient-file"]) && $_FILES["recipient-file"]["name"] && isset($_POST["type_id"]) && is_numeric($_POST["type_id"]))
        {
            $this->table->imported = Service_Newsletter::add_recipients_from_file($_FILES["recipient-file"], (int) $_POST["type_id"]);
        }
    }

    public function _form_action_change_visibility($data)
    {
        $this->orm->find($data["id"]);
        $this->module_service->change_visibility($data["id"], $data["state_value"], false, "allowed");
        $this->data_saved = true;
    }

}
?>
