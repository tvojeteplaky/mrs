<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Article_Category_Edit extends Controller_Hana_Edit
{
    protected $with_route=true;
    protected $item_name_property=array("nazev"=>"s názvem");

    protected $tabs = array('tab-seo' => 'Seo');

    protected $max_tree_level = 2;

    public function before() {
        $this->orm=new Model_Article_Category();

        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->condition("Položka musí mít minimálně 3 znaky.")->set();
        $this->auto_edit_table->row("nadpis")->type("edit")->label("Nadpis")->set();

        if ($this->max_tree_level > 1) {
            $this->auto_edit_table->row("parent_id")->type("selectbox")->label("Nadřazená kategorie")->item_settings(array("max_tree_level" => $this->max_tree_level))->data_src(array("column_name" => "nazev", "orm_tree" => true, "null_row" => "---", "language" => true))->set();
        }
        
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->default_value(1)->label("Zobrazit")->set();
        $this->auto_edit_table->row("popis")->type("editor")->label("Text")->set();

        $this->auto_edit_table->type('tabbody')->item_settings(array('id' => 'tab-seo'))->set();
        $this->auto_edit_table->row("nazev_seo")->type("edit")->data_src(array("related_table_1"=>"route"))->label("Název SEO")->condition("(Pokud nebude položka vyplněna, vygeneruje se automaticky z názvu.)")->set();
        $this->auto_edit_table->row("title")->type("edit")->label("Titulek")->condition("(Pokud nebude položka vyplněna, použije se hodnota z názvu.)")->set();
        $this->auto_edit_table->row("description")->type("edit")->label("Popis")->set();
        $this->auto_edit_table->row("keywords")->type("edit")->label("Klíčová slova")->set();
    }

    protected function _form_action_main_prevalidate($data) {
        parent::_form_action_main_prevalidate($data);
        // specificka priprava dat, validace nedatabazovych zdroju (pripony obrazku apod.)
        if(!$data["title"] && $data["nazev"]){$data["title"]=$data["nazev"];}
        if(!$data["nadpis"] && $data["nazev"]){$data["nadpis"]=$data["nazev"];}

        if(!$data["nazev_seo"] && $data["nazev"]){
            $data["nazev_seo"]=seo::uprav_fyzicky_nazev($data["nazev"]); $data["nazev_seo"]=$data["nazev_seo"];
        }elseif($data["nazev_seo"]){
            $data["nazev_seo"]=seo::uprav_fyzicky_nazev($data["nazev_seo"]);
        }

        // defaultni akce v routes
        $data["module_action"] = "category";
        $data["module_id"] = db::select("id")->from("modules")->where("kod","=","article")->execute()->get("id");

        return $data;
    }
}