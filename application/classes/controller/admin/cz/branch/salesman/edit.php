<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Branch_Salesman_Edit extends Controller_Hana_Edit
{

    public function before()
    {
        $this->orm = new Model_Salesman();
        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden" => true))->label("# ID")->set();
        $this->auto_edit_table->row("name")->type("edit")->label("Jméno")->set();
        $this->auto_edit_table->row("role")->type("edit")->label("Role")->set();
        $this->auto_edit_table->row("phone")->type("edit")->label("Telefon")->set();
        $this->auto_edit_table->row("email")->type("edit")->label("Email")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();
        // $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        // $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"png","delete_link"=>true))->label("Náhled obrázku")->set();
    }

    // protected function _form_action_main_postvalidate($data)
    // {
    //     parent::_form_action_main_postvalidate($data);

    //     if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
    //     {
    //         $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
    //         $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data["name"]), true, 'png');
    //     }
    // }

}