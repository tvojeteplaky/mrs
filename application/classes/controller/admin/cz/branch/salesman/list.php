<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Branch_Salesman_List extends Controller_Hana_List
{
    public function before()
    {
        $this->orm = new Model_Salesman();
        parent::before();
    }


    protected function _column_definitions()
    {
        $this->auto_list_table->column("id")->label("# ID")->width(50)->set();

        $this->auto_list_table->column("name")->type("link")->label("Jméno")->item_settings(array("hrefid" => $this->base_path_to_edit))->css_class("txtLeft")->filterable()->sequenceable()->width(250)->set();
     //   $this->auto_list_table->column("branch_name")->label("Pobočky")->data_src(array("related_table_1" => "branches", "column_name" => "nazev", 'language' => true, "orm_tree" => false, "multiple" => true, 'condition' => array('language_id', '=', 1)))->width(250)->sequenceable("branch_data.nazev")->filterable(array("col_name" => "branches.id"))->set();
        $this->auto_list_table->column("phone")->type("text")->label("Telefon")->css_class("txtLeft")->filterable()->sequenceable()->set();
        $this->auto_list_table->column("email")->type("text")->label("Email")->css_class("txtLeft")->filterable()->sequenceable()->set();
        if (Kohana::config("languages")->get("enabled"))
            $this->auto_list_table->column("available_languages")->type("languages")->item_settings(array("hrefid" => $this->base_path_to_edit))->width(60)->set();
        $this->auto_list_table->column("poradi")->type("changeOrderShifts")->label("")->sequenceable()->width(32)->exportable(false)->printable(false)->set();
        $this->auto_list_table->column("zobrazit")->type("switch")->item_settings(array("action" => "change_visibility", "states" => array(0 => array("image" => "lightbulb_off.png", "label" => "neaktivní"), 1 => array("image" => "lightbulb.png", "label" => "aktivní"))))->sequenceable()->filterable()->label("")->width(32)->set();
        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    }

    /*
    protected function _orm_setup()
    {
        $this->orm->join("salesmans_branches")->on("salesmans_branches.salesman_id", "=", "salesmans.id");
        $this->orm->join("branches")->on("branches.id", "=", "salesmans_branches.branch_id");
    }
    */
}