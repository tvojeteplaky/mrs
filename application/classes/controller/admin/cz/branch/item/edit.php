<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Branch_Item_Edit extends Controller_Hana_Edit
{

    protected $tabs = array('tab-info' => 'Údaje'/*, 'tab-map' => "Nastavení mapy"*/);

    public function before()
    {
        $this->orm = new Model_Branch();
        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden" => true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->set();
        $this->auto_edit_table->row("nadpis")->type("edit")->label("Nadpis")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();
        $this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
        $this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"png","delete_link"=>true))->label("Náhled obrázku")->set();
        $this->auto_edit_table->row("salesman_id")->type("selectbox")->label("Zástupci")->data_src(array("related_table_1" => "salesman", "column_name" => "name", "order_by" => array('poradi', 'asc'), "orm_tree" => false, 'multiple' => true))->set();

        $this->auto_edit_table->row("tab1")->type('tabbody')->item_settings(array('id' => 'tab-info'))->set();
        $this->auto_edit_table->row("phone")->type("edit")->label("Telefon")->set();
        $this->auto_edit_table->row("email")->type("edit")->label("Email")->set();
        $this->auto_edit_table->row("fax")->type("edit")->label("Fax")->set();
        $this->auto_edit_table->row("opening_hours")->type("textarea")->label("Otevírací doba")->set();
        $this->auto_edit_table->row("address")->type("textarea")->label("Adresa")->set();
        $this->auto_edit_table->row("lat")->type("edit")->label("Latitude (zeměpisná šířka)")->set();
        $this->auto_edit_table->row("lng")->type("edit")->label("Longitude (zeměpisná délka)")->set();


        $this->auto_edit_table->row("ic")->type("edit")->label("IČ")->set();
        $this->auto_edit_table->row("dic")->type("edit")->label("DIČ")->set();
        $this->auto_edit_table->row("icp")->type("edit")->label("IČP")->set();
        $this->auto_edit_table->row("comment")->type("edit")->label("Komentář")->set();

    }

    protected function _form_action_main_prevalidate($data)
    {
        $data = parent::_form_action_main_prevalidate($data);

        $data['lng'] = str_replace(',', '.', $data['lng']);
        $data['lat'] = str_replace(',', '.', $data['lat']);

        return $data;
    }

    protected function _form_action_main_postvalidate($data)
    {
        parent::_form_action_main_postvalidate($data);

        $this->module_service->bind_categories($data['salesman_id'], 'salesman', 'salesman', false);

        if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
        {
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
            $this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data["nazev"]), true);
        }
    }

    protected function _form_action_main_image_delete($data)
    {
        $this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'photo_src', 'ext', false, 'photo', $this);
    }

}