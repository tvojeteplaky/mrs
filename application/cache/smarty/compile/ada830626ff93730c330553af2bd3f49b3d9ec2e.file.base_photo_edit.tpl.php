<?php /* Smarty version Smarty-3.1.11, created on 2016-05-24 10:14:51
         compiled from "/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/base_photo_edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3228621457440d7b2807e2-05946970%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ada830626ff93730c330553af2bd3f49b3d9ec2e' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/base_photo_edit.tpl',
      1 => 1463480209,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3228621457440d7b2807e2-05946970',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'entity_name' => 0,
    'photos' => 0,
    'item' => 0,
    'admin_path' => 0,
    'media_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_57440d7b40abc5_72325708',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57440d7b40abc5_72325708')) {function content_57440d7b40abc5_72325708($_smarty_tpl) {?>


<script type="text/javascript">
          $(function(){
            var quicksort_item;
            $('#PhotoPreviewBox .item').mousedown(function(){quicksort_item=$(this);});
            $('#PhotoPreviewBox').sortable({
                opacity: 0.8,
                cursor: 'move'
                });

             $('#PhotoPreviewBox').bind('sortupdate', function(event, ui) {
             quicksort_item.addClass('sortUpdate');
             $('#PhotoPreviewBox').sortable('disable');
             xdata=($('#PhotoPreviewBox').sortable('serialize'));

             $.ajax({
                         type: "GET",
                         url: "?photoedit_action_reorder_drag=<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
&item[]="+quicksort_item.attr('id')+"&"+xdata,
                         success: function(msg){
                         //alert("Obdržená data: " + msg);
                         $('#ContentSection').html(msg);
                         },
                         error: function(XMLHttpRequest, textStatus, errorThrown){
                         //$("#LogoAjax").attr("src","".$this->resource_path."/admin/img/server_delete.png");
                         //alert("Chyba zpracování požadavku. XMLHttpRequest: " + XMLHttpRequest + ", textStatus: " + textStatus+ ", errorThrown: " + errorThrown);
                         }
                     });

            });
            
            
            
          });
</script>


<div id="PhotoPreviewBox">
<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['photos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
  <div class="item" id="item_<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
">
    <a name="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" class="scrolItem"></a>
    <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['src_ad'];?>
" class="fancybox" rel="gallery" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
">
      <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['src_at'];?>
" alt="náhled vloženého obrázku" title="<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
" />
    </a>
    <div class="footer">
      <div class="title"><?php if (!$_smarty_tpl->tpl_vars['item']->value['nazev']&&!$_smarty_tpl->tpl_vars['item']->value['language_id']){?>-- nenaplněno pro jaz. verzi --<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
<?php }?></div>
      <div class="tools">
        
        <?php if ($_smarty_tpl->tpl_vars['item']->value['move_up']){?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['admin_path']->value;?>
?photoedit_action_reorder=<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
&amp;photo_id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
&amp;reorder_direction=up" class="ajaxelement img92">
          <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/left.gif" class="left" alt="vyměnit s předchozím" title="vyměnit s předchozím" />
        </a>
        <?php }else{ ?>
          <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/none.gif" class="left" alt="první položka" />
        <?php }?>
        
        <a class="ajaxelement" href="<?php echo $_smarty_tpl->tpl_vars['admin_path']->value;?>
?photoedit_action_visibility=<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
&amp;photo_id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
&amp;state_value=<?php if ($_smarty_tpl->tpl_vars['item']->value['zobrazit']){?>0<?php }else{ ?>1<?php }?>">
          <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/<?php if ($_smarty_tpl->tpl_vars['item']->value['zobrazit']){?>lightbulb<?php }else{ ?>lightbulb_off<?php }?>.png" alt="zobrazit / skrýt" title="zobrazit / skrýt" />
        </a>
        
        <a class="ajaxelement" href="<?php echo $_smarty_tpl->tpl_vars['admin_path']->value;?>
?main_gallery_editphoto=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
">
          <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/image_edit.png" alt="editovat" title="editovat" />
        </a>
        
        <a href="<?php echo $_smarty_tpl->tpl_vars['admin_path']->value;?>
?photoedit_action_delete=<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
&amp;photo_id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" class="ajaxelement confirmDelete">
          <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/delete.png" alt="smazat" title="smazat" />
        </a>
        
        <input type="checkbox" name="selitem[<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
]" />
        
        
        <?php if ($_smarty_tpl->tpl_vars['item']->value['move_down']){?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['admin_path']->value;?>
?photoedit_action_reorder=<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
&amp;photo_id=<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
&amp;reorder_direction=down" class="ajaxelement img92">
          <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/right.gif" class="right" alt="vyměnit s následujícím" title="vyměnit s následujícím" />
        </a>
        <?php }else{ ?>
          <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/none.gif" class="right" alt="poslední položka" />
        <?php }?>
        
      </div>
    </div>
  </div>
<?php } ?>  <div class="clear"></div> 
</div>

  <?php if (!empty($_smarty_tpl->tpl_vars['photos']->value)){?>
<div class="row">    
    <div class="col-md-12 text-right" id="photo-delete">
        <div class="panel panel-default">
            <div class="panel-body">
                <input type="hidden" name="delitem_gallery" value="<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
">        
                vybrat vše <input type="checkbox" name="sellAll" class="sellAll" id="sellAll" />
                <button type="submit" class="btn btn-warning" name="delitem" value="smazat označené" onclick="javascript: return confirm('Opravdu smazat vybrané položky?'); ">smazat označené</button>       
            </div>
        </div>
    </div>
</div>
  <?php }?>
  <?php }} ?>