<?php /* Smarty version Smarty-3.1.11, created on 2016-05-24 18:03:52
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/page/homepage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:646134628573b861eaa72d8-04706852%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fba9ea4aeed4ef4b27dc14d031450d7f01bac3b5' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/page/homepage.tpl',
      1 => 1464105818,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '646134628573b861eaa72d8-04706852',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573b861ec3b642_23657905',
  'variables' => 
  array (
    'item' => 0,
    'media_path' => 0,
    'owner' => 0,
    'url_base' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573b861ec3b642_23657905')) {function content_573b861ec3b642_23657905($_smarty_tpl) {?><?php if (!is_callable('smarty_function_static_content')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.static_content.php';
if (!is_callable('smarty_function_widget')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.widget.php';
?>
<section class="hp__banner">
    <div class="row text-center">
        <div class="small-12 columns">
            <h1 class="wow slideInUp"><?php echo $_smarty_tpl->tpl_vars['item']->value['nadpis'];?>
</h1>
            <div class="wow slideInUp">
            <?php echo $_smarty_tpl->tpl_vars['item']->value['uvodni_popis'];?>

            </div>
            <a href="#anchor-more-info" class="hp__banner-btn">Zjistěte o nás více</a>
            <a href="#anchor-more-info" title="Zjistěte o nás více" class="hp__banner-get-more"><img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
img/hp-banner-arrow.png"></a>
        </div>
    </div>
</section>


<section class="hp__facts" id="anchor-more-info">
    <div class="row text-center">
        <h2>Krátká fakta o nás</h2>
        <div class="medium-4 columns medium-text-left small-text-center">
            <p><span class="after" id="countMembers">1500+</span>členů pobočného spolku Zlín</p>
        </div>
        <div class="medium-4 columns  medium-text-left small-text-center">
            <p><span class="after" id="countYears">92let</span>organizovaného sportovního <br> rybolovu ve Zlíně</p>
        </div>
        <div class="medium-4 columns  medium-text-left small-text-center">
            <p><span id="countRace">60+</span>proškolených nových rybářů ročně</p>
        </div>
    </div>
</section>



<section class="hp__about-us">
    <div class="row">
        <div class="medium-12 text-center wow slideInUp">
            <?php echo smarty_function_static_content(array('code'=>"o-nas-box"),$_smarty_tpl);?>

            <div class="mb-xxl">
            <?php if (!empty($_smarty_tpl->tpl_vars['owner']->value['facebook'])){?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['owner']->value['facebook'];?>
" title="Navštivte náš facebook" class="hp_about-us__fb-btn">Navštivte náš facebook</a>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['owner']->value['instagram'])){?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['owner']->value['instagram'];?>
" title="Navštivte náš intagram" class="hp_about-us__int-btn">Navštivte náš instagram</a>
            <?php }?>
            </div>
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
o-nas" title="Zjistěte více" class="hp_button-more">Zjistěte více</a>
        </div>
    </div>
</section>


<?php echo smarty_function_widget(array('controller'=>"article"),$_smarty_tpl);?>



<section class="hp__photos">
    <div class="row text-center">
        <h2>Poslední přidané fotografie</h2>
        <?php echo smarty_function_widget(array('controller'=>"newsletter"),$_smarty_tpl);?>

        
        <?php echo smarty_function_widget(array('controller'=>"gallery"),$_smarty_tpl);?>

    </div>
</section>
<?php }} ?>