<?php /* Smarty version Smarty-3.1.11, created on 2016-05-18 09:50:28
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/gallery/detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:874627021573c1ec4859372-85513915%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '315561f292838be3af4f8b4a9ae95d01ae4f4ccb' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/gallery/detail.tpl',
      1 => 1463476931,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '874627021573c1ec4859372-85513915',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'url_base' => 0,
    'item' => 0,
    'photos' => 0,
    'photo' => 0,
    'pagination' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573c1ec4953f97_91719926',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573c1ec4953f97_91719926')) {function content_573c1ec4953f97_91719926($_smarty_tpl) {?><section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
fotogalerie" title="Seznam fotografií" class="sub__banner__btn float-right">Seznam fotografií</a>
			<h1><?php echo $_smarty_tpl->tpl_vars['item']->value['nadpis'];?>
</h1>
			<p>
				<?php echo $_smarty_tpl->tpl_vars['item']->value['uvodni_popis'];?>

			</p>
		</div>
	</div>
</section>
<section class="photos__detail wow slideInUp">
<?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['photo']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['photos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['photo']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['photo']->iteration=0;
 $_smarty_tpl->tpl_vars['photo']->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['gallery']['iteration']=0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['gallery']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value){
$_smarty_tpl->tpl_vars['photo']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['photo']->key;
 $_smarty_tpl->tpl_vars['photo']->iteration++;
 $_smarty_tpl->tpl_vars['photo']->index++;
 $_smarty_tpl->tpl_vars['photo']->first = $_smarty_tpl->tpl_vars['photo']->index === 0;
 $_smarty_tpl->tpl_vars['photo']->last = $_smarty_tpl->tpl_vars['photo']->iteration === $_smarty_tpl->tpl_vars['photo']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['gallery']['first'] = $_smarty_tpl->tpl_vars['photo']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['gallery']['iteration']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['gallery']['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['gallery']['last'] = $_smarty_tpl->tpl_vars['photo']->last;
?>
	<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['gallery']['first']||$_smarty_tpl->getVariable('smarty')->value['foreach']['gallery']['index']==6){?>
		
	<div class="row text-center">
	<?php }?>
		<div class="large-2 medium-4 small-6 columns end">
			<a href="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo']['ad'];?>
" data-lightbox="roadtrip" data-title="<?php echo $_smarty_tpl->tpl_vars['photo']->value['popis'];?>
"><img class="thumbnail" src="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo']['small'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['photo']->value['nazev'];?>
"></a>
		</div>
	<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['gallery']['last']||$_smarty_tpl->getVariable('smarty')->value['foreach']['gallery']['iteration']==6){?>
	</div>
	<?php }?>
<?php } ?>
	
	<div class="row">
		<div class="medium-12 columns">
			<?php echo $_smarty_tpl->tpl_vars['pagination']->value;?>

		</div>
	</div>
</section><?php }} ?>