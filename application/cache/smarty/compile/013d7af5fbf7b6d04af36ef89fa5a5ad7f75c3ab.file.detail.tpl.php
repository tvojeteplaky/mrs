<?php /* Smarty version Smarty-3.1.11, created on 2016-05-17 23:00:25
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/article/detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1245874019573b8669411ff2-24903704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '013d7af5fbf7b6d04af36ef89fa5a5ad7f75c3ab' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/article/detail.tpl',
      1 => 1463476926,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1245874019573b8669411ff2-24903704',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573b86694f1086_31896357',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573b86694f1086_31896357')) {function content_573b86694f1086_31896357($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_bbcode')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/modifier.bbcode.php';
if (!is_callable('smarty_function_widget')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.widget.php';
?><section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1><?php echo smarty_modifier_bbcode($_smarty_tpl->tpl_vars['item']->value['nadpis']);?>
</h1>
        </div>
    </div>
</section>
<article class="news__detail wow slideInUp">
    <div class="row">
        <div class="small-12 columns">
            <?php echo $_smarty_tpl->tpl_vars['item']->value['uvodni_popis'];?>

            <p>
                <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['photo']['ad'])){?>
                    <div class="large-4 float-right article__img">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['photo']['ad'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
">
                    </div>
                <?php }?>
                
                <?php echo $_smarty_tpl->tpl_vars['item']->value['popis'];?>

            </p>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <?php echo smarty_function_widget(array('controller'=>"contact",'action'=>"show"),$_smarty_tpl);?>

        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <h2>Kam se podívat dál?</h2>
            <?php echo $_smarty_tpl->tpl_vars['item']->value['odkazy'];?>

        </div>
    </div>
</article><?php }} ?>