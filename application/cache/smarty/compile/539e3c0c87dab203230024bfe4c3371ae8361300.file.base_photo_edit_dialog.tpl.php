<?php /* Smarty version Smarty-3.1.11, created on 2016-05-24 10:39:58
         compiled from "/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/base_photo_edit_dialog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20319121615744135e6ef191-16283064%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '539e3c0c87dab203230024bfe4c3371ae8361300' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/base_photo_edit_dialog.tpl',
      1 => 1463480211,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20319121615744135e6ef191-16283064',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_errors' => 0,
    'nazev' => 0,
    'popis' => 0,
    'zobrazit' => 0,
    'language_id' => 0,
    'nahled_src' => 0,
    'photo_id' => 0,
    'entity_name' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5744135e856466_67580585',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5744135e856466_67580585')) {function content_5744135e856466_67580585($_smarty_tpl) {?><div id="modalDialog">
<div class="modal fade" id="ModalForm1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editace položky</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php if (count($_smarty_tpl->tpl_vars['form_errors']->value)>0){?>
                        <p class="text-danger">Chyba: obrázek nemohl být uložen.</p>
		            <?php }?>
                    <div class="form-group <?php if (!empty($_smarty_tpl->tpl_vars['form_errors']->value['nazev'])){?>has-error<?php }?>">
                        <label class="control-label col-sm-2" for="nazev">Název</label>
                        <div class="col-sm-10">
                            <input type="text" name="nazev" class="form-control input-sm" id="nazev" value="<?php if (!empty($_smarty_tpl->tpl_vars['nazev']->value)){?><?php echo $_smarty_tpl->tpl_vars['nazev']->value;?>
<?php }?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="popis">Popis</label>
                        <div class="col-sm-10">
                            <textarea name="popis" class="form-control input-sm" id="popis" value="<?php if (!empty($_smarty_tpl->tpl_vars['popis']->value)){?><?php echo $_smarty_tpl->tpl_vars['popis']->value;?>
<?php }?>"><?php if (!empty($_smarty_tpl->tpl_vars['popis']->value)){?><?php echo $_smarty_tpl->tpl_vars['popis']->value;?>
<?php }?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="zobrazit">Zobrazit</label>
                        <div class="col-sm-10 text-left">
                            <input type="checkbox" name="zobrazit" id="zobrazit" <?php if (!empty($_smarty_tpl->tpl_vars['zobrazit']->value)&&$_smarty_tpl->tpl_vars['zobrazit']->value){?> checked="checked"<?php }?>>
                        </div>
                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['language_id']->value==1){?>
                        <div class="form-group <?php if (!empty($_smarty_tpl->tpl_vars['form_errors']->value['src'])){?>has-error<?php }?>">
                            <label class="control-label col-sm-2" for="zdroj">Zdroj</label>
                            <div class="col-sm-10 text-left">
                                <input type="file" class="form-control" name="gallery_image_src" id="zdroj" value="<?php if (!empty($_smarty_tpl->tpl_vars['popis']->value)){?><?php echo $_smarty_tpl->tpl_vars['popis']->value;?>
<?php }?>">
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="form-group">
                            <p class="text-info col-sm-12">Novou fotku lze vkládat po přepnutí na základní jazykovou verzi.</p>
                        </div>
                    <?php }?>
                    <div class="form-group">
                        <div class="col-xs-12">
                        <?php if (!empty($_smarty_tpl->tpl_vars['nahled_src']->value)){?>                            
                            <img src="<?php echo $_smarty_tpl->tpl_vars['nahled_src']->value;?>
" alt="náhled vloženého obrázku" class="img-responsive" title="<?php echo $_smarty_tpl->tpl_vars['popis']->value;?>
" />
                        <?php }?>
                        </div>
                    </div>
                    <input type="hidden" name="photo_id" value="<?php echo $_smarty_tpl->tpl_vars['photo_id']->value;?>
" />
                    <input type="hidden" name="language_id" value="<?php echo $_smarty_tpl->tpl_vars['language_id']->value;?>
" />
                    <input type="hidden" name="nahled_src" value="<?php echo $_smarty_tpl->tpl_vars['nahled_src']->value;?>
" />
                    <input type="hidden" name="photoedit_action_add" value="<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Uložit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>                
            </div>
        </div>
    </div>
</div>
</div>



<script type="text/javascript">
     $(function() {
      // premisteni modalniho formulare mimo hlavni formular - jinak se neodeslou data
      var data=$("#modalDialog").html();
      $("#modalDialog").remove();
      $('#JqueryFormIN').append(data);      
      
      $('#ModalForm1').modal();
    
      $('#ModalForm1').on('shown.bs.modal', function(e){
        
      });



      });
</script>

   <?php }} ?>