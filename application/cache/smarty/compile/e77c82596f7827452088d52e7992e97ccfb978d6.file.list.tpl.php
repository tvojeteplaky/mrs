<?php /* Smarty version Smarty-3.1.11, created on 2016-05-18 01:20:33
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/gallery/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1072557535573ba7412a5161-05921476%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e77c82596f7827452088d52e7992e97ccfb978d6' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/gallery/list.tpl',
      1 => 1463476930,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1072557535573ba7412a5161-05921476',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'galleries' => 0,
    'year' => 0,
    'gallery_years' => 0,
    'gallery' => 0,
    'url_base' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573ba741347862_79005379',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573ba741347862_79005379')) {function content_573ba741347862_79005379($_smarty_tpl) {?><section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<h1><?php echo $_smarty_tpl->tpl_vars['item']->value['nadpis'];?>
</h1>
			<p>
				<?php echo $_smarty_tpl->tpl_vars['item']->value['uvodni_popis'];?>

			</p>
		</div>
	</div>
</section>
<section class="photos wow slideInUp">
<?php  $_smarty_tpl->tpl_vars['gallery_years'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gallery_years']->_loop = false;
 $_smarty_tpl->tpl_vars['year'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['galleries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gallery_years']->key => $_smarty_tpl->tpl_vars['gallery_years']->value){
$_smarty_tpl->tpl_vars['gallery_years']->_loop = true;
 $_smarty_tpl->tpl_vars['year']->value = $_smarty_tpl->tpl_vars['gallery_years']->key;
?>
	<div class="row">
		<div class="medium-12">
		 <h2>Fotografie <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</h2>
		 <?php  $_smarty_tpl->tpl_vars['gallery'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['gallery']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['gallery_years']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['gallery']->key => $_smarty_tpl->tpl_vars['gallery']->value){
$_smarty_tpl->tpl_vars['gallery']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['gallery']->key;
?>
		 	<div class="medium-6 large-3 end columns">
				<div class="photos__item">
				<img src="<?php echo $_smarty_tpl->tpl_vars['gallery']->value['photo']['small'];?>
">
				<div class="overlay">
					<a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
<?php echo $_smarty_tpl->tpl_vars['gallery']->value['nazev_seo'];?>
" title="Zobrazit celou fotogalerii">
					<h3><?php echo $_smarty_tpl->tpl_vars['gallery']->value['nazev'];?>
</h3>
					<span>Počet fotografií: <?php echo $_smarty_tpl->tpl_vars['gallery']->value['count'];?>
</span>
					</a>
				</div>
				</div>
			</div>
		 <?php } ?>
		</div>
	</div>
<?php } ?>
</section><?php }} ?>