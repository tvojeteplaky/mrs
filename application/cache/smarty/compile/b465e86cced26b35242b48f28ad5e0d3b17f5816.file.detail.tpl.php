<?php /* Smarty version Smarty-3.1.11, created on 2016-05-18 11:51:11
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/contact/detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1011370525573ba722533869-64655379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b465e86cced26b35242b48f28ad5e0d3b17f5816' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/contact/detail.tpl',
      1 => 1463565069,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1011370525573ba722533869-64655379',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573ba722624395_48659905',
  'variables' => 
  array (
    'item' => 0,
    'owner' => 0,
    'employees' => 0,
    'employee' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573ba722624395_48659905')) {function content_573ba722624395_48659905($_smarty_tpl) {?><?php if (!is_callable('smarty_function_static_content')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.static_content.php';
if (!is_callable('smarty_function_widget')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.widget.php';
?>
<section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1><?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
</h1>
            <p>
                <?php echo $_smarty_tpl->tpl_vars['item']->value['uvodni_popis'];?>

            </p>
        </div>
    </div>
</section>

<article>
    <div class="row text-center contact__pictograms wow slideInUp">
        <div class="medium-4 columns">
            <h2 class="icn-post">Doručovací adresa</h2>
            <p>
                <?php echo $_smarty_tpl->tpl_vars['owner']->value['firma'];?>
 <br>
                <?php echo $_smarty_tpl->tpl_vars['owner']->value['ulice'];?>
 <br>
                <?php echo $_smarty_tpl->tpl_vars['owner']->value['psc'];?>
 <?php echo $_smarty_tpl->tpl_vars['owner']->value['mesto'];?>

            </p>
        </div>
        <div class="medium-4 columns">
            <h2 class="icn-keyboard">Jak nás zastihnete?</h2>
            <p>
                <strong>Tel.:</strong> <a href="tel:<?php echo $_smarty_tpl->tpl_vars['owner']->value['tel'];?>
" title="Zavolejte nám"><?php echo $_smarty_tpl->tpl_vars['owner']->value['tel'];?>
</a> <br>
                <strong>E-mail:</strong> <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['owner']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['owner']->value['email'];?>
</a> <br><br>
                <strong>IČ:</strong>  <?php echo $_smarty_tpl->tpl_vars['owner']->value['ic'];?>
<br>
                <strong>Číslo bankovního účtu:</strong> <br>
                <?php echo $_smarty_tpl->tpl_vars['owner']->value['ucet'];?>

            </p>
        </div>
        <div class="medium-4 columns">
            <h2 class="icn-clock">Úřední hodiny</h2>
            <?php echo smarty_function_static_content(array('code'=>"uredni-hodiny"),$_smarty_tpl);?>

        </div>
    </div>

    <div class="row">
        <div class="medium-7 columns wow slideInLeft">
        <section class="contact__list">
            <h2>Kontaktní osoby</h2>
            <?php  $_smarty_tpl->tpl_vars['employee'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['employee']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['employees']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['employee']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['employee']->iteration=0;
 $_smarty_tpl->tpl_vars['employee']->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['employees']['iteration']=0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['employees']['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['employee']->key => $_smarty_tpl->tpl_vars['employee']->value){
$_smarty_tpl->tpl_vars['employee']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['employee']->key;
 $_smarty_tpl->tpl_vars['employee']->iteration++;
 $_smarty_tpl->tpl_vars['employee']->index++;
 $_smarty_tpl->tpl_vars['employee']->first = $_smarty_tpl->tpl_vars['employee']->index === 0;
 $_smarty_tpl->tpl_vars['employee']->last = $_smarty_tpl->tpl_vars['employee']->iteration === $_smarty_tpl->tpl_vars['employee']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['employees']['first'] = $_smarty_tpl->tpl_vars['employee']->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['employees']['iteration']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['employees']['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['employees']['last'] = $_smarty_tpl->tpl_vars['employee']->last;
?>
               <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['employees']['first']||$_smarty_tpl->getVariable('smarty')->value['foreach']['employees']['index']%2==0){?>
                    <div class="row columns dotted">
                <?php }?> 
                    <div class="medium-6 columns">
                    <strong><?php echo $_smarty_tpl->tpl_vars['employee']->value['name'];?>
</strong> <br>
                    <?php echo $_smarty_tpl->tpl_vars['employee']->value['role'];?>

                    <table>
                        <tr><td>Tel.:</td><td><a href="tel:<?php echo $_smarty_tpl->tpl_vars['employee']->value['phone'];?>
" title="Zavolat"><?php echo $_smarty_tpl->tpl_vars['employee']->value['phone'];?>
</a></td></tr>
                        <tr><td>E-mail:</td><td><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['employee']->value['email'];?>
" title="Napsat e-mail"><?php echo $_smarty_tpl->tpl_vars['employee']->value['email'];?>
</a></td></tr>
                    </table>
                </div>
                <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['employees']['last']||$_smarty_tpl->getVariable('smarty')->value['foreach']['employees']['iteration']%2==0){?>
                    </div>
                <?php }?> 
            <?php } ?>
        </section>
        </div>
        <div class="medium-5 columns wow slideInRight">
            <div class="contact__form">
                <?php echo smarty_function_widget(array('controller'=>'contact','action'=>'show'),$_smarty_tpl);?>

            </div>
            <div class="contact__info-box">
                <p>
                    <?php echo smarty_function_static_content(array('code'=>"povolenky"),$_smarty_tpl);?>

                </p>
            </div>
        </div>
    </div>
</article>

<?php }} ?>