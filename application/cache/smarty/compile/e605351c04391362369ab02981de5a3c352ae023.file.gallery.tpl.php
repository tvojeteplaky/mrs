<?php /* Smarty version Smarty-3.1.11, created on 2016-05-24 10:14:51
         compiled from "/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/gallery.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18742280457440d7b4760c7-78702406%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e605351c04391362369ab02981de5a3c352ae023' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/gallery.tpl',
      1 => 1463480210,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18742280457440d7b4760c7-78702406',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'form_url' => 0,
    'edtiable_languages' => 0,
    'back_to_item' => 0,
    'back_to_item_text' => 0,
    'back_link' => 0,
    'back_link_text' => 0,
    'clone_link' => 0,
    'media_path' => 0,
    'clone_link_text' => 0,
    'k' => 0,
    'sel_language_id' => 0,
    'disable_other_languages' => 0,
    'item' => 0,
    'copy_lang_link' => 0,
    'main_language' => 0,
    'message' => 0,
    'script' => 0,
    'edit_table' => 0,
    'key' => 0,
    'row_errors' => 0,
    'row_parameters' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_57440d7b60fe96_20622492',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57440d7b60fe96_20622492')) {function content_57440d7b60fe96_20622492($_smarty_tpl) {?>

<div class="row">  
    <form action="<?php echo $_smarty_tpl->tpl_vars['form_url']->value;?>
" method="post" enctype="multipart/form-data" name="EditForm" id="EditForm">
    <div class="col-md-<?php if (empty($_smarty_tpl->tpl_vars['edtiable_languages']->value)){?>12<?php }else{ ?>4<?php }?>">
      <?php if (isset($_smarty_tpl->tpl_vars['back_to_item']->value)&&$_smarty_tpl->tpl_vars['back_to_item']->value){?>
        <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['back_to_item']->value;?>
">
            <span class="glyphicon-chevron-left glyphicon"></span>
            <span><?php echo $_smarty_tpl->tpl_vars['back_to_item_text']->value;?>
</span>       
        </a>
      <?php }?>      
      <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['back_link']->value;?>
">
          <span class="glyphicon-chevron-left glyphicon"></span>
          <span><?php echo $_smarty_tpl->tpl_vars['back_link_text']->value;?>
</span>          
      </a>
      <?php if ($_smarty_tpl->tpl_vars['clone_link']->value){?>
        <a class="btn btn-default" href="<?php echo $_smarty_tpl->tpl_vars['clone_link']->value;?>
">
            <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/page_copy.png" alt="Otevře formulář pro vložení nové položky s předvyplněnými základními daty" />
            <span><?php echo $_smarty_tpl->tpl_vars['clone_link_text']->value;?>
</span></a>
      <?php }?>
      
    </div>   

    <?php if (!empty($_smarty_tpl->tpl_vars['edtiable_languages']->value)){?>        
    <div class="col-md-8 text-right">
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['edtiable_languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <a class="<?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['sel_language_id']->value){?>active<?php }elseif($_smarty_tpl->tpl_vars['disable_other_languages']->value){?>disabled<?php }?> btn btn-default" <?php if ($_smarty_tpl->tpl_vars['disable_other_languages']->value&&$_smarty_tpl->tpl_vars['k']->value!=$_smarty_tpl->tpl_vars['sel_language_id']->value){?>href="#" title="Uložte formulář nejprve v základní jazykové verzi"<?php }else{ ?>href="?admlang=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" title="jazyková verze: <?php echo $_smarty_tpl->tpl_vars['item']->value;?>
"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</a>
        <?php } ?>
        <?php if ($_smarty_tpl->tpl_vars['sel_language_id']->value!=1&&$_smarty_tpl->tpl_vars['copy_lang_link']->value){?>
          <a href="?copylang=true" class="btn btn-info">kopírovat z <?php echo $_smarty_tpl->tpl_vars['main_language']->value;?>
</a></li>
        <?php }?>      
    </div>
    <?php }?>
</div>
     
    
    <?php if (!empty($_smarty_tpl->tpl_vars['message']->value)){?>  
<div class="row">
    <div class="col-md-12 space-5">
        <?php if ($_smarty_tpl->tpl_vars['message']->value=="error"){?> 
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Chyba:</strong> Při zpracování dat došlo k chybě, zkontrolujte prosím zadané údaje.
        </div>
        <?php }elseif($_smarty_tpl->tpl_vars['message']->value=="ok"){?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Změny byly uloženy.
        </div>
        <?php }elseif($_smarty_tpl->tpl_vars['message']->value=="deleted"){?>        
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Zvolené položky byly smazány.
        </div>
        <?php }elseif($_smarty_tpl->tpl_vars['message']->value=="highlight"){?>
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Změna byla provedena.
        </div>
        <?php }?>
    </div>
</div>
		<?php }?>

     
    <?php if ($_smarty_tpl->tpl_vars['script']->value){?>
    <script type="text/javascript">
    <?php echo $_smarty_tpl->tpl_vars['script']->value;?>

    </script>
    <?php }?>
<?php if (isset($_smarty_tpl->tpl_vars['edit_table']->value['data_section']['link_new'])&&array_key_exists("link_new",$_smarty_tpl->tpl_vars['edit_table']->value['data_section'])){?>
<div class="row">
    <div class="col-md-12 margin-top-10 margin-bottom-10">
        <?php echo $_smarty_tpl->tpl_vars['edit_table']->value['data_section']['link_new'];?>

    </div>
</div>
<?php }?>
<div class="row">
    <div class="col-md-12 margin-top-10">
      <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['edit_table']->value['data_section']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?> 
        <?php if ($_smarty_tpl->tpl_vars['key']->value=="link_new"){?><?php continue 1?><?php }?>
          
            <?php echo $_smarty_tpl->tpl_vars['item']->value;?>

            <?php if (empty($_smarty_tpl->tpl_vars['row_errors']->value[$_smarty_tpl->tpl_vars['key']->value])){?>
                <?php if (!empty($_smarty_tpl->tpl_vars['row_parameters']->value[$_smarty_tpl->tpl_vars['key']->value]['condition'])){?>
                    <p class="text-info"><?php echo $_smarty_tpl->tpl_vars['row_parameters']->value[$_smarty_tpl->tpl_vars['key']->value]['condition'];?>
</p>
                <?php }?>
              <?php }else{ ?>
              <p class="text-danger"><?php echo $_smarty_tpl->tpl_vars['row_errors']->value[$_smarty_tpl->tpl_vars['key']->value];?>
</span>
              <?php }?>
      <?php } ?>

    <input type="hidden" id="hana_edit_action" name="hana_form_action" value="main" /> 
              
    </form>
        <form id="JqueryFormIN" role="form" action="<?php echo $_smarty_tpl->tpl_vars['form_url']->value;?>
" method="post" enctype="multipart/form-data"></form>
    </div>
</div>


<?php }} ?>