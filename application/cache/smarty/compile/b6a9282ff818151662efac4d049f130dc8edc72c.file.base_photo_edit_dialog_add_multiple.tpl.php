<?php /* Smarty version Smarty-3.1.11, created on 2016-05-24 10:27:16
         compiled from "/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/base_photo_edit_dialog_add_multiple.tpl" */ ?>
<?php /*%%SmartyHeaderCode:206223995857441064b8b400-94943611%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6a9282ff818151662efac4d049f130dc8edc72c' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/edit/base_photo_edit_dialog_add_multiple.tpl',
      1 => 1463480209,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '206223995857441064b8b400-94943611',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'media_path' => 0,
    'entity_name' => 0,
    'max_files' => 0,
    'base_admin_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_57441064d2ba32_99809366',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57441064d2ba32_99809366')) {function content_57441064d2ba32_99809366($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.flash.min.js" type="text/javascript"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/agile-uploader-3.0.js" type="text/javascript"></script>

<div id="modalDialog">
    <div class="modal fade" id="ModalForm2" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Hromadné přidání fotografií</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" name="photoedit_action_add_multiple" value="<?php echo $_smarty_tpl->tpl_vars['entity_name']->value;?>
" />
                        <div id="Multiple"></div>
                        <div class="correct mediumMT">Maximální počet současně vkládaných obrázků: <?php echo $_smarty_tpl->tpl_vars['max_files']->value;?>
.</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="upload_photo">Nahrát</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
                </div>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    $(function() {

        // premisteni modalniho formulare mimo hlavni formular - jinak se neodeslou data
        var data=$("#modalDialog").html();
        $("#modalDialog").remove();
        $('#JqueryFormIN').append(data);

        $('#ModalForm2').modal();

        $('#ModalForm2').on('shown.bs.modal', function(e){

        });

        $("button#upload_photo").click(function(e){
            document.getElementById('agileUploaderSWF').submit();
            e.preventDefault();
        });

        $('#Multiple').agileUploader({
            submitRedirect: '<?php echo $_smarty_tpl->tpl_vars['base_admin_path']->value;?>
?message=highlight',
            formId: 'JqueryFormIN',
            flashVars: {
                firebug: false,
                form_action: '<?php echo $_smarty_tpl->tpl_vars['base_admin_path']->value;?>
',
                max_width: 1920,
                max_height: 1080,
                jpg_quality: 90,
                file_limit: <?php echo $_smarty_tpl->tpl_vars['max_files']->value;?>
,
                max_post_size: (10000 * 1024),
            }
        });








    });
</script>

<?php }} ?>