<?php /* Smarty version Smarty-3.1.11, created on 2016-05-18 06:52:19
         compiled from "/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/base_template.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1239052223573bf50309a230-79621909%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '09c808aa9cb871f5d77a888ddf92ab4e76f787c3' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/modules/hana/views/admin/base_template.tpl',
      1 => 1463479911,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1239052223573bf50309a230-79621909',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'media_path' => 0,
    'admin_theme' => 0,
    'title' => 0,
    'admin_menu' => 0,
    'web_owner' => 0,
    'url_base' => 0,
    'link' => 0,
    'link_path' => 0,
    'child' => 0,
    'child2' => 0,
    'center_section' => 0,
    'admin_content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573bf503314f35_63172161',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573bf503314f35_63172161')) {function content_573bf503314f35_63172161($_smarty_tpl) {?><?php if (!is_callable('smarty_function_translate')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.translate.php';
?>
<!DOCTYPE html>
<html lang=cs>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="Pavel Herink"/>
    <meta name="copyright" content="2011"/>
    <meta name="description" content="Systém pro správu obsahu Hana verze 2"/>
    <meta name="keywords" content=""/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/style-print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/jquery-ui-1.8.6.custom.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/showLoading.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/agile-uploader.css" type="text/css" media="screen"/>


    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/jquery.fancybox.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/<?php echo $_smarty_tpl->tpl_vars['admin_theme']->value['nazev_css'];?>
.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/bootstrap-datetimepicker.min.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/bootstrap-colorpicker.min.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/select2.min.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/css/jquery.smartmenus.bootstrap.css" type="text/css"/>

    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.old.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery-ui-1.8.18.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/locales/bootstrap-datetimepicker.cs.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/daterangepicker.jQuery.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.showLoading.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.tristateCheckbox.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.tablednd.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.uploadify.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.sortable.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/hana.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/js/admin.js"></script>

    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 - administrační rozhranní</title>
    
        <script type="text/javascript">

            // Load the Visualization API and the piechart package.
            google.load('visualization', '1.0', {'packages': ['corechart', 'geochart']});
        </script>
    
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <!-- hlavicka -->
            <header>
                <!--  hlavni menu L1 -->
                <?php if (!empty($_smarty_tpl->tpl_vars['admin_menu']->value)){?>
                    <nav class="navbar navbar-<?php if ($_smarty_tpl->tpl_vars['admin_theme']->value['inverse']){?>inverse<?php }else{ ?>default<?php }?>" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <?php if ($_smarty_tpl->tpl_vars['web_owner']->value['default_title']){?>
                                <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
" data-toggle="popover" data-trigger="hover" data-content="<?php echo smarty_function_translate(array('str'=>"Budete přesměrován na frontend"),$_smarty_tpl);?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['web_owner']->value['default_title'];?>
</a>
                            <?php }?>
                        </div>
                        <div class="collapse navbar-collapse" id="main-navbar-collapse-1">
                            <ul class="navbar-nav nav">
                                <?php  $_smarty_tpl->tpl_vars['link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['admin_menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value){
$_smarty_tpl->tpl_vars['link']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['link']->key;
?>
                                    <li class="<?php if (in_array($_smarty_tpl->tpl_vars['link']->value['id'],$_smarty_tpl->tpl_vars['link_path']->value)){?>active<?php }?>">
                                        <a href="<?php if (!empty($_smarty_tpl->tpl_vars['link']->value['children'])){?>#<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['link']->value['href'];?>
<?php }?>"><?php echo $_smarty_tpl->tpl_vars['link']->value['nazev'];?>
 <?php if (!empty($_smarty_tpl->tpl_vars['link']->value['children'])){?><b class="caret"></b><?php }?></a>
                                        <?php if (!empty($_smarty_tpl->tpl_vars['link']->value['children'])){?>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['link']->value['nazev'];?>
</a></li>
                                                <li class="divider"></li>
                                                <?php  $_smarty_tpl->tpl_vars['child'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['child']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['link']->value['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['child']->key => $_smarty_tpl->tpl_vars['child']->value){
$_smarty_tpl->tpl_vars['child']->_loop = true;
?>
                                                    <li class="<?php if (in_array($_smarty_tpl->tpl_vars['child']->value['id'],$_smarty_tpl->tpl_vars['link_path']->value)){?>active<?php }?>">
                                                        <a href="<?php if (!empty($_smarty_tpl->tpl_vars['child']->value['children'])){?>#<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['child']->value['href'];?>
<?php }?>"><?php echo $_smarty_tpl->tpl_vars['child']->value['nazev'];?>
 <?php if (!empty($_smarty_tpl->tpl_vars['child']->value['children'])){?><b class="caret"></b><?php }?></a>
                                                        <?php if (!empty($_smarty_tpl->tpl_vars['child']->value['children'])){?>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="<?php echo $_smarty_tpl->tpl_vars['child']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['child']->value['nazev'];?>
</a></li>
                                                                <li class="divider"></li>
                                                                <?php  $_smarty_tpl->tpl_vars['child2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['child2']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['child']->value['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['child2']->key => $_smarty_tpl->tpl_vars['child2']->value){
$_smarty_tpl->tpl_vars['child2']->_loop = true;
?>
                                                                    <li class="<?php if (in_array($_smarty_tpl->tpl_vars['child2']->value['id'],$_smarty_tpl->tpl_vars['link_path']->value)){?>active<?php }?>">
                                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['child2']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['child2']->value['nazev'];?>
</a>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php }?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php }?>
                                    </li>
                                <?php } ?>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                
                                <li><a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
admin/logout">Odhlásit</a></li>
                            </ul>
                        </div>
                    </nav>
                <?php }?>
            </header>
        </div>
    </div>

    <!-- stredni cast -->
    <div class="row">
        <?php if (!empty($_smarty_tpl->tpl_vars['center_section']->value)){?><?php echo $_smarty_tpl->tpl_vars['center_section']->value;?>
<?php }?>
        <!-- hlavni obsah -->
        <div class="col-md-12" id="ContentSection">
            <?php if (!empty($_smarty_tpl->tpl_vars['admin_content']->value)){?><?php echo $_smarty_tpl->tpl_vars['admin_content']->value;?>
<?php }?>
        </div>
    </div>
    <footer>
        <div class="row">
            <div class="col-sm-12 text-right">
                <p><a href="http://www.dgstudio.cz"><img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
admin/img/dg_logo<?php if ($_smarty_tpl->tpl_vars['admin_theme']->value['id']==2){?>_light<?php }?>.png" alt="DG Studio logo"></a></p>
            </div>
        </div>
    </footer>
</div>
</body>
</html><?php }} ?>