<?php /* Smarty version Smarty-3.1.11, created on 2016-05-17 22:59:11
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/gallery/widget.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2111179695573b861f0e3b76-93007119%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e8d897f60fa757a604a0e823bc854afa9b814421' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/gallery/widget.tpl',
      1 => 1463476931,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2111179695573b861f0e3b76-93007119',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'photos' => 0,
    'url_base' => 0,
    'photo' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573b861f114ff5_63404867',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573b861f114ff5_63404867')) {function content_573b861f114ff5_63404867($_smarty_tpl) {?><div class="row columns wow slideInUp">
	<?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['photo']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['photos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value){
$_smarty_tpl->tpl_vars['photo']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['photo']->key;
?>
			<div class="medium-4 columns">
				<div class="hp__photos__item">
					<a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
<?php echo $_smarty_tpl->tpl_vars['photo']->value['nazev_seo'];?>
" title="Zobrazit fotogalerii">
						<img src="<?php echo $_smarty_tpl->tpl_vars['photo']->value['photo_src'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['photo']->value['nazev'];?>
">
						<div class="overlay">
							<h4></h4>
							<p>Album: <?php echo $_smarty_tpl->tpl_vars['photo']->value['nazev'];?>
</p>
						</div>
						<div class="hp__photos__gradient"></div>
						<div class="hp_photos__item__hover">
						</div>
					</a>
				</div>
			</div>
	<?php } ?>
	</div>
	<div class="row columns">
			<a href="/fotogalerie" title="Celá fotogalerie" class="hp_button-more">Celá fotogalerie</a>
		</div>
<?php }} ?>