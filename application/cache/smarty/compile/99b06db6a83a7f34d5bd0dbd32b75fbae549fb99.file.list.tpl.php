<?php /* Smarty version Smarty-3.1.11, created on 2016-05-18 01:17:48
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/article/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:474714257573ba69c1c9325-15969665%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99b06db6a83a7f34d5bd0dbd32b75fbae549fb99' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/article/list.tpl',
      1 => 1463476925,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '474714257573ba69c1c9325-15969665',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'items' => 0,
    'url_base' => 0,
    'pagination' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573ba69c368213_37168028',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573ba69c368213_37168028')) {function content_573ba69c368213_37168028($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/vendor/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/vendor/smarty/plugins/modifier.truncate.php';
?><section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1><?php echo $_smarty_tpl->tpl_vars['item']->value['nadpis'];?>
</h1>
            <p>
                <?php echo $_smarty_tpl->tpl_vars['item']->value['uvodni_popis'];?>

            </p>
        </div>
    </div>
</section>
<article class="news wow slideInUp">
    <?php if (!empty($_smarty_tpl->tpl_vars['items']->value)){?>
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
            <div class="row">
                <div class="medium-5 columns">
                    <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['photo']['home'])){?>
                        <img class="thumbnail" src="<?php echo $_smarty_tpl->tpl_vars['item']->value['photo']['home'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
">
                    <?php }?>
                    
                </div>
                <div class="medium-7 columns">
                    <h2><?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
</h2>
                    <span><strong>Autor:</strong> <?php echo $_smarty_tpl->tpl_vars['item']->value['author'];?>
  • <strong>Datum:</strong> <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value['date'],"%d. %m. %Y");?>
</span>
                    <p>
                       <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value['uvodni_popis'],600,'');?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev_seo'];?>
" title="Číst celý článek">Číst celý článek...</a>
                    </p>
                </div>
            </div>
        <?php } ?>
    <?php }?>
    <div class="row">
        <div class="medium-12 columns">
            <?php echo $_smarty_tpl->tpl_vars['pagination']->value;?>

        </div>
    </div>
</article><?php }} ?>