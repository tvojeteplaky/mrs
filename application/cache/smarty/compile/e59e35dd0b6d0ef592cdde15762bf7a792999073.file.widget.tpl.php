<?php /* Smarty version Smarty-3.1.11, created on 2016-06-01 08:50:26
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/article/widget.tpl" */ ?>
<?php /*%%SmartyHeaderCode:449931134573b861ed90a71-29747594%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e59e35dd0b6d0ef592cdde15762bf7a792999073' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/article/widget.tpl',
      1 => 1464763820,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '449931134573b861ed90a71-29747594',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573b861ef3f235_67409829',
  'variables' => 
  array (
    'items' => 0,
    'url_base' => 0,
    'item' => 0,
    'cat' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573b861ef3f235_67409829')) {function content_573b861ef3f235_67409829($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/vendor/smarty/plugins/modifier.truncate.php';
?><?php if (!empty($_smarty_tpl->tpl_vars['items']->value)){?>
<section class="hp__news">
    <div class="row text-center  wow slideInUp">
        <h2>Novinky</h2>
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <div class="medium-6 columns text-left">
            <div class="hp__news__item">
            <a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev_seo'];?>
" title="Zobrazit celou novinku">
        <?php if ($_smarty_tpl->tpl_vars['item']->value['photo_src']){?>
            <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value['photo']['home'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
">
            <?php }?>
            <div class="overlay">

                <h3><?php echo $_smarty_tpl->tpl_vars['item']->value['nazev'];?>
</h3>
                <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['categories'])){?>
                <span>
                <?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['cat']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['cat']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value){
$_smarty_tpl->tpl_vars['cat']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['cat']->key;
 $_smarty_tpl->tpl_vars['cat']->iteration++;
 $_smarty_tpl->tpl_vars['cat']->last = $_smarty_tpl->tpl_vars['cat']->iteration === $_smarty_tpl->tpl_vars['cat']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['article_cat']['last'] = $_smarty_tpl->tpl_vars['cat']->last;
?>
                     <?php echo $_smarty_tpl->tpl_vars['cat']->value['nazev'];?>

                     <?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['article_cat']['last']){?>
                         &bull;
                     <?php }?>
                 <?php } ?> </span>
                 <?php }?>
                 <p><?php echo smarty_modifier_truncate(strip_tags($_smarty_tpl->tpl_vars['item']->value['uvodni_popis']),120,"...",false);?>
</p>
            </div>
            <div class="gradient"></div>
            </a>
            </div>
        </div>
        <?php } ?> 

        <a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
novinky" title="Všechny novinky" class="hp_button-more">Všechny novinky</a>
    </div>
    

</section>
<?php }?><?php }} ?>