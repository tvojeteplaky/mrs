<?php /* Smarty version Smarty-3.1.11, created on 2016-06-23 14:06:15
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/base_template.tpl" */ ?>
<?php /*%%SmartyHeaderCode:859432773573b861f191355-85588964%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '409d80bfa63da779fbf963e2866a1a445f12a269' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/base_template.tpl',
      1 => 1466683566,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '859432773573b861f191355-85588964',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573b861f27cd76_14381511',
  'variables' => 
  array (
    'web_setup' => 0,
    'web_owner' => 0,
    'page_keywords' => 0,
    'page_description' => 0,
    'page_title' => 0,
    'page_name' => 0,
    'media_path' => 0,
    'main_content' => 0,
    'url_base' => 0,
    'owner' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573b861f27cd76_14381511')) {function content_573b861f27cd76_14381511($_smarty_tpl) {?><?php if (!is_callable('smarty_function_widget')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.widget.php';
if (!is_callable('smarty_function_static_content')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.static_content.php';
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="author" content="<?php echo $_smarty_tpl->tpl_vars['web_setup']->value['nazev_dodavatele'];?>
"/>
    <meta name="copyright" content="<?php echo $_smarty_tpl->tpl_vars['web_owner']->value['copyright'];?>
" />
    <meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['page_keywords']->value;?>
"/>
    <meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['page_description']->value;?>
 "/>
    <meta name="robots" content="all,follow"/>
    <meta name="googlebot" content="snippet,archive"/>
    <title><?php echo $_smarty_tpl->tpl_vars['page_title']->value;?>
 &bull; <?php echo $_smarty_tpl->tpl_vars['page_name']->value;?>
</title>
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700&subset=latin-ext'>
    
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
css/foundation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
css/css.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/hana.js"></script>
    <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/app.js"></script>
</head>
<body>


<div id="sticky-head">
<header id="anchor-top">
    <div class="row">
        <div class="small-12 large-3 columns small-text-center header__logo">
            <a href="/" title="<?php echo $_smarty_tpl->tpl_vars['page_title']->value;?>
">
                <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
img/logo.png" alt="Logo">
            </a>
        </div>
        <div class="small-12 large-9 columns">
            <div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium">
               <button class="menu-icon" type="button" data-toggle></button>
               <div class="title-bar-title">Menu</div>
            </div>
           <?php echo smarty_function_widget(array('controller'=>"navigation",'action'=>"main"),$_smarty_tpl);?>

        </div>
    </div>
</header>
</div>
<?php echo $_smarty_tpl->tpl_vars['main_content']->value;?>



<?php echo smarty_function_widget(array('controller'=>"site",'action'=>"message"),$_smarty_tpl);?>



<section class="hp__map">
    <div class="row text-center">
        <h2>Kde nás najdete</h2>
    </div>
    <div class="hp__map__contact" style="z-index:1">
        <div class="hp__map__contact__text">
            <strong><?php echo $_smarty_tpl->tpl_vars['web_owner']->value['firma'];?>
</strong>
            <p><?php echo $_smarty_tpl->tpl_vars['web_owner']->value['ulice'];?>
, <br> <?php echo $_smarty_tpl->tpl_vars['web_owner']->value['psc'];?>
 <?php echo $_smarty_tpl->tpl_vars['web_owner']->value['mesto'];?>
</p>
            <p><a href="tel:<?php echo $_smarty_tpl->tpl_vars['web_owner']->value['tel'];?>
"><?php echo $_smarty_tpl->tpl_vars['web_owner']->value['tel'];?>
</a> <br> <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['web_owner']->value['email'];?>
"><?php echo $_smarty_tpl->tpl_vars['web_owner']->value['email'];?>
</a></p>
        </div>
        <a href="<?php echo $_smarty_tpl->tpl_vars['url_base']->value;?>
kontakty" title="Kontaktujte nás" class="hp__map__contact__btn">Kontaktujte nás</a>
        <a href="#anchor-top" class="slide" title="Zpět nahoru ↑"><span class="hp__map__contact__b-top"></span></a>
    </div>
    <div id="map"></div>
    
</section>


<div class="social-buttons">
<?php if (!empty($_smarty_tpl->tpl_vars['owner']->value['facebook'])){?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['owner']->value['facebook'];?>
" title="Facebook"><img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
img/icn-facebook-social.png" alt="fb icon"></a>
            <?php }?>
            <?php if (!empty($_smarty_tpl->tpl_vars['owner']->value['instagram'])){?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['owner']->value['instagram'];?>
" title="Instagram"><img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
img/icn-instagram-social.png" alt="instagram icon"></a>
            <?php }?>
</div>


<footer>
    <div class="row">
        
        <div class="small-12 columns divblock" >
        <?php echo smarty_function_static_content(array('code'=>"site_index"),$_smarty_tpl);?>

           
        </div>
        
    </div>
</footer>
<div class="footer-text">
    <div class="row">
        <p>Copyright &copy; 2016 Moravský rybářský svaz z.s.</p>
        <span>
            Realizace:
            <a href="http://www.dgstudio.cz/" title="DG studio - Tvorba webových stránek, on-line marketing, desing a grafika" target="_blank">
                <img src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
img/dgstudio_logo.png" alt="dgstudio">
            </a>
        </span>
    </div>
</div>

<script type="text/javascript">
    window.address = JSON.parse('<?php echo $_smarty_tpl->tpl_vars['web_owner']->value['latlng'];?>
');
</script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/lightbox.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/foundation.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/countUp.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/js.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['media_path']->value;?>
js/map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpYcN7hji42drFr1V_LgkRffwwcM8PWoY&amp;callback=initMap"
    async defer></script>


    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MXSPDZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MXSPDZ');</script>
<!-- End Google Tag Manager -->


</body>
</html>
<?php }} ?>