<?php /* Smarty version Smarty-3.1.11, created on 2016-05-18 01:18:38
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/revir/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1129947543573ba6ce3374d6-26821705%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7309d55f80e720d3b0b8d5fb81a03c858d979760' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/revir/list.tpl',
      1 => 1463476936,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1129947543573ba6ce3374d6-26821705',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'reviry' => 0,
    'revir_oblast' => 0,
    'revir' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573ba6ce5381f6_02306190',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573ba6ce5381f6_02306190')) {function content_573ba6ce5381f6_02306190($_smarty_tpl) {?><?php if (!is_callable('smarty_function_widget')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.widget.php';
?><section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<h1>Rybářské revíry</h1>
			<p>
			</p>
		</div>
	</div>
</section>

<article class="grounds" id="stickyPage">
	<div class="row">
		<div class="large-3 columns float-right wow slideInRight" data-sticky-container>
			<div class="sticky" id="sticky" data-sticky data-options="data-sticky-on: large" data-anchor="stickyPage">
				<?php echo smarty_function_widget(array('controller'=>'revir','action'=>'subnav'),$_smarty_tpl);?>

				<?php echo smarty_function_widget(array('controller'=>'contact','action'=>'box_widget'),$_smarty_tpl);?>

			</div>
		</div>
		<div class="large-9 columns float-left wow slideInUp">
		<?php  $_smarty_tpl->tpl_vars['revir_oblast'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['revir_oblast']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['reviry']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['reviry']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['revir_oblast']->key => $_smarty_tpl->tpl_vars['revir_oblast']->value){
$_smarty_tpl->tpl_vars['revir_oblast']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['revir_oblast']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['reviry']['iteration']++;
?>
			<h2 id="<?php echo $_smarty_tpl->tpl_vars['revir_oblast']->value['nav_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['revir_oblast']->value['nazev'];?>
</h2>
			<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true" role="tablist">
			<?php  $_smarty_tpl->tpl_vars['revir'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['revir']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['revir_oblast']->value['childs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['revir']->key => $_smarty_tpl->tpl_vars['revir']->value){
$_smarty_tpl->tpl_vars['revir']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['revir']->key;
?>
				<li class="accordion-item">
			       <a href="#collapse<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['reviry']['iteration'];?>
" role="tab" class="accordion-title" id="collapse<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['reviry']['iteration'];?>
-heading"><?php echo $_smarty_tpl->tpl_vars['revir']->value['nazev'];?>
</a>
			       <div id="collapse<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['reviry']['iteration'];?>
" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['reviry']['iteration'];?>
d-heading">
			       
			      <?php if (!empty($_smarty_tpl->tpl_vars['revir']->value['info'])||!empty($_smarty_tpl->tpl_vars['revir']->value['photo']['ad'])){?>
			       	 <div class="row">
			       		<div class="large-6 columns">
			       			<h4><?php echo $_smarty_tpl->tpl_vars['revir']->value['nadpis'];?>
</h4>
			       			<?php echo $_smarty_tpl->tpl_vars['revir']->value['popis'];?>

			       		</div>
			       		<div class="large-6 columns">
			       			<?php echo $_smarty_tpl->tpl_vars['revir']->value['info'];?>

			       			<?php if (!empty($_smarty_tpl->tpl_vars['revir']->value['photo']['ad'])){?>
			       				<img class="thumbnail" src="<?php echo $_smarty_tpl->tpl_vars['revir']->value['photo']['ad'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['revir']->value['nazev'];?>
">
							<?php }?>
			       		</div>
			       </div>
			       <?php }else{ ?>
			       <h4><?php echo $_smarty_tpl->tpl_vars['revir']->value['nadpis'];?>
</h4>
			       	<?php echo $_smarty_tpl->tpl_vars['revir']->value['popis'];?>

			       <?php }?>
			      
			       </div>
			    </li>
			<?php } ?>
			    
			</ul>
		<?php } ?>

		</div>
	</div>
</article>
<?php }} ?>