<?php /* Smarty version Smarty-3.1.11, created on 2016-05-18 07:30:30
         compiled from "/var/www/mrszlin.cz/domains/www/application/views/page/structured.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1933967077573bfdf69fc9e4-14581616%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cdf7aaf59df488c260c5c241275cb46adb5c1d16' => 
    array (
      0 => '/var/www/mrszlin.cz/domains/www/application/views/page/structured.tpl',
      1 => 1463476933,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1933967077573bfdf69fc9e4-14581616',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'item' => 0,
    'childs' => 0,
    'child' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_573bfdf6abe9b2_73349422',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573bfdf6abe9b2_73349422')) {function content_573bfdf6abe9b2_73349422($_smarty_tpl) {?><?php if (!is_callable('smarty_function_widget')) include '/var/www/mrszlin.cz/domains/www/modules/smarty/plugins/function.widget.php';
?><section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<h1><?php echo $_smarty_tpl->tpl_vars['item']->value['nadpis'];?>
</h1>
			<p>
				<?php echo $_smarty_tpl->tpl_vars['item']->value['uvodni_popis'];?>

			</p>
		</div>
	</div>
</section>

<article class="became-fisher" id="stickyPage">
	<div class="row">
		<div class="large-4 columns float-right" data-sticky-container>
			<div class="sticky" id="sticky" data-sticky data-options="data-sticky-on: large" data-anchor="stickyPage">
				<?php echo smarty_function_widget(array('controller'=>'page','action'=>'structured_subnav'),$_smarty_tpl);?>

				<?php echo smarty_function_widget(array('controller'=>'contact','action'=>'box_widget'),$_smarty_tpl);?>

			</div>
		</div>
		<div class="large-8 columns float-left">
		<?php  $_smarty_tpl->tpl_vars['child'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['child']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['childs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['child']->key => $_smarty_tpl->tpl_vars['child']->value){
$_smarty_tpl->tpl_vars['child']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['child']->key;
?>
			<div class="row" id="<?php echo $_smarty_tpl->tpl_vars['child']->value['nazev_seo'];?>
">
				<div class="small-12 columns">
					<h2 class="<?php echo $_smarty_tpl->tpl_vars['child']->value['nav_class'];?>
"><?php echo $_smarty_tpl->tpl_vars['child']->value['nadpis'];?>
</h2>
					<?php echo $_smarty_tpl->tpl_vars['child']->value['uvodni_popis'];?>

					<p>
						<?php if (!empty($_smarty_tpl->tpl_vars['child']->value['photo']['ad'])){?>
							<div class="large-5 float-right article__img">
								<img src="<?php echo $_smarty_tpl->tpl_vars['child']->value['photo']['ad'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['child']->value['nadpis'];?>
">
							</div>
							
						<?php }?>
						<?php echo $_smarty_tpl->tpl_vars['child']->value['popis'];?>

					</p>
				</div>
			</div>
			
		<?php } ?>
		</div>
	</div>
</article><?php }} ?>