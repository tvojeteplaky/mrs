<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

    'BASE_GPWEBPAY_URL'       => 'https://test.3dsecure.gpwebpay.com/webservices/services/pgw',
    'GPWEBPAY_WSDL'       => str_replace('\\', '/',DOCROOT).'modules/gpwebpay/wsdl/pgw-standard_test.wsdl',
    'BASE_URL_CREATE_ORDER_REQUEST'   =>'https://test.3dsecure.gpwebpay.com/rb/order.do',
    //'BASE_URL_CREATE_ORDER_REQUEST'   =>'https://3dsecure.gpwebpay.com/rb/order.do',
    'BASE_URL_CREATE_ORDER_RESPONSE'  =>'http://army.dgsbeta.cz/gpwebpay_response',
    'MERCHANTNUMBER'=>'9666164009', //53402484
    'PRIVATE_KEY_WEB' =>str_replace('\\', '/',DOCROOT).'application/files/gpwebpay/testarmy_private_key.pem',
    'PRIVATE_KEY_WEB_PASSWORD'=>'test',
    'PUBLIC_KEY_GPWEBPAY'=>str_replace('\\', '/',DOCROOT).'modules/gpwebpay/files/muzo.signing_test.pem',

);