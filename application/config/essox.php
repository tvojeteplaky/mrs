<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

    'BASE_ESSOX_URL'       => 'https://fltest.essox.cz/leshop', // TESTOVACI
    //'BASE_ESSOX_URL'       => 'https://eshop.essox.cz', //OSTRY
    'OM_NUMBER'       => 1201001, // TESTOVACI
    //'OM_NUMBER'       => 12155002, //OSTRY
    'USERNAME'   => 'apptest', //TESTOVACI
    //'USERNAME'       => '12155002armymarket', //OSTRY
    'CODE'  => 'essox2010', //TESTOVACI
    //'CODE',       => '6Lj08UYsg66hw6O', //OSTRY
);