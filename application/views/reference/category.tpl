{capture assign=subnav}
    {widget controller=reference action=subnav}
{/capture}
<div class="category" id="reference">
    <header class="text-center {if $item.photo_src}with-photo{/if}">
        {if $item.photo_src}
            <img alt="{$item.nazev} photo"
                 data-interchange="[{$media_path}photos/reference/category/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/reference/category/images-{$item.id}/{$item.photo_src}-header.jpg, large]">
        {/if}
        <div class="heading {if $item.header_right}to-right{/if}">
            <div class="row column">
                <div class="content {if !$item.header_right}text-center{else}text-left{/if}">
                    <h1>{$item.nazev|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">
        {if strlen(trim($subnav)) > 0}
            <div class="medium-3 small-12 columns">
                {$subnav}
            </div>
        {/if}
        <div class="medium-{if strlen(trim($subnav)) > 0}9{else}12{/if} small-12 columns">
            {$item.popis}
            <div class="row items">
                {foreach $items as $reference}
                    <div class="medium-6 small-12 columns item">
                        <a href="{$url_base}{$reference.route.nazev_seo}">
                            {if $reference.photo_src}
                                <img src="{$media_path}photos/reference/item/images-{$reference.id}/{$reference.photo_src}-big-preview.jpg" alt="{$reference.nazev} photo" class="float-center">
                            {/if}
                            <h5>{$reference.nazev}</h5>
                            {if $reference.date}
                                <span class="date">
                                {$reference.date|date_format:'%e. %m. %Y'}
                            </span>
                            {/if}
                        </a>
                    </div>
                {/foreach}
            </div>
            {if $item.show_contactform}
                <div class="contact-wrapper">
                    <h4>Máte zájem o naše služby?</h4>
                    {widget controller="contact" action="show"}
                </div>
            {/if}
        </div>
    </div>
</div>