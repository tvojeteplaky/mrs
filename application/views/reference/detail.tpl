{capture assign=subnav}
    {widget controller=reference action=subnav}
{/capture}
<div class="detail" id="reference">
    <header class="text-center">
        <div class="heading {if $item.header_right}to-right{/if}">
            <div class="row column">
                <div class="content {if !$item.header_right}text-center{else}text-left{/if}">
                    <h1>{$item.nazev|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">
        {if strlen(trim($subnav)) > 0}
            <div class="medium-3 small-12 columns">
                {$subnav}
            </div>
        {/if}
        <div class="medium-{if strlen(trim($subnav)) > 0}9{else}12{/if} small-12 columns">
            {if $item.photo_src}
                <img alt="{$item.nazev} photo"
                     data-interchange="[{$media_path}photos/reference/item/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/reference/item/images-{$item.id}/{$item.photo_src}-big.jpg, large]">

            {/if}
            {$item.popis}
            {widget controller="gallery" action="carousel_widget" param="full-preview"}
            <div class="contact-wrapper">
                <h4>Máte zájem o podobný projekt?</h4>
                {widget controller="contact" action="show"}
            </div>
        </div>
    </div>
</div>