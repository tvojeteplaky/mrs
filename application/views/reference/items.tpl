{foreach $items as $reference}
    <div class="item" data-id="{$reference.id}" data-category="{$reference.reference_category_id}">
        <div class="bg" style="background-image: url('{$media_path}photos/reference/item/images-{$reference.id}/{$reference.photo_src}-t2.png')"></div>
        <div class="info">
            <div class="display-table height">
                <div class="display-row">
                    <div class="display-cell middle">
                        <h3>{$reference.main_nadpis}</h3>
                        <h4>{$reference.nadpis}</h4>
                        <a href="#" class="transparent primary button detail auto">
                            <span class="helper">+</span>
                            <span class="abs">+</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/foreach}