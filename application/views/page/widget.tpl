<div class="row" data-equalizer>
	{foreach from=$items item=item key=key name=sluzby}
		<a href="{$url_base}{$item.nazev_seo}">
                <div class="large-4 medium-6 columns" data-equalizer-watch
                     data-waypoint
                     data-animation="fadeIn{if $smarty.foreach.sluzby.iteration % 3 == 0}Right{elseif $smarty.foreach.sluzby.iteration % 2 == 0}Up{else}Left{/if}">
                    <h2 class="headerWithInfo">
                        {$item.nadpis}
                        <br/>
                        <small>{$item.podnadpis}</small>
                    </h2>
                    <img src="{$item.photo}" alt="{$item.nadpis}"/>
                    <p>{$item.uvodni_popis}</p>
                    <span class="moreInfo">{translate str="Více info"}<i class="fa fa-angle-right"></i></span>
                </div>
            </a>
	{/foreach}
</div>