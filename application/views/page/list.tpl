<section id="indexHeader" class="parallax-window" data-parallax="scroll" data-image-src="{$item.banner.large}">
    <div class="row">
        <div class="large-12 medium-6 columns" data-waypoint data-animation="fadeIn">
            <h1>{$item.nadpis}</h1>
            <h2>{$item.podnadpis}</h2>
            <p>{$item.uvodni_popis}</p>
        </div>
        {*widget controller="article" action="widget"*}
    </div>
    <div class="service hide-for-medium-down">
        <div class="row" data-equalizer>
            {foreach from=$item.childrens_first item=child key=key name=serv}
                <a href="{$url_base}{$child.nazev_seo}">
                    <div class="large-4 columns" data-equalizer-watch
                         data-waypoint
                         {if $smarty.foreach.serv.last}
                             data-waypoint-next
                         {/if}
                         data-animation="fadeIn{if $smarty.foreach.serv.iteration % 3 == 0}Right{elseif $smarty.foreach.serv.iteration % 2 == 0}Up{else}Left{/if}">
                        <h2 class="headerWithInfo">
                            {$child.nadpis}
                            <br/>
                            <small>{$child.podnadpis}</small>
                        </h2>
                    </div>
                </a>
            {/foreach}
        </div>
    </div>
</section> <!--index header-->
<section class="service">
    <div class="row" data-equalizer>
        {foreach from=$item.childrens_first item=child key=key name=serv}
            <a href="{$url_base}{$child.nazev_seo}">
                <div class="large-4 columns" id="box-{$smarty.foreach.serv.iteration}" data-equalizer-watch
                     data-waypoint
                     {if !$smarty.foreach.serv.last}
                        data-next
                     {/if}
                     data-animation="fadeIn{if $smarty.foreach.serv.iteration % 3 == 0}Right{elseif $smarty.foreach.serv.iteration % 2 == 0}Up{else}Left{/if}">
                    <h2 class="headerWithInfo hide-for-large-up">
                        {$child.nadpis}
                        <br/>
                        <small>{$child.podnadpis}</small>
                    </h2>
                    <img src="{$child.photo}" alt=""/>
                    <p>{$child.uvodni_popis}</p>
                    <span class="moreInfo">{translate str="Více info"}<i class="fa fa-angle-right"></i></span>
                </div>
            </a>
        {/foreach}
    </div>
</section>
<section id="serviceList">
    <div class="row" data-waypoint data-animation="fadeInUp">
        <div class="small-12 columns">
            <h2 class="headerWithInfo">
                {$item.popis}
            </h2>
        </div>
    </div>
    <div data-waypoint data-animation="zoomIn">
    {foreach from=$item.childrens_all item=child key=key name=childrens}
        {if $smarty.foreach.childrens.first ||$smarty.foreach.childrens.index%3==0}
            <div class="row" data-equalizer>
        {/if}
        <a  href="{$url_base}{$child.nazev_seo}" class="large-4 columns item" data-equalizer-watch>
            <h3>{$child.nadpis}</h3>
            <p>{$child.uvodni_popis|strip_tags:false|truncate:150}</p>
            <span class="moreInfo">{translate str="Více info"}<i class="fa fa-angle-right"></i></span>
        </a>
        {if $smarty.foreach.childrens.last ||$smarty.foreach.childrens.iteration%3==0}
            </div>
        {/if}
    {/foreach}
    </div>
</section>
{literal}
    <script type="text/javascript">

        $(function(){
            getH1Title($("section#indexHeader h1").first().text(), function(header){
                $("section#indexHeader h1").html(header);
            });
        });
    </script>
{/literal}