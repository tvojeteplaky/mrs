{* Banner s nadpisem *}
<section class="hp__banner">
    <div class="row text-center">
        <div class="small-12 columns">
            <h1 class="wow slideInUp">{$item.nadpis}</h1>
            <div class="wow slideInUp">
            {$item.uvodni_popis}
            </div>
            <a href="#anchor-more-info" class="hp__banner-btn">Zjistěte o nás více</a>
            <a href="#anchor-more-info" title="Zjistěte o nás více" class="hp__banner-get-more"><img src="{$media_path}img/hp-banner-arrow.png"></a>
        </div>
    </div>
</section>

{* Boxy s fakty *}
<section class="hp__facts" id="anchor-more-info">
    <div class="row text-center">
        <h2>Krátká fakta o nás</h2>
        <div class="medium-4 columns medium-text-left small-text-center">
            <p><span class="after" id="countMembers">1500+</span>členů pobočného spolku Zlín</p>
        </div>
        <div class="medium-4 columns  medium-text-left small-text-center">
            <p><span class="after" id="countYears">93let</span>organizovaného sportovního <br> rybolovu ve Zlíně</p>
        </div>
        <div class="medium-4 columns  medium-text-left small-text-center">
            <p><span id="countRace">100+</span>proškolených nových rybářů ročně</p>
        </div>
    </div>
</section>


{* O nás widget *}
<section class="hp__about-us">
    <div class="row">
        <div class="medium-12 text-center wow slideInUp">
            {static_content code="o-nas-box"}
            <div class="mb-xxl">
            {if !empty($owner.facebook)}
                <a href="{$owner.facebook}" title="Navštivte náš facebook" class="hp_about-us__fb-btn">Navštivte náš facebook</a>
            {/if}
            {if !empty($owner.instagram)}
                <a href="{$owner.instagram}" title="Navštivte náš intagram" class="hp_about-us__int-btn">Navštivte náš instagram</a>
            {/if}
            </div>
            <a href="{$url_base}o-nas" title="Zjistěte více" class="hp_button-more">Zjistěte více</a>
        </div>
    </div>
</section>

{* Novinky widget *}
{widget controller="article"}

{* Poslední nahrané fotky *}
<section class="hp__photos">
    <div class="row text-center">
        <h2>Poslední přidané fotografie</h2>
        {widget controller="newsletter"}
        
        {widget controller="gallery"}
    </div>
</section>
