{if !empty($items)}
    <ul class="main-submenu">
        {foreach $items as $item}
            <li>
                <a  class="slide" href="#{$item.nazev_seo}" title="{$item.nazev}">
                    {$item.nazev}
                </a>
            </li>
        {/foreach}
    </ul>
{/if}