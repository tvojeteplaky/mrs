<section id="indexInfo"  class="parallax-window" data-parallax="scroll" data-image-src="{$item.banner.large}">
	<div class="row" data-waypoint data-animation="fadeInDown">
		<div class="small-12 columns">
			<h2 class="headerWithInfo">{$item.nadpis}
				<br/>
	                    <small>{$item.podnadpis}</small>
                    </h2>

		<div class="row">
			{$item.uvodni_popis}
		</div>
		<a href="{$url_base}{$item.nazev_seo}"><span class="moreInfo">{translate str="Více info"}<i class="fa fa-angle-right"></i></span></a>
		</div>
	</div>
 </section>