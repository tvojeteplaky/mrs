<section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1>{$item.nadpis}</h1>
            <p>
                {$item.uvodni_popis}
            </p>
        </div>
    </div>
</section>
 {if not empty($item.files)}
 <section class="download">
            <div class="row small-up-1 medium-up-2 large-up-3">
            {foreach from=$item.files item=file key=key name=page_files}
                <div class="column">
                    <div class="item pdf">
                        <h2>{$file.nazev}</h2>
                        <a href="{$file.file}"class="button" download="download">Stáhnout soubor</a>
                    </div>
                </div>
            {/foreach}
            </div>
        </section>
{else}
<article id="stickyPage">
    <div class="row">
        <div class="large-4 columns float-right wow slideInRight" data-sticky-container>
            <div class="sticky" id="sticky" data-sticky data-options="data-sticky-on: large" data-anchor="stickyPage">
                {widget controller=page action=subnav}
                {widget controller=contact action=box_widget}
            </div>
        </div>
        <div class="large-8 columns float-left wow slideInUp">
           {$item.popis}
        </div>
    </div>
</article>
{/if}
