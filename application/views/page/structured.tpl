<section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<h1>{$item.nadpis}</h1>
			<p>
				{$item.uvodni_popis}
			</p>
		</div>
	</div>
</section>

<article class="became-fisher" id="stickyPage">
	<div class="row">
		<div class="large-4 columns float-right" data-sticky-container>
			<div class="sticky" id="sticky" data-sticky data-options="data-sticky-on: large" data-anchor="stickyPage">
				{widget controller=page action=structured_subnav}
				{widget controller=contact action=box_widget}
			</div>
		</div>
		<div class="large-8 columns float-left">
		{foreach from=$childs item=child key=key name=name}
			<div class="row" id="{$child.nazev_seo}">
				<div class="small-12 columns">
					<h2 class="{$child.nav_class}">{$child.nadpis}</h2>
					{$child.uvodni_popis}
					<p>
						{if !empty($child.photo.ad)}
							<div class="large-5 float-right article__img">
								<img src="{$child.photo.ad}" alt="{$child.nadpis}">
							</div>
							
						{/if}
						{$child.popis}
					</p>
				</div>
			</div>
			
		{/foreach}
		</div>
	</div>
</article>