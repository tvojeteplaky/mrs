<div style="font-family: sans-serif; font-size: 16px; margin: 0 auto; max-width: 62.5rem; width: 100%; padding: 0.978rem">
    <div style="float: left">
        <a href=""><img src="{$media_path}img/logo.png" alt="{$web_owner.firma} logo"></a>
    </div>
    <div style="float: right">
        {$web_owner.tel}
    </div>
    <div style="clear: both"></div>
</div>
<div style="margin-bottom: 1rem; font-family: sans-serif; font-size: 16px">
    <div style="background-color: #f2f2f2; margin: 0 auto; max-width: 62.5rem; width: 100%; padding: 0.978rem">

        {if $item.photo_src}
            <div style="float: right; padding: 0 0 1rem 1rem">
                <img src="{$media_path}photos/article/item/images-{$item.id}/{$item.photo_src}-t1.png" alt="{$item.nazev} photo">
            </div>
        {/if}


        <h3>{$item.nadpis}</h3>

        {$item.uvodni_popis}
        <div style="clear: both"></div>

    </div>
</div>

<div style="padding: 0.978rem; margin: 0 auto; max-width: 62.5rem; width: 100%">
    <div style="width: 100%">
        {$item.popis}
    </div>
</div>
<div style="background: rgb(245, 245, 245); font-family: sans-serif; font-size: 16px; margin: 0 auto; max-width: 62.5rem; width: 100%; padding: 0.978rem">
    <div>
        Odhlásit z odběru newsletteru se můžete <a href="/newsletter_unsubscribe?user_h={$email_hash}">zde</a>.
    </div>
</div>