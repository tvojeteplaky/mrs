<div style="background: white; font-family: sans-serif; font-size: 16px; margin: 0 auto; max-width: 62.5rem; width: 100%; padding: 0.978rem">
    <div style="float: left">
        <a href=""><img src="http://mrszlin.dgsbeta.cz/media/img/logo.png" alt="{$web_owner.firma} logo"></a>
    </div>
    <div style="float: right">
        {$web_owner.tel}
    </div>
    <div style="clear: both"></div>
</div>
<div style="margin-bottom: 1rem; font-family: sans-serif; font-size: 16px">
    <div style="background-color: #f2f2f2; margin: 0 auto; max-width: 62.5rem; width: 100%; padding: 0.978rem">

        <h3>{$item.nazev}</h3>

        {$item.popis}

    </div>
</div>
<div style="background: rgb(245, 245, 245); font-family: sans-serif; font-size: 16px; margin: 0 auto; max-width: 62.5rem; width: 100%; padding: 0.978rem">
    <div>
        Odhlásit z odběru newsletteru se můžete <a href="/newsletter_unsubscribe?user_h={if !empty($email_hash)}{$email_hash}{/if}">zde</a>.
    </div>
</div>
