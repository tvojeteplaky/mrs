{* sablona produktoveho carouselu - widget *}
<div class="our-offer">
    <div class="row">
        <div class="columns right">
            <h2>{translate str="Naše nabídka"}</h2>
            <div class="categories">
                <div class="row collapse">
                    {foreach from=$categories item=cat key=key name=top_cats}
                    <div class="large-3 medium-3 columns {if $smarty.foreach.top_cats.last}end{/if}">
                        <div class="category">
                            <div class="row collapse">
                                <div class="large-5 medium-12 small-5 columns">
                                    <a href="{$url_base}{$cat.nazev_seo}"><img alt="{$cat.nazev}" src="{$cat.photo}"></a>
                                </div>
                                <div class="large-7 medium-12 small-7 end columns">
                                    <a href="{$url_base}{$cat.nazev_seo}"><h3><span>{$cat.nazev}</span></h3></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>