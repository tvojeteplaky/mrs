
      <div class="products">
            <div class="row collapse" data-equalizer>
                {foreach from=$products item=product key=key name=name}
                <div class="large-4 medium-4 columns">
                            <div class="product"  data-equalizer-watch>
                            <div class="photo"> 
                                <a href="{$product.nazev_seo}"><img src="{$product.photo}" alt=""></a>
                            </div>
                                <a href="{$product.nazev_seo}"><h3>{$product.nazev}</h3></a>

                                <div class="row collapse">
                                    <div class="columns">
                                        <div class="price">
                                        {if $product.puvodni_cena>0 && $product.puvodni_cena>$product.cena_s_dph}
                                            <div class="old-price">Původní cena: {$product.puvodni_cena|currency:$language_code}</div>
                                        {/if}
                                            <div class="new-price">{$product.cena_s_dph|currency:$language_code}</div>
                                        
                                        </div>
                                    </div>
                                    <div class="columns bottom info">
                                        <a class="button" href="#">Koupit nyní</a>
                                    </div>
                                </div>

                                {if $product.percentage_discount>0}
                                    <div class="sale">-{(int)$product.percentage_discount}%</div>
                                {/if}
                                {if $product.action||$product.new||$product.sale_off}
                                    <div class="action">
                                    {if $product.action}
                                        Akční cena
                                    {elseif $product.new}
                                        Novinka
                                    {elseif $product.sale_off}
                                        Výprodej
                                    {/if}
                                    </div>
                                {/if}
                            </div>
                            </div>
                        
                    {*<div class="large-4 medium-6 columns">
                    <div class="product">
                        <a href="{$product.nazev_seo}">
                            <img src="{$product.photo}">
                        </a>

                        <div class="row">
                            <div class="columns">
                                <div class="old-price">{$product.puvodni_cena|currency:$language_code}</div>
                                <h2>{$product.cena_s_dph|currency:$language_code}</h2>

                                <div class="stored">{if $product.pocet_na_sklade > 0}Skladem{else}Není na skladě{/if}</div>
                                <a href="{$product.nazev_seo}" class="button">{translate str="Více informací"}</a>
                            </div>
                        </div>
                    </div>
                </div>*}
                {/foreach}


            </div>
        </div>