
  <div id="ProductDetailSection">
      <h1>{$item.nazev}</h1>
         <div class="photo defaultBox">
           
            {if !empty($item.photo_detail)}
              <img src="{$item.photo_detail}" alt="{$item.nazev}" title="{$item.nazev}" />
            {else}
            {/if}

	    
	    
           
         </div>
         
         <div class="info">
         <div class="pretext">
           {$item.uvodni_popis}
         </div>
         
         {if $item.k_prodeji}
         <div class="defaultTabs">
            <ul>
              <li><a href="#tabs-1">Informace</a></li>
              {if $item.product_action_type==1}
              <li><a href="#tabs-2">Akce</a></li>
              {else if $item.product_action_type==2}
              <li><a href="#tabs-2">Novinka</a></li>
              {else if $item.product_action_type==3}
              <li><a href="#tabs-2">Dárek</a></li>
              {/if}
              
            </ul>
            
            <div id="tabs-1">
               <div class="cart">
                  {if !$item.gift}
                  <form action="" method="post" class="cart_form">
                    <div class="priceBox">
                       <div class="left">
                         <div class="price">{$item.cena_s_dph|currency:$language_code} <span>s DPH</span></div>
                         <div class="priceNoTax">{$item.cena_bez_dph|currency:$language_code} bez DPH</div>
                         {*<div class="priceNoTax">{$item.cena_bez_dph|currency:$language_code}</div>*}
                       </div>
                       <div class="right">
                         {static_content code="detail-produktu-doprava-text"}
                       </div>
                    </div>
                    
                  
                  {*
                  <table border="0">
                    <tr>
                      <td class="col1">Cena bez DPH</td>
                      <td><span class="price">{$item.cena_bez_dph|currency:$language_code}</span></td>
                    </tr>
                    <tr>
                      <td><span class="bold red">Cena s DPH</span></td>
                      <td><span class="bold red">{$item.cena_s_dph|currency:$language_code}</span></td>
                    </tr>
                    <tr>
                      <td>Množství</td>
                      <td><input type="text" value="1" name="quantity" class="txt" /></td>
                    </tr>
                  </table>
                    {hana_secured_post action="add_item" module="shoppingcart"}
                    <input type="hidden" name="product_id" value="{$item.id}" />
                    <input type="submit" name="add_item" class="subm" value="vložit do košíku" />
                  *}
                                      
                  </form>
                  {/if}
               </div>
               
            </div>
            
            
            
            {if $item.product_action_type==1 || $item.product_action_type==2 || $item.product_action_type==3}
            <div id="tabs-2">
               <div class="padding10">
               {$item.akce_text}
               </div>
            </div>
            {/if}
            
           </div> 
            
            
            <div class="bottom"></div>
          {/if} 
     
         

         
      </div>
      
      <div class="correct"></div>
      <div class="description">
         <a name="Commentsform"></a>
         <div class="defaultTabs">
            <ul>
              <li><a href="#tabs-11">Popis</a></li>
              {if $item.k_prodeji}
              {*<li><a href="#tabs-12" class="tabs-12">Poradna</a></li> *}
              {/if}
              {if !empty($item.odborne_informace)}<li><a href="#tabs-13">Informace pro odbornou veřejnost</a></li>{/if}
            </ul>
            
            <div id="tabs-11">
              <div class="padding10">
              {$item.popis}
              </div>
            </div>
            
            {if $item.k_prodeji}
            {*
            <div id="tabs-12">
              <div class="padding10">
              {widget action="index" controller="comments" param=$item.id}
              {if $shopper.id}  
                {widget action="show_form" controller="comments" param=$item.id}
              {else}
                <div class="noregComments">Příspěvky mohou vkládat pouze přihlášení uživatelé.</div>
              {/if}
              </div>
              <div class="correct"></div>
            </div>
            *}
            {/if}
            
            {if !empty($item.odborne_informace)}
            <div id="tabs-13">
              <div class="confirmation">
              Následující stránky jsou určeny pouze pro odborné pracovníky ve zdravotnictví, tj. osobám oprávněným předepisovat a vydávat léky. Zde naleznete odkaz na Veřejně přístupnou odbornou informační službu pro odbornou zdravotnickou veřejnost, souhrny údajů o přípravku a různé potřebné doplňkové služby, jako poskytování odborných informací, zajímavé odkazy apod. 
              <br /><br />
              <a href="#" class="right button buttonDefault">Potvrzuji, že jsem specialista</a>
              </div>
              <div class="afterConfirmation noDisplay">
              {$item.odborne_informace}
              </div>
            </div>
            {/if}
            
            <div class="bottom"></div>
        
      
      </div>
      
      
      {*
      <h2 class="bigMT">{translate str="Dotaz na produkt"}</h2>
      
      {widget action="show" controller="contact" name="contactform"}
      *}
   </div>
   
</div>

{literal}
  <script type="text/javascript"> 
  $(document).ready(function(){
    $(".defaultTabs").tabs();
  });
  </script>
{/literal}