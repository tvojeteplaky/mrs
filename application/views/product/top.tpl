{if !empty($products)}
<div class="top-offer">
    <div class="row">
        <div class="columns">
            <h1>{translate str="Top nabídka"}</h1>
            <div class="products">
                <div class="row collapse" data-equalizer>
                    {foreach name=product from=$products key=key item=item}
                    <div class="large-4 medium-4 columns {if $smarty.foreach.product.last}end{/if}">
                        <div class="product" data-equalizer-watch>
                        <div class="photo">
                            <a href="{$url_base}{$item.nazev_seo}">{if isset($item.photo)}<img src="{$item.photo}" alt="{$item.nazev}">{/if}</a>
                        </div>
                            <a href="{$url_base}{$item.nazev_seo}"><h3>{$item.nazev}</h3></a>
                            <div class="row collapse">
                                        <div class="columns">
                                            <div class="price">
                                                {if $item.puvodni_cena >0 && $item.puvodni_cena>$item.cena_s_dph}
                                                    <div class="old-price">Dříve: {$item.puvodni_cena|currency:$language_code}</div>
                                                {/if}
                                                <div class="new-price">{$item.cena_s_dph|currency:$language_code}</div>
                                            </div>
                                        </div>
                                    </div>
                            {if $item.percentage_discount>0}
                            <div class="sale">-{(int)$item.percentage_discount}%</div>
                            {/if}
                            {if $item.action||$item.new||$item.sale_off}
                            <div class="action">
                                {if $item.action}
                                Akční cena
                                {elseif $item.new}
                                Novinka
                                {elseif $item.sale_off}
                                Výprodej
                                {/if}
                            </div>
                            {/if}
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
{/if}