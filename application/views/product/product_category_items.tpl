{* vnitrni vypis kategorii *}
{if !empty($products)}
<div class="products">
    <div class="show-for-large-up">
        {foreach name=product from=$products key=key item=item}
        {if $smarty.foreach.product.first || $smarty.foreach.product.index%3 eq 0}
        <div class="row collapse" data-equalizer>
            {/if}
            <div class="large-4 columns">
                <div class="product" data-equalizer-watch >
                    {if !empty($item.photo)}
                    <div class="photo">
                        <a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
                        <img src="{$item.photo}" alt="{$item.nazev}">
                        </a>
                    </div>
                    {/if}
                    <a href="{$url_base}{$item.nazev_seo}"><h3>{$item.nazev}</h3></a>
                    {if $item.k_prodeji}
                    <div class="row collapse">
                        <div class="columns bottom info">
                            <div class="row">
                                <div class="columns">
                                    <div class="price">
                                        {if $item.puvodni_cena>$item.cena_s_dph}
                                        <div class="old-price">{$item.puvodni_cena|currency:$language_code}</div>
                                        {/if}
                                        <div class="new-price">{$item.cena_s_dph|currency:$language_code}</div>
                                    </div>
                                </div>
                                <div class="columns">

                                    <a class="button" href="{$url_base}{$item.nazev_seo}">Koupit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    {if $item.percentage_discount>0}
                    <div class="sale">-{(int)$item.percentage_discount}%</div>
                    {/if}
                    {if $item.action||$item.new||$item.sale_off}
                    <div class="action"> {if $item.action}
                        Akční cena
                        {elseif $item.new}
                        Novinka
                        {elseif $item.sale_off}
                        Výprodej
                    {/if}</div>{/if}
                    <div class="stage">{if ($item.in_stock<1)}{translate str="Na objednání"}{else}{translate str="Skladem"}{/if}</div>
                    {/if}
                </div>
            </div>
            {if $smarty.foreach.product.last || $smarty.foreach.product.iteration%3 eq 0}
        </div>
        {/if}
        {/foreach}
    </div>
    <div class="hide-for-large-up">
        {foreach name=product from=$products key=key item=item}
        {if $smarty.foreach.product.first || $smarty.foreach.product.index%2 eq 0}
        <div class="row collapse" data-equalizer>
            {/if}
            <div class="medium-6 columns">
                <div class="product" data-equalizer-watch>
                    {if !empty($item.photo)}
                    <a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
                    <img src="{$item.photo}" alt="{$item.nazev}">
                    </a>
                    {/if}
                    <a href="{$url_base}{$item.nazev_seo}"><h3>{$item.nazev}</h3></a>
                    {if $item.k_prodeji}
                    <div class="row collapse">
                        <div class="columns">
                            <div class="price">
                                {if $item.puvodni_cena>$item.cena_s_dph}
                                <div class="old-price">{$item.puvodni_cena|currency:$language_code}</div>
                                {/if}
                                <div class="new-price">{$item.cena_s_dph|currency:$language_code}</div>
                            </div>
                        </div>
                        <div class="columns">
                            <a class="button" href="{$url_base}{$item.nazev_seo}">Koupit</a>
                        </div>
                    </div>
                    {if $item.percentage_discount>0}
                    <div class="sale">-{(int)$item.percentage_discount}%</div>
                    {/if}
                    {if $item.action||$item.new||$item.sale_off}
                    <div class="action"> {if $item.action}
                        Akční cena
                        {elseif $item.new}
                        Novinka
                        {elseif $item.sale_off}
                        Výprodej
                    {/if}</div>{/if}
                    {/if}
                </div>
            </div>
            {if $smarty.foreach.product.last || $smarty.foreach.product.iteration%2 eq 0}
        </div>
        {/if}
        {/foreach}
    </div>
</div>
{/if}
{literal}
<script type="text/javascript">
    $(document).foundation();
</script>
{/literal}
{*<div class="col-md-4 col-sm-12"><div class="item ">
                                    <div class="itemIN">
                                                            <div class="photo">
                                                                                    {if !empty($item.photo)}
                                                                                        <a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}"><img class="img-responsive" src="{$item.photo}" alt="{$item.nazev}" /></a>
                                                                                    {else}
                                                                                    {/if}
                                                            </div>
                                                            <h2><a href="{$url_base}{$item.nazev_seo}">{$item.nazev}</a></h2>
        {*
                                                            <div class="content">
                                                                                {$item.uvodni_popis}
                                                            </div>
        *}
        {*if $item.k_prodeji}
                                                                <div class="row">
                                                                                        <div class="price col-md-6 col-sm-6  col-xs-6">
                <span>{$item.cena_s_dph|currency:$language_code}</span></br>*}{* s DPH<br />*}
                {*if $item.puvodni_cena>$item.cena_s_dph}
                <span class="old_price">  {$item.puvodni_cena|currency:$language_code}</span> *}{* s DPH<br />*}
                {*<span class="noDph">{$item.cena_bez_dph|currency:$language_code}</span> bez DPH<br />*}
                {*/if}
                                                                                        </div>
                                                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                                                <form action="" method="post" class="cart_form">
                                                                                                                                        {hana_secured_post action="add_item" module="shoppingcart"}
                                                                                                                                        <input type="hidden" name="product_id" value="{$item.id}" />
                                                                                                                                        <input type="hidden" value="1" name="quantity" class="txt" />
                                                                                                                                        <input type="submit" name="add_item" class="subm addToCart" value="DO KOŠÍKU" />
                                                                                                                </form>
            </div>*}
            {*<a href="{$url_base}{$item.nazev_seo}" class="addToCart">Do košíku</a>*}
        {*</div>
                                                            {/if}
                                                            <div class="indicator product-detail">
                                                                                    {if $item.product_action_type == 1 || $item.action}
                                                                                        <div class="action act_1" title="{translate str="Akce"}">{$item.akce_text}</div>
                                                                                    {/if}
                                                                                    {if $item.product_action_type == 2 || $item.new}
                                                                                        <div class="action act_2" title="{translate str="Novinka"}">{$item.akce_text}</div>
                                                                                    {/if}
                                                                                    {if $item.product_action_type == 4 || $item.sale_off}
                                                                                        <div class="action act_4" title="Výprodej">{$item.akce_text}</div>
                                                                                    {/if}
                                                            </div>
                                                            {if $item.product_action_type == 3 || $item.gift}
                                                                <div class="gift" title="{translate str="Dárek"}">{$item.akce_text}</div>
                                                            {/if}
                                                            {if $item.pocet_na_sklade > 0}
                                                                <div class="stock_indicator stock" title="Na skladě"></div>
                                                            {else}
                                                                <div class=" stock_indicator nostock" title="Není na skladě {$item.pocet_na_sklade}"></div>
                                                            {/if}
        *}
        {*
                                                            <div class="footer">
                                                                                <form action="" method="post" class="cart_form">
                                                                                                        {hana_secured_post action="add_item" module="shoppingcart"}
                                                                                                        <input type="hidden" name="product_id" value="{$item.id}" />
                                                                                                        <input type="text" value="1" name="quantity" class="txt" />
                                                                                                        <input type="submit" name="add_item" class="subm" value="vložit do košíku" />
                                                                                </form>
                                                            </div>
        *}
    {* </div>
            </div>
        </div>
        {/foreach}
    </div>
{literal}
<script type="text/javascript">
        $(".cart_form").submit(function(e){
            $.ajax({
                type: "POST",
                data: $(this).serialize(),
                //url: "url::base().url::current().Kohana::config('config.url_suffix').",
                success: function(response){
                    var json = eval("(" + response + ")");
                    $('#MessageWidget').html(json.MessageWidget);
                    //console.log("json : "+json);
                    //for (var key in json) {
                    //   $('#'+key).html(json[key]);
                    //}
                    $("#ShoppingCartPopup form").html(json["main_content"]);
                    $('#MinicartWidget').html(json.MinicartWidget);
                    $('.bs-example-modal-lg').modal('show');
                    // $("#ShoppingCartPopup").bPopup({
                    // closeClass:'closeShC',
                    //   });
                    //
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    //alert("Chyba: "+textStatus);
                    console.log("Chyba ajaxu: "+textStatus);
                }
            });
            return false;
        });
</script>
{/literal}
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div id="ShoppingCartPopup" class="">
        <form action="" method="post">
        </form>
        <a class="btn-army btn btn_full" onclick="$(document).ready(function() { $('.bs-example-modal-lg').modal('hide');})" href="">ZPĚT DO ESHOPU</a>
        <a class="btn-army btn right_close btn_full" href="{$url_base}nakupni-kosik">K POKLADNĚ</a>
    </div>
</div>
</div>
{literal}
<script type="text/javascript">
                $(".refresh").submit(
                    function(e)
                    {
                        //e.preventDefault();
                        if($(this).is("input"))
                        {
                            $("#TempProperty").attr("name",$(this).attr("id"));
                            $.ajax({
                                type: "POST",
                                data: $("#ShoppingCartPopup form").serialize(),
                                //url: "url::base().url::current().Kohana::config('config.url_suffix').",
                                success: function(response){
                                    var json = eval("(" + response + ")");
                                    var form = json["main_content"];
                                    if(form==""){
                                        $('.bs-example-modal-lg').modal('hide');
                                        //$(".closeShC").click();
                                    }else{$("#ShoppingCartPopup form").html(form);
                                        $('.bs-example-modal-lg').modal('show');
                                    }
                                    $('#MinicartWidget').html(json.MinicartWidget);
                                    //alert('test');
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown){
                                    //alert("Chyba: "+textStatus);
                                    console.log("Chyba ajaxu: "+textStatus);
                                }
                            });
                            return false;
                        }
                        else
                        {
                            $.ajax({
                                type: "POST",
                                url: $(this).attr("href"),
                                success: function(response){
                                    var json = eval("(" + response + ")");
                                    var form = json["main_content"];
                                    if(form==""){
                                        $('.bs-example-modal-lg').modal('hide');
                                        //$(".closeShC").click();
                                    }else
                                    {$("#ShoppingCartPopup form").html(form);
                                        $('.bs-example-modal-lg').modal('show');
                                    }
                                    $('#MinicartWidget').html(json.MinicartWidget);
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown){
                                    console.log("Chyba ajaxu: "+textStatus);
                                }
                            });
                            return false;
                        }
                    }
                );
</script>
{/literal}
</div>
{/if*}