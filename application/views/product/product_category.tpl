{* vypis kategorie produktu - vnejsi obalujici sablona *}
<div id="ProductListSection">
    <div class="content-products">
        <div class="row">
            <div class="large-3 columns">
                {widget controller="product" action="category_subnav"}
            </div>
            <div class="large-9 columns">
                <h1>{if not empty($category.nazev)}{$category.nazev}{elseif not empty($page.nazev)}{$page.nazev}{/if}</h1>
                <div class="intro">
                    <div class="row">
                        <div class="large-6 large-push-6 medium-6 medium-pull-6 columns">
                            {if not empty($category.uvodni_popis)}{$category.uvodni_popis} <a href="#">{translate str="Čtěte více&raquo;"}</a>{/if}
                        </div>
                        <div class="large-6 large-pull-6 medium-6 medium-push-6 columns">
                            <img src="{if not empty($category.photo)}{$category.photo}{elseif not empty($page.photo_detail)}{$page.photo_detail}{/if}" alt="{if not empty($category.photo)}{$category.nazev}{elseif not empty($page.photo_detail)}{$page.nazev}{/if}">
                        </div>
                    </div>
                </div>
                {if not empty($category.id)}
                {widget controller="product" action="category_widget" param=$category.id}
                {else}
                {widget controller="product" action="category_widget" param=0}
                {/if}
                {if empty($category) || (!$empty_category && !$category.no_show_products) }
                <form action="{if isset($current_page)}{$current_page}{/if}" method="get" id="ProductListForm">
                    <div class="filters-container">
                        <div class="row">
                            <div class="columns">
                                <div class="filters">
                                    <div class="row collapse">
                                        <div class="large-5  medium-6 columns">
                                            <div class="order">
                                                <span>Řadit dle:</span>
                                                <div class="order-box">Cena</div>
                                                {*<div class="order-box">
                                                    <a id="name" class="img_click sort {if ($products_order=="nazev" && $sort=="asc") || (!$products_order && !$sort)}topar{else}downar{/if}" data-sort="{if ($products_order=="nazev" && $sort=="asc") || (!$products_order && !$sort)}nazev:desc{else}nazev:asc{/if}" href="#">Název</a>
                                                </div>*}
                                                <div class="order-box">
                                                <select id="price" class="sort" placeholder="Ceny">
                                                    <option value="products.poradi:asc">---</option>
                            
                                                    <option value="price:asc"{if ($products_order=="price" && $sort=="asc") || (!$products_order && !$sort)} selected="selected"{/if}>Od nejlevnějších</option>
                                                    <option value="price:desc"{if ($products_order=="price" && $sort=="desc")} selected="selected"{/if}>Od nejdražších</option>
                                                </select>
                                                    {*<a id="price" class="img_click sort {if ($products_order=="price" && $sort=="asc") || (!$products_order && !$sort)}topar{else}downar{/if}" data-sort="{if ($products_order=="price" && $sort=="asc") || (!$products_order && !$sort)}price:desc{else}price:asc{/if}" href="#">Cena</a>*}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-4 medium-5 columns">
                                            <div class="row collapse">
                                                <div class="order">
                                                    <label><input type="checkbox"  name="stock" class="sklaack" value="stock"{if !empty($stock)} checked="checked"{/if}>Skladem</label>
                                                    <label><input type="checkbox" name="action" class="sklaack" value="action"{if !empty($action)} checked="checked"{/if}>Akce</label>
                                                    <label><input type="checkbox" name="new" class="sklaack" value="new" {if !empty($new)} checked="checked"{/if}>Novinky</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="large-3 medium-1 columns">
                                            <div class="products_show_pages">
                                                <div class="row">
                                                    <div class="ĺarge-8 medium-8 columns"><label class="right">
                                                        Zobrazit produktů:
                                                    </label></div>
                                                    <div class="ĺarge-4 medium-4 columns">
                                                        <select name="products_show_pages" class="showmuch" >
                                                            <option value="15" {if $items_per_page == 15}selected{/if}>15</option>
                                                            <option value="30" {if $items_per_page == 30}selected{/if}>30</option>
                                                            <option value="60" {if $items_per_page == 60}selected{/if}>60</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="large-12 columns">
                                            <div class="row">
                                                <div class="large-2 columns">
                                                    <span class="right">Cenový rozsah:</span>
                                                </div>
                                                <div class="large-1 columns">
                                                    <span class="min right">{$price_selected_min}</span>
                                                </div>
                                                <div class="large-7 columns">
                                                    <div id="range"></div>
                                                </div>
                                                <div class="large-1 end columns">
                                                    <span class="max left">{$price_selected_max}</span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="sort" value="{if (!$products_order && !$sort)}products.poradi:asc{else}{$products_order}:{$sort}{/if}" />
                    <input type="hidden" name="price_selected_min" value="{$price_selected_min}" id="amount">
                    <input type="hidden" name="price_selected_max" value="{$price_selected_max}" id="amount1">
                </form>
                {/if}
                {*{if !$empty_category && !$category.no_show_products}
                                                <form action="{if isset($current_page)}{$current_page}{/if}" method="get" id="ProductListForm">
                                                            <div class="filters-basic">
                                                                        <div class="row">
                                                                                    <div class="large-4 medium-4 columns">
                                                                                                <label>Řadit dle: </label> {if ($products_order=="price" && $sort=="asc") || (!$products_order && !$sort)}
                                                                                                <span class="img_click"><img id="cena" class="" data-sort='price:desc' src="{$media_path}img/bgr_subnav_sel2.png" />
                                                                                                {else}
                                                                                                <span class="img_click"><img id="cena" class="img_click" data-sort='price:asc' src="{$media_path}img/bgr_subnav_sel.png" />
                                                                                                {/if}
                                                                                                Ceny</span>
                                                                                                {if ($products_order=="nazev" && $sort=="asc") || (!$products_order && !$sort)}
                                                                                                <span class="img_click"><img id="cena"  class="img_click" data-sort='nazev:desc' src="{$media_path}img/bgr_subnav_sel2.png" />
                                                                                                {else}
                                                                                                <span class="img_click"><img id="cena"  class="img_click" data-sort='nazev:asc' src="{$media_path}img/bgr_subnav_sel.png" />
                                                                                                {/if}
                                                                                                Názvu</span>
                                                                                    </div>
                                                                                    <div class="large-4 medium-4 columns">
                                                                                                <div class="slider" style="float: left">
                                                                                                            <input type="text" name="price_selected_min" class="amount" value="" id="amount" readonly="readonly" data-autosize-input='{ "space": 10 }'  />
                                                                                                            <div class="slider-range"><div id="slider-range"></div></div>
                                                                                                            <input type="text" name="price_selected_max" class="amount" value="" id="amount1" readonly="readonly" />
                                                                                                </div>
                                                                                    </div>
                                                                                    <div class="large-4 medium-4 columns">
                                                                                                <label><input type="checkbox" class="sklaack" id="stock" name="stock" value="stock"{if !empty($stock)} checked="checked"{/if}/>
                                                                                    Skladem</label>
                                                                                    <label><input type="checkbox" class="sklaack" id="action" name="action" value="action"{if !empty($action)} checked="checked"{/if}/>
                                                                        Akce</label>
                                                                        <label><input type="checkbox" class="sklaack" id="new" name="new" value="new" {if !empty($new)} checked="checked"{/if}/>
                                                            Novinky</label>
                                                </div>
                                    </div>
                                    <input type="hidden" name="sort" value="products.poradi:asc" />
                        </div>
        {/if}*}
        <div class="row collapse">
            <div class="columns">
                <div class="right count"><small>{$total_items} {if $total_items==1}položka{elseif $total_items>0 && $total_items<5}položky{else}položek{/if}</small></div>
            </div>
        </div>
        {if not empty($product_category_items)}
        {$product_category_items}
        {/if}
    </div>
</div>
<div class="row">
    <div class="columns">
        <div class="pagination center">
            {$pagination}
        </div>
    </div>
</div>
</div>
</div>
<div class="our-offer">
{if isset($category.popis)}
<div class="row">
<div class="columns">
    {$category.popis}
</div>
</div>
{/if}
</div>
{*widget action="homepage_list" controller="article"*}

{* obsluzny js pro celou kategorii produktu *}
{literal}
<script>
function sendForm(){
//$("#ProductListForm").submit();
//  $(".products").showLoading();
$.ajax({
type: "GET",
data: $("#ProductListForm").serialize(),
url: "{/literal}{if isset($current_page)}{$current_page}{/if}{literal}#menufix",
success: function(response){
var json = eval("(" + response + ")");
$('#page-title').html(json.page_title);
//$('#page-description').html(json.page_description);
//$('#page-keywords').html(json.page_keywords);
$('.products').html(json.main_content);
//$('#ShoppingSection').html(json.cart_widget);
//$('#ShopperSection').html(json.user_widget);
// $("#CommoditySectionList .products").hideLoading();
},
error: function(XMLHttpRequest, textStatus, errorThrown){
alert("Chyba: "+textStatus);
}
});
/*
$.get("{/literal}{if isset($current_page)}{$current_page}{/if}{literal}", $("#ProductListForm").serialize(),
function(data){
$("#MainContent").html(data);
}
);
*/
}
$(document).ready(function() {
    $body = $("body");
    $( "div#range" ).slider({
    range: true,
    min: {/literal}{$prices.price_min}{literal},
    max: {/literal}{$prices.price_max}{literal},
    values: [ {/literal}{$price_selected_min}{literal}, {/literal}{$price_selected_max}{literal} ],
    slide: function( event, ui ) {
        $("span.min").text(ui.values[ 0 ]);
        $("span.max").text(ui.values[ 1 ]);
    },
    change: function(event, ui) {
        $body.addClass("loading");
        $body.find("input[name=price_selected_min]").val(ui.values[0]);
        $body.find("input[name=price_selected_max]").val(ui.values[1]);
        $("#ProductListForm").submit();
    }
    });

    /*if (window.location.hash.substr(1) == "nase-nabidka") {
        $('html, body').animate({
            scrollTop: $("body").find("#nase-nabidka").offset().top - 2
        }, 2500);
    }*/
});
$("select#price").change(function() {
$("#ProductListForm [name=sort]" ).val($(this).val());
$body.addClass("loading");
$("#ProductListForm").submit();
});
$("a.img_click").click(function(event){
event.preventDefault();
$("#ProductListForm [name=sort]" ).val($(this).data('sort'));
//alert($(this).data('sort'));
$body.addClass("loading");
$("#ProductListForm").submit();
});
$(".manufacturer_checkbox, .sklaack").click(function(){
    $body.addClass("loading");
$("#ProductListForm").submit();
});
$(".showmuch").change(function() {
    $body.addClass("loading");
$("#ProductListForm").submit();
});

$(window).load(function() {
    var maxHeight = 0;
    $('.product .photo img').each(function() {
      h = $(this).height();
      if (h > maxHeight) maxHeight = h;
    });

    $('.product .photo').each(function() { 
      $(this).height(maxHeight);
    });
});

</script>
<script type="text/javascript">
$(document).ready(function() {
    
/*
$("div.productListItem").mouseover(function() {
$("#" + $(this).attr("id") + "_t").addClass("sel");
$("#" + $(this).attr("id") + " .overBox").css("display","block");
}).mouseout(function(){
$("#" + $(this).attr("id") + "_t").removeClass("sel");
$("#" + $(this).attr("id") + " .overBox").css("display","none");
});
*/
/* nahrada radiobuttonu *//*
$('.pages_checkbox').checkBox({addVisualElement: false});
$('.sort_checkbox').checkBox({addVisualElement: false});
*/
/* obsluha slideru *//*
$("#slider-range").slider({
range: true,
min: {/literal*}{*$prices.price_min+0*}{*literal},
max: {/literal*}{*$prices.price_max+0*}{*literal},
values: [{/literal*}{*$price_selected_min*}{*literal}, {/literal*}{*$price_selected_max}*}{*literal}],
slide: function(event, ui) {
$("#amount1").val(ui.values[0]);
$("#amount2").val(ui.values[1]);
$("#am1").html(ui.values[0]+' <span>Kč</span>');
$("#am2").html(ui.values[1]+' <span>Kč</span>');
},
change: function(event, ui) {
sendForm();
}
});*/
/* obsluha odesilacich formularovych prvku */
$("#products_filter_manufacturer").on(function(){sendForm();});
$(".pages_checkbox").click(function(){sendForm();});
/* odeslani formulare - spolecna funkce */
// obsluha ajaxoveho odesilani pozadavku na zarazeni do "oblibenych"
$(".favorite").live("click",function(){
var item = $(this);
$.ajax({
type: "GET",
url: item.attr("href"),
success: function(response){
var json = eval("(" + response + ")");
//item.toggleClass("sel", json.main_content);
if (json.main_content) {
item.addClass("sel");
item.text("z oblíbených");
}else{
item.removeClass("sel");
item.text("do oblíbených");
}
},
error: function(XMLHttpRequest, textStatus, errorThrown){
alert("Chyba: "+textStatus);
}
});
return false;
});
});
</script>
{/literal}