{* sablona subnavigace *}
                <a id="menufix" href="#">Veškeré produkty</a>
                <div id='cssmenu'>
                    <ul>
                        {foreach name=navL1 from=$links key=key item=item}
                          <li class="{if not empty($item.children)}has-sub {/if}{if $smarty.foreach.navL1.last}last{/if}{if array_key_exists($key,$sel_links)}{if !isset($item.children)} last-active{/if} active open{/if}"><a href='{$url_base}{$item.nazev_seo}'><span>{$item.nazev}</span></a>
                            {if not empty($item.children)}
                              <ul>
                                {foreach name=navL2 from=$item.children key=key item=itemL2}
                                  <li class="{if not empty($itemL2.children)}has-sub {/if}{if $smarty.foreach.navL2.last}last{/if}{if array_key_exists($key,$sel_links)}{if !isset($itemL2.children)} last-active{/if} active open{/if}"><a href='{$url_base}{$itemL2.nazev_seo}'><span>{$itemL2.nazev}</span></a>
                                  {if not empty($itemL2.children)}
                                    <ul>
                                      {foreach name=navL3 from=$itemL2.children key=key item=itemL3}
                                        <li class="{if not empty($itemL3.children)}has-sub {/if}{if $smarty.foreach.navL3.last}last{/if}{if array_key_exists($key,$sel_links)}{if !isset($itemL3.children)} last-active{/if} active open{/if}"><a href='{$url_base}{$itemL3.nazev_seo}'><span>{$itemL3.nazev}</span></a>
                                          {if not empty($itemL3.children)}
                                            <ul>
                                              {foreach name=navL4 from=$itemL3.children key=key item=itemL4}
                                                <li class="{if $smarty.foreach.navL4.last}last{/if}{if array_key_exists($key,$sel_links)}{if !isset($itemL4.children)} last-active{/if} active{/if}"><a href='{$url_base}{$itemL4.nazev_seo}'><span>{$itemL4.nazev}</span></a></li>
                                              {/foreach}
                                            </ul>
                                          {/if}
                                        </li>
                                      {/foreach}
                                    </ul>
                                  {/if}
                                </li>
                                {/foreach}
                              </ul>
                            {/if}
                          </li>
                        {/foreach}
                    </ul>
                </div>
                <div id='cssmenu' class="second">
                  <strong>Nepřehlédněte</strong>
                    <ul>
                    <li><a href='/nabidka?action=1' class="strong"><span>{"Akční cena"|upper}</span></a></li>
                        <li><a class="strong" href='/nabidka?sale_off=1'><span>{"Výprodej"|upper}</span></a></li>
                        <!-- Do class k li přidat active jestli má být otevřené "active has-sub" -->
                        <li><a class="strong" href='/nabidka?top=1'><span>{"Nejprodávenější sestavy"|upper}</span></a>
                        </li>
                        <li><a class="strong" href='/nabidka?new=1'><span>{"Novinka"|upper}</span></a></li>
                    </ul>
                  </div>
                <script type="text/javascript">
                  $(document).ready(function() {
                    
                  });
                </script>
{*<div class="category-menu">
        <div class="large-3 columns">
            <ul class="side-nav" role="navigation" title="Menu">
            {foreach name=navL1 from=$links key=key item=item}
                <li{if not empty($item.children)} class="accordion-navigation"{/if}><a href="{if empty($item.children)}{$url_base}{if empty($item.indexpage)}{$item.nazev_seo}{/if}{else}#panel{$smarty.foreach.navL1.iteration}a{/if}">{$item.nazev}</a>
                {if not empty($item.children)}
                 <ul id="panel{$smarty.foreach.navL1.iteration}a" class="secondary-nav content">
                        {foreach from=$item.children item=item2 key=key name=navL2}
                          <li><a href="{$item2.nazev_seo}">{$item2.nazev}</a></li>
                        {/foreach}
                    </ul>
                </li>
                {/if}
            {/foreach}
            </ul>
        </div>
    </div>*}


{*<div class="col-md-3 col-sm-4 left_menu">
{if isset($title) && $links|count}<div class="title">{$title}</div>{/if}


<div role="navigation" id="Subnav" class=" navbar navbar-cat">
              <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-category">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{if isset($title) && $links|count}{$title}{/if}</a>
          </div>
       <div class="navbar-collapse navbar-category collapse">
 <ul class="nav navbar-nav">
   {foreach name=nav from=$links key=key item=item}
      <li class="{if array_key_exists($key,$sel_links)}sel{/if}">
        <a href="{$url_base}{if empty($item.indexpage)}{$item.nazev_seo}{/if}" title="{$item.nazev}">{$item.nazev}</a>
        {if !empty($item.children) && array_key_exists($key,$sel_links)}
          <ul>
          {foreach name=navL2 from=$item.children key=key2 item=item2}
              <li {if array_key_exists($key2,$sel_links)}class="sel"{/if}><a href="{$url_base}{$item2.nazev_seo}" title="{$item2.nazev}">{$item2.nazev}</a>
                 {if !empty($item2.children) && array_key_exists($key2,$sel_links)}
                  <ul>
                  {foreach name=navL3 from=$item2.children key=key3 item=item3}
                      <li {if array_key_exists($key3,$sel_links)}class="sel"{/if}><a href="{$url_base}{$item3.nazev_seo}" title="{$item3.nazev}">{$item3.nazev}</a>
                          {if !empty($item3.children) && array_key_exists($key3,$sel_links)}
                          <ul>
                            {foreach name=navL4 from=$item3.children key=key4 item=item4}
                                <li {if array_key_exists($key4,$sel_links)}class="sel"{/if}><a href="{$url_base}{$item4.nazev_seo}" title="{$item4.nazev}">{$item4.nazev}</a>
                                    {if !empty($item4.children) && array_key_exists($key4,$sel_links)}
                                    <ul>
                                      {foreach name=navL5 from=$item4.children key=key5 item=item5*}
                                          {*<li {if array_key_exists($key5,$sel_links)}class="sel"{/if}><a href="{$url_base}{$item5.nazev_seo}">{$item5.nazev}</a></li>*}
                                         {* <li {if array_key_exists($key5,$sel_links)}class="sel"{/if}><a href="{$url_base}{$item5.nazev_seo}" title="{$item5.nazev}">{$item5.nazev}</a>
                                              {if !empty($item5.children) && array_key_exists($key5,$sel_links)}
                                              <ul>
                                                {foreach name=navL6 from=$item5.children key=key6 item=item6}
                                                    <li {if array_key_exists($key6,$sel_links)}class="sel"{/if}><a href="{$url_base}{$item5.nazev_seo}" title="{$item6.nazev}">{$item6.nazev}</a></li>
                                                {/foreach}
                                              </ul>
                                              {/if}
                                          </li>

                                      {/foreach}
                                    </ul>
                                    {/if}
                                </li>
                            {/foreach}
                          </ul>
                          {/if}
                      </li>
                  {/foreach}
                  </ul>
                 {/if}


              </li>
          {/foreach}
          </ul>
        {/if}
      </li>
   {/foreach}
 </ul></div>

</div>

   <div class="title">{translate str="Doporučujeme"}</div>

   <div id="recomended" class="hidden-xs">
   {if !empty($recomended)}
  {foreach name=recomended from=$recomended key=k item=reco}
	  <div class=" col-md-12 col-sm-12 col-xs-4 item">
	    <div class=" col-xs-2 image">
	      {if !empty($reco.photo)}
		  <a href="{$url_base}{$reco.nazev_seo}" title="{$reco.nazev}"><img class="img-responsive" src="{$reco.photo}" alt="{$reco.nazev}" /></a>
	      {/if}
	    </div>
	    <div class="  col-xs-10  text_reco">
		<div class="row"> <a href="{$url_base}{$reco.nazev_seo}">{$reco.nazev}</a></div>
		<div class="row">
		    <div class="col-xs-6 left_price">
			 <span class="old_price">  {$reco.puvodni_cena|currency:$language_code}</span>
		    </div>
		    <div class="col-xs-6 right_price">
			 <span>{$reco.cena_s_dph|currency:$language_code}</span>
		    </div>
		</div>
	    </div>
      </div>
  {/foreach}
   {/if}
   </div>
 <div class="image_left_reco hidden-xs">
     <div class="col-md-12 col-sm-12 col-xs-4">
     <img class="img-responsive" src="{$media_path}img/vymenime.jpg">
      </div>
      <div class="col-md-12 col-sm-12 col-xs-4">
      <img class="img-responsive"  src="{$media_path}img/doporucujeme.jpg">
      </div>
 </div>
      <div class="clearfix"></div>
</div>*}