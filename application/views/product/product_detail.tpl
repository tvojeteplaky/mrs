{* sablona detailu produktu *}
<div class="content-product">
    <div class="row">

        <div class="large-3 columns">
            {widget controller="product" action="category_subnav"}
        </div>
        <div class="large-9 columns">
            <h1>{$item.nazev}</h1>

            <div class="intro">
                <div class="row">
                    <dfiv class="large-5  medium-6 columns gallery">
                        <div class="product">
                            <div class="photo-box">
                                <a href="{$item.photo_lightbox}" data-lightbox="main_product" data-index="0" data-title="{$item.nazev}" class="big"><img src="{$item.photo_detail}" alt="{$item.nazev}"></a>
                                {foreach from=$item.fotogalerie item=photo key=key name=name}
                                    <a href="{$photo.photo_detail}" data-lightbox="main_product" data-index="{$smarty.foreach.name.iteration}" data-title="{$item.nazev}" class="big hidden"><img alt="{$photo.nazev}" src="{$photo.photo_detail}" data-lightbox="main_product"></a>
                                {/foreach}

                                {if $item.percentage_discount>0}
                                    <div class="sale">-{(int)$item.percentage_discount}%</div>
                                {/if}
                                {if $item.action||$item.new||$item.sale_off}
                                    <div class="action">
                                        {if $item.action}
                                            Akční cena
                                        {elseif $item.new}
                                            Novinka
                                        {elseif $item.sale_off}
                                            Výprodej
                                        {/if}
                                    </div>
                                {/if}
                                <div class="stage">{if ($item.in_stock<1)}{translate str="Na objednání"}{else}{translate str="Skladem"}{/if}</div>
                            </div>
                        </div>

                        <div class="slider1">
                            {foreach from=$item.fotogalerie item=photo key=key name=name}
                                <div>
                                    <a href="{$photo.photo_detail}" data-index="{$smarty.foreach.name.iteration}" data-lightbox="main_product" data-title="{$item.nazev}" class="small"><img alt="{$photo.nazev}" src="{$photo.photo_thumb}" data-lightbox="main_product"></a>
                                </div>
                            {/foreach}
                        </div>
                    </dfiv>
                    <div class="large-7  medium-6  columns">
                        {* <p>{$item.uvodni_popis|strip_tags:false|truncate:400:""} <a href="#" class="slideTo" data-target="more">Čtěte více&raquo;</a></p>*}

                        {if $item.files}
                            <div class="pattern">
                                {foreach from=$item.files item=file key=key name=name}
                                    <a href="{$file.file}" download><img class="left grayscale" alt="" src="{$media_path}images/pdf.png"><span class="left">{$file.nazev}</span>

                                        <div class="clearfix"></div>
                                    </a>
                                {/foreach}
                            </div>
                        {/if}
                        <div class="price-box">
                            <div class="row collapse">
                                <div class="large-7 medium-6 small-6 columns">
                                    {if $item.puvodni_cena>0 and $item.puvodni_cena>$item.cena_s_dph}
                                        <div class="old-price">Původní cena: {$item.puvodni_cena|currency:$language_code}</div>
                                    {/if}
                                    <h2>Cena: {$item.cena_s_dph|currency:$language_code}</h2>

                                    <div class="price">({$item.cena_bez_dph|currency:$language_code} bez DPH)</div>
                                </div>
                                <div class="large-5 medium-6 small-6 columns">
                                    <div class="right">
                                        <button class="slideTo" data-target="variants">Vybrat variantu</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        {if not empty($item.odborne_informace)}
                            <div class="tech-info">
                                <div class="table-container">
                                    <div class="odborne">
                                        {$item.odborne_informace}
                                    </div>
                                </div>
                            </div>
                        {/if}
                        <div class="tech-info">
                            {$item.uvodni_popis}
                        </div>
                        {*{widget controller="form" action="show" param="offer"}
                        <a href="#" id="offer-modal" data-reveal-id="individual-offer"
                        >
                            <div class="individual-offer">
                                <span>{$item.uvodni_popis}</span>
                            </div>
                        </a>*}
                    </div>
                </div>
            </div>
            <h2 id="more">Popis</h2>

            <div class="row collapse">
                {$item.popis}
            </div>
            {*if not empty($item.odborne_informace)}
            <div class="tech-info">
         <div class="row collapse">
             <div class="large-6 medium-6 columns"><h2>Technické parametry</h2></div>
         </div>
         <div class="row collapse">
             <div class="columns">
                 <a href="#" id="tech-data" class="right more">Skrýt technické parametry <span class="arrow-down arrow-up"></span></a>
             </div>
         </div>

     </div>
         {/if*}
            <h2>Vzorník barev</h2>

            <div class="row">
                <div class="columns">
                    <div class="slider2">
                        {foreach from=$item.vzornik item=vzor key=key name=name}
                            <div>
                                <a href="{$vzor.photo_detail}" data-index="{$smarty.foreach.name.iteration}" data-lightbox="vzornik" data-title="{$vzor.nazev}"><img alt="{$vzor.nazev}" src="{$vzor.photo_thumb}" data-lightbox="vzornik"></a>
                            <div class="description">{$vzor.nazev}</div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>

            <h2>Objednávka</h2>

            <div class="table-container" id="variants">
                <table>
                    <thead>
                    <tr>
                        <th>Kód produktu</th>
                        <th>Barva</th>
                        <th>Dostupnost</th>
                        <th>Cena bez DPH</th>
                        <th>Cena s DPH</th>
                        <th width="80"></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$variants item=variant key=key name=variants_variants}
                        {if !$variant.doplnek}
                            <tr>
                                <form action="" method="post" class="cart_form" id="form_order">
                                    <td>{$variant.code}</td>
                                    <td>{$variant.nazev}</td>
                                    <td class="{if $variant.pocet_na_sklade > 0 || !is_numeric($variant.pocet_na_sklade)}green{else}red{/if} strong">{if is_numeric($variant.pocet_na_sklade)}

                                            {if $variant.pocet_na_sklade > 0}Skladem{else}Není skladem{/if} {if $variant.pocet_na_sklade > 5}
                                            {elseif $variant.pocet_na_sklade > 0} {$variant.pocet_na_sklade}ks
                                            {/if}{else}{$variant.pocet_na_sklade}{/if}</td>
                                    <td>{$variant.cena_bez_dph|currency:$language_code}</td>
                                    <td class="strong">{$variant.cena_s_dph|currency:$language_code}</td>
                                    <td><input name="quantity" type="number" value="1"/></td>
                                    <td>
                                        <button type="submit" class="right">Koupit</button>
                                    </td>
                                    <input type="hidden" name="product_id" value="{$variant.id}">
                                    {hana_secured_post action="add_item" module="shoppingcart"}
                                </form>
                            </tr>
                        {/if}
                    {/foreach}
                    </tbody>
                </table>
            </div>
            <div class="prislusenstvi">
                <h2>Příslušenství</h2>

                <div class="table-container">
                    <table>
                        <thead>
                        <tr>
                            <th>Kód produktu</th>
                            <th>Barva</th>
                            <th>Dostupnost</th>
                            <th>Cena bez DPH</th>
                            <th>Cena s DPH</th>
                            <th width="80"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$variants item=variant key=key name=variants_variants}
                            {if $variant.doplnek}
                                <tr>
                                    <form action="" method="post" class="cart_form" id="form_order">
                                        <td>{$variant.code}</td>
                                        <td>{$variant.nazev}</td>
                                        <td class="{if $variant.pocet_na_sklade > 0 || !is_numeric($variant.pocet_na_sklade)}green{else}red{/if} strong">{if is_numeric($variant.pocet_na_sklade)}

                                                {if $variant.pocet_na_sklade > 0}Skladem{else}Není skladem{/if} {if $variant.pocet_na_sklade > 5}
                                                {elseif $variant.pocet_na_sklade > 0} {$variant.pocet_na_sklade}ks
                                                {/if}{else}{$variant.pocet_na_sklade}{/if}</td>
                                        <td>{$variant.cena_bez_dph|currency:$language_code}</td>
                                        <td class="strong">{$variant.cena_s_dph|currency:$language_code}</td>
                                        <td><input name="quantity" type="number" value="1"/></td>
                                        <td>
                                            <button type="submit" class="right">Koupit</button>
                                        </td>
                                        <input type="hidden" name="product_id" value="{$variant.id}">
                                        {hana_secured_post action="add_item" module="shoppingcart"}
                                    </form>
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row individual-offer-parent" data-equalizer data-equalizer-mq="medium-up">
                <div class="medium-6 medium-push-6 small-12 columns" data-equalizer-watch>
                    {widget controller="form" action="show" param="offer"}
                    <a href="#" id="offer-modal" data-reveal-id="individual-offer"
                            >
                        <div class="individual-offer">
                            <span>Mám zájem o <strong>individualní cenovou nabídku</strong> (v případě objednání nad <br>20 000,- Kč bez DPH)</span>
                        </div>
                    </a>
                </div>
                <div class="medium-6 medium-pull-6 small-12 columns bottom" data-equalizer-watch>
                    <h2>Máte dotaz, napište nám</h2>
                </div>
            </div>

            {widget controller="contact" action="show"}

            <h2>Související produkty</h2>
            {widget controller="product" action="random"}

        </div>
    </div>
</div>
{literal}
    <script>
        $(document).ready(function () {
            // Hover fotky
            $("a.small").on("mouseover", function () {
                $("a.big").hide();
                $("a.big[data-index='"+$(this).attr("data-index")+"']").show();

                var height = $(".photo-box").height();
                if ($("a.big[data-index='"+$(this).attr("data-index")+"']").find('img').height() > height)
                    $(".photo-box").height($("a.big[data-index='"+$(this).attr("data-index")+"']").find('img').height());
            });

            $(".gallery").on("mouseleave", function () {
                $("a.big").hide();
                $("a.big[data-index='0']").show();
            });

            if ($(".photo-box").find('a.big img').length > 1) {
                var height = 0;
                $(".photo-box").find('a.big img').each(function() {
                    var parent = $(this).parent('a.big');
                    var setted = parent.hasClass('hidden');

                    parent.removeClass('hidden');
                    if ($(this).height() > height)
                        height = $(this).height();

                    if (setted)
                        parent.addClass('hidden');
                });

                $(".photo-box").height(height);
            }

            $('.slider1').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 6,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 641,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 380,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                    ,
                    {
                        breakpoint: 260,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $('.slider2').slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 6,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 900,
                        settings: {
                            infinite: true,
                            dots: false,
                            slidesToShow: 5

                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 4

                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 3

                        }
                    },
                    {
                        breakpoint: 380,
                        settings: {
                            slidesToShow: 2

                        }

                    }
                    ,
                    {
                        breakpoint: 260,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        });
    </script>
{/literal}
{literal}
    <script type="text/javascript">
        $(function () {
            $(".slideTo").click(function (event) {
                event.preventDefault();
                var variantId = "#" + $(this).data("target");
                $('html, body').animate({
                    scrollTop: $("body").find(variantId).offset().top - 2
                }, 2000);
            });
            $("#offer-modal").click(function () {
                $("#individual-offer").foundation('reveal', 'open');
            });

            var numRow = $(".prislusenstvi table tbody tr").length;
            if (numRow == 0) {
                $(".prislusenstvi").hide();
            }


            var open = true;
            var place = $(".tech-info .table-container");
            //place.hide();
            $(".tech-info a.more").click(function (event) {
                event.preventDefault();
                if (open) {
                    open = false;
                    place.hide(400);
                    $(this).html("Zobrazit technické parametry <span class=\"arrow-down\">");
                } else {
                    open = true;
                    place.show(400);
                    $(this).html("Skrýt technické parametry <span class=\"arrow-down arrow-up\">");
                }
            });

        });

    </script>
{/literal}
{*}
<div class="row">
    

      <div class="large-9 columns">

        <div class="row">
            <div class="columns">
                <h1>{$item.nazev}</h1>
            </div>
        </div>
        <div class="row">
            <div class="large-6 medium-6 columns">
            <img src="{$item.photo_detail}" alt="{$item.nazev}">
            {if not empty($item.fotogalerie)}
                            <div class="slider2">
                {foreach from=$item.fotogalerie item=photo key=key name=name}
                    <div>
                        <img alt="{$photo.nazev}" src="{$photo.photo_detail}">
                    </div>
                {/foreach}
            </div>
            {/if}


            </div>
        <div class="large-6 medium-6 columns product-detail">
                <div class="row">
                    <div class="columns">
                        <div class="old-price">{$item.puvodni_cena|currency:$language_code}</div>
                        <h2>{$item.cena_s_dph|currency:$language_code}</h2>

                        <div class="stored">{if $item.pocet_na_sklade > 0}Skladem{else}Není na skladě{/if}</div>
                    </div>
                </div>
                {if not empty($item.popis)}
                    <h3>{translate str="Popis"}</h3>
                    <p>{$item.popis}
                    </p>
                {/if}
                
                {foreach from=$item.files item=file key=key name=name}
                     <div class="row">
                    <div class="large-6 medium-6 columns">
                        <a href="{$file.file}" class="button" download>{$file.ext}</a>
                    </div>
                    <div class="large-6 medium-6 columns">
                        <label>{$file.nazev}</label>
                    </div>
                </div>
                {/foreach}

            </div>
      </div>
      {if not empty($item.odborne_informace)}
       <div class="row">
            <div class="columns">
                <h3>{translate str="Technické parametry"}</h3>
                {$item.odborne_informace}
            </div>
        </div>
        {/if}

        {if not empty($item.vzornik)}
        <div class="row">
            <div class="columns">
                <h2>{translate str="Vzorník"}</h2>
            </div>
        </div>

        <div class="row">
            <div class="columns">
                <div class="slider" >
                    {foreach from=$item.vzornik item=vzor key=key name=name}
                    <div ><img src="{$vzor.photo_detail}" alt="{$vzor.nazev}" ></div>
                    {/foreach}
                </div>
            </div>
        </div>
        {/if}
        {if not empty($variants)}
            
        
        <div class="row">
            <div class="columns">
                <h2>{translate str="Objednávka"}</h2>
            </div>
        </div>

        <div class="row">
            <div class="columns">
                <table>
                    <thead>
                    <tr>
                        <th>{translate str="Katalog. číslo"}</th>
                        <th>{translate str="Dostupnost"}</th>
                        <th>{translate str="Cena bez DPH"}</th>
                        <th>{translate str="Včetně DPH"}</th>
                        <th>{translate str="Počet kusů"}</th>

                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$variants item=variant key=key name=variants_variants}
                    {if !$variant.doplnek}
                          <tr>
                    <form action="" method="post" class="cart_form" id="form_order">
                        <td>{$variant.code}</td>
                        <td>{if $variant.pocet_na_sklade}skladem {else} není na skladě{/if}</td>
                        <td>{$variant.cena_bez_dph|currency:$language_code}</td>
                        <td>{$variant.cena_s_dph|currency:$language_code}</td>
                        <td width="80"><input name="quantity" type="number" min="1" max="{$variant.pocet_na_sklade}" value="1"></td>
                        <td>
                            <button>{translate str="Přidat do košíku"}</button>
                        </td>
                        <input type="hidden" name="product_id" value="{$variant.id}">
                        {hana_secured_post action="add_item" module="shoppingcart"}
                        </form>
                    </tr>
                    {/if}
                    {/foreach}

                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="row">
            <div class="columns">
                <h2>{translate str="Doplňky"}</h2>
            </div>
        </div>
         <div class="row">
            <div class="columns">
                <table>
                    <thead>
                    <tr>
                        <th>{translate str="ID produktu"}</th>
                        <th>{translate str="Název"}</th>
                        <th>{translate str="Cena bez DPH"}</th>
                        <th>{translate str="Včetně DPH"}</th>
                        <th>{translate str="Počet kusů"}</th>

                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$variants item=doplnek key=key name=variants_variants}
                    {if $doplnek.doplnek}
                          <tr>
                    <form action="" method="post" class="cart_form" id="form_order">
                        <td>{$doplnek.code}</td>
                        <td>{if $doplnek.pocet_na_sklade}skladem {else} není na skladě{/if}</td>
                        <td>{$doplnek.cena_bez_dph|currency:$language_code}</td>
                        <td>{$doplnek.cena_s_dph|currency:$language_code}</td>
                        <td width="80"><input name="quantity"  type="number" min="1" max="{$doplnek.pocet_na_sklade}" value="1"></td>
                        <td>
                            <button>{translate str="Přidat do košíku"}</button>
                        </td>
                        <input type="hidden" name="product_id" value="{$doplnek.id}">
                        {hana_secured_post action="add_item" module="shoppingcart"}
                        </form>
                    </tr>
                    {/if}
                    {/foreach}

                    </tbody>
                </table>
            </div>
        </div>

        {/if}
         <div class="row">
            <div class="large-6 medium-6 columns">
                <textarea placeholder="zadejte dotaz"></textarea>
            </div>
            <div class="large-6 medium-6 columns">
                <button>{translate str="Mám zájem o individuální cenu"}</button>
            </div>
        </div>

         <div class="row">
            <div class="columns">
                <h2>{translate str="Podobné zboží"}</h2>
            </div>
        </div>*}

{*widget controller="product" action="random"*}
{*  </div>
</div>
<div class="our-offer">
        <div class="row">
            <div class="columns">
                <h2>Luk nábytek</h2>
            </div>
        </div>
        <div class="row">
            <div class="columns">
                {static_content code="about"}
            </div>
        </div>
    </div>
    {widget action="homepage_list" controller="article"}
    <script>
        $(document).ready(function () {
            $('.slider').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 5,
                slidesToScroll: 5,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.slider2').slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        });
    </script>

{*<div class="modal fade" id="essoxCalculatorModal" tabindex="-1" role="dialog" aria-labelledby="essoxCalculatorModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Kalkulačka splátek</h4>
            </div>
                <iframe src="{$essoxCalculatorUrl}" id="essoxCalculator"></iframe>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>*}



{*<div id="product_detail" class="col-md-9 col-sm-8">
{widget name="breadcrumbs" controller="navigation" action="breadcrumbs"}
<div class="clearfix"></div>
<div id="">
    <div class="photo defaultBox col-md-6 ">
        {if !empty($item.photo_detail)}
        <div class="item_images ">
            <a href="{$item.photo_lightbox}" class="fancybox-group lightbox_target" rel="group1" title="{$item.nazev}" >
                <img class="img-responsive main_photo" src="{$item.photo_detail}" alt="{$item.nazev}"  title="{$item.nazev}" />
            </a>

            {if $item.pocet_na_sklade > 0}
                <div class="stock_indicator stock" title="Na skladě"></div>
            {else}
                <div class=" stock_indicator nostock" title="Není na skladě"></div>
            {/if}


            {if $item.product_action_type == 3 || $item.gift}
                <div class="product-detail gift" title="{translate str="Dárek"}">{$item.akce_text}</div>
            {/if}
        </div>

        {if !empty($item.fotogalerie)}
            <div class="">
                <div class="col-md-4 col-sm-4 col-xs-4 ">

                    <div class="photo_detail">

                        <img class="img-responsive" src="{$item.photo_detail}" >

                    </div>
                </div>
                {foreach name=fotos from=$item.fotogalerie key=key item=photo}
                    <div class="col-md-4 col-sm-4 col-xs-4 ">

                        <div class="photo_detail">
                            <a href="{$photo.photo_detail}" class="fancybox-group hidden" rel="group1" title="{$photo.nazev}"></a>
                            <img class="img-responsive" src="{$photo.photo_detail}" >

                        </div>
                    </div>
                {/foreach}
            </div>

        {/if}
        <div class="clearfix"></div>
    </div>
    {else}
    {/if}
    <script>

        $( window ).load(function() {
            $( ".photo_detail img" ).click(function() {
                var src=$(this).attr('src');
                $( ".main_photo" ).attr('src', src)
                $( ".lightbox_target" ).attr('href', src)

            });


        });
    </script>
    <div class="product-detail indicator">
        {if $item.product_action_type == 1 || $item.action}
            <div class="action act_1" title="{translate str="Akce"}">{$item.akce_text}</div>
        {/if}
        {if $item.product_action_type == 2 || $item.new}
            <div class="action act_2" title="{translate str="Novinka"}">{$item.akce_text}</div>
        {/if}
        {if $item.product_action_type == 4 || $item.sale_off}
            <div class="action act_4" title="Výprodej">{$item.akce_text}</div>
        {/if}
    </div>
</div>
<div class="info col-md-6">
    <h1>{$item.nazev}</h1>
    <div class="row">
        {$item.uvodni_popis}
    </div>
    {if $item.k_prodeji}
        {if !$item.gift}
            <form action="" method="post" class="cart_form" id="form_order">
                <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Výrobce:</strong> </div><div  class="col-md-8 col-sm-6 col-xs-6">{$item.vyrobce}</div></div>
                <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Kód produktu:</strong></div><div  class="col-md-8 col-sm-6 col-xs-6">{$item.code}</div></div>
                <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Dostupnost:</strong></div><div  class="col-md-8 col-sm-6 col-xs-6">
                        <select id="stock">
                            <option value="{$item.pocet_na_sklade}">zvolit pobočku</option>
                            {foreach name=stock from=$item.sklady key=key item=sklad}
                                <option value="{$sklad.quantity}">{$sklad.code}</option>
                            {/foreach}

                        </select>
                        <span class="in_stock">Skladem <span id="quantity">{$item.pocet_na_sklade}</span> ks</span> </div></div>
                {if !empty($item.varianty )}
                    <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Varianta: </strong></div><div  class="col-md-8 col-sm-6 col-xs-6">
                            <select id="varianty">
                                <option>zvolit variantu</option>
                                {foreach name=varianty from=$item.varianty key=var item=varianta}
                                    <option {if  $varianta.id==$item.id}selected{/if} value="{$varianta.id}">  {$varianta.nazev}</option>*}
{* <option value="{$varianta.id}">{$varianta.nazev}</option>*}
{*/foreach}


</select>*}{*$item.varianty|@print_r*}


{* </div></div>

{/if}
<div class="row atributes">
<div  class="col-md-4 col-sm-6 col-xs-6"><strong>Počet kusů:</strong> </div>
<div  class="col-md-8 col-sm-6 col-xs-6">
 <div class="input-group counter_army">
<span class="input-group-btn">
<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quantity">
    <span class="glyphicon glyphicon-minus"></span>
</button>
</span>
     <input type="text" id="quantity" name="quantity" class="form-control input-number" value="{if $item.pocet_na_sklade == 0}0{else}1 {/if}" min="1" max="{$item.pocet_na_sklade}">
<span class="input-group-btn">
<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity">
    <span class="glyphicon glyphicon-plus"></span>
</button>
</span>
 </div>
     </div>
</div>
<div class="row">
<div class="col-md-6 col-sm-3 col-xs-6">
 <div class="price_no_tax">{$item.cena_bez_dph|currency:$language_code}</div>
</div>
<div class="col-md-6 col-sm-6 col-xs-6 ">
 <div class="price_tax text-right">{$item.cena_s_dph|currency:$language_code} <span>s DPH</span></div>
</div>
</div>
<div class="row add_cart">
<div class="col-sm-12">
 {hana_secured_post action="add_item" module="shoppingcart"}
 <input type="hidden" name="product_id" value="{$item.id}" />

 <div class="btn-group pull-right">
     <button type="submit" name="add_item" class="btn btn-army" value="DO KOŠÍKU"  {if $item.pocet_na_sklade == 0}disabled {/if} />DO KOŠÍKU</button>

     <button type="button" class="btn btn-army dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
         <span class="caret"></span>
         <span class="sr-only">Rozevřít nabídku</span>
     </button>
     <ul class="dropdown-menu" role="menu">
         <li><a href="#" id="essoxCalculatorTrigger">Kalkulačka splátek</a></li>
         {if $shopper.id}
             <li><a href="#" id="watchdog" data-toggle="modal" data-target=".bs-example-modal-sm">{translate str="Hlídat zboží"}</a></li>
         {/if}
     </ul>
 </div>
</div>
</div>
</form>
<script type="text/javascript">
$("#essoxCalculatorTrigger").click(function(e) {
show_calculator(e);
})
</script>
{/if}


{*if $item.product_action_type==1 || $item.product_action_type==2 || $item.product_action_type==3}
<div id="tabs-2">
<div class="padding10">
{$item.akce_text}
</div>
<div class="correct"></div>
</div>
{/if*}


{*/if}
{if $shopper.id}
    <div class="modal fade bs-example-modal-sm" id="watchdog-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <form action="" method="post" id="form_watchdog">
                    <div class="modal-body">
                        {hana_secured_post action="add_watch" module="watchdog"}
                        <input type="hidden" name="product_id" value="{$item.id}">
                        <div class="form-group">
                            <label class="control-label" for="watchdog-cena"><strong>{translate str="Hlídat cenu"}: </strong></label>
                            <input type="number" name="price" min="0" value="{$item.cena_s_dph}" id="watchdog-cena" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="in_stock"><strong>{translate str="Hlídat dostupnost"}: </strong></label>
                            <input type="checkbox" name="in_stock" id="in_stock" value="1">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
                        <button type="submit" class="btn btn-primary" id="form-save">Uložit</button>
                    </div>
                </form>
                <script type="text/javascript">
                    set_watchdog();
                </script>
            </div>
        </div>
    </div>
{/if}

</div>

<div class="clearfix"></div>

<div class="row tabs">
<ul class="nav nav-pills nav-custom">
    <li class="active"><a href="#popis" data-toggle="tab">Popis produktu</a></li>
    <li><a href="#Contactform" data-toggle="tab">Dotaz k prodeji</a></li>
    {if !empty($item.youtube)}<li><a href="#video" data-toggle="tab">Video k Produktu</a></li>{/if}
    <li><a href="#download" data-toggle="tab">Ke stažení</a></li>
    <li><a href="#rating" data-toggle="tab">Hodnocení</a></li>
    <li><a href="#similar" data-toggle="tab">Podobné produkty</a></li>
</ul>

<div id='content' class="tab-content">
    <div class="tab-pane active" id="popis">
        {$item.popis}
    </div>
    <div class="tab-pane" id="Contactform">
        {widget action="show" controller="contact" name="contactform"}
    </div>
    {if !empty($item.youtube)}
        <div class="tab-pane" id="video">
            <div class="video-container">
                <iframe width="405" height="290" src="//www.youtube.com/embed/{$item.youtube}" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    {/if}
    <div class="tab-pane" id="download">
        ke stažení
    </div>
    <div class="tab-pane" id="rating">
        {widget controller="rating" action="widget"}
    </div>
    <div class="tab-pane" id="similar">
        <div id="ProductListSection">
            {widget controller="product" action="similar_widget"}
        </div>
    </div>
</div>

</div>

{*
<div class="description">
<a name="Commentsform"></a>
<div class="defaultTabs">
  <ul>
    <li><a href="#tabs-11">Popis</a></li>
    {if $item.k_prodeji}

    {/if}
    {if !empty($item.odborne_informace)}<li><a href="#tabs-13">Informace pro odbornou veřejnost</a></li>{/if}
  </ul>

  <div id="tabs-11">
    <div class="padding10">
    {$item.popis}
    </div>
  </div>

  {if $item.k_prodeji}
  {*
  <div id="tabs-12">
    <div class="padding10">
    {widget action="index" controller="comments" param=$item.id}
    {if $shopper.id}
      {widget action="show_form" controller="comments" param=$item.id}
    {else}
      <div class="noregComments">Příspěvky mohou vkládat pouze přihlášení uživatelé.</div>
    {/if}
    </div>
    <div class="correct"></div>
  </div>
  *}{*
            {/if}

            {if !empty($item.odborne_informace)}
            <div id="tabs-13">
              <div class="confirmation">
              Následující stránky jsou určeny pouze pro odborné pracovníky ve zdravotnictví, tj. osobám oprávněným předepisovat a vydávat léky. Zde naleznete odkaz na Veřejně přístupnou odbornou informační službu pro odbornou zdravotnickou veřejnost, souhrny údajů o přípravku a různé potřebné doplňkové služby, jako poskytování odborných informací, zajímavé odkazy apod.
              <br /><br />
              <a href="#" class="right button buttonDefault">Potvrzuji, že jsem specialista</a>
              </div>
              <div class="afterConfirmation noDisplay">
              {$item.odborne_informace}
              </div>
            </div>
            {/if}

            <div class="bottom"></div>
          </div>

      </div>


      {*
      <h2 class="bigMT">{translate str="Dotaz na produkt"}</h2>

      {widget action="show" controller="contact" name="contactform"}
      *}
{*</div>

</div>


{literal}
    <script type="text/javascript">
        /*Metoda na sklady nutno dodělat ajax*/
        $('#stock').on('change', function() {
            var quantity=parseInt(this.value);
            $('#quantity').html(quantity);
        });
    </script>
    <script type="text/javascript">
    /*Metoda pro volální varianty produktu*/
    $('#varianty').on('change', function() {
        $.ajax({
            type: "POST",
            url: "{/literal}?{hana_secured_get action="detail_form" module="product"  }{literal}",
            data: 'varianta_id='+$('#varianty').val(),
            success: function(response) {
                var json = eval("(" + response + ")");
                for (var key in json) {
                    $('#'+key).html(json[key]);
                }
                var form = json["main_content"];
                $('#form_order').html(form);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                console.log("Chyba ajaxu: "+textStatus);
            }
        });
    })
</script>
{/literal}
{literal}

    <script type="text/javascript">

        $(".cart_form").submit(function(){

            $.ajax({
                type: "POST",
                data: $(".cart_form").serialize(),
                //url: "url::base().url::current().Kohana::config('config.url_suffix').",
                success: function(response){
                    var json = eval("(" + response + ")");
                    for (var key in json) {
                        $('#'+key).html(json[key]);
                    }
                    $('#MessageWidget .clickChange').click();
                    var form = json["main_content"];

                    if(form==""){
                        $('.bs-example-modal-lg').modal('hide');
                        $(".clickChange").click();
                    }else{$("#ShoppingCartPopup form").html(form);
                        $('.bs-example-modal-lg').modal('show');
                    }

                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    //alert("Chyba: "+textStatus);
                    console.log("Chyba ajaxu: "+textStatus);
                }
            });
            return false;
        });
    </script>

{/literal}

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="ShoppingCartPopup" class="">
                <form action="" method="post">

                </form>
                <a class="btn-army btn btn_full " onclick="$(document).ready(function() { $('.bs-example-modal-lg').modal('hide');})" href="">ZPĚT DO ESHOPU</a>
                <a class="btn-army btn right_close btn_full" href="{$url_base}nakupni-kosik">K POKLADNĚ</a>
            </div>
        </div>
    </div>
*}

