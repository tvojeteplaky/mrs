
		       <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Výrobce:</strong> </div><div  class="col-md-8 col-sm-6 col-xs-6">{$item.vyrobce}</div></div>
		       <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Kód produktu:</strong></div><div  class="col-md-8 col-sm-6 col-xs-6">{$item.code}</div></div>
		       <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Dostupnost:</strong></div><div  class="col-md-8 col-sm-6 col-xs-6">
			       <select id="stock">
				   <option value="{$item.pocet_na_sklade}">zvolit pobočku</option>
				   {foreach name=stock from=$item.sklady key=key item=sklad}
				       <option value="{$sklad.quantity}">{$sklad.code}</option>
				   {/foreach}
				  
				</select>    
				   <span class="in_stock">Skladem <span id="quantity">{$item.pocet_na_sklade}</span> ks</span> </div></div>
		         {if !empty($item.varianty )} 
                       <div class="row atributes"><div  class="col-md-4 col-sm-6 col-xs-6"><strong>Varianta: </strong></div><div  class="col-md-8 col-sm-6 col-xs-6">
                               <select id="varianty">
                                    <option>zvolit variantu</option>
                                   {foreach name=varianty from=$item.varianty key=var item=varianta}
                                       <option {if  $varianta.id==$item.id}selected{/if} value="{$varianta.id}">  {$varianta.nazev}</option>
				      {* <option value="{$varianta.id}">{$varianta.nazev}</option>*}
				   {/foreach}
                            
				   
			       </select>{*$item.varianty|@print_r*}
                              
                               
			   </div></div>	
                         
                         {/if} 
		       <div class="row atributes">
			   <div  class="col-md-4 col-sm-6 col-xs-6"><strong>Počet kusů:</strong> </div>
			   <div  class="col-md-8 col-sm-6 col-xs-6">		      	      
			       <div class="input-group counter_army">         
				   <span class="input-group-btn">           
				       <button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="quantity">
					   <span class="glyphicon glyphicon-minus"></span>
				       </button>
				   </span>
				   <input type="text" id="quantity" name="quantity" class="form-control input-number" value="{if $item.pocet_na_sklade == 0}0{else}1 {/if}" min="1" max="{$item.pocet_na_sklade}">
				   <span class="input-group-btn">
				       <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quantity">
					   <span class="glyphicon glyphicon-plus"></span>
				       </button>
				   </span>
			       </div>
			   </div>
		       </div>
		       <div class="row add_cart">
			   <div class="col-md-3 col-sm-3 col-xs-6">
			       <div class="price_no_tax">{$item.cena_bez_dph|currency:$language_code}</div>
			   </div>			   
			   <div class="col-md-6 col-sm-6 col-xs-6">                       
			       <div class="price_tax">{$item.cena_s_dph|currency:$language_code} <span>s DPH</span></div>
			   </div>
			   <div class="col-md-3 col-sm-3 col-xs-3">
				{hana_secured_post action="add_item" module="shoppingcart"}
				<input type="hidden" name="product_id" value="{$item.id}" />
				<input type="submit" name="add_item" class="btn btn-army" value="DO KOŠÍKU" {if $item.pocet_na_sklade == 0}disabled {/if} /> 
			   </div>
		       </div>
		      
    {literal} 
  <script type="text/javascript">
	/*Metoda na sklady nutno dodělat ajax*/
	$('#stock').on('change', function() {
	    var quantity=parseInt(this.value);
	    $('#quantity').html(quantity);
        });
    </script>   
    <script type="text/javascript">
        /*Metoda pro volální varianty produktu*/
        $('#varianty').on('change', function() {
            $.ajax({
                type: "POST",
                url: "{/literal}?{hana_secured_get action="detail_form" module="product"  }{literal}",
                data: 'varianta_id='+$('#varianty').val(),
                success: function(response) {
                    var json = eval("(" + response + ")");
                    for (var key in json) {
                        $('#'+key).html(json[key]);  
                    }
                     var form = json["main_content"];
                     $('#form_order').html(form);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                console.log("Chyba ajaxu: "+textStatus);
                           }
            });
        })
    </script> 
    {/literal}