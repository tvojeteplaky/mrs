
<div id="registerModal" class="reveal-modal" data-reveal aria-labelledby="Registrační formulář" aria-hidden="true" role="dialog">
<h2>Registrace zákazníka</h2>
<form method="post">
  {if !empty($registration_error)}<div data-alert class="alert-box alert radius">{$registration_error}<a href="#" class="close">&times;</a></div>{/if}
    <div id="RegistrationTypeSrc">
      <input type="radio" name="user-registration[typ_uzivatele]" value="user" id="RegistrationTypeUser" class="registrationType" {if empty($data.typ_uzivatele) || $data.typ_uzivatele=="user"}checked="checked"{/if} />
      <label for="RegistrationTypeUser">Registrace uživatele</label>
            &nbsp; &nbsp;
      <input type="radio" name="user-registration[typ_uzivatele]" value="company" id="RegistrationTypeCompany" class="registrationType" {if !empty($data.typ_uzivatele) && $data.typ_uzivatele=="company"}checked="checked"{/if} />
      <label for="RegistrationTypeCompany">Registrace firmy</label>

      
    </div>
    <fieldset>
      {form_input name="user-registration.nazev"  class='form-control' label="Jméno a příjmení *" data=$data errors=$errors}
      {form_input name="user-registration.email" class='form-control'  label="Email *" data=$data errors=$errors}
      {form_input name="user-registration.telefon" class='form-control' label="Telefon *" data=$data errors=$errors}
      {form_password name="user-registration.password"  class='form-control'   label="Heslo *" errors=$errors}
      {form_password name="user-registration.password_confirm"  class='form-control'  label="Potvrdit heslo *" errors=$errors}
    </fieldset>
    <fieldset id="CompanyFields">
      {form_input name="user-registration.ic" class='form-control' label="IČ" data=$data errors=$errors}
      {form_input name="user-registration.nazev_organizace"  class='form-control' label="Název organizace" data=$data errors=$errors}
      {form_input name="user-registration.dic"  class='form-control' label="DIČ" data=$data errors=$errors}
    </fieldset>
    <fieldset>
      {form_input name="user-registration.ulice" class='form-control'  label="Ulice a číslo popisné *" data=$data errors=$errors}
      {form_input name="user-registration.psc" class='form-control'  label="PSČ *" data=$data errors=$errors}
      {form_input name="user-registration.mesto" class='form-control'  label="Město *" data=$data errors=$errors}
    </fieldset>

    <div class="row">
    <input type="checkbox" name="user-registration[obchodni_podminky]" value="agree" class="checkbox" /> &nbsp; {translate str="Souhlasím s"} <a target="_blank" href="{$url_base}obchodni-podminky" title="{translate str="Souhlasím s obchodními podmínkami"}">	{translate str="obchodními podmínkami"}</a>
    {if !empty($errors.obchodni_podminky)}<span class="error" style="margin-left: 150px;"> - {translate str="nutno potvrdit souhlas"}</span>{/if}  
    </div>
    
    <div class="bottomButtons">
      {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
      {hana_secured_post action="registration" module="user"}
      <input type="hidden" name="show_registration_popup" value="1" />
     <button type="submit">Registrovat se</button>
    </div>
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  </form>
</div>



{literal}
<script type="text/javascript">
$(document).ready(function(){

  $('a.register-click').on('click', function() {
    $('#registerModal').foundation('reveal','open');
  });
  {/literal}{if $show_popup}{literal}
    $('#registerModal').foundation('reveal','open');
  {/literal}{/if}{literal}

  if ($('#RegistrationTypeCompany').is(':checked'))
  {
      $("#CompanyFields").show();
  }
  $("#CompanyFields").hide();
  $(".registrationType").click(function(e){
      if($(this).val()=="company")
      {
         $("#CompanyFields").slideDown();
      }
      else
      {
         $("#CompanyFields").slideUp();
      }
    
    });
   
   /*$("#ReadFromAres").click(function(){
     $(".ajaxLoader").show();
     $(".aresError").text("");
   
     $.ajax({
      method: "GET",
      dataType: "json",
      url: "?ic="+$("#input_ic").val(),
     }).done(function(data) {
        $(".ajaxLoader").hide();
        
        if(data.ares_error)
        {
          $(".aresError").text(data.ares_error); 
        }
        else
        {
          $("#input_nazev_organizace").val(data.company_name);
          $("#input_mesto").val(data.municipality);
          $("#input_ulice").val(data.street + " " + data.street_number);
          $("#input_psc").val(data.postal_code);
          $("#input_dic").val(data.tax_id);
        }
        
     });
   
   });*/
   
    
});
</script>
{/literal}
{* action popup *}
{*
 <div id="ActionPopup" class="noDisplay">
  <div class="closeRF">x</div>
      <div class="text1">Zaregistrujte se a získejte slevu při prvním nákupu.</div>
      <form action="#" method="post">
        <input type="text" {if !empty($email)}value="{$email}"{/if}name="registration-action-email" {if !empty($errors)}class="borderRed"{/if}/>
        <input type="submit" name="odeslat" value="registrovat" />
        
      </form>
      <div class="text2">Vložením e-mailu souhlasíte se zasíláním informačního zpravodaje o novinkách a akčních cenách. Slevu lze využít pouze při prvním nákupu.</div>
  </form>
</div>

{if $show_action_popup}
{literal}
<script type="text/javascript">
$(document).ready(function(){
    $("#ActionPopup").bPopup({
        closeClass:'closeRF',
    });
 
});
</script>
{/literal}
{/if}
*}
