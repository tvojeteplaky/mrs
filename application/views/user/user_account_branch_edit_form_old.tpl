{* sablona pro pridani/editaci pobocky uzivatele *}
{assign var='editmode' value=$editmode|default:false}
<form action="" method="post">
  <h1>{if $editmode}{translate str="Změna údajů o doručovací adrese"}{else}{translate str="Vložení nové doručovací adresy"}{/if}</h1>

  <table summary="formulář na přidání nebo editaci pobočky">
    <tr class="sectiontableentry2">
      <td colspan="2">{form_input name="branchedit.nazev" label="Jméno/Název firmy *" data=$data errors=$errors }</td>
    </tr>
    <tr>
      <td colspan="2">{form_input name="branchedit.ulice" label="Ulice a číslo popisné *" data=$data errors=$errors }</td>
    </tr>
    <tr>
      <td>{form_input name="branchedit.email" label="E-mail *" data=$data errors=$errors }</td>
      <td>{form_input name="branchedit.telefon" label="Telefon" data=$data errors=$errors }</td>
    </tr>
    <tr>
      <td>{form_input name="branchedit.mesto" label="Město *" data=$data errors=$errors }</td>
      <td>{form_input name="branchedit.psc" label="PSČ *" data=$data errors=$errors }</td>
    </tr>
    <tr class="sectiontableentry2">
      <td colspan="2" class="needTxt">* {translate str="povinné údaje"}</td>
    </tr> 
  </table>
  
  {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
  {if !empty($data.id)}<input type="hidden" name="branchedit[id]" value="{$data.id}" />{/if}
  {hana_secured_post action="branch_edit" module="user" }
  

  <div class="bottomButtons">
    <a class="left button" href="{$url_base}{if !empty($redirect)}{$redirect}{/if}">{translate str="Zpět bez uložení"}</a>
    <input type="submit" name="next_step" value="{translate str="Uložit"}" class="right button" />
  </div>
</form>
     
  