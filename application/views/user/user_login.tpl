{* sablona s prihlasovacim formularem pro uzivatele *}
<div id="login" class="order-form">
  <form action="" method="post">
    <h3>Stávající zákazník - přihlásit se</h3>
    <div class="row">
        <div class="large-7 medium-9 columns">
            <div class="row">
                <div class="large-6 medium-6 columns"><input type="text"
                    placeholder="Emailová adresa" name="username" class="{if !empty($login_errors.username)}error{/if}"/>
                    {if !empty($login_errors.username)}<small class="error">{$login_errors.username}</small>{/if}
                </div>

                <div class="large-6 medium-6 columns">

                <input type="password"
                placeholder="Heslo" name="password" class="{if !empty($login_errors.password)}error{/if}"/>
                {if !empty($login_errors.password)}<small class="error">{$login_errors.password}</small>{/if}
                </div>
            </div>
            <a href="/zapomenute-heslo">Zapoměl jste heslo?</a>
            <button type="submit" class="button right">Přihlásit se &raquo;</button>
        </div>
        <div class="large-5 medium-3 columns">
        </div>
    </div>
    {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
    {hana_secured_post action="login" module="user"}
  </form>
</div>
