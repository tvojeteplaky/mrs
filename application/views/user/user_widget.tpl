

<div class="login-container">
 <div class="row">
                        <div class="columns">
{if !$logged_in}
 <div class="login">
                                 <span>
                                     <a href="#" class="login-click">{translate str="Přihlásit se"}</a>
                                    <a href="#" class="register-click">{translate str="Registrovat se"}</a>
                                 </span>
                            </div>
{else}
<span>{translate str="Přihlášený uživatel"}:</span> <a href="{$url_base}ucet-uzivatele" class="bold">{$user.nazev}</a><br> <a href="{$url_base}ucet-uzivatele-nastaveni">[{translate str="nastavení účtu"}]</a> <a href="{$url_base}{translate str="odhlasit"}">[{translate str="odhlásit"}]</a>
{/if}
	</div></div>
	</div>


  {widget controller="user" action="show_registration_form_popup"}
  {widget controller="user" action="show_login_form_popup"}