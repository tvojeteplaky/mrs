{* tabulka s vypisem pobocek (fakturacnich adres) uzivatele *}
<h3>{translate str="Seznam doručovacích adres"}</h3>
  {if $branches|@count>0}
  <table summary="Seznam dodacích adres" class="branchesTable">
  {foreach from=$branches key=key item=item}
  <tr><td class="strong">{$item.nazev}</td><td>{$item.ulice}</td><td>{$item.psc}, {$item.mesto}</td><td><a href="{$url_base}{translate_route route="editace-dodaci-adresy"}?branch_id={$item.id}">{translate str="Upravit"}</a></td> <td><a href="?branch_id={$item.id}&h_param={translate str="ucet-uzivatele"}&{hana_secured_get module="user" action="branch_delete"}" onclick="javascript:return confirm('Opravdu smazat pobočku {$item.nazev}?');">{translate str="Smazat"}</a></td></tr>
  {/foreach}
  </table>
  {else}
  <p> -- {translate str="nebyla vložena žádná doručovací adresa"} -- </p>
  {/if}
  <br />
<p><a class="right" href="{$url_base}editace-dodaci-adresy">{translate str="Vložit novou doručovací adresu"}</a></p>