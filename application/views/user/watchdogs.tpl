
{widget name="subnav" controller="user" action="subnav"}


<div class="col-md-9 col-sm-8">
    {widget name="breadcrumbs" controller="navigation" action="breadcrumbs"}
    <div id="dogs">
        <div class="table-responsive">
            <table class="table" id="watchdogs-table">
                <thead>
                    <th>
                        Název produktu
                    </th>
                    <th>
                        Aktuální cena
                    </th>
                    <th>
                        Hlídaná cena / dostupnost
                    </th>
                    <th></th>
                </thead>
                <tbody>
                {foreach from=$dogs item=item}
                    <tr>
                        <td>
                            <a href="{$url_base}{$item.product.nazev_seo}">
                                {$item.product.nazev}
                            </a>
                        </td>
                        <td>
                            {$item.product.cena_s_dph|currency:$language_code}
                        </td>
                        <td>
                            <form action="" method="post" class="edit">
                                <div class="input-group input-group-sm">
                                    <input type="number" min="0" class="form-control" value="{$item.price}" name="price" data-id="{$item.id}">
                                    <input type="hidden" name="product_id" value="{$item.product.id}">
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="in_stock" value="1" {if $item.in_stock}checked="checked"{/if}>
                                    </span>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">
                                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>

                                {hana_secured_post action="add_watch" module="watchdog"}
                            </form>
                        </td>
                        <td class="text-right">
                            <form action="" method="post" class="delete">
                                    <input type="hidden" name="watchdog_id" value="{$item.id}">
                                    {hana_secured_post action="delete_watch" module="watchdog"}
                                    <button class="btn btn-default btn-sm" type="submit">
                                        <span class="glyphicon glyphicon-remove red" aria-hidden="true"></span>
                                    </button>
                            </form>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        initialize_watchdogs({$shopper.id});
    })
</script>
