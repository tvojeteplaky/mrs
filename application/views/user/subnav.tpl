<ul class="side-nav" role="navigation" title="Menu">
    {foreach name=navL1 from=$links key=key item=item}
    <li{if not empty($item.children)} class="accordion-navigation"{/if}><a href="{if empty($item.children)}{$url_base}{if empty($item.indexpage)}{$item.nazev_seo}{/if}{else}#panel{$smarty.foreach.navL1.iteration}a{/if}">{$item.nazev}</a>
        {if not empty($item.children)}
        <ul id="panel{$smarty.foreach.navL1.iteration}a" class="secondary-nav content">
            {foreach from=$item.children item=item2 key=key name=navL2}
            <li><a href="{$item2.nazev_seo}">{$item2.nazev}</a></li>
            {/foreach}
        </ul>
    </li>
    {/if}
    {/foreach}
</ul>