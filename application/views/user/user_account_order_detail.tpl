{* detail objednavky uzivatele - v uzivatelskem uctu *}

<div class="row">
  <div class="large-4 columns">{widget name="subnav" controller="user" action="subnav"}</div>
  <div class="large-8 columns">
    <h1>Objednávka číslo: {$order_code}</h1>
    
    <div id="ShoppingDetailSection">
      <div class="correct bottomLine"></div>
      <div id="OrderCompleteSectionBottom">
      
      <div class="colHalf">
        <h2>Dodavatel</h2>
        <strong>{$owner_data.firma}</strong><br />
        {$owner_data.ulice}<br />
        {$owner_data.mesto}<br />
        {$owner_data.psc}<br />
        <br />
        {$owner_data.email}<br />
        {$owner_data.tel}<br /> 
        <br />
        IČ: {$owner_data.ic}<br />
        DIČ: {$owner_data.dic}<br />
       
      </div>
      
      <div class="colHalf">
        <h2>Odběratel</h2>
        <strong>{$billing_data.nazev}</strong><br />
        {$billing_data.ulice}<br />
        {$billing_data.mesto}<br />
        {$billing_data.psc}<br />
        <br />
        {$billing_data.email}<br />
        {$billing_data.telefon}<br />
        <br />
        <table summary="Způsob dopravy a platby">
          <tr>
            <td class="bold" style="width: 70px">Doprava: </td>
            <td>{$doprava}</td>
          </tr>
          <tr>
            <td class="bold">Platba: </td>
            <td>{$platba}</td>
          </tr>
        </table>
      </div>
      
      <div class="colHalf correct"></div>
      <div class="colHalf">
      
      <h3>Dodací adresa</h3>
        {if !$branch_data || !$branch_data.email}
        Stejná jako fakturační
        {else}
        
        {$branch_data.nazev}<br />
        {$branch_data.ulice}<br />
        {$branch_data.mesto}<br />
        {$branch_data.psc}<br />
        <br />
        {$branch_data.email}<br />
        {$branch_data.telefon}<br />
        
        {/if}
      
      </div>
      <div class="correct"></div>
      
      <h2>Zakoupené zboží</h2>
      <table class="orderSectionTable productListTable" style="width: 100%">
          <th class="order-col-nazev">Název produktu</th>         
          <th class="w100">Množství</th>        
          <th class="txtRight w100">Cena s DPH za ks</th>   
          <th class="txtRight w100">Cena s DPH</th>   
 
        </tr>
  
        {foreach key=k item=product name=prod from=$cart_products}
         <tr>
           {*
           <td>
           {$product.code}
           </td>
           *}
        
           <td>
           {$product.nazev}
           </td>
           <td class="txtRight">
           {$product.units}
           </td>
           <td class="txtRight">
           {$product.price_with_tax|currency:'cz'}
           </td>
           <td class="txtRight">
           {$product.total_price_with_tax|currency:'cz'}
           </td>
         </tr>
       {/foreach}
      </table> 
      
      
      <table summary="celková cena" class="right priceSummaryTable">
      {if !empty($price_values.voucher_discount) && $price_values.voucher_discount>0}
      <tr class="priceSummaryRow">
        <td colspan="2">&nbsp;</td>
        <td class="txtRight">(Celková sleva na kupón - již započítána v cenách produktů: </td>
        <td colspan="2" class="txtRight"> {$price_values.voucher_discount|currency:'cz'})</td>
      </tr>
      {/if}
      <tr class="priceSummaryRow">
        <td colspan="5">&nbsp;</td>
      </tr>  
      <tr class="priceSummaryRow">
        <td colspan="2">&nbsp;</td>
        <td class="txtRight">Cena za platební metodu:</td>
        <td colspan="2" class="txtRight">{if $price_values.payment_price==0}zdarma{else}{$price_values.payment_price|currency:'cz'}{/if}</td>
      </tr>
      <tr class="priceSummaryRow">
        <td colspan="2">&nbsp;</td>
        <td class="txtRight">Cena za dopravu: </td>
        <td colspan="2" class="txtRight">{if $price_values.shipping_price==0}zdarma{else}{$price_values.shipping_price|currency:'cz'}{/if}</td>
      </tr>
      
      
      
      <tr class="summaryRow totalRow"> 
        <td colspan="2">&nbsp;</td>
        <td class="txtRight">
          Cena celkem
        </td>
        <td class="total">
          <span class="bold">{$price_values.total|currency:'cz'}</span>
        </td>
      </tr> 
      <tr class="summaryRow">
        <td colspan="4" class="txtRight"><br />cena zboží bez DPH <span class="bold">{$price_values.total_wo_dph|currency:cz:2}</span></td>
      </tr>
      </table>
      
      <br />
      <div class="correct"></div>
      <h2>Vaše poznámka</h2>
      {$billing_data.poznamka}
      <br /><br />
      </div>
    </div>
  </div>
  </div>


