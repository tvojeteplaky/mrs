{* sablona vstupni registracni stranky uzivatele *}
<div class="row">
  <div class="large-4 columns">{widget name="subnav" controller="user" action="subnav"}</div>
  <div class="large-8 columns">
      <div id="orders">
    <h1>Historie objednávek a faktur</h1>
    <div class="content">{static_content code="uzivatel-moje-objednavky"}</div>
    
    <div id="ShoppingDetailSection">
      {if !empty($unfinished_orders)}
      <table summary="nevyřízené objednávky" class="orderSectionTable productListTable table-striped" style="width: 100%">
    <tr class="headRow"><th>Objednávka</th><th>Datum </th><th>Cena bez DPH</th>{*<th>Datum odeslání objednávky</th>*}<th>Cena s DPH</th><th>Číslo faktury</th></tr>   
        {foreach from=$unfinished_orders key=key item=item}
    <tr title="Detail"  onclick="document.location = 'detail-objednavky-uzivatele/{$item.id}';">
      <td>{$item.order_code}</td>
      <td>{$item.order_date|date:'cz'}</td>
       <td>{$item.order_total_without_vat} Kč </td>
      {*<td>{$item.order_state_popis}</td>*}
      {*<td>{$item.order_close_date|date:'cz'}</td>*}
      <td>{$item.order_total_with_vat} Kč</td>
        <td>{$item.order_code}</td>
      {*<td><a href="{$url_base}detail-objednavky-uzivatele/{$item.id}" class="button">Detail objednávky</a></td>*}
        </tr>
        {/foreach}
      </table>
      {/if}
      
      {if !empty($finished_orders)}
      <h2>Vyřízené objednávky</h2>
      
      <table summary="nevyřízené objednávky" class="orderSectionTable productListTable" style="width: 100%">
        <tr><th>Číslo objednávky</th><th>Datum objednávky</th><th>Stav objednávky</th>{*<th>Datum odeslání objednávky</th>*}<th>Cena celkem</th><th>&nbsp;</th></tr>   
        {foreach from=$finished_orders key=key item=item}
        <tr>
          <td>{$item.order_code}</td>
          <td>{$item.order_date|date:'cz'}</td>
          <td>{$item.order_state_popis}</td>
          {*<td>{$item.order_close_date|date:'cz'}</td>*}
          <td>{$item.order_total_CZK} Kč s <span class="bold">DPH</span></td>
          <td><a href="{$url_base}detail-objednavky-uzivatele/{$item.id}" class="button">Detail objednávky</a></td>
        </tr>
        {*<tr><td colspan="5">&nbsp;</td></tr>*}  
        {/foreach}
      </table>
      {/if}
      {if empty($unfinished_orders) && empty($finished_orders)}
      <br />
      <p class="bold">Zatím nebyla provedena žádná objednávka.</p>
      {/if}
    </div>
  </div>
  </div>
</div>

  

 



