{* sablona stranky uzivatele *}



<div class="row">
  <div class="large-4 columns">
    <strong>Můj účet</strong>
    {widget controller="user" action="subnav"}
  </div>
  <div class="large-8 columns">
    <h1>{$item.nadpis}</h1>
    <p><strong>{translate str="Číslo zákazníka"}</strong>: {$shopper.id}</p>
    <h3>{translate str="Základní údaje"}</h3>
    <div class="zakladni-udaje-uzivatele columns">
       <table>
         <tr><td colspan="2" class="title">{$shopper.nazev}</td></tr>
         <tr><td colspan="2">{$shopper.ulice}</td></tr>
         <tr><td colspan="2">{$shopper.psc}, {$shopper.mesto}</td></tr>
         <tr><td colspan="2">&nbsp;</td></tr>
         {if $shopper.nazev_organizace}<tr><td>Název organizace:</td><td class="bold">{$shopper.nazev_organizace}</td></tr>{/if}
         {if $shopper.ic}<tr><td>IČ:</td><td class="bold">{$shopper.ic}</td></tr>{/if}
         {if $shopper.dic}<tr><td>DIČ:</td><td class="bold">{$shopper.dic}</td></tr>{/if}
         {if $shopper.dic || $shopper.ic}<tr><td colspan="2">&nbsp;</td></tr>{/if}
       </table>
       <table>  
         <tr><td>Tel:</td><td class="bold">{$shopper.telefon}</td></tr>
         <tr><td>E-mail:</td><td>{$shopper.email}</td></tr>
       </table>
       <p><a class="right" href="{$url_base}ucet-uzivatele-nastaveni">{translate str="Upravit základní údaje"}</a></p>
     </div>
            
     {$branch_list}
   </div>
   </div>
