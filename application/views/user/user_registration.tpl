{* sablona pro registraci uzivatele *}
{assign var='included' value=$included|default:false}
{assign var='editmode' value=$editmode|default:false}
{if $editmode}
<h1>{if $editmode}{translate str="Změna registračních údajů"}{else}{translate str="Registrace nového uživatele"}{/if}</h1>
{else}
<div id="new" class="order-form">
    <h3>Nový zákazník - registrace</h3>
    {/if}
    <div id="RegistrationForm">
        <form action="#reg" method="post" class="form-inline">
            {if !empty($registration_error)}<div data-alert class="alert-box alert radius">{$registration_error}<a href="#" class="close">&times;</a></div>{/if}
            <div id="RegistrationTypeSrc">
                {if !$editmode and !$included}
                <label for="RegistrationTypeCompany"> <input type="radio" name="user-registration[typ_uzivatele]" value="company" id="RegistrationTypeCompany" class="registrationType" {if empty($data.typ_uzivatele) || $data.typ_uzivatele=="company"}checked="checked"{/if} />
            {translate str="Registrace firmy"}</label>
            &nbsp; &nbsp;
            <label for="RegistrationTypeUser">  <input type="radio" name="user-registration[typ_uzivatele]" value="user" id="RegistrationTypeUser" class="registrationType" {if !empty($data.typ_uzivatele) && $data.typ_uzivatele=="user"}checked="checked"{/if} />
        {translate str="Registrace uživatele"}</label>
        {/if}
    </div>
    {if $editmode}
    <fieldset>
        {form_input name="user-registration.nazev"  class='form-control' label="Jméno a příjmení *" data=$data errors=$errors}
        {form_input name="user-registration.email" class='form-control'  label="Email *" data=$data errors=$errors}
        {form_input name="user-registration.telefon" class='form-control' label="Telefon *" data=$data errors=$errors}
        {form_password name="user-registration.password"  class='form-control'   label="Heslo *" errors=$errors}
        {form_password name="user-registration.password_confirm"  class='form-control'  label="Potvrdit heslo *" errors=$errors}
    </fieldset>
    <fieldset id="CompanyFields">
        {form_input name="user-registration.ic" class='form-control' label="IČ" data=$data errors=$errors}
        {form_input name="user-registration.nazev_organizace"  class='form-control' label="Název organizace" data=$data errors=$errors}
        {form_input name="user-registration.dic"  class='form-control' label="DIČ" data=$data errors=$errors}
    </fieldset>
    <fieldset>
        {form_input name="user-registration.ulice" class='form-control'  label="Ulice a číslo popisné *" data=$data errors=$errors}
        {form_input name="user-registration.psc" class='form-control'  label="PSČ *" data=$data errors=$errors}
        {form_input name="user-registration.mesto" class='form-control'  label="Město *" data=$data errors=$errors}
    </fieldset>
    <div class="row">
        <div class="columns">
        <button type="submit" class="right">Uložit údaje</button>
        {if !$editmode}<input type="checkbox" name="user-registration[obchodni_podminky]" value="agree" class="checkbox" /> &nbsp; {translate str="Souhlasím s"} <a target="_blank" href="{$url_base}obchodni-podminky" title="{translate str="Souhlasím s obchodními podmínkami"}">  {translate str="obchodními podmínkami"}</a>
        {if !empty($errors.obchodni_podminky)}<span class="error"> - {translate str="nutno potvrdit souhlas"}</span>{/if}
        {else}<input type="hidden" name="user-registration[obchodni_podminky]" value="agree" />
        {/if}
    </div>
    </div>
    {else}
    <div class="row">
        <div class="large-7 medium-9 columns">
            <div class="row">
                <div class="small-6 columns"><label><input type="radio" name="user-registration[typ_uzivatele]" value="user" id="RegistrationTypeUser" class="registrationType" {if empty($data.typ_uzivatele) || $data.typ_uzivatele=="user"}checked="checked"{/if} /> {translate str="Registrace uživatele"}</label></div>
                <div class="small-6 columns"><label><input type="radio" name="user-registration[typ_uzivatele]" value="company" id="RegistrationTypeCompany" class="registrationType" {if !empty($data.typ_uzivatele) && $data.typ_uzivatele=="company"}checked="checked"{/if} /> {translate str="Registrace firmy"}</label></div>
            </div>
            <div class="row">
                <div class="large-6 medium-6 columns">
                {form_input name="user-registration.nazev"  placeholder="Jméno a příjmení" data=$data errors=$errors}
                </div>
                <div class="large-6 medium-6 columns">
                {form_input name="user-registration.email" placeholder="Email" data=$data errors=$errors}
                </div>
            </div>
             <div class="row">
                <div class="large-6 medium-6 columns">
                {form_password name="user-registration.password"  placeholder="Heslo" errors=$errors}
                </div>
                <div class="large-6 medium-6 columns">
                {form_password name="user-registration.password_confirm"  placeholder="Potvrdit heslo" errors=$errors}
                </div>
            </div>
            <div class="row">
                <div class="large-6 medium-6 columns">
                  {form_input name="user-registration.telefon" placeholder="Telefonní číslo" data=$data errors=$errors}
                </div>
                <div class="large-6 medium-6 columns">
                {form_input name="user-registration.ulice" placeholder="Ulice a číslo popisné" data=$data errors=$errors}
                </div>
            </div>
            <div class="row">
                <div class="large-6 medium-6 columns">
                {form_input name="user-registration.psc" placeholder="PSČ" data=$data errors=$errors}
              </div>
              <div class="large-6 medium-6 columns">
              {form_input name="user-registration.mesto" placeholder="Město" data=$data errors=$errors}
              </div>
          </div>
        <div class="CompanyFields">
                        <div class="row">
                   <div class="large-12 columns">
                       {form_input name="user-registration.nazev_organizace" placeholder="Název Firmy" data=$data errors=$errors}
                   </div> 
                </div>
          <div class="row">
              <div class="large-6 medium-6 columns">
              {form_input name="user-registration.ic" placeholder="IČ" data=$data errors=$errors}
              </div>
              <div class="large-6 medium-6 columns">
              {form_input name="user-registration.dic"  placeholder="DIČ" data=$data errors=$errors}
              </div>
          </div>
      </div>
<div class="row"> <div class="small-6 medium-6"><input type="checkbox" name="user-registration[obchodni_podminky]" value="agree" class="checkbox" /> &nbsp; Souhlasím s <a target="_blank" href="{$url_base}obchodni-podminky" title="Souhlasím s obchodními podmínkami">    obchodními podmínkami</a>
                    {if !empty($errors.obchodni_podminky)}<span class="error">nutno potvrdit souhlas</span>{/if}</div>

                    <div class="small-6 medium-6"><button type="submit" class="button right"
                >Pokračovat &raquo;</button></div>
                    </div>
</div>
<div class="large-5 medium-3 columns">
</div>
</div>
{/if}
{*
            {if !empty($branch_list)}
            <div class="bottom">
                    {$branch_list}
            </div>
            <div id="correct"></div>
            {/if}
*}
{if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
{hana_secured_post action="registration" module="user"}
{if !$included}
{if $editmode}<a href="{$url_base}{translate str="ucet-uzivatele"}" class="left button">{translate str="Zpět bez uložení"}</a>{/if}
<button type="submit" name="registration">{if $editmode}{translate str="Uložit změny"}{else}{translate str="Registrovat"}{/if}</button>
{/if}
</form>
</div>
{if $included}
</div>
{/if}

{literal}
<script type="text/javascript">
$(document).ready(function(){
if ($('#RegistrationTypeCompany').is(':checked'))
{
    $(".CompanyFields").show();
} else {
    $(".CompanyFields").hide();
}
$(".registrationType").click(function(e){
    if($(this).val()=="company")
    {
        $(".CompanyFields").slideDown();
    }
    else
    {
        $(".CompanyFields").slideUp();
    }
    });


/*$("#ReadFromAres").click(function(){
    $(".ajaxLoader").show();
    $(".aresError").text("");
    $.ajax({
    method: "GET",
    dataType: "json",
    url: "registrace-uzivatele?ic="+$("#input_ic").val(),
    }).done(function(data) {
        $(".ajaxLoader").hide();
        if(data.ares_error)
        {
        $(".aresError").text(data.ares_error);
        }
        else
        {
        $("#input_nazev_organizace").val(data.company_name);
        $("#input_mesto").val(data.municipality);
        $("#input_ulice").val(data.street + " " + data.street_number);
        $("#input_psc").val(data.postal_code);
        $("#input_dic").val(data.tax_id);
        }
    });
});*/
});
</script>
{/literal}
