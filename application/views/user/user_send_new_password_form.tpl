<div class="row">
    <div class="columns">
        <h1>Zaslání zapomenutého hesla</h1>
    </div>
</div>
{if !empty($status_message)}
<div class="row">
    <div class="columns">
        <div data-alert class="alert-box">
            {$status_message}
            <a href="#" class="close">&times;</a>
        </div>
    </div>
</div>
{/if}
{if $show_form}
<div class="row">
  <div class="columns">
    <p>Uveďte prosím svoji e-mailovou adresu, zadanou při registraci. Bude vám doručen e-mail s informacemi pro vygenerování nového hesla:</p>
  </div>
</div>
<form action="#" method="post" class="newPasswordForm">
    <input type="text" name="kontrolni_retezec" style="display: none;">
    <div class="row">
      <div class="columns">
         <label for="email">E-mail
            <input type="email" name="email" id="email"><input type="submit" value="odeslat" name="send" class="button left" />
         </label>
      </div>
    </div>
   
    {hana_secured_post action="send_new_password_link" module="user"}
    
</form>
{/if}
</div>