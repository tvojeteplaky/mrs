<section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<a href="{$url_base}fotogalerie" title="Seznam fotografií" class="sub__banner__btn float-right">Seznam fotografií</a>
			<h1>{$item.nadpis}</h1>
			<p>
				{$item.uvodni_popis}
			</p>
		</div>
	</div>
</section>
<section class="photos__detail wow slideInUp">
{foreach from=$photos item=photo key=key name=gallery}
	{if $smarty.foreach.gallery.first || $smarty.foreach.gallery.index == 6}
		
	<div class="row text-center">
	{/if}
		<div class="large-2 medium-4 small-6 columns end">
			<a href="{$photo.photo.ad}" data-lightbox="roadtrip" data-title="{$photo.popis}"><img class="thumbnail" src="{$photo.photo.small}" alt="{$photo.nazev}"></a>
		</div>
	{if $smarty.foreach.gallery.last || $smarty.foreach.gallery.iteration == 6}
	</div>
	{/if}
{/foreach}
	
	<div class="row">
		<div class="medium-12 columns">
			{$pagination}
		</div>
	</div>
</section>