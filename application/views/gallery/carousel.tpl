{if !empty($photos)}
    <div class="orbit gallery" role="region" aria-label="Gallery" data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
        <ul class="orbit-container">
            <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous</span></button>
            <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next</span></button>
            <li class="orbit-slide is-active">
                <div>
                    <div class="row">
                        {foreach $photos as $photo name=gallery}

                        {if $smarty.foreach.gallery.index > 0 && $smarty.foreach.gallery.index % 4 == 0}
                    </div>
                </div>
            </li>
            <li class="orbit-slide">
                <div>
                    <div class="row">
                        {/if}

                        <div class="medium-3 small-6 columns text-center">
                            <a href="{$photo.photo_detail}" data-fancybox-group="gallery" class="fancybox" title="{$photo.popis}">
                                <img src="{$photo.photo}" alt="{$photo.nazev} preview">
                            </a>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </li>
        </ul>
    </div>
{/if}


