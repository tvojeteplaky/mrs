<div class="row columns wow slideInUp">
	{foreach from=$photos item=photo key=key name=name}
			<div class="medium-4 columns">
				<div class="hp__photos__item">
					<a href="{$url_base}{$photo.nazev_seo}" title="Zobrazit fotogalerii">
						<img src="{$photo.photo_src}" alt="{$photo.nazev}">
						<div class="overlay">
							<h4>{*$photo.nazev*}</h4>
							<p>Album: {$photo.nazev}</p>
						</div>
						<div class="hp__photos__gradient"></div>
						<div class="hp_photos__item__hover">
						</div>
					</a>
				</div>
			</div>
	{/foreach}
	</div>
	<div class="row columns">
			<a href="/fotogalerie" title="Celá fotogalerie" class="hp_button-more">Celá fotogalerie</a>
		</div>
