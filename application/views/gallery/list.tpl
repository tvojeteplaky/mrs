<section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<h1>{$item.nadpis}</h1>
			<p>
				{$item.uvodni_popis}
			</p>
		</div>
	</div>
</section>
<section class="photos wow slideInUp">
{foreach from=$galleries item=gallery_years key=year name=name}
	<div class="row">
		<div class="medium-12">
		 <h2>Fotografie {$year}</h2>
		 {foreach from=$gallery_years item=gallery key=key name=name}
		 	<div class="medium-6 large-3 end columns">
				<div class="photos__item">
				<img src="{$gallery.photo.small}">
				<div class="overlay">
					<a href="{$url_base}{$gallery.nazev_seo}" title="Zobrazit celou fotogalerii">
					<h3>{$gallery.nazev}</h3>
					<span>Počet fotografií: {$gallery.count}</span>
					</a>
				</div>
				</div>
			</div>
		 {/foreach}
		</div>
	</div>
{/foreach}
</section>