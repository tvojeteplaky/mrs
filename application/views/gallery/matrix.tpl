{if !empty($photos)}
    <div class="row matrix-gallery">
        {foreach $photos as $photo}
            <div class="large-3 medium-4 small-6 columns text-center item">
                <a href="{$photo.photo_detail}" data-fancybox-group="matrix_gallery" class="fancybox" title="{$photo.popis}" alt="{$photo.nazev}">
                    <img src="{$photo.photo}" alt="{$photo.nazev} preview">
                    <div class="text-left">{$photo.nazev}</div>
                </a>
            </div>
        {/foreach}
    </div>
{/if}


