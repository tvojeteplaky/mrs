{if !empty($photos)}
    <div class="owl-carousel-default">
        {foreach $photos as $photo}
            <div class="slide item">
                <a href="{$photo.photo_detail}" rel="gallery" class="fancybox" title="{$photo.popis}" alt="{$photo.nazev}">
                    <img src="{$photo.photo}" alt="{$photo.nazev} preview">
                </a>
            </div>
        {/foreach}
    </div>
{/if}