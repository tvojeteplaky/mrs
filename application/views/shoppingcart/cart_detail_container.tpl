{* obalujici formular nakupniho kosiku s navigacnimi tlacitky *}
{*<h1>{translate str="Nákupní košík"}</h1>*}
{if $total_products >0}
  {* obalujici formular nakupniho kosiku s navigacnimi tlacitky *}
  <form action="" method="post" class="quantityForm">

  <div class="shopping-chart-body">
    <div class="row">
        <div class="columns">
    {* tabulka s obsahem kosiku *}
    {$cart_detail_table}
    
    
    {* tabulka s cenovymi soucty *}
    {*{$order_price_summary_table}*}
  
    {if $priceToFreeShipping > 0}
    <div class="shippingInfo"><span class="bold">Získejte dopravu zdarma!</span><br />
    Nakupte ještě alespoň za {$priceToFreeShipping} Kč a získejte dopravu zdarma.
    </div>
    {/if}
    {hana_secured_multi_post module="shoppingcart" action="refresh"}

     <button class="button right" name="go_to_order" type="submit">Pokračovat &raquo;</button>
                <button class="button right secondary" name="refresh" type="submit">Přepočítat ceny</button>
               
            </div>
        </div>
    </div>
     {hana_secured_multi_post action="go_to_order"}
    
    {*<div class="orderButtons">
      <a href="{$url_base}kategorie" title="pokračovat v nákupu" class="btn btn-army prev_step">Pokračovat v nákupu</a>
     
      <button class="btn btn-army next_step" type="submit" name="go_to_order">Pokračovat</button>*}
     
  </form>
  
{else}
<p>
Košík neobsahuje žádné zboží.
</p>
{/if}
</div>   
   
   
   
   
   
   {literal}
    <script type="text/javascript">
      $(document).ready(function() {
        
        $(".ajaxradio").change(function(){
          //$('#payment-table').showLoading();
          AjaxSubmitForm(); 
        });
        
        function AjaxSubmitForm(){
          $.ajax({
                 type: "POST",
                 data: $(".cartOrderForm").serialize(),
                 //url: "url::base().url::current().Kohana::config('config.url_suffix').",
                 success: function(response){
                 var json = eval("(" + response + ")");
                 //$('#page-title').html(json.page_title);
                 //$('#page-description').html(json.page_description);
                 //$('#page-keywords').html(json.page_keywords);
                 $('#CenterSection').html(json.main_content);
                 $('#ShoppingSection').html(json.cart_widget);
                 $('#ShopperSection').html(json.user_widget);
                 $('#payment-table').hideLoading();
                 },
                 error: function(XMLHttpRequest, textStatus, errorThrown){
                 alert("Chyba: "+textStatus);
                 }
         });
       }
      
     });
    </script>
  {/literal}
