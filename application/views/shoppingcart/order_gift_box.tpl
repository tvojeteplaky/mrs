{* darkove krabicky *}
{if !empty($gift_boxes)}
<div id="GiftBoxes">
<h3>Výběr dárkového balení</h3><br />
{foreach $gift_boxes as $gift_box}
  <div class="item">
    <div class="photo"><a href="{$media_path}photos/giftbox/item/images-{$gift_box.id}/{$gift_box.photo_src}-ad.jpg" class="lightbox-enabled" rel="lightbox-box-{$gift_box.id}" title="{$gift_box.code}"><img src="{$media_path}photos/giftbox/item/images-{$gift_box.id}/{$gift_box.photo_src}-t1.jpg" title="{$gift_box.code}" /></a></div>
    <label for="gb_{$gift_box.id}">
    <input type="radio" name="gift_box" value="{$gift_box.id}" id="gb_{$gift_box.id}" {if ($selected_gift_box==null && $gift_box@iteration==1) || $selected_gift_box==$gift_box.id}checked="checked"{/if}> <span>{$gift_box.code}</span>
    </label>
  </div>
{/foreach}
<div class="correct"></div>
</div>
<div id="GiftBoxesBottom"></div>
{/if}