{* sablona detailu kosiku - podle nastaveni - moznost zobrazeni v editovatelnem, nebo v read-only modu *}
{* tato sablona je soucasti objednavky (krok 1. - kosik) - nadrazena sablona: order_container.tpl *}
{assign var='read_only' value=$read_only|default:false}

            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Kód produktu</th>
                            <th>Název produktu</th>
                            <th class="r">Cena za kus bez DPH</th>
                            <th class="r">Cena za kus s DPH</th>
                            <th width="80px" class="r">Počet kusů</th>
                            <th class="r">Cena celkem bez DPH</th>
                            <th class="r">Cena celkem s DPH</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach key=k item=product name=prod from=$cart_products}
                        <tr>
                            <td><img src="{$product.parent.photo}" style="max-width: 51px;" alt=""/></td>
                            <td>{$product.code}</td>
                            <td><a href="{$url_base}{$product.parent.nazev_seo}">{$product.parent.nazev} - {$product.nazev} </a><p class="cart-paragraf">{$product.pocet_na_sklade}</p></td>
                            <td class="r">{$product.cena_bez_dph|currency:$language_code}</td>
                            <td class="strong" class="r">{$product.cena_s_dph|currency:$language_code}</td>
                            <td class="r">{if !$read_only}<input type="number" title="Obnovit množství v košíku" class="txtQuantity" min="1" max="999" name="product_quantity[{$product.id}]" value="{$product.mnozstvi}" />{else}{$product.mnozstvi}{/if}</td>
                            <td class="r">{$product.cena_celkem_bez_dph|currency:$language_code}</td>
                            <td class="strong r">{$product.cena_celkem_s_dph|currency:$language_code}</td>
                            <td class="r">{if !$read_only} <a href="?item_id={$product.id}&amp;{hana_secured_get module="shoppingcart" action="remove_item"}">ODSTRANIT</a>{/if}</td></tr>
                      {/foreach}
                        <tr><td colspan="6"><span class="right">Celkem v košíku:</span></td><td class="r" colspan="2"><span>{$price_total_products|currency:$language_code}</span></td><td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <input type="hidden" name="temp" id="TempProperty" value="1" />
                <a class="button left secondary" href="{$url_base}nabidka ">&laquo Pokračovat v nákupu</a>
                







    {*<h2>Nákupní košík</h2>*}
   {* <div class="productListTable   ">
        <table  summary="nákupní košík" border="0" class="orderSectionTable table shopping_cart        {if !$read_only} deletable{/if} ">
            <tr class="headRow">
                <th class="foto">Foto</th>
                <th class="">Kod produktu </th>
                <th class="kod">Název produktu</th>
                <th class="txtRight w120">Cena</th>
                <th class="w100">Množství</th>
                <th class="txtRight w120">Dostupnost</th>
                <th class="txtRight w100">Celkem Kč</th>

                <th class="w30">&nbsp;</th>
            </tr>

            {foreach key=k item=product name=prod from=$cart_products*}
            {*if $product.cena_s_dph>0 || $read_only*}{* v kosiku mohou byt darky *}
           {* <tr class="{if $smarty.foreach.prod.iteration mod 2 == 0}sectiontableentry2{else}sectiontableentry1{/if} select_table_row">
                <td><img src="{$media_path}photos/product/item/images-{$product.id}/{$product.photo_src}-t2.jpg" width="51" /></td>

                <td>{$product.code}&nbsp;</td>
                <td><a class="productTitle" href="{$product.nazev_seo}">{$product.nazev}</a></td>*}


                {*  <td class="txtRight bold">{if $product.cena_bez_dph>0}{$product.cena_bez_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>
                <td class="txtRight">{if $product.cena_celkem_bez_dph>0}{$product.cena_celkem_bez_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>
                <td class="txtRight">{if $product.cena_celkem_s_dph>0}{$product.cena_celkem_s_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>*}
                {*<td class="txtRight price_item text-right">{if $product.cena_s_dph>0}{$product.cena_s_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>
                <td>*
                    {*if !$read_only}
                                        <input type="text" title="Obnovit množství v košíku" class="txtQuantity" size="4" maxlength="4" name="product_quantity[{$product.id}]" value="{$product.mnozstvi}" />
                    {else*}
                    {*$product.mnozstvi*}
                    {*/if*}
                    {*$product.jednotka}
                </td>

                <td>dostupnost</td>
                <td class="txtRight price_item text-right">{if $product.cena_celkem_s_dph>0}{$product.cena_celkem_s_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>
                <td>
                    {if !$read_only}
                    <a href="?item_id={$product.id}&{hana_secured_get module="shoppingcart" action="remove_item"}" class="cartActionButton" id="remove_item"><img src="{$media_path}img/ico_delete.gif" alt="Vyjmout zboží z košíku" /></a>
                    {else}
                    &nbsp;
                    {/if}
                </td>
            </tr>
            {/if}
            {/foreach}
        </table>
        <table id="summary_table">*}
            {* mezisoucty *}
            {*<tr class="lastRow">*}
                {*
                    <td>
                    {*if !$read_only}
                        <input type="hidden" name="temp" id="TempProperty" value="1" />
                        
                        {hana_secured_multi_post module="shoppingcart" action="refresh"}
                        <input class="button refresh cartActionButton" type="submit" name="refresh" id="refresh" value="přepočítat" />
                                            
                    {*<a href="#"  onclick="{literal}javascript:jQuery('form[class=\'quantityForm\']').submit(); return false;{/literal}">přepočítat</a>*}
                    {*/if}
                </td>  *}
              {*  <td  ><span class="noInPopup">Celkem v košíku</span></td>
                <td  class="total_price" >{$price_total_products|currency:$language_code}</td>

            </tr>*}

            {* funkcionalita nabidnuti darku k nakupu *}
            {*if !empty($presents) && !$read_only}
            <tr class="noInPopup"><td>&nbsp;</td></tr>
            {foreach name=pre from=$presents key=key item=item}
            <tr {if $smarty.foreach.prod.iteration mod 2 == 0}class="sectiontableentry2 noInPopup"{else}class="sectiontableentry1 noInPopup"{/if}>
                <td><input class="txtQuantity" type="radio" name="gift_item" value="{$item.id}" {if empty($sel_present) && $smarty.foreach.pre.first}checked="checked"{/if}{if !empty($selected_present) && $selected_present==$item.id}checked="checked"{/if} /><a href="{$item.nazev_seo}">{$item.nazev}</a></td>
                <td>
                    1 ks
                </td>
                <td class="txtRight bold">{translate str="zdarma"}</td>
                <td class="txtRight">{translate str="zdarma"}</td>

            </tr>
            {/foreach}
            {/if}
        </table>
    </div>
    <div class="productListTableBottom"></div>*}
    {*
    {if !empty($voucher) && $voucher}
    <div class="voucherLine bold noInPopup">{translate str="Slevový kupón byl započítán do cen produktů, sleva ve výši:"} {$voucher.discount_value}%</div>
    {else}
        {if !$read_only}
        <div class="defaultBoxSlim right noInPopup">
            <div><input type="checkbox" name="ExtendFormChb" id="ExtendFormChb" /> <label for="ExtendFormChb" id="ExtendFormLab" class="bold">Mám slevový kupón, který chci uplatnit</label></div>
            <div id="ExtendForm" class="noDisplay">
                    <div class="content defaultMT">
                        <div class="voucherLine">
                                <label for="voucher">{translate str="Zadejte číslo slevového kupónu"}:</label>
                                <input type="text" name="voucher" id="voucher" />
                        </div>
                    </div>
                    <div class="bottom"></div>
            </div>
        </div>
        {/if}
    {/if}
    *}
    {literal}
    <script type="text/javascript">
        $(document).ready(function() {
        $("#ExtendFormChb").click(function(){
            $("#ExtendForm").slideToggle();
        });
        
        if($('#ExtendFormChb').is(':checked'))
        {
        $("#ExtendForm").show();
        }
        
        });
    </script>
    {/literal}

    {*
    <img class="right_close" src="{$media_path}img/close.png" onclick="$(document).ready(function() { $('.bs-example-modal-lg').modal('hide');})"/>
    <h2>Vložili jste do košíku</h2>
    {foreach key=k item=product name=prod from=$cart_products}
    {if $product.cena_s_dph>0 || $read_only}{* v kosiku mohou byt darky *}{*
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12">
            <img class="img-responsive" src="{$media_path}photos/product/item/images-{$product.id}/{$product.photo_src}-t2.jpg" />
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            <h3><a class="productTitle" href="{$product.nazev_seo}">{$product.nazev}</a></h3>
            <div class="row">
                <div class="col-md-2 col-sm-3 col-xs-6"> Množství</div>
                <div class="col-md-2 col-sm-3 col-xs-6" > {$product.mnozstvi}</div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-3 col-xs-6"> Cena</div>
                <div class="col-md-2 col-sm-3 col-xs-6"> {if $product.cena_celkem_s_dph>0}{$product.cena_celkem_s_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</div>

            </div>
        </div>
    </div>

</div>
{/if}
{/foreach}

*}