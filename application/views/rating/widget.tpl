<div class="row" id="rating-widget">
    <div class="col-sm-12">
        <div class="row top">
            <div class="col-sm-4">
                <span class="average">Průměrné hodnocení:</span>
            </div>
            <div class="col-sm-8">
                {for $i = 1 to $loops}
                    <span class="average-star">
                        <span class="star" {if $i == $loops}style="width: {$last_width}px"{/if}>
                        </span>
                    </span>
                {/for}
                {for $i = 1 to 5-$loops}
                    <span class="empty-star">
                    </span>
                {/for}
            </div>
        </div>
        <div class="row middle">
            <form action="#RatingForm" method="post" class="form">
                <h3>{translate str="Vaše hodnocení"}</h3>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="name">{translate str="Jméno"}</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="rating[name]" required class="form-control" id="name" value="{if $logged}{$user.nazev}{elseif isset($data.name)}{$data.name}{/if}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="email">{translate str="Email"}</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="email" name="rating[email]" class="form-control" id="email" value="{if $logged}{$user.email}{elseif isset($data.email)}{$data.email}{/if}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="email">{translate str="Komentář"}</label>
                    </div>
                    <div class="col-sm-8">
                        <textarea name="rating[comment]" class="form-control" id="email">{if isset($data.comment)}{$data.comment}{/if}</textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="email">{translate str="Hodnocení"}</label>
                    </div>
                    <div class="col-sm-8">
                        <span id="rating-stars">
                            {for $i = 1 to 5}
                                <span class="star" data-rating="{$i}" data-active="false"></span>
                            {/for}
                        </span>
                        <input type="hidden" value="0" name="rating[rating]" required class="form-control" id="rating">
                    </div>
                </div>
                <input type="hidden" name="rating[product_id]" value="{$product.id}">
                {hana_secured_post action="save" module="rating"}
                <button type="submit" class="btn btn-army" name="send" class="button right defaultMT">Odeslat</button>
            </form>
        </div>
        <div class="row bottom">
            <div class="col-sm-12">
                <h3>{translate str="Hodnocení zákazníků"}</h3>
                {foreach from=$ratings item=rating}
                    <div class="row user-rating">
                        <div class="col-sm-12">
                            <header class="row">
                                <div class="col-sm-3">
                                    <span class="jmeno">{$rating.name}</span>
                                </div>
                                <div class="col-sm-3">
                                    {for $i = 1 to $rating.rating}
                                        <img src="{$media_path}img/star_full_light.png">
                                    {/for}
                                </div>
                                <div class="col-sm-6 text-right">
                                    {$rating.created|date_format:"j. n. Y"}
                                </div>
                            </header>
                            <article>
                                {$rating.comment}
                            </article>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>