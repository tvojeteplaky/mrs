{if !empty($items)}
    <div class="backup-menu">
        <div class="row">
            <ul class="breadcrumbs">
                <li><a href="{$url_homepage}">{translate str="Hlavní stránka"}</a></li>
                {foreach from=$items item=item key=key}
                    {if isset($item.nazev)}
                        <li><a href="{if isset($item.nazev_seo)}{$url_base}{$item.nazev_seo}{else}#{/if}">{$item.nazev}</a></li>
                    {/if}
                {/foreach}
            </ul>
        </div>
    </div>
{/if}