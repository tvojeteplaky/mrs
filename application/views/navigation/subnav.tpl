{if !empty($items)}
    <ul class="main-submenu">
        {foreach $items as $item}
            <li class="{if array_key_exists($item.nazev_seo, $breadcrumbs)}active{/if}">
                <a href="{$url_base}{$item.nazev_seo}" title="{$item.nazev}">
                    {$item.nazev}
                </a>
            </li>
        {/foreach}
    </ul>
{/if}