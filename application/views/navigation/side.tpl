<!-- whatever you want goes here -->
<ul>
    {foreach $links as $link}
        <li>
            {if $link.unclickable}
                <label>{$link.nazev}</label>
            {else}
                <a href="{if isset($link.url) && $link.url}{$link.url}{else}{$url_base}{$link.nazev_seo}{/if}" class="{if array_key_exists($link.nazev_seo, $sel_links)}active{/if} {if !empty($link.children)}has-children{/if}">{$link.nazev}</a>
            {/if}
            {if !empty($link.children)}
                </li>
                {foreach $link.children as $child name=children}
                <li class="children">
                    <a href="{$url_base}{$child.nazev_seo}" class="{if array_key_exists($child.nazev_seo, $sel_links)}active{/if} child">{$child.nazev}</a>
                {if !$smarty.foreach.children.last}
                    </li>
                {/if}
                {/foreach}
            {/if}
        </li>
    {/foreach}
</ul>