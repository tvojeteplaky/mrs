{* dokument s objednavkou - detail objednavky, nebo obsah mailu *}	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="cs" />
  </head>
  <body style="text-align: left; font-family: Courier, serif; line-height: 1.5; font-size: 10pt; ">
  <table>
     <tr>
       <td colspan="2"><h1 style="font-family: Times New Roman, serif; font-size: 10pt; text-align:right; font-weight:bold;">ZÁLOHOVÁ FAKTURA &nbsp; <span>{$order.order_code_invoice}</span></h1></td>
     </tr>
     <tr>
       <td colspan="2">&nbsp;</td>
     </tr>
     <tr>
       <!-- sloupec s dodavatelem -->
       <td style="width: 400px; vertical-align: top;">
         
         <div style="font-weight: bold;">Dodavatel:</div>
         {*<img src="{$resources_path}img/img_dodavatel_logo.gif" />*}
  
           <table>
             <tr><td colspan="2">{$order.owner_firma}</td></tr>
             <tr><td colspan="2">{$order.owner_ulice}</td></tr>
             <tr><td colspan="2">{$order.owner_psc} {$order.owner_mesto}</td></tr>
             <tr>
               <td colspan="2">&nbsp;</td>
             </tr>
             
             <tr><td>Ústav:</td><td>{$order.owner_banka}</td></td>
             <tr><td>Účet:</td><td>{$order.owner_cislo_uctu}</td></td>
             <tr><td>IČ:</td><td>{$order.owner_ic}</td></td>
             <tr><td>DIČ:</td><td>{$order.owner_dic}</td></td>
             
             <tr>
               <td colspan="2">Údaj o spisové značce:<br /> Krajský soud v Brně <br /> oddíl C, vložka 21356</td>
             </tr>
             
             <tr><td>Tel.:</td><td>{$order.owner_tel}</td></td>
             <tr><td>Fax:</td><td>{$order.owner_fax}</td></td>
             <tr><td>Iban:</td><td>{$order.owner_iban}</td></td>
             <tr><td>SWIFT:</td><td>{$order.owner_swift}</td></td>
           </table>  
            
       </td>
       
       <!-- sloupec s odberatelem -->
       <td style="width: 400px; vertical-align: top;">
         <div style="font-weight: bold;">Odběratel:</div>
         <br />
         <div>IČ odb:{$order.order_shopper_ic}</div>
         <div>DIČ odb:{$order.order_shopper_dic}</div>
         <br />
           
           <table style="border: 2px solid #000; width:250px">
             <tr><td colspan="2" class="title">{$order.order_shopper_name}<br /></td></tr>
             <tr><td colspan="2">{$order.order_shopper_street}</td></tr>
             <tr><td colspan="2">{$order.order_shopper_zip} {$order.order_shopper_city}</td></tr>
             <tr><td colspan="2">&nbsp;</td></tr>

             <tr><td>Tel:</td><td style="font-weight:bold;">{$order.order_shopper_phone}</td></tr>
             <tr><td>E-mail:</td><td>{$order.order_shopper_email}</td></tr>
           </table>
           <!-- alternativne dodaci adresa -->
           <br />
           {*
           <div style="font-weight: bold;">Dodací adresa:</div>
           {if $order.no_delivery_address}
           <p>stejná jako fakturační</p>
           {else}
           
           <table>
             <tr><td colspan="2">{$order.order_branch_name}</td></tr>
             <tr><td colspan="2">{$order.order_branch_street}</td></tr>
             <tr><td colspan="2">{$order.order_branch_zip}, {$order.order_branch_city}</td></tr>
             <tr><td colspan="2">&nbsp;</td></tr>
              <tr>  			
                <td>Telefon: &nbsp;</td>			
                <td>{$order.order_branch_phone}</td>		  
              </tr>		  		  
              <tr>  			
                <td>E-mail:</td>			
                <td>{$order.order_branch_email}</td>		  
              </tr>	
           </table>
           
           {/if}
           *}
         </div>
         {*
         <!-- volitelny text -->
         <!--
         <p class="p20">
            Tellus dictumst molestie Lorem Sed Cras Pellentesque quis nec egestas et. Quis tempor wisi at facilisi velit sem Nullam Sed Vestibulum ante. Id In pretium eu convallis Integer et at ac augue metus.
         </p>
         -->
         *}
       </td>
       </tr>
       <tr><td colspan="2">&nbsp;</td></tr>
      
    </table>
    

      <table style="width: 800px">
         <tr>
         <td style="width: 400px">
         <table>
           <tr>
             <td>Dodalo střed.:</td>
             <td>&nbsp;</td>
           </tr>
           <tr>
             <td>Dodací list:</td>
             <td>&nbsp;</td>
           </tr>
           <tr>
             <td>Objednávka/HS:</td>
             <td>&nbsp;</td>
           </tr>
           <tr>
             <td>Způsob dopravy:</td>
             <td>{$order.order_shipping_nazev}</td>
           </tr>
           <tr>
             <td>Kon. příjemce:</td>
             <td>Dtto</td>
           </tr>
         </table>
      </td>
      <td style="width: 400px">
          <table>
           <tr>
             <td>Datum splatnosti:</td>
             <td>{$order.date_to}</td>
           </tr>
           <tr>
             <td>Forma úhrady:</td>
             <td>Příkaz</td>
           </tr>
           <tr>
             <td>Konstantní symbol</td>
             <td>{$order.owner_konst_s}</td>
           </tr>
           <tr>
             <td colspan="2">&nbsp;</td>
           </tr>
           <tr>
             <td>Datum vystavení:</td>
             <td>{$order.date_from}</td>
           </tr>
         </table>
      </td>
    </table>  
    
    <div style="min-height: 400px;">
    <table style="width: 800px">
       <tr>
         <td colspan="2" class="orderItems" border="0" cellspacing="0" cellpadding="3" width="100%" style="width:100%;border-collapse:collapse">
           <!-- vypis objednaneho zbozi -->
           <table border="0" cellspacing="0" cellpadding="0" style="width: 800px;border-collapse:collapse">
              <tr style="border: 1px solid #000;"><th style="text-align:left;padding-left:10px;">Název produktu</th>{*<th style="background-color: #DBDBDB;text-align:left;padding-left:10px;" width="80">Kód</th>*}<th style="text-align:left;padding-left:10px;" width="80">Množství</th><th style="padding-right:10px;text-align:right;" class="priceCol" width="150">Kč bez DPH / m.j.</th><th style="padding-right:10px;text-align:right;" class="priceCol" width="130">Kč s DPH / m.j.</th><th style="padding-right:10px;text-align:right;" width="110">Cena s dph</th></tr>  	  	  	 
              
              {foreach item=item key=k name=iter from=$order.products}        	        
              {if $item.total_price_with_tax >0}
              <tr {if $smarty.foreach.iter.iteration mod 2 == 0}{/if}>	          
                <td style="text-align:left;padding-left:10px;">
                {if !empty($item.zmeneno)}* {assign var='zmeneno' value=true}{/if}{$item.nazev}
                </td>	          
                {*<td style="text-align:left;padding-left:10px;">{$item.code}</td>	*}
                <td style="text-align:left;padding-left:10px;">{$item.units} {$item.jednotka}</td>
                <td style="text-align:right;padding-left:10px;">{$item.price_without_tax} Kč</td>	                    
                <td style="text-align:right;padding-left:10px;">{$item.price_with_tax|currency:cz}</td>	          
                <td style="text-align:right;padding-left:10px;">{$item.total_price_with_tax|currency:cz}</td>	        
              </tr>	
              {/if}        
              {/foreach}
              
              {*
              {if $order.order_voucher_discount>0}
              <tr ><td colspan="5" style="text-align: right;">Ceny produktů byly sníženy (slevový kupón). Celková sleva: {$order.order_voucher_discount} Kč</td></tr>
              {/if}
              
              <tr><td colspan="5">&nbsp;</td></tr>
              {if isset($zmeneno)}
              <tr><td colspan="5" style="text-align: right;">* množství položky bylo upraveno</td></tr>
              {/if}
              *}
              
            </table>
          </td>
        </tr>
          
        {*  
        <tr>
          <td colspan="2">
             <!-- udaje pro dopravu -->
             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;border-collapse:collapse">
               <tr style="border-bottom: 1px solid #DBDBDB; background-color: #DBDBDB;"><th style="background-color: #DBDBDB;text-align:left;padding-left:10px;">Způsob dopravy</th><th style="background:#DBDBDB;text-align:left;padding-left:10px;">&nbsp;</th><th class="priceCol" style="background-color: #DBDBDB;text-align:right;padding-right:10px;" width="158">Cena</th></tr>
               <tr><td style="padding-left: 10px;">{$order.order_shipping_nazev}</td><td>{$order.order_shipping_descr}</td><td  style="text-align:right;padding-right:10px;">{$order.order_shipping_price|currency:cz}</td></tr>
             </table>
          </td>
        </tr>
       
        <tr>
          <td colspan="2">
             <!-- udaje pro platbu -->
             <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;border-collapse:collapse">
               <tr style="border-bottom: 1px solid #DBDBDB; background-color: #DBDBDB;"><th style="background:#F3F3F0;text-align:left;padding-left:10px;">Způsob platby</th><th style="background:#F3F3F0;text-align:right;padding-right:10px;" class="priceCol" width="158">Cena</th></tr>
               <tr><td style="padding-left: 10px;">{$order.order_payment_nazev}</td><td  style="text-align:right;padding-right:10px;">{$order.order_payment_price|currency:cz}</td></tr>
             </table>
          </td>
        </tr>  
       
        <tr>
         <td colspan="2" class="orderItems">          
            <table border="0" cellspacing="0" cellpadding="0" style="width:800; border-collapse:collapse">  
              
              <tr><td colspan="4" style="text-align:right;font-weight:bold;">Mezisoučet (bez DPH): </td><td style="text-align:right;padding:0 10px; width: 150px;">{$order.order_total_without_vat|currency:cz}</td></tr>   
              <tr><td colspan="4" style="text-align:right;font-weight:bold;">Mezisoučet (s DPH): 	</td><td style="text-align:right;padding:0 10px; width: 150px;">{$order.order_total_with_vat|currency:cz}</td></tr>  
              <tr><td colspan="4" style="text-align:right;font-weight:bold;">DPH objednaného zboží celkem: 	</td><td style="text-align:right;padding:0 10px; width: 150px;">{$order.order_total_vat|currency:cz}</td></tr>  
              <tr><td colspan="5">&nbsp;</td></tr>
              <tr><td colspan="4" style="text-align:right;font-weight:bold;">Dopravné a balné: </td><td style="text-align:right;padding:0 10px; width: 150px;">{$order.order_shipping_price|currency:cz}</td></tr>   
              <tr><td colspan="4" style="text-align:right;font-weight:bold;">Cena (sleva) za platební metodu: </td><td style="text-align:right;padding:0 10px; width: 150px;">{$order.order_payment_price|currency:cz}</td></tr>   
              <tr><td colspan="5">&nbsp;</td></tr>
              {if $order.order_discount!=0}
              <tr><td colspan="4" style="text-align:right;font-weight:bold;">Sleva za první nákup: </td><td style="text-align:right;padding:0 10px; width: 150px;">{$order.order_discount|currency:cz}</td></tr>   
              <tr><td colspan="5">&nbsp;</td></tr>
              {/if}
              <tr><td colspan="4" style="width:82.5pt;background:#DBDBDB;padding:3.0pt 11.25pt 3.0pt 1.5pt; text-align:right;font-weight:bold;">Celkem:</td><td style="background:#DBDBDB; padding: 3px 10px 3px 10px; text-align:right; font-size: 16pt; width: 150px;">{$order.order_total_CZK|currency:cz}</td></tr>

           </table>
         </td>
       </tr>
        *}
       {*
       <tr>
         <td colspan="2">
           <div style="border-bottom: 1px solid #000;"></div>
         </td>
       </tr>
       <tr>
         <td colspan="2">
           <p style="font-weight: bold;">Poznámka zákazníka:</p>
           <p>{$order.order_shopper_note}</p>
         </td>
       </tr>
       <tr> 
         <td colspan="2" class="orderAddition p20">
           <p style="font-weight: bold;">Sphere program:</p>
           <p>{$order.order_shopper_custommer_code}</p>
         </td>
       </tr>  
       *}
  </table>
  </div>
  
  
  <br />
  <br />
  <br />
  <br />
  <div style="float:right; width: 250px; margin-left: 400px;">Cena (sleva) za platební metodu:</div> <br />
  <div style="float:right; width: 150px; margin-left: 600px; padding: 5px; text-align: right; font-weight: bold;">{$order.order_payment_price|currency:cz}</div> <br /><br />
  
  <div style="float:right; width: 150px; margin-left: 600px;">Cena za dopravu:</div> <br />
  <div style="float:right; width: 150px; margin-left: 600px; padding: 5px; text-align: right; font-weight: bold;">{$order.order_shipping_price|currency:cz}</div> <br /><br />
  
  {if $order.order_discount}
  <div style="float:right; width: 150px; margin-left: 600px;">Sleva za první nákup:</div> <br />
  <div style="float:right; width: 150px; margin-left: 600px; padding: 5px; text-align: right; font-weight: bold;">- {$order.order_discount|currency:cz}</div> <br /><br />
  {/if}
  
  <div style="float:right; width: 150px; margin-left: 600px;">Celkem k úhradě:</div> <br />
  <div style="float:right; width: 150px; margin-left: 600px; padding: 5px; border: 2px solid #000; text-align: right; font-weight: bold;">{$order.order_total_CZK|currency:cz}</div> <br />         
  <br />
  <p style="font-size: 8pt;">Vystavil: Věra Řezníčková, telefon: 577 056 327, 577 056 323. Cena vč. poplatků za recyklaci systém EKO-KOM, ev.č.EK-F00010031</p>
  </body>
</html>