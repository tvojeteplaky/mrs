{if !empty($branches)}
    <script type="text/javascript" src="{$media_path}js/map.js"></script>
    <div class="row branches-widget">
        <div class="branches">
            <div class="small-12 column">
                <div class="row">
                    <div class="medium-7 column">
                        <div id="branch-map">
                            <ul class="info small-block-grid-2 show-for-large-up">
                                <li>
                                    <img src="{$media_path}img/mapa_prodejni_misto.png" alt="Prodejní místo">
                                    Prodejní místo
                                </li>
                                <li>
                                    <img src="{$media_path}img/mapa_prodej_vw_uzitkove_vozy.png" alt="Prodej VW užitkové vozy">
                                    Prodej VW užitkové vozy
                                </li>
                                <li>
                                    <img src="{$media_path}img/mapa_servisni_misto.png" alt="Servisní místo">
                                    Servisní místo
                                </li>
                                <li>
                                    <img src="{$media_path}img/mapa_vyrobni_zavod.png" alt="Výrobní závod">
                                    Výrobní závod
                                </li>
                            </ul>
                            {capture assign=map}
                                {$media_path}img/map.png
                            {/capture}
                            <img src="{$map|trim}" alt="Mapa poboček" id="map">
                            {foreach from=$branches item=branch name=branch}
                                <a href="{$url_base}{translate_route route="kontakty"}?branch_open={$branch.id}" data-id="{$branch.id}" class="pin {if $branch.map_servisni_misto}servis{/if} {if $branch.map_prodej_vw}vw-prodej{/if}" data-href="{if $controller == "contact"}false{else}true{/if}" id="pin-{$branch.id}">
                                    {if $branch.map_type == 0}
                                        <img src="{$media_path}img/mapa_prodejni_misto.png" alt="prodejni misto">
                                    {elseif $branch.map_type == 1}
                                        <img src="{$media_path}img/mapa_vyrobni_zavod.png" alt="Výrobní závod">
                                    {/if}
                                    {if $branch.map_nazev}
                                        <p class="nazev">
                                            {str_replace('\n', '<br>', $branch.map_nazev)|bbcode}
                                        </p>
                                    {/if}

                                    {if $branch.tooltip_phone || $branch.tooltip_email}
                                        <div class="bubble">
                                            <span class="tel">{$branch.tooltip_phone}</span>
                                            <span class="email">{$branch.tooltip_email}</span>
                                        </div>
                                    {/if}
                                </a>
                                <script type="text/javascript">
                                    Map.addPin({
                                        coords: {
                                            x: {$branch.left_coord},
                                            y: {$branch.top_coord}
                                        },
                                        id: {$branch.id},
                                        item: $('#pin-{$branch.id}')
                                    });
                                </script>
                            {/foreach}
                        </div>
                        <ul class="small-block-grid-2 show-for-medium-down">
                            <li>
                                <img src="{$media_path}img/mapa_prodejni_misto.png" alt="Prodejní místo">
                                Prodejní místo
                            </li>
                            <li>
                                <img src="{$media_path}img/mapa_prodej_vw_uzitkove_vozy.png" alt="Prodej VW užitkové vozy">
                                Prodej VW užitkové vozy
                            </li>
                            <li>
                                <img src="{$media_path}img/mapa_servisni_misto.png" alt="Servisní místo">
                                Servisní místo
                            </li>
                            <li>
                                <img src="{$media_path}img/mapa_vyrobni_zavod.png" alt="Výrobní závod">
                                Výrobní závod
                            </li>
                        </ul>
                        {*
                        <div class="google-map" id="map-branches"></div>
                        *}
                    </div>
                    <div class="medium-5 column">
                        <ul class="small-block-grid-2 branch-list">
                            {foreach from=$branches item=branch name=branch}
                                <li>
                                    <a href="{$url_base}{translate_route route="kontakty"}?branch_open={$branch.id}" class="branch-link" data-id="{$branch.id}" data-href="{if $controller == "contact"}false{else}true{/if}">{$branch.nazev}</a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{$media_path}js/branch.js"></script>
    <script type="text/javascript">

        $(function () {
            Map.init($("#branch-map img#map"), {$map|width}, $(".branch-link"));
        })


        {*
        var image = '{$media_path}img/map_pointer_active.png';

        function init_map() {
            //branches_map();
        }
        function branches_map() {
            var markers = [];
            var MY_MAPTYPE_ID = 'man_map_style';

            var mapOptions = {
                zoom: 7,
                center: new google.maps.LatLng(49.8037633, 15.4749126),
                mapTypeId: MY_MAPTYPE_ID,
                disableDefaultUI: true,
                scrollwheel: false
            };

            var map = new google.maps.Map(document.getElementById('map-branches'), mapOptions);

            var image = '{$media_path}img/map_pointer.png';
            var image_active = '{$media_path}img/map_pointer_active.png';
            {foreach $branches as $branch}
            var marker_{$branch.id} = new google.maps.Marker({
                position: new google.maps.LatLng({$branch.lat}, {$branch.lng}),
                map: map,
                icon: image
            });
            markers.push(marker_{$branch.id});
            google.maps.event.addListener(marker_{$branch.id}, 'mouseover', function (e) {
                this.setIcon(image_active);
            });

            google.maps.event.addListener(marker_{$branch.id}, 'mouseout', function (e) {
                this.setIcon(image);
            });

            google.maps.event.addListener(marker_{$branch.id}, 'click', function (e) {
                this.setIcon(image_active);
                window.location.assign("{$url_base}{translate_route route="kontakty"}?branch_open={$branch.id}");
            });
            {/foreach}

            var styledMapOptions = {
                name: 'MAN Style'
            };

            var customMapType = new google.maps.StyledMapType(App.map_styles, styledMapOptions);

            map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

            Branch.initialize_branch_links(markers, image, image_active);

        }

        *}
    </script>
{/if}