
<section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1>{$item.nazev}</h1>
            <p>
                {$item.uvodni_popis}
            </p>
        </div>
    </div>
</section>

<article>
    <div class="row text-center contact__pictograms wow slideInUp">
        <div class="medium-4 columns">
            <h2 class="icn-post">Doručovací adresa</h2>
            <p>
                {$owner.firma} <br>
                {$owner.ulice} <br>
                {$owner.psc} {$owner.mesto}
            </p>
        </div>
        <div class="medium-4 columns">
            <h2 class="icn-keyboard">Jak nás zastihnete?</h2>
            <p>
                <strong>Tel.:</strong> <a href="tel:{$owner.tel}" title="Zavolejte nám">{$owner.tel}</a> <br>
                <strong>E-mail:</strong> <a href="mailto:{$owner.email}">{$owner.email}</a> <br><br>
                <strong>IČ:</strong>  {$owner.ic}<br>
                <strong>Číslo bankovního účtu:</strong> <br>
                {$owner.ucet}
            </p>
        </div>
        <div class="medium-4 columns">
            <h2 class="icn-clock">Úřední hodiny</h2>
            {static_content code="uredni-hodiny"}
        </div>
    </div>

    <div class="row">
        <div class="medium-7 columns wow slideInLeft">
        <section class="contact__list">
            <h2>Kontaktní osoby</h2>
            {foreach from=$employees item=employee key=key name=employees}
               {if $smarty.foreach.employees.first||$smarty.foreach.employees.index%2==0}
                    <div class="row columns dotted">
                {/if} 
                    <div class="medium-6 columns">
                    <strong>{$employee.name}</strong> <br>
                    {$employee.role}
                    <table>
                        <tr><td>Tel.:</td><td><a href="tel:{$employee.phone}" title="Zavolat">{$employee.phone}</a></td></tr>
                        <tr><td>E-mail:</td><td><a href="mailto:{$employee.email}" title="Napsat e-mail">{$employee.email}</a></td></tr>
                    </table>
                </div>
                {if $smarty.foreach.employees.last||$smarty.foreach.employees.iteration%2==0}
                    </div>
                {/if} 
            {/foreach}
        </section>
        </div>
        <div class="medium-5 columns wow slideInRight">
            <div class="contact__form">
                {widget controller=contact action=show}
            </div>
            <div class="contact__info-box">
                <p>
                    {static_content code="povolenky"}
                </p>
            </div>
        </div>
    </div>
</article>
{* 
{capture assign=subnav}
    {widget controller=page action=subnav}
{/capture}
<div class="detail" id="contact">
    <header class="text-center {if $item.photo_src}with-photo{/if}">
        {if $item.photo_src}
            <img alt="{$item.nazev} photo"
                 data-interchange="[{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header.jpg, large]">
        {/if}
        <div class="heading">
            <div class="row">
                <div class="small-12 columns">
                    <h1>{$item.nadpis|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">
        {if strlen(trim($subnav)) > 0}
            <div class="medium-3 small-12 columns">
                {$subnav}
            </div>
        {/if}
        <div class="medium-{if strlen(trim($subnav)) > 0}9{else}12{/if} small-12 columns">
            {$item.uvodni_popis}
            {if !empty($branches)}
                <div class="row branches">
                    {foreach $branches as $branch}
                        <div class="medium-4 small-6 columns branch">
                            <h3 class="highlight">{$branch.nazev}</h3>

                            <p>
                                {nl2br($branch.address)|bbcode}
                            </p>
                            {if $branch.phone || $branch.email}
                                <div class="contact">
                                    {if $branch.phone}
                                        <span class="phone highlight">
                                {$branch.phone}
                            </span>
                                    {/if}
                                    {if $branch.email}
                                        <span class="email highlight">
                                {$branch.email}
                            </span>
                                    {/if}
                                </div>
                            {/if}
                            {if $branch.ic}
                                IČ: {$branch.ic}{if $branch.dic}<br>{/if}
                            {/if}
                            {if $branch.dic}
                                DIČ: {$branch.dic}
                            {/if}
                        </div>
                    {/foreach}
                </div>
            {/if}
            {$item.popis}
            {if !empty($employees)}
                <div class="row employees">
                    {foreach $employees as $employee}
                        <div class="large-4 small-6 employee columns">
                            <div class="callout">
                                <h4>{$employee.name}
                                    <small class="light">{$employee.role}</small>
                                </h4>
                                <div class="contact">
                                    {if $employee.phone}
                                        <span class="phone highlight">
                                {$employee.phone}
                            </span>
                                    {/if}
                                    {if $employee.email}
                                        <span class="email highlight">
                                {$employee.email}
                            </span>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>
            {/if}
            <h1 class="text-center">{translate str="Kontaktní formulář"}</h1>
            {widget controller=contact action=show}
        </div>
    </div>
</div> *}
