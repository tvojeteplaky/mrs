<div id="contact-form">
    <form action="?" method="post">
        <div class="row">
            <div class="small-6 columns">
                <input type="text" placeholder="{translate str='Váš email'}*" name="contactform[email]" value="{if isset($data.email)}{$data.email}{/if}" required>
            </div>
            <div class="small-6 columns">
                <input type="text" placeholder="{translate str='Vaše telefonní číslo'}" name="contactform[telefon]" value="{if isset($data.telefon)}{$data.telefon}{/if}">
            </div>
        </div>

        <input type="text" placeholder="{translate str='Jméno a příjmení'}*" name="contactform[jmeno]" value="{if isset($data.jmeno)}{$data.jmeno}{/if}" required>

        <textarea placeholder="Vaše zpráva*" name="contactform[zprava]" required rows="5">{if isset($data.zprava)}{$data.zprava}{/if}</textarea>

        <div class="hide">
            <input type="hidden" name="kontrolni_text" class="hide">
        </div>
        {hana_secured_post action="send" module="contact"}
        <button class="button" type="submit">{translate str="Odeslat zprávu"}</button>


    </form>
</div>