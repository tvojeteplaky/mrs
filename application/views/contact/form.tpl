 {if !$column}
 <h2>Máte dotaz? Kontaktujte nás!</h2>
            <div class="row">

                <form action="?" method="post">
                <div class="medium-4 columns">
                    <input type="text" name="contactform[jmeno]" value="{if isset($data.jmeno)}{$data.jmeno}{/if}" placeholder="Jméno a příjmení" required="">
                    <input type="text" name="contactform[email]" value="{if isset($data.email)}{$data.email}{/if}" {literal}patern="([a-zA-Z0-9_\.\+-]+\@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+|\+?\(?\d{2,4}\)?[\d\s-]{3,})"{/literal} placeholder="Telefonní číslo (E-mailová adresa)" required="">
                    <input type="submit" name="send" value="Odeslat Vaši zprávu">
                </div>
                <div class="medium-8 columns">
                    <textarea name="contactform[zprava]" placeholder="Text Vaši zprávy" required=""></textarea>
                </div>
                {hana_secured_post action="send" module="contact"}
                </form>
            </div>
 {else}
 <h3>Kontaktní formulář</h3>
                <form action="?" method="post">
                    <input type="text" name="contactform[jmeno]" value="{if isset($data.jmeno)}{$data.jmeno}{/if}" placeholder="Jméno a příjmení" required="">
                    <input type="text" name="contactform[email]" value="{if isset($data.email)}{$data.email}{/if}" {literal}patern="([a-zA-Z0-9_\.\+-]+\@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+|\+?\(?\d{2,4}\)?[\d\s-]{3,})"{/literal} placeholder="Telefonní číslo (E-mailová adresa)" required="">
                    <textarea name="contactform[zprava]" placeholder="Text Vaši zprávy" required=""></textarea>
                    <input type="submit" name="send" value="Odeslat Vaši zprávu">
                    {hana_secured_post action="send" module="contact"}
                </form>
 {/if}

{* <form class="contact-form text-left" action="?" method="post">
    <div class="row">
        <div class="medium-{if $controller == "contact"}4{else}12 large-5{/if} small-12 columns">
            <div class="row">
                <div class="small-12 column">
                    <input type="text" required aria-required="true" value="{if isset($data.jmeno)}{$data.jmeno}{/if}" name="contactform[jmeno]" placeholder="Jméno a příjmení*" class="{if isset($errors.jmeno)}error{/if}">
                </div>
            </div>
            <div class="row">
                <div class="small-12 column">
                    <input type="email" required aria-required="true" value="{if isset($data.email)}{$data.email}{/if}" name="contactform[email]" placeholder="Emailová adresa*" class="{if isset($errors.email)}error{/if}">
                </div>
            </div>
            <div class="row">
                <div class="small-12 column">
                    <input type="tel" name="contactform[tel]" required aria-required="true" value="{if isset($data.tel)}{$data.tel}{/if}" placeholder="Telefonní číslo*" class="{if isset($errors.tel)}error{/if}">
                </div>
            </div>
            <div class="row">
                <div class="small-12 column">
                    <div id="g-recaptcha"></div>
                </div>
            </div>
        </div>
        <div class="medium-{if $controller == "contact"}8{else}12 large-7{/if} small-12 columns">
            <textarea name="contactform[zprava]" placeholder="Text vaší zprávy..." required aria-required="true" class="{if isset($errors.zprava)}error{/if}">{if isset($data.zprava)}{$data.zprava}{/if}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="medium-{if $controller == "contact"}8{else}12 large-7{/if} medium-offset-{if $controller == "contact"}4{else}0 large-offset-5{/if} small-offset-0 columns">
            <button type="submit" class="success button">{translate str="Odeslat zprávu"}</button>Položky označené * jsou povinné
        </div>
    </div>
    <div class="row">
        <div class="small-12 column">
            {hana_secured_post action="send" module="contact"}
        </div>
    </div>
</form>
<div class="tiny reveal" id="captcha-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
    <p>Prosím ověřte, že nejste robot.</p>
    <button class="close-button" data-close aria-label="Close reveal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
</script> *}