<ul class="pagination text-center" role="navigation" aria-label="Pagination">
              <li class="pagination-previous"><a href="<?= HTML::chars($page->url($current_page-1)) ?>" aria-label="Previous page">Předchozí <span class="show-for-sr">page</span></a></li>
              <?php for ($i = 1; $i <= $total_pages; $i++): ?>
                <?php if ($i == $current_page): ?>
                    <li class="current"><span class="show-for-sr">You're on page</span> <?= $i ?></li>
                <?php else: ?>
                    <li><a href="<?= HTML::chars($page->url($i)) ?>" aria-label="Page <?= $i ?>"><?= $i ?></a></li>
                 <?php endif ?>
            <?php endfor ?>
              <li class="pagination-next"><a href="<?= HTML::chars($page->url($current_page+1)) ?>" aria-label="Next page">Další <span class="show-for-sr">page</span></a></li>
            </ul>