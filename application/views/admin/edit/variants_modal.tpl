<div id="modalDialog">
<div class="modal fade in" id="ModalForm1" role="dialog" tabindex="-1" style="display: block;" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Editace variant nebo příslušenství</h4>
            </div>
            <div class="modal-body">
            <input type="hidden" name="variant_action_add" value="{$entity_name}" />
            {if empty($data)}
              <select class="form-control" name="count" id="count">
              {for $num=1 to 10}
                <option value="{$num}">{$num}</option>
              {/for}
              </select>
              {else}
                <input type="hidden" name="variant_id" value="{$data.id}">
                <input type="hidden" name="count" value="1">
              {/if}
              <table class="table"  class="form-group">
                <thead>
                                <tr class='row'>
                                    <th class="col-xs-2">Kód</th>
                                    <th class="col-xs-3">Název / Barva</th>
                                    <th class="col-xs-2">Na skladě</th>
                                    <th class="col-xs-1">Varianta/Doplněk</th>
                                    <th class="col-xs-4">Cena</th>
                                </tr>
                              </thead>
                                <tbody id="target">
                                  <tr class='copy row'>
                                    <td class="col-xs-2"><input type="text" name="code[]" placeholder="Kód" class="form-control" {if $data}value="{$data.code}" {/if} required></td>
                                    <td class="col-xs-3"><input type="text" name="nazev[]" placeholder="Název" class="form-control" {if $data}value="{$data.nazev}" {/if} required></td>
                                    <td class="col-xs-2"><input type="text" name="in_stock[]" placeholder="Ks" class="form-control" {if $data}value="{$data.pocet_na_sklade}" {/if} required></td>
                                    <td class"col-xs-1"><select class="form-control" name="doplnek[]" required>
                                      <option value="0" {if !$data || ($data && !$data.doplnek)}selected{/if}>Varianta</option>
                                      <option value="1" {if $data && $data.doplnek}selected{/if}>Doplněk</option>
                                    </select></td>
                                    <td class"col-xs-4">
                                      <table style="border:0">
                                      {if isset($price_table.one)}
                                        {foreach from=$price_table.one item=row key=key name=name}
                                         <tr class="row"><td class"col-xs-4">{$row.kod} </td>
                                         <td class"col-xs-4"> <input type="text" class="form-control" name="price_category_variant[{$row.id}][]" class="shortInput" value="{$row.cena}"> </td><td class"col-xs-4"> {$row.kratky_popis}</td></tr>
                                      {/foreach}
                                      {elseif isset($price_table.cats)}
                                      {foreach from=$price_table.cats item=row key=key name=name}
                                         <tr class="row"><td class"col-xs-4">{$row.kod} </td>
                                         <td class"col-xs-4"> <input type="text" class="form-control" name="price_category_variant[{$row.id}][]" class="shortInput" value="{if isset($row.cena)}{$row.cena}{else}0{/if}"> </td><td class"col-xs-4"> {if isset($row.cena)}{$row.kratky_popis}{/if}</td></tr>
                                      {/foreach}
                                      {/if}
                                      
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
              </table>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Uložit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
  </div>

{literal}
<script type="text/javascript">
        $(function() {
                            // premisteni modalniho formulare mimo hlavni formular - jinak se neodeslou data
                                // nasetovani modalniho formulare pridani zavady
                                var data=$("#modalDialog").html();
                                $("#modalDialog").remove();
                                $('#JqueryForm form').append(data);
                                $('#ModalForm1').modal();
                                $('#count').change(function() {
            multiplier(this);
        });

                            });
                            
</script>
{/literal}

                                 {literal}
<script type="text/javascript">
    function insert (row,num) {
        var myrow = row.eq(-1);
        myrow.find('input.born').datepicker();
        var newDiv = myrow.clone(false).attr("id", "someRow"+num).insertAfter(myrow);

    }
    function multiplier(me) {
        var row = $('tr.copy');
            var need = $(me).val()*1;
            var have = row.length;
            var target = '#target';
            if(need>have) {
                var diff = need-have;
                for (var i = 0; i < diff; i++) {
                    insert(row,i)
                };
            }
            else if(need<have){
                var diff = have-need;
                for (var i = 1; i <= diff; i++) {
                    row.eq(-i).remove();
                };
            }
    }

</script>
{/literal}