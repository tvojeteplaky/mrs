<table class="table">
	<thead>
		<tr>
			<th>Kód</th>
			<th>Název</th>
			<th>Ve skladu</th>
			<th>Cena</th>
			<th>Editovat</th>
			<th>Smazat</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="6" class="info"><strong>Varianty produktu</strong></td>
		</tr>
	{foreach from=$variants item=variant key=key name=name}
		<tr>
			<td>{$variant.code}</td>
			<td>{$variant.nazev}</td>
			<td>{$variant.pocet_na_sklade}</td>
			<td>
				<table style="border:0">
				{foreach from=$price_table.many[$variant.id] item=row key=key name=name}
						<tr>
						<td>{$row.kod}&nbsp;</td>
						<td>{$row.cena}</td>
						<td>&nbsp;{$row.kratky_popis}</td>
					</tr>
					{/foreach}
				</table>
			</td>
			<td><a class="" href="{$admin_path}?variants_edit={$entity_name}&amp;variant_id={$variant.id}">Editovat</a></td>
			<td><a class="confirmDelete" href="{$admin_path}?variant_action_delete={$entity_name}&amp;variant_id={$variant.id}">Smazat</a></td>
		</tr>
	{/foreach}
			<tr>
			<td colspan="6" class="info"><strong>Doplňky k produktu</strong></td>
		</tr>
	{foreach from=$doplnky item=doplnek key=key name=name}
		<tr>
			<td>{$doplnek.code}</td>
			<td>{$doplnek.nazev}</td>
			<td>{$doplnek.pocet_na_sklade}</td>
			<td>
				<table style="border:0">
				{foreach from=$price_table.many[$doplnek.id] item=row key=key name=name}
						<tr>
						<td>{$row.kod}&nbsp;</td>
						<td>{$row.cena}</td>
						<td>&nbsp;{$row.kratky_popis}</td>
					</tr>
					{/foreach}
				</table>
			</td>
			<td><a class="" href="{$admin_path}?variants_edit={$entity_name}&amp;variant_id={$doplnek.id}">Editovat</a></td>
			<td><a class="confirmDelete" href="{$admin_path}?variant_action_delete={$entity_name}&amp;variant_id={$doplnek.id}">Smazat</a></td>
		</tr>
	{/foreach}
	</tbody>
		<tfoot>
			<tr>
				<td colspan="4">
					<a class="btn btn-primary" href="{$admin_path}?variants_edit={$entity_name}">Nové varianty nebo příslušenství</a>
				</td>
			</tr>
		</tfoot>
</table>
<script type="text/javascript">
	$(function(){
		
	});
</script>