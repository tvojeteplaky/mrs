<section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1>{$item.nadpis}</h1>
            <p>
                {$item.uvodni_popis}
            </p>
        </div>
    </div>
</section>
<article class="news wow slideInUp">
    {if !empty($items)}
        {foreach $items as $item}
            <div class="row">
                <div class="medium-5 columns">
                    {if !empty($item.photo.home)}
                        <img class="thumbnail" src="{$item.photo.home}" alt="{$item.nazev}">
                    {/if}
                    
                </div>
                <div class="medium-7 columns">
                    <h2>{$item.nazev}</h2>
                    <span><strong>Autor:</strong> {$item.author}  • <strong>Datum:</strong> {$item.date|date_format:"%d. %m. %Y"}</span>
                    <p>
                       {$item.uvodni_popis|truncate:600:''} <a href="{$url_base}{$item.nazev_seo}" title="Číst celý článek">Číst celý článek...</a>
                    </p>
                </div>
            </div>
        {/foreach}
    {/if}
    <div class="row">
        <div class="medium-12 columns">
            {$pagination}
        </div>
    </div>
</article>