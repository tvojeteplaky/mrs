<section id="article" class="old">
    {if !empty($items)}
        <div class="panel">
            <div class="row">
                <div class="small-12 news">
                    <ul class="small-block-grid-1 medium-block-grid-2 news-old">
                        {foreach $items as $article}
                            <li>
                                <a href="{$url_base}{$article.nazev_seo}">
                                    <div class="n-box" data-equalizer>
                                        {if $article.photo_src}
                                            <div class="large-4 medium-12 small-5 columns photo" data-equalizer-watch>
                                                <div class="bg">

                                                    <img src="{$media_path}photos/article/item/images-{$article.id}/{$article.photo_src}-t2.png"
                                                         alt="{$article.nazev} photo">

                                                </div>

                                            </div>
                                        {/if}
                                        <div class="large-8 medium-12 small-7 columns n-box-info" data-equalizer-watch>
                                            <h6>{$article.nazev}</h6>

                                            {if mb_strlen($article.uvodni_popis) > 100}
                                                {$article.uvodni_popis|mb_substr:0:95}...
                                                </p>
                                            {else}
                                                {$article.uvodni_popis}
                                            {/if}

                                            <div class="news-more"></div>

                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </li>
                        {/foreach}

                    </ul>
                </div>
            </div>
        </div>
    {/if}
</section>