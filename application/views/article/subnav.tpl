<div id="nextNews" class="large-4 columns" data-waypoint data-animation="fadeInRight">
    <h2>{translate str="Další novinky"}</h2>
    {foreach from=$items item=item key=key name=articles}
        <div>
                    <span class="textIcons">
                        <a><i class="fa fa-calendar"></i>{$item.date|date_format:"%d."} {$item.month}, {$item.date|date_format:"%Y"}</a>
                    </span>
            <h3>{$item.nadpis}
                <br/>
                {$item.podnadpis}
            </h3>
            <p>{$item.popis|truncate:150|strip_tags:false}</p>
            <a href="{$url_base}{$item.nazev_seo}"><span class="moreInfo">Detail novinky<i class="fa fa-angle-right"></i></span></a>
        </div>
    {/foreach}
</div>
