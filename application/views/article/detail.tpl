<section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1>{$item.nadpis|bbcode}</h1>
        </div>
    </div>
</section>
<article class="news__detail wow slideInUp">
    <div class="row">
        <div class="small-12 columns">
            {$item.uvodni_popis}
            <p>
                {if !empty($item.photo.ad)}
                    <div class="large-4 float-right article__img">
                        <img src="{$item.photo.ad}" alt="{$item.nazev}">
                    </div>
                {/if}
                
                {$item.popis}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            {widget controller="contact" action="show"}
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <h2>Kam se podívat dál?</h2>
            {$item.odkazy}
        </div>
    </div>
</article>