{if !empty($items)}
<section class="hp__news">
    <div class="row text-center  wow slideInUp">
        <h2>Novinky</h2>
        {foreach $items as $item name=iter}
        <div class="medium-6 columns text-left">
            <div class="hp__news__item">
            <a href="{$url_base}{$item.nazev_seo}" title="Zobrazit celou novinku">
        {if $item.photo_src}
            <img src="{$item.photo.home}" alt="{$item.nazev}">
            {/if}
            <div class="overlay">

                <h3>{$item.nazev}</h3>
                {if !empty($item.categories)}
                <span>
                {foreach from=$item.categories item=cat key=key name=article_cat}
                     {$cat.nazev}
                     {if !$smarty.foreach.article_cat.last}
                         &bull;
                     {/if}
                 {/foreach} </span>
                 {/if}
                 <p>{$item.uvodni_popis|strip_tags:false|truncate:120:"...":false}</p>
            </div>
            <div class="gradient"></div>
            </a>
            </div>
        </div>
        {/foreach} 

        <a href="{$url_base}novinky" title="Všechny novinky" class="hp_button-more">Všechny novinky</a>
    </div>
    

</section>
{/if}