{if !empty($boxes)}
    <div class="boxes">
        <div class="row">
            <div class="small-12 columns text-center">
                <h3><span class="thin">Námi</span> zajišťované služby</h3>
            </div>
        </div>
        <div class="row">
            {foreach $boxes as $box}
                <div class="medium-4 small-6 columns text-center box">
                    {if $box.link}
                    <a href="{$box.link}">
                        {/if}
                        {if $box.photo_src}
                            <div class="img-wrapper">
                                <img src="{$media_path}photos/box/item/images-{$box.id}/{$box.photo_src}-main.png" alt="{$box.nazev}">
                            </div>
                        {/if}
                        <h3 class="highlight">{$box.nazev}</h3>
                        {$box.popis}
                        {if $box.link}
                    </a>
                    {/if}
                </div>
            {/foreach}
        </div>
    </div>
{/if}