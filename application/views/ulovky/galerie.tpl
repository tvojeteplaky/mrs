<section class="sub__banner">
    <div class="row text-left">
        <div class="small-12 columns">
            <h1>{$item.nadpis}</h1>
            <p>
                {$item.uvodni_popis}
            </p>
        </div>
    </div>
</section>
<section class="your-catch">
	{if isset($photos)}
		{$counter = 1}
		{foreach from=$photos item=photo key=key name=name}
			{if ($counter-1)%6==0}<div class="row text-center">{/if}
			<div class="large-2 medium-4 small-6 columns end">
				<a href="{$media_path}photos/page/item/gallery/images-{$photo.page_id}/{$photo.photo_src}-ad.jpg" data-lightbox="roadtrip">
				<img class="thumbnail" src="{$media_path}photos/page/item/gallery/images-{$photo.page_id}/{$photo.photo_src}-at.jpg" alt="">	
				<p>{$photo.popis}</p>
				</a>
			</div>
			{if ($counter%6==0) || ($counter == $photos_count)}</div>{/if}
		{$counter = $counter + 1}
		{/foreach}
	{/if}

	<div class="row">
		<div class="medium-12 columns">
{$pagination}
		</div>
	</div>

	<div class="row">
		<div class="fb-contact clearfix">
			<div class="fb-banner">
				<a href="https://www.facebook.com/Moravsk%C3%BD-ryb%C3%A1%C5%99sk%C3%BD-svaz-MO-Zl%C3%ADn-1644651189090137/" target="_blank" title="Facebook - Moravský rybářský svaz, MO Zlín">
				</a>
			</div>
			<div class="fb-plugin">
				<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FMoravsk%25C3%25BD-ryb%25C3%25A1%25C5%2599sk%25C3%25BD-svaz-MO-Zl%25C3%25ADn-1644651189090137%2F&tabs=timeline&width=500&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="500" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			</div>
		</div>
	</div>

</section>
