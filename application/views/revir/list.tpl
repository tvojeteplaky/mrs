<section class="sub__banner">
	<div class="row text-left">
		<div class="small-12 columns">
			<h1>Rybářské revíry</h1>
			<p>
			</p>
		</div>
	</div>
</section>

<article class="grounds" id="stickyPage">
	<div class="row">
		<div class="large-3 columns float-right wow slideInRight" data-sticky-container>
			<div class="sticky" id="sticky" data-sticky data-options="data-sticky-on: large" data-anchor="stickyPage">
				{widget controller=revir action=subnav}
				{widget controller=contact action=box_widget}
			</div>
		</div>
		<div class="large-9 columns float-left wow slideInUp">
		{foreach from=$reviry item=revir_oblast key=key name=reviry}
			<h2 id="{$revir_oblast.nav_id}">{$revir_oblast.nazev}</h2>
			<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true" role="tablist">
			{foreach from=$revir_oblast.childs item=revir key=key name=name}
				<li class="accordion-item">
			       <a href="#collapse{$smarty.foreach.reviry.iteration}" role="tab" class="accordion-title" id="collapse{$smarty.foreach.reviry.iteration}-heading">{$revir.nazev}</a>
			       <div id="collapse{$smarty.foreach.reviry.iteration}" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel{$smarty.foreach.reviry.iteration}d-heading">
			       {*<iframe src="{$revir.map}" width="100%" height="265" frameborder="0" style="border:0" allowfullscreen></iframe>*}
			      {if !empty($revir.info) || !empty($revir.photo.ad)}
			       	 <div class="row">
			       		<div class="large-6 columns">
			       			<h4>{$revir.nadpis}</h4>
			       			{$revir.popis}
			       		</div>
			       		<div class="large-6 columns">
			       			{$revir.info}
			       			{if !empty($revir.photo.ad)}
			       				<img class="thumbnail" src="{$revir.photo.ad}" alt="{$revir.nazev}">
							{/if}
			       		</div>
			       </div>
			       {else}
			       <h4>{$revir.nadpis}</h4>
			       	{$revir.popis}
			       {/if}
			      
			       </div>
			    </li>
			{/foreach}
			    
			</ul>
		{/foreach}

		</div>
	</div>
</article>
