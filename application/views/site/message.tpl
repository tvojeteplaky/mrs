{if !empty($messages)}
    <div class="reveal" id="message-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
        {foreach $messages as $message}
            <p>{$message}</p>
        {/foreach}
        <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <script type="text/javascript">
        $(function() {
           $('#message-modal').foundation('open');
        });
    </script>
{/if}
