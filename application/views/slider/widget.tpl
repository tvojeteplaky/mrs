{if !empty($sliders) && !empty($sliders[0]['slides'])}
    {foreach $sliders as $slider}
        <div class="orbit orbit-slider" role="region" aria-label="{$slider.nazev}" {if count($slider.slides) > 1}data-orbit{/if}>
            <ul class="orbit-container">
                {foreach $slider.slides as $slide name=slides}
                    {if $slide.photo_src}
                        <li class="{if $smarty.foreach.slides.first}is-active{/if} orbit-slide">
                            {if $slide.link && !$slide.button_text}
                            <a href="{$slide.link}">
                                {/if}
                                <div class="slide">
                                    <img class="orbit-image" src="{$media_path}photos/slider/item/images-{$slide.id}/{$slide.photo_src}-main.jpg" alt="{$slide.nazev}">

                                    <div class="content-wrapper">
                                        <div class="row column">
                                            <div class="content">
                                                {if $slide.nadpis}
                                                    <h2 class="light">{$slide.nadpis|bbcode}</h2>
                                                {/if}
                                                {$slide.text}
                                                {if $slide.button_text}
                                                    <a href="{$slide.link}" class="success button link">{$slide.button_text}</a>
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {if $slide.link && !$slide.button_text}
                            </a>
                            {/if}
                        </li>
                    {/if}
                {/foreach}
            </ul>
            {if count($slider.slides) > 1}
                <nav class="orbit-bullets">
                    {foreach $slider.slides as $slide name=slides}
                        <button {if $smarty.foreach.slides.first}class="is-active"{/if} data-slide="{$smarty.foreach.slides.index}"></button>
                    {/foreach}
                </nav>
            {/if}
        </div>
    {/foreach}
{/if}