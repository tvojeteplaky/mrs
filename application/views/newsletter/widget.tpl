<div class="small-12 columns">
            <div class="row">
                <div class="medium-12 colums small-text-center medium-text-left hp__newsletter">
                    <form action="" method="post">
                        <input type="email" name="sender_email" value="" placeholder="Vaše e-mailová adresa" required>
                        <input type="submit" name="send" value="OK">
						<span class="hp_newsletter__who">JSEM:</span>
						<span class="hp__newsletter__male"><input type="radio" name="gender" value="2" checked> Muž</span>
						<span class="hp_newsletter__female"><input type="radio" name="gender" value="3"> Žena</span>
						<span class="hp_newsletter__youth"><input type="radio" name="gender" value="1"> Mládež</span>
                         <input type="text" name="kontrolni_cislo" class="hide">
                        {hana_secured_post action="subscribe_to_newsletter" module="newsletter"}
                    </form>
                    <h3>Odběr novinek - newsletter</h3>
                    <p>
                        Chcete vždy vědět aktuality jako první? Chcete dostávat pravidelné aktuality z oboru rybářství?<br>
                        Zadejte Váš email a přihlašte si náš odběr novinek!
                    </p>
                </div>
            </div>
        </div>
