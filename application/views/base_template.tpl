{*
CMS system Hana ver. 2.6 (C) Pavel Herink 2012
zakladni spolecna sablona layoutu stranek

automaticky generovane promenne spolecne pro vsechny sablony:
-------------------------------------------------------------
    $url_base      - zakladni url
    $tpl_dir       - cesta k adresari se sablonama - pro prikaz {include}
    $url_actual    - autualni url
$url_homepage  - cesta k homepage
    $media_path    - zaklad cesty do adresare "media/" se styly, obrazky, skripty a jinymi zdroji
$controller
$controller_action
$language_code - kod aktualniho jazyku
$is_indexpage  - priznak, zda jsme na indexove strance

    $web_setup     - pole se zakladnim nastavenim webu a udaji o tvurci (DB - admin_setup)
    $web_owner     - pole se zakladnimi informacemi o majiteli webu - uzivatelske informace (DB owner_data)
-------------------------------------------------------------

doplnujici custom Smarty funkce
-------------------------------------------------------------
                                    {translate str="nazev"}                                      - prelozeni retezce
                    {static_content code="index-kontakty"}                       - vlozeni statickeho obsahu
    {widget name="nav" controller="navigation" action="main"}    - vlozeni widgetu - parametr "name" je nepovinny, parametr "action" je defaultne (pri neuvedeni) na hodnote "widget"
        {hana_secured_post action="add_item" [module="shoppingcart"]}        nastaveni akce pro zpracovani formulare (interni overeni parametru))
{hana_secured_multi_post action="obsluzna_akce_kontroleru" [submit_name = ""] [module="nazev_kontoleru"]}
{$product.cena|currency:$language_code}
{hana_secured_get action="show_suggestions" module="search"} h_module=search&h_action=show_suggestions&h_secure=60c6c1049b070ce38018671b704955df

Promenne do base_template:
-------------------------------------------------------------
{$page_description}
{$page_keywords}
{$page_name} - {$page_title}
{$main_content}
{include file="`$tpl_dir`dg_footer.tpl"}
{if !empty($web_owner.ga_script)}
    {$web_owner.ga_script}
    {/if}


*}
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="author" content="{$web_setup.nazev_dodavatele}"/>
    <meta name="copyright" content="{$web_owner.copyright}" />
    <meta name="keywords" content="{$page_keywords}"/>
    <meta name="description" content="{$page_description} "/>
    <meta name="robots" content="all,follow"/>
    <meta name="googlebot" content="snippet,archive"/>
    <title>{$page_title} &bull; {$page_name}</title>
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700&subset=latin-ext'>
    {*<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,700&subset=latin,latin-ext'>*}
    <link rel="stylesheet" type="text/css" href="{$media_path}css/foundation.css">
    <link rel="stylesheet" type="text/css" href="{$media_path}css/lightbox.css">
    <link rel="stylesheet" type="text/css" href="{$media_path}css/css.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="{$media_path}js/hana.js"></script>
    <script type="text/javascript" src="{$media_path}js/app.js"></script>
</head>
<body>

{* HEADER s navigací *}
<div id="sticky-head">
<header id="anchor-top">
    <div class="row">
        <div class="small-12 large-3 columns small-text-center header__logo">
            <a href="/" title="{$page_title}">
                <img src="{$media_path}img/logo.png" alt="Logo">
            </a>
        </div>
        <div class="small-12 large-9 columns">
            <div class="title-bar" data-responsive-toggle="main-menu" data-hide-for="medium">
               <button class="menu-icon" type="button" data-toggle></button>
               <div class="title-bar-title">Menu</div>
            </div>
           {widget controller="navigation" action="main"}
        </div>
    </div>
</header>
</div>
{$main_content}


{widget controller="site" action="message"}

{* Mapa *}
<section class="hp__map">
    <div class="row text-center">
        <h2>Kde nás najdete</h2>
    </div>
    <div class="hp__map__contact" style="z-index:1">
        <div class="hp__map__contact__text">
            <strong>{$web_owner.firma}</strong>
            <p>{$web_owner.ulice}, <br> {$web_owner.psc} {$web_owner.mesto}</p>
            <p><a href="tel:{$web_owner.tel}">{$web_owner.tel}</a> <br> <a href="mailto:{$web_owner.email}">{$web_owner.email}</a></p>
        </div>
        <a href="{$url_base}kontakty" title="Kontaktujte nás" class="hp__map__contact__btn">Kontaktujte nás</a>
        <a href="#anchor-top" class="slide" title="Zpět nahoru ↑"><span class="hp__map__contact__b-top"></span></a>
    </div>
    <div id="map"></div>
    
</section>

{* Tlačítka sociálních sítí*}
<div class="social-buttons">
{if !empty($owner.facebook)}
                <a href="{$owner.facebook}" title="Facebook"><img src="{$media_path}img/icn-facebook-social.png" alt="fb icon"></a>
            {/if}
            {if !empty($owner.instagram)}
                <a href="{$owner.instagram}" title="Instagram"><img src="{$media_path}img/icn-instagram-social.png" alt="instagram icon"></a>
            {/if}
</div>

{* Patička *}
<footer>
    <div class="row">
        {*<div class="medium-3 columns footer__logo">
            <img src="{$media_path}img/logo-footer.png" alt="logo">
        </div>*}
        <div class="small-12 columns divblock" >
        {static_content code="site_index"}
           
        </div>
        
    </div>
</footer>
<div class="footer-text">
    <div class="row">
        <p>Copyright &copy; 2016 Moravský rybářský svaz z.s.</p>
        <span>
            Realizace:
            <a href="http://www.dgstudio.cz/" title="DG studio - Tvorba webových stránek, on-line marketing, desing a grafika" target="_blank">
                <img src="{$media_path}img/dgstudio_logo.png" alt="dgstudio">
            </a>
        </span>
    </div>
</div>
{* Javascripty *}
<script type="text/javascript">
    window.address = JSON.parse('{$web_owner.latlng}');
</script>
<script type="text/javascript" src="{$media_path}js/lightbox.js"></script>
<script type="text/javascript" src="{$media_path}js/foundation.min.js"></script>
<script type="text/javascript" src="{$media_path}js/countUp.min.js"></script>
<script type="text/javascript" src="{$media_path}js/wow.min.js"></script>
<script type="text/javascript" src="{$media_path}js/js.js"></script>
<script src="{$media_path}js/map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpYcN7hji42drFr1V_LgkRffwwcM8PWoY&amp;callback=initMap"
    async defer></script>

{literal}
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MXSPDZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MXSPDZ');</script>
<!-- End Google Tag Manager -->
{/literal}

</body>
</html>
