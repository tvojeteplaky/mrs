{* sablona tabulky pro vyber platby *}
{* tato sablona je soucasti objednavky - nadrazena sablona "order_payment_payment.tpl" *}
{*<div class="modal fade" id="essoxCalculatorModal" tabindex="-1" role="dialog" aria-labelledby="essoxCalculatorModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Kalkulačka splátek</h4>
            </div>*}
            {*<iframe src="{$essoxCalculatorUrl}" id="essoxCalculator"></iframe>*}
            {*<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
            </div>
        </div>
    </div>
</div>*}
{assign var="selected" value=false}
  {foreach from=$payment_types key=key item=item}

     {if $item.type.id == 4 && $order_total < 3000}
          {continue}
      {/if}

      <div class="small-12 columns">
        <input name="payment_id"  class="ajaxradio"  type="radio" value="{$item.id}" {if $item.checked}{assign var="selected" value=true}checked="checked" {/if}/><label>{$item.nazev} ({if $item.cena>0}{$item.cena|currency:$language_code}{else}zdarma{/if}) {if !empty($item.popis)}<span data-tooltip aria-haspopup="true" class="has-tip tip-top radius" title="{$item.popis}"><span class="fi-info"></span></span>{/if}</label>

      </div>
     {* <div class="small-2 columns"><span>
        {if $item.cena<>0}
        {if $item.typ==1}
          {$item.cena|currency:'cz'} 
        {elseif $item.typ==2}
          {$item.cena} %
        {/if}
      {else}
        zdarma
        {/if}
      </span>
      </div>*}
  {/foreach}





{*<h2>Způsob platby</h2>
<table summary="formulář s výběrem způsobu platby" id="payment-table" class="orderSectionTable paymentTable">

  {assign var="selected" value=false}
  {foreach from=$payment_types key=key item=item}
      {if $item.type.id == 4 && $order_total < 3000}
          {continue}
      {/if}
  <tr class="sectiontableentry2">*}
    {*<td class="col1">{if $item.icon}<img src="{$media_path}img/payment/{$item.icon}.gif" />{/if}</td>*}
  {*}  <td class="col2">
          <label for="payment-variant-{$item.id}" class=""> <input type="radio" name="payment_id" class="chbradio ajaxradio" value="{$item.id}" id="payment-variant-{$item.id}" {if $item.checked}{assign var="selected" value=true}checked="checked" {/if}/> 
 <span class="green_bold">{$item.nazev}</span> {if !empty($item.popis)} {$item.popis}{/if} </label>
        {if $item.type.id == 4}
            <a href="#" id="essoxCalculatorTrigger">Kalkulačka splátek</a>
            <script type="text/javascript">
                $("#essoxCalculatorTrigger").click(function(e) {
                    show_calculator(e);
                })
            </script>
        {/if}
    </td>
    <td class="price ">
          <span>
     
      {if $item.cena<>0}
        {if $item.typ==1}
          {$item.cena|currency:'cz'} 
        {elseif $item.typ==2}
          {$item.cena} %
        {/if}
      {else}
        zdarma
      {/if}
     
      </span>
    </td>
    
  </tr>  
  {/foreach}
</table>*}

{if $selected==false}
  {literal}
  <script type="text/javascript">
  $(document).ready(function(){
    $("input:radio[name=payment_id][disabled=false]:first").click();
    $('#payment-table').showLoading(); AjaxSubmitForm();
  });
  
  
  </script>
  {/literal}
{/if}

{*
<tr class="summaryRow">
 <td colspan="2" class="txtRight"><label class="strong" for="payment_id">Vyberte druh platby</label></td>
 <td class="shp_col_2">
    <select class="right ajaxradio" name="payment_id" id="payment_id">
      {foreach from=$payment_types key=key item=item}
      <option class="chbradio" value="{$item.id}" id="payment-variant-{$item.id}" {if $item.checked}selected="selected" {/if}>{$item.nazev}</option>
      {/foreach}
    </select>
 </td>
 <td class="shp_col_3">
    <div class="price txtRight">{$current_payment_price|currency:'cz'}</div>
 </td>
</tr>
*}     