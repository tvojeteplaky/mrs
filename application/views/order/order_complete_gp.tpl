<div class="row">

    <div class="col-md-6">

        <img src="{$media_path}img/complete.jpg" class="responsive" style="margin: auto"/>

    </div>
    <div class="col-md-6" >

        <h1 id="thanks_order"> {translate str="Vaše objednávka byla úspěšně odeslána"}</h1>
        {if $gp_order->getResultText() != ""}
        <h2><strong>{translate str="Zpráva od platební bránky"} GP Webpay:</strong> {$gp_order->getResultText()}.</h2>
        {/if}
        {static_content code="objednavka-odeslana"}

    </div>



</div>
<div class="row">
    <a href="{$url_base}" class="btn btn-army prev_step">{translate str="DO ESHOPU"}</a>
    <a href="{$url_base}ucet-uzivatele" class="btn btn-army next_step">{translate str="MŮJ ÚČET"}</a>
</div>

<!-- Google Code for sale Conversion Page -->
{literal}
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 981163316;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "CouICNz9qgcQtLrt0wM";
        var google_conversion_value = 0;
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/981163316/?value=0&amp;label=CouICNz9qgcQtLrt0wM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
{/literal}

<iframe width="119" height="22" frameborder="0" scrolling="no" src="http://c.imedia.cz/checkConversion?c=99999764&color=ffffff&v={$order_total}"></iframe>