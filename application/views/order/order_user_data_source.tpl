{* sablona formulare sdruzujici tabulky pro vyber zpusobu zadani dat o uzivateli v objednavce (prihlaseni,registrace s prihlasenim,pouhe zadani dat) *}
{* tato sablona je soucasti objednavky (krok 2-3.) - nadrazena sablona: order_container.tpl *}
{assign var='action' value=$action|default:""}
<ul class="tabs" data-tab role="tablist" id="userTabs">
    <li class="tab-title active" role="presentation"><a href="#bez_reg" role="tab" tabindex="0"
    aria-selected="true" aria-controls="bez_reg">Nákup bez registrace</a></li>
    <li class="tab-title " role="presentation"><a href="#reg" role="tab" tabindex="0"
    aria-selected="false" aria-controls="reg">Nový zákazník - registrace</a></li>
    <li class="tab-title" role="presentation"><a href="#login" role="tab" tabindex="0"
        aria-selected="false"
    aria-controls="login">Stávající zákazník - přihlásit se</a></li>


</ul>


<div class="tabs-content">
  <section role="tabpanel" aria-hidden="false" class="content active" id="bez_reg">
    {$order_unregistered_user}
  </section>
  <section role="tabpanel" aria-hidden="true" class="content" id="reg">
    {$user_registration}
  </section>
  <section role="tabpanel" aria-hidden="true" class="content" id="login">
    {$user_login}
  </section>

</div>
{*
<div id="OrderDataSource">
    <div class="defaultBoxHalf left">
            <div class="content">
                <h2>{translate str="Již jsem zde nakupoval"}</h2>
                {$user_login}
            </div>
            <div class="bottom"></div>
    </div>

    <div class="defaultBoxHalf right">
            <div class="content">
                <h2>{translate str="Jsem nový zákazník"}</h2>
                <div class="left">
                    <form action="" id="NoSendForm">
                            <label for="RegEmail" class="title">Zadejte e-mail</label>
                            <input type="text" name="reg_email" id="RegEmail" />
                            <input type="button" name="show_reg_form" value="registrovat" id="ShowRegForm" />
                    </form>
                </div>
                <div class="right">
                        <div class="title">Co získáte registrací?</div>
                        <ul>
                            <li>Zajímavé slevy při opakovaných objednávkách</li>
                            <li>Možnost vznášet dotazy do poradny</li>
                            <li>Informace o novinkách na váš E-mail</li>
                        </ul>
                </div>
                <div class="correct foot"></div>
                <a href="#" id="ShowUnregForm">nebo nakupte rychle a bez registrace</a>
                
            </div>
            <div class="bottom"></div>
    </div>

    <div class="correct"></div>
        <a name="form" id="form"></a>
        
        <div class="chbox user-registration defaultBoxFull" {if $action=="registration"}style="display: block"{/if}>
            <div class="content">
                <h2>{translate str="Registrace uživatele"}</h2>
                
                {$user_registration}
            </div>
            <div class="bottom"></div>
        </div>
        
        <div class="chbox order-unregistered-user defaultBoxFull" {if $action=="add_unregistered_user"}style="display: block"{/if}>
            <div class="content">
                <h2>{translate str="Nákup bez registrace"}</h2>
                
                {$order_unregistered_user}
            </div>
            <div class="bottom"></div>
        </div>
        
</div>*}

{literal}
<script type="text/javascript">
    $(document).ready(function() {
      $(document).foundation('tab', 'reflow');
    $('#NoSendForm').submit(function() {
        return false;
    });
    
    $("#RegEmail").keyup(function(e){
        if(e.keyCode == 13)
        {
            e.preventDefault();
            $("#ShowRegForm").click();
            return false;
        }
    });
    $("#ShowRegForm").click(function(){
        $(".chbox").slideUp().promise().done(function(){$(".user-registration").slideDown();});
        $("#input_email").val($("#RegEmail").val());
        
    });
    
    $("#ShowUnregForm").click(function(e){
        e.preventDefault();
        
        $(".chbox").slideUp().promise().done(function(){$(".order-unregistered-user").slideDown();});
        
    });
    });
</script>
{/literal}