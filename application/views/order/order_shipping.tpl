{* sablona tabulky pro vyber dopravy *}
{* tato sablona je soucasti objednavky - nadrazena sablona: order_shipping_payment.tpl *}
{*<h2>Způsob doručení</h2>*}
{$class=""}

{foreach from=$shipping_types key=key item=item}
<div class="columns">
<input name="shipping_id" value="{$item.id}" type="radio" {if $item.checked}checked="checked" {/if}/><label>{$item.nazev} ({if $item.cena>0}{$item.cena|currency:'cz'} {else}zdarma{/if}){if !empty($item.popis)} <span data-tooltip aria-haspopup="true" class="has-tip tip-top radius" title="{$item.popis}"><span class="fi-info"></span></span>{/if}</label>
</div>
{/foreach}
{if $class!=""}
</table>
</div>
{/if}
{*<a href="#" id="table_{$item.class}" class="shippingTitle {if $class==""}first{/if}">{if $item.class=="cz"}Česká republika <img src="{$media_path}img/shipping/ico_cz.gif" title="Česká republika" class="right" />{else if $item.class=="sk"}Slovensko <img src="{$media_path}img/shipping/ico_sk.gif" title="Slovensko" class="right" />{/if}</a>
*}
{*<div class="shippingTableContainer table_{$item.class}">
<table summary="formulář s výběrem způsobu dopravy" class="orderSectionTable shippingTable table_{$item.class}">
{$class=$item.class}
{/if}
<tr class="sectiontableentry2">*}
    {*  <td class="col1">{if $item.icon}<img src="{$media_path}img/shipping/{$item.icon}.gif" />{/if}</td>*}
    {*<td class="col2">
                <label for="shipping-variant-{$item.id}" class=""><input type="radio" class="chbradio ajaxradio" name="shipping_id" value="{$item.id}" id="shipping-variant-{$item.id}" {if $item.checked}checked="checked" {/if}/>
    <span class="green_bold">{$item.nazev}</span> {if !empty($item.popis)} {$item.popis}{/if}</label>
    </td>
    <td class="price">
            <span>
        {if $item.cena>0}{$item.cena|currency:'cz'} {else}zdarma{/if}
        </span>
    </td>
</tr>
{/foreach}
</table>
</div>
*}
{literal}
<script type="text/javascript">
$(document).ready(function(){
$(".shippingTableContainer").hide();

$("input.chbradio[checked=checked]").closest(".shippingTableContainer").show().addClass("visible");

$("a.shippingTitle").click(function(){
    var id=$(this).attr("id");
    $(".shippingTableContainer.visible").slideUp().removeClass("visible").promise().done(function(){$(".shippingTableContainer."+id).slideDown().addClass("visible");});
    
});
});
</script>
{/literal}


{*
<tr class="summaryRow">
<td colspan="2" class="txtRight"><label class="strong" for="payment_id">Vyberte druh dopravy</label></td>
<td  class="shp_col_2">
    
    <select class="right ajaxradio" name="shipping_id">
    {foreach from=$shipping_types key=key item=item}
    <option  class="chbradio" value="{$item.id}" id="shipping-variant-{$item.id}" {if $item.checked}selected="selected" {/if}>{$item.nazev}</option>
    {/foreach}
    </select>
</td>
<td class="shp_col_3">
    <div class="price txtRight">{$current_shipping_price|currency:'cz'} </div>
</td>
</tr>
*}