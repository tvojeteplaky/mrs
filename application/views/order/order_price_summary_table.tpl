{* tabulka se souctem cen (doprava, platebni metoda, apod. + cena s dph celkem), mimo souctu kosiku - ten si zobrazi sam *}
  
<table class="orderSectionTable priceSummaryTable" summary="tabulka cenových součtů">
{foreach name=pv from=$price_values key=key item=item}
  <tr>
    <td class="col1 txtRight">{if isset($item.class)}<span class="{$item.class}">{$item.name}</span>{else}{$item.name}{/if}</td>
    <td class="col2 txtRight">
    {if isset($item.class)}<span class="{$item.class}">{/if}
      {if $item.value<>0}{if empty($item.type) || $item.type==1}{$item.value|currency:'cz'}{elseif $item.type==2}{$item.value}%{else}{$item.value|currency:'cz'}{/if}{else}zdarma{/if}
    {if isset($item.class)}</span>{/if} 
    </td>
    <td class="col3"></td>
  </tr>
{/foreach}
</table>
