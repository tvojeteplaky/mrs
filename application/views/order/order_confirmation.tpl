{* sablona pro potvrzeni objednavky *}
{* tato sablona je soucasti objednavky (krok 4. - potvrzeni objednavky) - nadrazena sablona: order_container.tpl *}

<div class="productListTable col-sm-8 table"> 
  <table summary="nákupní košík" border="0" class="orderSectionTable table summary_table ">  
  <tr class="headRow">    
    <th class="order-col-nazev">Foto</th>         
       
    <th>Název produktu</th>  	 
    <th class="order-col-nazev r">Počet kusů</th>      
    <th class="txtRight w120 r">Bez DPH za ks</th>  	
    <th class="txtRight w100 r">Celkem bez DPH</th>
    <th class="txtRight w100 r">Celkem s DPH</th>  	
  
  </tr>
  
  {foreach key=k item=product name=prod from=$cart_products}     
  <tr>	
    <td><img src="{$product.parent.photo}" width="51" /></td>	
    <td><a class="productTitle" href="{$product.nazev_seo}">{$product.parent.nazev} - {$product.nazev}</a></td>
    <td class="r">
        {$product.mnozstvi}&nbsp;{$product.jednotka}
    </td>
    <td class="txtRight r">{if $product.cena_bez_dph>0}{$product.cena_bez_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>	
    <td class="txtRight r">{if $product.cena_celkem_bez_dph>0}{$product.cena_celkem_bez_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>	
    <td class="txtRight r">{if $product.cena_celkem_s_dph>0}{$product.cena_celkem_s_dph|currency:$language_code}{else}{translate str="zdarma"}{/if}</td>	
     
  </tr>
  {/foreach}
  </table>
  <table class="table orderSummaryLastStep">
  {* mezisoucty *}
  <tr class="firstRow">    
    <td class="txtRight">Cena zboží celkem</td>    
    <td class="txtRight r" >{$price_total_products|currency:$language_code}</td>    
  </tr>

  
  {foreach name=pv from=$price_values key=key item=item}
  {if not $smarty.foreach.pv.last}
  <tr {if isset($item.class)}class="{$item.class}"{/if}>
    <td class="col1 txtRight " >{$item.name}</td>
    <td class="col2 r">
    {if $key == 'montaz' && $item.value == 0}
      <em>Nepožadováno</em>
    {else}
    {if isset($item.class)}<span class="{$item.class}">{/if}
      {if $item.value<>0}{if empty($item.type) || $item.type==1}{$item.value|currency:'cz'}{elseif $item.type==2}{$item.value}%{else}{$item.value|currency:'cz'}{/if}{else}<em>zdarma</em>{/if}
    {if isset($item.class)}</span>{/if}
    {/if}
    </td>
  </tr>
  {else} 
    <tr class="withoutVatRow">
      <td>{$item.name}</td>
      <td class="r">
        {if isset($item.class)}<span class="{$item.class}">{/if}
          {if $item.value<>0}{if empty($item.type) || $item.type==1}{$item.value|currency:'cz'}{elseif $item.type==2}{$item.value}%{else}{$item.value|currency:'cz'}{/if}{else}<em>zdarma</em>{/if}
        {if isset($item.class)}</span>{/if}
      </td>
    </tr>
    {/if}
{/foreach}

</table>
{if !empty($voucher) && $voucher}
<div class="voucherLine bold noInPopup txtRight">{translate str="Slevový kupón byl započítán do cen produktů, sleva ve výši:"} {$voucher.discount_value}%</div>
{/if}

{if !empty($gift_box_code)}
<div class="voucherLine bold noInPopup txtRight">{translate str="Typ dárkového balení:"} {$gift_box_code}</div>
{/if}

<div class="correct"></div>
</div>
<div class="confirmationTableBottom"></div>

{*
<div class="bigMT">
  <input type="checkbox" name="ExtendFormChb" id="ExtendFormChb" {if !empty($data.ExtendFormChb)}checked="checked"{/if} />  <label for="ExtendFormChb" class="extendFormLb" class="bold">Přeji si načíst body do Sphare programu, nebo přidat poznámku k objenávce.</label>
</div>
*}

<div class="row">
  <div class="columns orderLastStep">
  {$order_summary_table}
  </div>
</div>
 
<form action="#FormBottom" method="post" id="formBottom">
<div class="left bigMT noDisplay table" id="ExtendForm">
  <table summary="Poznámka zákazníka, doplňující údaje">
    {*
    <tr>
      <td colspan="2"><label for="customer_order_code" class="bold">Sphere program (kód)</label><input type="text" id="customer_order_code" name="customer_order_code" {if !empty($data.customer_order_code)}value="{$data.customer_order_code}"{/if} /> </td>
    </tr>
    *}
   
    <tr>
      <td colspan="2"><label for="poznamka" class="bold">Poznámka</label><textarea id="poznamka" name="poznamka" rows="6" cols="80" class="form-control">{if !empty($data.poznamka)}{$data.poznamka}{/if}</textarea></td>
    </tr>
     <tr>
    <td colspan="2">
      <label>
        <input type="checkbox" value="agree" name="wantactions" checked="">
        Souhlasím se zasíláním Akčních nabídek
      </label>
    </td>
    </tr>
  </table>
</div>

<a name="FormBottom" id="FormBottom"></a>
<div class="bottomButtons">
  {hana_secured_post action="send_order" module="order"}
  <input type="hidden" name="poznamka" value="">
{* <a href="dodaci-udaje" title="zpět na dodací údaje" class="btn btn-army prev_step">{translate str="O krok zpět"}</a>*}
  
  {*
  <div class="obchodniPodminky">
    {translate str="Souhlasím s"} <a target="_blank" href="{$url_base}obchodni-podminky" title="{translate str="Souhlasím s obchodními podmínkami"}">	{translate str="obchodními podmínkami"}</a> <input type="checkbox" name="obchodni_podminky" value="agree" class="checkbox" />
    {if !empty($errors.obchodni_podminky)}<span class="error bold" style="margin-left: 150px;"> - {translate str="nutno potvrdit souhlas"}</span>{/if}
  </div>
  *}
  <div class="row">
  <div class="columns">
 <input type="submit" name="next_step" value="{translate str="Závazně objednat"}" class="button right" />
  </div> 
</div>
</div>
</form>
  <div class="clearfix"></div>
{literal}
  <script type="text/javascript">
    $(document).ready(function() {

      $('html, body').animate({
          scrollTop: $("body").find(".orderSummaryLastStep").offset().top - 2
      }, 2500);

     /* $("#ExtendFormChb").click(function(){
        $("#ExtendForm").slideToggle();
    });
    
    if($('#customer_order_code').val() || $('#poznamka').val())
    {
       $("#ExtendForm").show();
       $("#ExtendFormChb").attr("checked","checked");
       $
    }*/

    });
  </script>
{/literal}

