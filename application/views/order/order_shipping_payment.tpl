{* sablona formulare sdruzujici tabulky pro vyber dopravy a platby *}
{* tato sablona je soucasti objednavky (krok 2. - doprava a platba) - nadrazena sablona: order_container.tpl *}

{*<h1>{translate str="Platba a doprava"}</h1>*}
  <form action="" method="post" id="ship-paym-form" class="jquery_css ajax-form">
    {* vlozene sablony s dopravou a platbou: *}
    
    {*
    {$cart_detail_table}
    *}
    
    <div class="payment">
      <div class="row">
        <div class="columns">
          <h3>Vyberte způsob dopravy</h3>
        </div>
        
      {$order_shipping}
      </div>
    </div>
    
    <div class="payment">
  <div class="row">
  <div class="columns">
  <h3>Vyberte způsob platby</h3>
  </div>
      {$order_payment}  
      </div>
    </div>

       <div class="payment">
  <div class="row">
  <div class="columns">
  <h3>Doplňové služby</h3>
  </div>
        <div class="columns">
        <input type="checkbox" name="montaz" value="1" {if $montaz_selected}checked="checked"{/if}><label>Montáž ({$montaz_price|currency:$language_code} bez DPH) <span data-tooltip aria-haspopup="true" class="has-tip tip-top radius" title="Při zakoupení zboží je možno objednat montáž. Cena za montáž je {$setting.instalation_percentage_after_limit} % z celkové ceny zboží bez DPH. Minimální cena montáže je {$setting.instalation_price_under_limit},- Kč bez DPH (zboží v hodnotě do {$setting.instalation_price_limit},- Kč bez DPH)."><span class="fi-info"></span></span></label>
        </div>  
      </div>
    </div>
      
    
    
    
    
    
    {* tabulka s cenovymi soucty *}
    {*
    {$order_price_summary_table}
    *}
       
    {hana_secured_multi_post action="refresh_shipping_payment"} {* hidden prvky pro ajaxove prepocitani kosiku *}
  
    <div class="orderButtons">
 {*<a href="{$url_base}dodaci-udaje" title="zpět na dodací údaje" class="btn-army btn prev_step">{translate str="Zpět"}</a>*}
      {hana_secured_multi_post action="save_shipping_payment"}
    
    
         
      <button  type="submit" name="save_shipping_payment" value="" class="button right"> Pokračovat &raquo;</button>
     
    </div>
  </form>


{literal}
  <script type="text/javascript">
    $(document).ready(function() {
    $('html, body').animate({
        scrollTop: $("body").find(".payment").offset().top - 2
    }, 2500);
      $(".ajaxradio").click(function(){$('#payment-table').showLoading(); AjaxSubmitForm(); });
    });
  </script>
{/literal}

