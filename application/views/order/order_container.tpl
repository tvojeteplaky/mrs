{* sablona ktera slouzi k "obaleni" prislusnych sekci a doplneni kroku objednavky a tlacitek sekce *}
{assign var='order_step' value=$order_step|default:0}

<div class="content-homepage">

<div class="row">
            <div class="columns">
                <div class="shopping-chart-header">
                    <div class="row collapse">
                        <div class="large-3 columns">
                            <a class="button {if $order_step eq 0}active{/if}  first" href="{$url_base}nakupni-kosik">{translate str="Zboží v košíku"}</a>
                        </div>
                        <div class="large-3 columns">
                            {if $order_step > 0}
                                <a class="button{if $order_step eq 1} active{/if}" href="{$url_base}dodaci-udaje">{translate str="Fakturační a dodací údaje"}</a>
                            {else}
                                <span class="button">{translate str="Fakturační a dodací údaje"}</span>
                            {/if}
                        </div>
                        <div class="large-3 columns">
                            {if $order_step > 1}
                                <a class="button {if $order_step eq 2} active{/if}" href="{$url_base}doprava-a-platba">{translate str="Doprava a platba"}</a>
                            {else}
                                <span class="button">{translate str="Doprava a platba"}</span>
                            {/if}
                        </div>
                        <div class="large-3 columns">
                            {if $order_step > 2}
                                <a class="button {if $order_step eq 3} active{/if} last" href="{$url_base}potvrzeni-objednavky">{translate str="Dokončení objednávky"}</a>
                            {else}
                                <span class="button">{translate str="Dokončení objednávky"}</span>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>               
     <div class="shopping-chart-body">
            <div class="row">
                <div class="columns">
                <h1>{$page.nazev}</h1>
         {if isset($cart_table)}

            {$cart_table}


         {/if}
                             </div>
    </div>
    </div>
       <div class="shopping-chart-body">
            <div class="row">
                <div class="columns">
            {$content}
                             </div>
    </div>
    </div>
    </div>
  {*  <div class="orderSteps os_{$order_step}">

        <a href="{$url_base}nakupni-kosik" class="orderBox ob_1 orderBox-selected">{translate str="Košík"}</a>

        {if $order_step > 0}
            <a href="{$url_base}dodaci-udaje" class="orderBox ob_3 orderBox-selected">{translate str="Dodací údaje"}</a>
        {else}
            <span class="orderBox ob_2">{translate str="Dodací údaje"}</span>
        {/if}


        {if $order_step > 1}
            <a href="{$url_base}doprava-a-platba" class="orderBox ob_2 orderBox-selected">{translate str="Doprava a platba"}</a>
        {else}
            <span class="orderBox ob_3">{translate str="Doprava a platba"}</span>
        {/if}


        {if $order_step > 2}
            <a href="{$url_base}potvrzeni-objednavky" class="orderBox ob_4 orderBox-selected last">{translate str="Potvrzení"}</a>
        {else}
            <span class="orderBox ob_4 last">{translate str="Potvrzení"}</span>
        {/if}

    </div>
    {if isset($cart_table)}{$cart_table}{/if}
    {$content}

    <div class="clearfix"></div>
    <div class="container">
        <div class="col-md-12">
            <div class="clearfix"></div>
            <h2 >ZÁKAZNÍCI TAKÉ KUPUJÍ</h2>
            <div class="clearfix"></div>

            <div id="ProductListSection" class="customers-buy">
                <div class="products">
                    {widget controller='product' action='favourite_widget'}
                </div>
            </div>
        </div>
    </div>










    {*






             <h2>ZÁKAZNÍCI TAKÉ KUPUJÍ</h2>

            <div class="well">
                <div id="myCarousel" class="carousel slide">

                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                      <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                      <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                            </div>
                            <!--/row-->
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                                <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                      <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                      <div class="col-sm-2"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a>
                                </div>
                            </div>
                            <!--/row-->
                        </div>
                        <!--/item-->
                    </div>
                    <!--/carousel-inner-->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>

                    <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
                </div>
                <!--/myCarousel-->
            </div>
            <!--/well-->
        </div>
    </div>
      *}

