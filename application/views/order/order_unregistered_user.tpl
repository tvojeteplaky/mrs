{* sablona pro rychle zadani osobnich udaju v objednavce, pokud se uzivatel nechce registrovat *}
<form action="#bez_reg" method="post" id="RegistrationForm"  class="form-inline">
    <div id="shipping" class="order-form">
        <h3 id="fakturacniAdresa">Jednorázový nákup - bez registrace</h3>
        <div class="row">
            <div class="large-7 medium-9 columns">
                 <div class="row">
                    <div class="small-6 columns"><label><input type="radio" name="user-registration[typ_uzivatele]" value="user" id="RegistrationTypeUser" class="registrationType" {if empty($unregistered_user_data.typ_uzivatele) || $unregistered_user_data.typ_uzivatele=="user"}checked="checked"{/if} /> {translate str="Nákup na uživatele"}</label></div>
                    <div class="small-6 columns"><label><input type="radio" name="user-registration[typ_uzivatele]" value="company" id="RegistrationTypeCompany" class="registrationType" {if !empty($unregistered_user_data.typ_uzivatele) && $unregistered_user_data.typ_uzivatele=="company"}checked="checked"{/if} /> {translate str="Nákup na firmu"}</label></div>
                </div>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.nazev" placeholder="Jméno a příjmení" required="true" data=$unregistered_user_data errors=$errors}
                    </div>
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.email" placeholder="Email" required="true" data=$unregistered_user_data errors=$errors}
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.telefon" placeholder="Telefonní číslo" required="true" data=$unregistered_user_data errors=$errors}
                    </div>
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.ulice" placeholder="Ulice a číslo popisné" required="true" data=$unregistered_user_data errors=$errors}
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.psc" placeholder="PSČ" required="true" data=$unregistered_user_data errors=$errors}
                    </div>
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.mesto" placeholder="Město" required="true" data=$unregistered_user_data errors=$errors}
                    </div>
                </div>
                <div class="CompanyFieldsUnreg">
                <div class="row">
                   <div class="large-12 columns">
                       {form_input name="unregistered-user.nazev_organizace" placeholder="Název Firmy" data=$unregistered_user_data errors=$errors}
                   </div> 
                </div>
                    
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.ic" placeholder="IČ" data=$unregistered_user_data errors=$errors}
                    </div>
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user.dic" placeholder="DIČ" errors=$errors}
                    </div>
                </div>
            </div>
            </div>
            <div class="large-5 medium-3 columns">
            </div>
        </div>
    </div>
    <div id="alt-shipping" class="order-form">
    <h3 class="dodaciMore" id="dodaciAdresa">Dodací adresa - pokud se liší od fakturační <span class="triangle down"></span></h3>

        <div class="row">
            <div class="large-7 medium-9 columns">
            <div class="hideMore">
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user-branch.nazev" placeholder="Jméno a příjmení"  data=$unregistered_user_data_branch errors=$errors.branch_errors}
                    </div>
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user-branch.email" placeholder="Email"  data=$unregistered_user_data_branch errors=$errors.branch_errors}
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user-branch.telefon" placeholder="Telefonní číslo"   data=$unregistered_user_data_branch errors=$errors.branch_errors}
                    </div>
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user-branch.ulice" placeholder="Ulice a číslo popisné"  data=$unregistered_user_data_branch errors=$errors.branch_errors}
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        {form_input name="unregistered-user-branch.psc" placeholder="PSČ"  data=$unregistered_user_data_branch errors=$errors.branch_errors}
                    </div>
                    <div class="large-6 medium-6 columns">
                    {form_input name="unregistered-user-branch.mesto" placeholder="Město"  data=$unregistered_user_data_branch errors=$errors.branch_errors}</div>
                </div>
                <input type="hidden" name="unregistered-user-branch[branch_enabled]" value="enabled">
            </div>
                <div class="row"> <div class="small-6 medium-6"><input type="checkbox" class='form-control' name="unregistered-user[obchodni_podminky]" value="agree" class="checkbox" /> &nbsp; Souhlasím s <a target="_blank" href="{$url_base}obchodni-podminky" title="Souhlasím s obchodními podmínkami">    obchodními podmínkami</a>
                    {if !empty($errors.obchodni_podminky)}<span class="error">nutno potvrdit souhlas</span>{/if}</div>

                    <div class="small-6 medium-6"><button type="submit" class="button right"
                >Pokračovat &raquo;</button></div>
                    </div>
                
            </div>
            <div class="large-5 medium-3 columns">
            </div>
        </div>
    </div>
    {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
    {hana_secured_post action="add_unregistered_user" module="order"}
</form>
{literal}
<script type="text/javascript">
    $(document).ready(function(){
        if ($('#RegistrationTypeCompany').is(':checked'))
        {
            $(".CompanyFieldsUnreg").show();
        } else {
            $(".CompanyFieldsUnreg").hide();
        }
        $(".registrationType").click(function(e){
            if($(this).val()=="company")
            {
                $(".CompanyFieldsUnreg").slideDown();
            }
            else
            {
                $(".CompanyFieldsUnreg").slideUp();
            }
            });
        var dodaciOtevreny = false;
        $(".dodaciMore").click(function(){
            if(dodaciOtevreny) {
                $(".hideMore").slideUp();
                dodaciOtevreny = false;
                $(".dodaciMore").find("span").removeClass("up").addClass("down");
            } else {
                $(".hideMore").slideDown();
                dodaciOtevreny = true;
                $(".dodaciMore").find("span").addClass("up");
            }
        });

        hiddenInputHasValue = false;
        $('input[type=text][name*=unregistered-user-branch]').each(function() {
            if ($(this).val() != "") hiddenInputHasValue = true;
        });

        if (hiddenInputHasValue) {
            $(".hideMore").slideDown();
            dodaciOtevreny = true;
            $(".dodaciMore").find("span").addClass("up");
        }

        if (window.location.hash.substr(1) == "dodaciAdresa") {
            $(".hideMore").slideDown();
            dodaciOtevreny = true;
            $(".dodaciMore").find("span").addClass("up");
            $('html, body').animate({
                scrollTop: $("body").find("#dodaciAdresa").offset().top - 2
            }, 2500);
        } else if (window.location.hash.substr(1) == "fakturacniAdresa") {
            $('html, body').animate({
                scrollTop: $("body").find("#fakturacniAdresa").offset().top - 2
            }, 2500);
        } else {
            $('html, body').animate({
                scrollTop: $("body").find("#userTabs").offset().top - 2
            }, 2500);
        }
    });
</script>
{/literal}

{*<form action="#bez_reg" method="post" id="RegistrationForm"  class="form-inline">
    {if !empty($unregistered_user_error)}<div class="caption captError">{$unregistered_user_error}</div>{/if}
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.nazev" class='form-control' label="Jméno a příjmení *" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.email" class='form-control' label="Email *" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.telefon" class='form-control' label="Telefon *" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.ulice" class='form-control' label="Ulice a číslo popisné *" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.psc" class='form-control' label="PSČ *" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.mesto" class='form-control' label="Město *" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.ic" class='form-control' label="IČ" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user.dic" class='form-control' label="DIČ" data=$unregistered_user_data errors=$errors}</div>
    </div>
    <div class="row">*}
        {*form_input type="checkbox" class="checkbox" class='form-control' name="unregistered-user-branch.branch_enabled" data=$unregistered_user_data_branch*}
        {*<h3 class="extendFormLb">
            <label for="input_branch_enabled" class="extendFormLb">{translate str="Dodací adresa se liší od fakturační"}
            </label>
            </h3>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user-branch.nazev" class='form-control' label="Název *" data=$unregistered_user_data_branch errors=$errors.branch_errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user-branch.email" class='form-control' label="Email *" data=$unregistered_user_data_branch errors=$errors.branch_errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user-branch.telefon" class='form-control' label="Telefon" data=$unregistered_user_data_branch errors=$errors.branch_errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user-branch.ulice" class='form-control' label="Ulice a číslo popisné *" data=$unregistered_user_data_branch errors=$errors.branch_errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user-branch.psc" class='form-control' label="PSČ *" data=$unregistered_user_data_branch errors=$errors.branch_errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">{form_input name="unregistered-user-branch.mesto" class='form-control' label="Město *" data=$unregistered_user_data_branch errors=$errors.branch_errors}</div>
    </div>
    <div class="row">
            <div class="col-md-9">
                    <input type="checkbox" class='form-control' name="unregistered-user[obchodni_podminky]" value="agree" class="checkbox" /> &nbsp; Souhlasím s <a target="_blank" href="{$url_base}obchodni-podminky" title="Souhlasím s obchodními podmínkami">	obchodními podmínkami</a>
                    {if !empty($errors.obchodni_podminky)}<span class="error" style="margin-left: 150px;"> - nutno potvrdit souhlas</span>{/if}
            </div>
    </div>
    <div class="row">
            <div class="col-md-9"></div>
    </div>
    <div class="bot">
            {if !empty($redirect)}<input type="hidden" name="h_param" value="{$redirect}" /> {/if}
            {hana_secured_post action="add_unregistered_user" module="order"}
            <input type="submit" name="registration" class="button right" value="{translate str="Potvrdit"}" />
    </div>
</form>*}