    <div class="content-homepage">

        <div class="row">
            <div class="columns">
                <div class="shopping-chart-header">
                    <div class="row collapse">
                        <div class="large-3 columns">
                            <span class="button first">Zboží v košíku</span>
                        </div>
                        <div class="large-3 columns">
                            <span class="button">Fakturační a dodací údaje</span>
                        </div>
                        <div class="large-3 columns">
                            <span class="button ">Platba a doprava</span>
                        </div>
                        <div class="large-3 columns">
                            <span class="button last active">Dokončení objednávky</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Top offer -->
        <div class="shopping-chart-body">
            <div class="row">
                <div class="columns">
                    <h2 class="last-step">Děkujeme Vám za Vaši objednávku ve výši {$order_total|currency:$language_code}, její zpracování bude vyřešeno co nejdříve to bude možné....</h2>
                    <a class="button left secondary" href="/nabidka">&laquo Zpět do obchodu</a>
                </div>
            </div>
        </div>
        <!-- End of Top offer -->
    </div>


{*
<div class="row"> 
    
    <div class="col-md-6">
	
	<img src="{$media_path}img/complete.jpg" class="responsive" style="margin: auto"/>
    
    </div>
	    <div class="col-md-6" >
	
		<h1 id="thanks_order"> {translate str="Vaše objednávka byla úspěšně odeslána"}</h1>
{static_content code="objednavka-odeslana"}
    
    </div>



</div>
<div class="row">
<a href="{$url_base}" class="btn btn-army prev_step">{translate str="DO ESHOPU"}</a>
<a href="{$url_base}ucet-uzivatele" class="btn btn-army next_step">{translate str="MŮJ ÚČET"}</a>
</div>
*}
{*
<div id="OrderCompleteSectionBottom">

<h2>Objednávka číslo: {$order_code}</h2>

<div class="colHalf">
  <h2>Dodavatel</h2>
  <strong>{$owner_data.firma}</strong><br />
  {$owner_data.ulice}<br />
  {$owner_data.mesto}<br />
  {$owner_data.psc}<br />
  <br />
  {$owner_data.email}<br />
  {$owner_data.telefon}<br /> 
  <br />
  IČ: {$owner_data.ic}<br />
  DIČ: {$owner_data.dic}<br />
 
</div>

<div class="colHalf">
  <h2>Odběratel</h2>
  <strong>{$billing_data.nazev}</strong><br />
  {$billing_data.ulice}<br />
  {$billing_data.mesto}<br />
  {$billing_data.psc}<br />
  <br />
  {$billing_data.email}<br />
  {$billing_data.telefon}<br />
  <br />
  <table summary="Způsob dopravy a platby">
    <tr>
      <td class="bold" style="width: 70px">Doprava: </td>
      <td>{$doprava}</td>
    </tr>
    <tr>
      <td class="bold">Platba: </td>
      <td>{$platba}</td>
    </tr>
  </table>
</div>

<div class="colHalf correct"></div>
<div class="colHalf">

<h3>Dodací adresa</h3>
  {if !$branch_data || !$branch_data.nazev}
  Stejná jako fakturační
  {else}
  
  {$branch_data.nazev}<br />
  {$branch_data.ulice}<br />
  {$branch_data.mesto}<br />
  {$branch_data.psc}<br />
  <br />
  {$branch_data.email}<br />
  {$branch_data.telefon}<br />
  
  {/if}

</div>
<div class="correct"></div>

<h2>Zakoupené zboží</h2>
{$shopping_cart}


<table summary="celková cena" class="right">
{$order_price_summary_table}
</table>

<br />
<div class="correct"></div>
<h2>Vaše poznámka</h2>
{$billing_data.poznamka}
<br /><br />
</div>
*}
<!-- Google Code for sale Conversion Page -->
{literal}
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 981163316;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "CouICNz9qgcQtLrt0wM";
var google_conversion_value = 0;
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/981163316/?value=0&amp;label=CouICNz9qgcQtLrt0wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
{/literal}

<iframe width="119" height="22" frameborder="0" scrolling="no" src="http://c.imedia.cz/checkConversion?c=99999764&color=ffffff&v={$order_total}"></iframe>