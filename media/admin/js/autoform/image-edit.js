var ImageEdit = {
    form: null,
    submit: null,
    items: null,

    init: function(form, submit, items)
    {
        this.form = form;
        this.submit = submit;
        this.items = items;

        this.initForm();
        this.initSortable();
        this.initItems();
    },

    initForm: function()
    {
        var actual_form = this.form.parent().append("<form></form>");
        actual_form = actual_form.find('form').first();
        actual_form.addClass('form-horizontal');
        actual_form.attr('id', this.form.attr('id'));
        actual_form.attr('action', this.form.attr('data-action'));
        actual_form.attr('method', this.form.attr('data-method'));
        actual_form.attr('enctype', 'multipart/form-data');
        actual_form.html(this.form.html());
        this.form.remove();
        this.form = actual_form;
    },

    initSortable: function()
    {
        $('.sortable').sortable().bind('sortupdate', function() {
            ImageEdit.items = $(ImageEdit.items.selector);
            var data = "form[action]=reorder";
            ImageEdit.items.each(function(i) {
                if (data != '')
                    data += '&';
                data += 'newOrder[]='+$(this).attr('data-id');
            });

            $.post(
                '',
                data,
                function(data) {
                    //ImageEdit.handleMessage(data);
                },
                'json'
            );
        });
    },

    initItems: function()
    {
        ImageEdit.items.find('button.btn').click(function(e) {
            e.preventDefault();
            var item = $(this);
            var id = item.attr('data-id');

            if (item.hasClass('edit'))
                ImageEdit.handleEdit(id);
            else if(item.hasClass('delete'))
                ImageEdit.handleDelete(id);
        })

        $('a[data-target=#image-edit-modal]').first().click(function(e) {
            ImageEdit.cleanForm(true);
        })
    },

    handleEdit: function(id)
    {
        var data = 'form[action]=info&form[id]=' + id;
        $.post(
            '',
            data,
            function(data) {
                if (!Hana.is_undefined(data['code'])) {
                    //ImageEdit.handleMessage(data)
                } else {
                    ImageEdit.cleanForm();
                    ImageEdit.form.find('#nazev').val(data['nazev']);
                    ImageEdit.form.find('#odkaz').val(data['odkaz']);
                    ImageEdit.form.find('#popis').val(data['popis']);
                    if (data['zobrazit'] == 1)
                        ImageEdit.form.find('#zobrazit').prop('checked', true);
                    $('#image-edit-modal').modal();
                    $('#image-edit-action').on('click', function(e) {
                        e.preventDefault();
                        ImageEdit.editSend(id);
                    });
                }
            },
            'json'
        );
    },

    editSend: function(id)
    {
        ImageEdit.form.find("input[name='form[action]']").val('edit');
        ImageEdit.form.find("input[name='form[id]']").val(id);
        var ajax = ImageEdit.form.find('#image_edit_file').val().trim().length == 0;
        if (ajax) {
            var data = ImageEdit.form.serialize();
            $.post(
                '',
                data,
                function(data) {
                    if (!Hana.is_undefined(data['code'])) {
                        //ImageEdit.handleMessage(data)
                    } else {
                        $('#image-edit-modal').modal('hide');
                    }
                },
                'json'
            );

        } else {
            ImageEdit.form.trigger('submit');
        }
    },

    handleDelete: function(id)
    {
        var data = 'form[action]=delete&form[id]=' + id;
        $.post(
            '',
            data,
            function(data) {
                if (!Hana.is_undefined(data['code'])) {
                    //ImageEdit.handleMessage(data)
                } else {
                    var item = $('li[data-id='+id+']');
                    item.fadeOut('medium', function() {
                       $(this).remove();
                    });
                }
            },
            'json'
        );
    },

    handleMessage: function(data)
    {
        if (data['code'] > 1) {
            console.log('ERROR: ' + data['message']);
        } else if (data['code'] == 1){
            console.log('SUCCESS: ' + data['message'])
        } else
            console.log(data['message']);
    },

    cleanForm: function(zobrazit)
    {
        ImageEdit.form.find('#nazev').val('');
        ImageEdit.form.find('#odkaz').val('');
        ImageEdit.form.find('#popis').val('');
        ImageEdit.form.find('#zobrazit').prop('checked', false);
        if (!Hana.is_undefined(zobrazit) && zobrazit)
            ImageEdit.form.find('#zobrazit').prop('checked', true);
        ImageEdit.form.find('#image_edit_file').val('');
        $('#image-edit-action').off('click');
        ImageEdit.form.find("input[name='form[action]']").val('new');
        ImageEdit.form.find("input[name='form[id]']").val(0);
    }


};