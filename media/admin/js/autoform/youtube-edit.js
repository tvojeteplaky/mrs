var YoutubeEdit = {
    form: null,
    submit: null,
    items: null,

    init: function (form, submit, items) {
        this.form = form;
        this.submit = submit;
        this.items = items;

        this.initForm();
        this.initSortable();
        this.initItems();
    },

    initForm: function () {
        var actual_form = this.form.parent().append("<form></form>");
        actual_form = actual_form.find('form').first();
        actual_form.addClass('form-horizontal');
        actual_form.attr('id', this.form.attr('id'));
        actual_form.attr('action', this.form.attr('data-action'));
        actual_form.attr('method', this.form.attr('data-method'));
        actual_form.attr('enctype', 'multipart/form-data');
        actual_form.html(this.form.html());
        this.form.remove();
        this.form = actual_form;
    },

    initSortable: function () {
        $('.sortable').sortable().bind('sortupdate', function () {
            YoutubeEdit.items = $(YoutubeEdit.items.selector);
            var data = "form[action]=reorder";
            YoutubeEdit.items.each(function (i) {
                if (data != '')
                    data += '&';
                data += 'newOrder[]=' + $(this).attr('data-id');
            });

            $.post(
                '',
                data,
                function (data) {
                    //YoutubeEdit.handleMessage(data);
                },
                'json'
            );
        });
    },

    initItems: function () {
        YoutubeEdit.items.find('button.btn').click(function (e) {
            e.preventDefault();
            var item = $(this);
            var id = item.attr('data-id');

            if (item.hasClass('edit'))
                YoutubeEdit.handleEdit(id);
            else if (item.hasClass('delete'))
                YoutubeEdit.handleDelete(id);
        })

        $('a[data-target=#youtube-edit-modal]').first().click(function (e) {
            YoutubeEdit.cleanForm(true);
        })
    },

    handleEdit: function (id) {
        var data = 'form[action]=info&form[id]=' + id;
        $.post(
            '',
            data,
            function (data) {
                if (!Hana.is_undefined(data['error_code'])) {
                    //YoutubeEdit.handleMessage(data)
                } else {
                    YoutubeEdit.cleanForm();
                    YoutubeEdit.form.find('#nazev').val(data['nazev']);
                    YoutubeEdit.form.find('#code').val(data['code']);
                    if (data['zobrazit'] == 1)
                        YoutubeEdit.form.find('#zobrazit').prop('checked', true);
                    $('#youtube-edit-modal').modal();
                    $('#youtube-edit-action').on('click', function (e) {
                        e.preventDefault();
                        YoutubeEdit.editSend(id);
                    });
                }
            },
            'json'
        );
    },

    editSend: function (id) {
        YoutubeEdit.form.find("input[name='form[action]']").val('edit');
        YoutubeEdit.form.find("input[name='form[id]']").val(id);
        var data = YoutubeEdit.form.serialize();
        $.post(
            '',
            data,
            function (data) {
                if (!Hana.is_undefined(data['error_code'])) {
                    //YoutubeEdit.handleMessage(data)
                } else {
                    var iframe = $('li[data-id=' + id + ']').find('iframe');
                    iframe.attr('src', 'https://www.youtube.com/embed/' + YoutubeEdit.form.find('#code').val());
                    $('#youtube-edit-modal').modal('hide');
                }
            },
            'json'
        );


    },

    handleDelete: function (id) {
        var data = 'form[action]=delete&form[id]=' + id;
        $.post(
            '',
            data,
            function (data) {
                if (!Hana.is_undefined(data['error_code'])) {
                    //YoutubeEdit.handleMessage(data)
                } else {
                    var item = $('li[data-id=' + id + ']');
                    item.fadeOut('medium', function () {
                        $(this).remove();
                    });
                }
            },
            'json'
        );
    },

    handleMessage: function (data) {
        if (data['error_code'] > 1) {
            console.log('ERROR: ' + data['message']);
        } else if (data['error_code'] == 1) {
            console.log('SUCCESS: ' + data['message'])
        } else
            console.log(data['message']);
    },

    cleanForm: function (zobrazit) {
        YoutubeEdit.form.find('#nazev').val('');
        YoutubeEdit.form.find('#code').val('');
        YoutubeEdit.form.find('#zobrazit').prop('checked', false);
        if (!Hana.is_undefined(zobrazit) && zobrazit)
            YoutubeEdit.form.find('#zobrazit').prop('checked', true);
        $('#youtube-edit-action').off('click');
        YoutubeEdit.form.find("input[name='form[action]']").val('new');
        YoutubeEdit.form.find("input[name='form[id]']").val(0);
    }


};