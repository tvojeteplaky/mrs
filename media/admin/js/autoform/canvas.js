/**
 * Created by Tom Barbořík on 22.11.15.
 */

var Canvas = {
    e_canvas: null,
    image_input: null,
    image: null,
    canvas: null,
    finish_button: null,
    save_button: null,
    load_button: null,
    offset_x: 0,
    offset_y: 0,
    width: 0,
    height: 0,
    cropped: false,
    ext: "jpg",
    img: null,
    changeCrop: 0,
    minSize: 10,

    init: function (img, id, width, height, url, ext) {
        this.e_canvas = document.getElementById("image-canvas-"+id);
        this.finish_button = document.getElementById("finish-button-"+id);
        this.save_button = document.getElementById("save-button-"+id);
        this.canvas = this.e_canvas.getContext("2d");
        this.offset_x = this.offset_y = 0;
        this.width = width;
        this.height = height;
        this.url = url;
        this.ext = ext;
        this.img = img;

        this.save_button.setAttribute("disabled", true);
        this.finish_button.onclick = Canvas.finishClickReact;
        this.save_button.onclick = Canvas.saveClickReact;

        this.drawImg();
    },

    drawImg: function () {
        Canvas.offset_x = 0;
        Canvas.offset_y = 0;
        Canvas.cropped = false;
        Canvas.image = new Image;
        Canvas.image.onload = function () {
            Canvas.e_canvas.width = (Canvas.image.width > Canvas.width) ? Canvas.image.width : Canvas.width;
            Canvas.e_canvas.height = (Canvas.image.height > Canvas.height) ? Canvas.image.height : Canvas.height;
            Canvas.reDraw();
        };

        Canvas.image.src = Canvas.img;

        Mouse.registerDrag(Canvas.e_canvas, Canvas.move, Canvas.checkPosition);
    },

    checkPosition: function(event) {
        var allowedDifference = 5;
        var position = Canvas.e_canvas.getBoundingClientRect();
        var left = event.clientX - position.left;
        var top = event.clientY - position.top;

        var left_border =(Canvas.e_canvas.width - Canvas.width) / 2;
        var top_border =(Canvas.e_canvas.height - Canvas.height) / 2;

        // Checking if user wants to change the cropping border
        if ((top - top_border + allowedDifference) >= 0 && (left - left_border + allowedDifference) >= 0 &&
            (top - top_border - Canvas.height - allowedDifference) <= 0 && (left - left_border - Canvas.width - allowedDifference) <= 0) {
            if (Math.abs(left_border - left) <= allowedDifference && (top - top_border) >= 0) {
                Canvas.changeCrop = 1;
            } else if (Math.abs(left_border + Canvas.width - left) <= allowedDifference && (top - top_border) >= 0) {
                Canvas.changeCrop = 2;
            } else if (Math.abs(top_border - top) <= allowedDifference) {
                Canvas.changeCrop = 3;
            } else if (Math.abs(top_border + Canvas.height - top) <= allowedDifference) {
                Canvas.changeCrop = 4;
            } else {
                Canvas.changeCrop = 0;
                return true;
            }
        } else {
            Canvas.changeCrop = 0;
        }

        return true;
    },

    move: function(x, y) {
        if (Canvas.cropped || Canvas.changeCrop <= 0) {
            Canvas.offset_x = Canvas.offset_x + x;
            Canvas.offset_y = Canvas.offset_y + y;
        } else {
            switch (Canvas.changeCrop) {
                case 1: // Left
                    if (Canvas.isResizableWidth(Canvas.width - x * 2))
                        Canvas.width -= x * 2;
                    break;

                case 2: // Right
                    if (Canvas.isResizableWidth(Canvas.width + x * 2))
                        Canvas.width += x * 2;
                    break;

                case 3: // Top
                    if (Canvas.isResizableHeight(Canvas.height - y * 2))
                        Canvas.height -= y * 2;
                    break;

                case 4: // Bottom
                    if (Canvas.isResizableHeight(Canvas.height + y * 2))
                        Canvas.height += y * 2;
                    break;
            }
        }

        Canvas.reDraw(false, !Canvas.cropped);
    },

    reDraw: function (sizeChanged, stroke) {

        if (typeof sizeChanged != "undefined" && sizeChanged) {
            Canvas.offset_x = Canvas.offset_x - (Canvas.image.width - Canvas.e_canvas.width) / 2;
            Canvas.offset_y = Canvas.offset_y - (Canvas.image.height - Canvas.e_canvas.height) / 2;
        }

        if (typeof stroke == "undefined") {
            stroke = true;
        }

        Canvas.canvas.clearRect(0, 0, Canvas.e_canvas.width, Canvas.e_canvas.height);
        Canvas.canvas.drawImage(Canvas.image, Canvas.offset_x, Canvas.offset_y);

        if (stroke) {
            Canvas.canvas.beginPath();
            Canvas.canvas.setLineDash([10, 4]);
            Canvas.canvas.rect((Canvas.e_canvas.width - Canvas.width) / 2, (Canvas.e_canvas.height - Canvas.height) / 2, Canvas.width, Canvas.height);
            Canvas.canvas.stroke();
        }
    },

    isResizableWidth: function(to) {
        return (typeof to != "undefined" && to >= Canvas.minSize) || Canvas.width >= Canvas.minSize;
    },

    isResizableHeight: function(to) {
        return (typeof to != "undefined" && to >= Canvas.minSize) || Canvas.height >= Canvas.minSize;
    },

    finishClickReact: function() {
        Canvas.e_canvas.width = Canvas.width;
        Canvas.e_canvas.height = Canvas.height;
        Canvas.cropped = true;
        Canvas.reDraw(true, !Canvas.cropped);
        Canvas.save_button.removeAttribute("disabled");
        Canvas.save_button.removeAttribute("aria-disabled");
    },

    saveClickReact: function() {
        Canvas.save(Canvas.url, Canvas.ext);
    },

    save: function(url, img_type) {
        if (typeof img_type == "undefined") {
            img_type = "jpeg";
        }

        var img = Canvas.e_canvas.toDataURL("image/" + img_type);

        var data = img.replace(/^data:image\/\w+;base64,/, "");

        $.post(
            url,
            {
                crop:{
                    img: data,
                    type: img_type
                }
            },
            function() {
                Canvas.save_button.classList.remove("btn-primary");
                Canvas.save_button.classList.add("btn-success");
            }
        ).fail(function(data) {
            Canvas.save_button.classList.remove("btn-primary");
            Canvas.save_button.classList.add("btn-danger");
        });
    }
};

var Mouse = {
    lastCoords: {
        x: 0,
        y: 0
    },

    registeredElements: [],

    registerDrag: function(element, callback, downCondition) {
        Mouse.registeredElements.push({
            id: new Date().getUTCMilliseconds(),
            element: element,
            callback: callback,
            downCondition: downCondition
        });

        element.onmousedown = Mouse.mouseDownReaction;
    },

    mouseDownReaction: function(event) {
        var item = Mouse.getElement(event.srcElement);
        if (typeof item.downCondition != "undefined") {
            if (!item.downCondition(event))
                return;
        }

        Mouse.lastCoords.x = event.clientX;
        Mouse.lastCoords.y = event.clientY;

        item.element.onmousemove = Mouse.mouseMoveReaction;
        item.element.onmouseup = Mouse.mouseUpReaction;
    },

    mouseUpReaction: function(event) {
        event.srcElement.onmousemove = null;
    },

    mouseMoveReaction: function(event) {
        Mouse.getElement(event.srcElement).callback(event.clientX - Mouse.lastCoords.x, event.clientY - Mouse.lastCoords.y);
        Mouse.lastCoords.x = event.clientX;
        Mouse.lastCoords.y = event.clientY;
    },

    getElement: function(element) {

        for (var i = 0; i < Mouse.registeredElements.length; i++) {
            if (element == Mouse.registeredElements[i].element)
                return Mouse.registeredElements[i];
        }

        return false;
    }
};