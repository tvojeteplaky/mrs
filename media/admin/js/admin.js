/* 
/*
 * Obsluzny JavaScript pro admin.
 */


$(document).ready(function() {
                 
     $('[data-toggle="tooltip"]').tooltip({placement: 'auto'}); 
     $('[data-toggle="popover"]').popover({html: true});
    setup_hana_js_functionality();
});


function setup_hana_js_functionality()
{
    // inicializace datepickeru
    //$.datepicker.setDefaults($.extend({showMonthAfterYear: false}, $.datepicker.regional['cs']));
    $(".datepicker").datepicker({
        format      :   "dd.mm.yyyy",
        todayBtn    :   "linked",
        language    :   "cs",
        todayHighlight: true,
        keyboardNavigation: true

    });

    $(".fancybox").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'fade'
    });

    // filtrovaci daterangepicker
    // $('.daterangepicker').daterangepicker();

    
    // funkcionalita - "zaskrtnout vse"
    $(".sellAll").click(function()
    {
        var checked_status = this.checked;
        $("#PhotoPreviewBox .item .footer input[type=checkbox]:not(.filteringRow input), #table-section table input[type=checkbox]:not(.filteringRow input), #EditTableSection table input[type=checkbox]:not(.filteringRow input)").each(function()
            {
            this.checked = checked_status;
            });
    });

    // graficka uprava obecneho tlacitka
    $(".button").button();


    // zobrazovani/skryvani filtracniho radku
    $(".filteringSwitch a").click(function (e) {
        e.preventDefault();
        if ($(this).attr('data-toggled') == '0') {
            $(".filteringRow").removeClass("filteringRowHide");
            $(this).attr('data-toggled', '1')
        } else {
            e.preventDefault();
            $(".filteringRow").addClass("filteringRowHide");
            $(this).attr('data-toggled', '0')
        }
    });

    // ajaxove pozadavky v seznamech
    var xurl;

    $(".ajaxelement").click(function(e){
                   e.preventDefault();
                   xurl = $(this).attr("href");
                   if($(this).hasClass("confirmDelete"))
                   {
                     var result=window.confirm("Opravdu smazat položku?");
                     if(result==false) return false;
                   } 
                   
                   $('#ContentSection').showLoading(); 
                
                   $.ajax({
                     type: "GET",
                     url: ""+xurl+"",
                     success: function(msg){
                     //alert( "Obdržená data: " + msg );
                     $('#ContentSection').hideLoading(); 
                     $('#ContentSection').html(msg);
                     //$(this).attr("href",xurl);
                     },
                     error: function(XMLHttpRequest, textStatus, errorThrown){
                        $('#ContentSection tbody').hideLoading(); 
                        alert("Při zpracování dat došlo k chybě");
                        //alert("Při zpracování dat došlo k chybě: " + XMLHttpRequest + ", textStatus: " + textStatus+ ", errorThrown: " + errorThrown);
                     }
                  });
               
   });



    // Uprava nabídky select
    $("select").select2();

}



