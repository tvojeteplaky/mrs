module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
            // if any .less file changes in directory "public/css/" run the "less"-task.
            files: "sass/*.scss",
            tasks: ["sass"]
    },
    sass: {
        dist: {
          files: {
            'css/css.css': 'sass/index.scss'
          }
        }
    },
    });

  grunt.registerTask('default', ['watch']);

};