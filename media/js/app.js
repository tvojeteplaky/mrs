// var onloadCallback = function () {
//     grecaptcha.render('g-recaptcha', {
//         'sitekey': Contact_form.siteKey
//     });
// };

$(function () {
    App.init();
});

var App = {

    init: function () {
        $(document).foundation();

        $("a.slide").click(function(event) {
            event.preventDefault();
            $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
            }, 500);
        });

        $('.hp__banner a').click(function(){
            $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
            }, 500);
            return false;
        });


        var feed = new Instafeed({
            get: 'tagged',
            tagName: 'fishing',
            clientId: '467ede5a6b9b48ae8e03f4e2582aeeb3',
            limit: 16,
            template: '<a href="{{link}}" target="_blank"><img src="{{image}}" /><div class="likes">&hearts; {{likes}}</div></a>'
        });
        feed.run();


        // $("a[href='#print']").click(function (e) {
        //     e.preventDefault();
        //     window.print();
        // })

        // $(".fancybox").fancybox({
        //     openEffect: 'elastic',
        //     closeEffect: 'elastic',
        //     helpers: {
        //         overlay: {
        //             locked: false
        //         }
        //     }
        // });

        // Search.init($(".search-icon"), "vyhledat", "q");
        // Contact_form.init($(".contact-form"));


        // $(window).on("scroll", function () {
        //     if ($(this).scrollTop() > 0) {
        //         if (!$("header#top").hasClass("scrolled")) {
        //             Search.reset();
        //             $("header#top").addClass("scrolled");
        //         }
        //     }
        //     else {
        //         $("header#top").removeClass("scrolled");
        //         Search.reset();
        //     }
        // });
    }

};

var Contact_form = {
    form: null,
    siteKey: "6LecTRQTAAAAAMdGjzIGoNwxPIZ2FonKx6J55rO-",
    secretKey: "6LecTRQTAAAAAC_2oTwojKwqMO3-YBBlmIjszfQO",
    site: "https://www.google.com/recaptcha/api/siteverify",

    init: function (element) {
        this.form = element;
        element.on("submit", Contact_form.submit_react);
    },

    submit_react: function (e) {
        if (grecaptcha.getResponse() == "") {
            $("#captcha-modal").foundation("open");
            e.preventDefault();
        }
    }

}

var Search = {
    searchIcon: null,
    address: "",
    name: "q",

    init: function (searchIcon, whereAddress, name) {
        this.searchIcon = searchIcon;
        this.address = whereAddress;
        this.name = name;

        this.initSearchIcon();
    },

    initSearchIcon: function () {
        this.searchIcon.click(Search.clickListenerPositiveReaction);
    },

    reset: function () {

        Search.searchIcon.each(function () {
            if ($(this).hasClass("opened")) {
                var item = $(this);
                var form = item.find('form');

                item.removeClass("opened");
                item.off("click");

                form.hide("fast", function () {
                    item.click(Search.clickListenerPositiveReaction);
                });
            }
        });
    },

    clickListenerPositiveReaction: function (event) {
        event.preventDefault();
        var item = $(this);

        item.addClass("opened");
        item.off("click");

        var form = item.find('form');

        if (form.length <= 0) {
            var form = $(Search.makeForm());
            form.hide();
            item.append(form);
        }

        form.show("medium", function () {
            item.click(Search.clickListenerNegativeReaction);
        });
    },

    clickListenerNegativeReaction: function (event) {
        if (event.target.nodeName == "INPUT" || event.target.nodeName == "BUTTON" || event.target.nodeName == "FORM") {
            return;
        }

        event.preventDefault();
        var item = $(this);

        item.removeClass("opened");
        item.off("click");
        var form = $(this).find('form');
        form.hide("medium", function () {
            item.click(Search.clickListenerPositiveReaction);
        });
    },

    makeForm: function () {
        return "" +
            "<form action='" + Search.address + "' method='get' class='search-form'>" +
            "<input type='search' placeholder='Dotaz' name='" + Search.name + "'>" +
            "<button type='submit' class='expanded success button'>Vyhledat</button>" +
            "</form>"
            ;
    }
}