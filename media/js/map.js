var el = document.querySelector('#map');
var map;

function enableScrollingWithMouseWheel() {
    var el = $('#map');
    if (el.length > 0) {
        map.setOptions({
            scrollwheel: true
        });
    }
}

function disableScrollingWithMouseWheel() {
    var el = $('#map');
    if (el.length > 0 && map.length>0) {
        map.setOptions({
            scrollwheel: false
        });
    }
}

function initMap() {
    var el = $('#map');
     var myLatLng = window.address;
    map = new google.maps.Map(el[0], {
        zoom: 15,
        center: myLatLng,
        scrollwheel: false, // disableScrollingWithMouseWheel as default
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    if (el.length > 0) {
        google.maps.event.addListener(map, 'mousedown', function() {
            enableScrollingWithMouseWheel()
        });
    }
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Moravský rybářský svaz, z.s., PS Zlín'
      });
}
// if (el.length > 0) {
//     google.maps.event.addDomListener(window, 'load', init);
// }
$(document).ready(function() {
    $('body').on('mousedown', function(event) {
        var clickedInsideMap = $(event.target).parents('#map').length > 0;
        if (!clickedInsideMap) {
            disableScrollingWithMouseWheel();
        }
    });
    $(window).scroll(function() {
        disableScrollingWithMouseWheel();
    });
});