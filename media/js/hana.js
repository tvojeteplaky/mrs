var Hana = {

    /**
     * Translate text through ajax and then call callback function
     * @param text
     * @param callback
     */
    translate: function(text, callback)
    {
        $.get(
            'external/i18n',
            {
                translate: text
            },
            function(data) {
                if (!Hana.is_undefined(callback) && Hana.is_function(callback)) {
                    callback(data);
                }
            },
            'json'
        );
    },

    /**
     * Checks if item is undefined
     * @param item
     * @returns {boolean}
     */
    is_undefined: function(item)
    {
        return (typeof item === 'undefined');
    },

    /**
     * Checks if item is a function
     * @param func
     * @returns {boolean}
     */
    is_function: function(func)
    {
        var getType = {};
        return func && getType.toString.call(func) === '[object Function]';
    },

    /**
     * Checks if item has a link to a page
     * @param item
     * @returns {boolean}
     */
    has_link: function(item)
    {
        return !this.is_undefined(item) && !this.is_undefined(item.attr('href')) && (item.attr('href').length > 0) && (item.attr('href') != '#');
    }
};