INSERT INTO `admin_structure` (`id`, `module_code`, `submodule_code`, `module_controller`, `admin_menu_section_id`, `poradi`, `parent_id`, `zobrazit`, `global_access_level`) VALUES
(15, 'product', 'item', 'list', 1, 5, 0, 1, 0),
(16, 'product', 'item', 'list', 2, 2, 15, 1, 0),
(17, 'product', 'category', 'list', 2, 3, 15, 1, 0),
(18, 'product', 'manufacturer', 'list', 2, 5, 15, 1, 0),
(19, 'product', 'order', 'list', 2, 1, 15, 1, 0),
(20, 'product', 'shipping', 'list', 2, 6, 15, 1, 0),
(21, 'product', 'payment', 'list', 2, 7, 15, 1, 0),
(22, 'product', 'price', 'list', 2, 8, 15, 1, 0),
(23, 'product', 'tax', 'list', 2, 9, 15, 1, 0),
(30, 'product', 'shopper', 'list', 2, 4, 15, 1, 0),
(31, 'product', 'orderstates', 'list', 2, 10, 15, 1, 0),
(32, 'product', 'eshopsettings', 'edit/1/1', 2, 11, 15, 1, 0),
(33, 'product', 'voucher', 'list', 2, 12, 15, 0, 0);