﻿Důležité informace a povinnosti prodejce při nabízení a prezentaci finančních služeb Essox!

Z aktuálního znění zákona č.145/2010 Sb. o spotřebitelských úvěrech vyplívají pro prodejce následující povinnosti.

1) Prodejce je povinen ohlásit novou živnost "Poskytování nebo zprostředkování spotřebitelského úvěru", pokud tak již neučinil.
Detaily k ohlášení živnosti poskytne příslušný živnostenský úřad nebo reg.zástupce Essoxu.
Pokud tuto živnost prodejce nemá ohlášenou, tak nemůže nabízet a zprostředkovávat spotřebitelské úvěry.

2) Reklama a reprezentativní příklad financování (ustanovení §4), povinné informace v reklamě
Prodejce je povinen uvádět zákonem definovaný "reprezentativní příklad financování" u každé reklamy nebo nabídky obsahující tzv. náklady úvěru. Nákladem úvěru se rozumí jakkoli vyčíslená (v % nebo Kč) akontace, splátka úvěru, počet splátek, RPSN, úroková sazba, navýšení apod.
Reprezentativní příklad financování je k dispozici v implementačním balíčku ve složce "Texty na web", případně ho na vyžádání poskytne reg.zástupce Essoxu.
Součástí reklamy zprostředkovatele vždy musí být informace o tom, jestli zprostředkovatel vykonává zprostředkovatelskou činnost jen pro Essox nebo i další věřitele.

Důležité! Jakoukoli vlastní změnu v textu podmínek nákupu na splátky Essox nebo ve vlastních či upravených grafických prezentacích finančních produktů Essox je nutné předem konzultovat a schválit s reg.manažerem Essoxu, aby nedošlo k chybné prezentaci a tím i možnému postihu ze strany České obch.inspekce.

Případný dotaz k výše uvedenému nebo konzultaci směřujte na e-mail partner@essox.cz nebo eshopy@essox.cz

Celé znění zákona naleznete zde: http://www.mfcr.cz/cps/rde/xchg/mfcr/xsl/ft_spotr_uver_legCR_57562.html
