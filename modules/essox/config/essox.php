<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

    'BASE_ESSOX_URL'       => 'https://fltest.essox.cz/leshop', // TESTOVACI
    //'BASE_ESSOX_URL'       => 'https://eshop.essox.cz', //OSTRY
    'OM_NUMBER'       => 1201001, // TESTOVACI
    'USERNAME'   => 'apptest', //TESTOVACI
    'CODE'  => 'essox2010' //TESTOVACI
);