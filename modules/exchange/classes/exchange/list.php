<?php

class Exchange_List
{
    private $config;
    private $date;
    private $xml = null;

    function __construct()
    {
        $this->config = Kohana::config('exchange');
        if (!$this->check())
            $this->refresh();
        $this->load();
    }

    /**
     * Return exchange list date
     * @return DateTime
     */
    public function get_date()
    {
        return $this->date;
    }

    /**
     * Gets currency
     * @param string $code
     * @return Exchange_Rate
     * @throws Exception
     */
    public function get_currency($code)
    {
        $code = strtoupper($code);
        $value = null;

        if ($code == $this->config['default'])
            return new Currency($code, 1);

        if (!is_null($this->xml)) {
            $elements = $this->xml->Cube;
            foreach ($elements as $element)
                if ($element['currency'] == $code) {
                    $value = $element['rate'];
                    break;
                }

            if (is_null($value))
                throw new Exception('Rate not found.');
            else
                return new Currency($code, $value);
        }
        else
            throw new Exception('Exchange XML is not loaded.');
    }

    /**
     * Checkes if file is actual or exists
     * @return bool
     */
    private function check()
    {
        $downloaded = null;
        $actual = null;
        try {
            $downloaded = simplexml_load_file($this->config['rate_list']);
            $actual = simplexml_load_file($this->config['url']);
        } catch (Exception $e) {
            return false;
        }
        return ((string) $actual->Cube->Cube['time']) == ((string) $downloaded->Cube->Cube['time']);
    }

    /**
     * Downloads actual rates
     * @return bool
     */
    private function refresh()
    {
        try {
            $content = file_get_contents($this->config['url']);
            file_put_contents($this->config['rate_list'], $content);
            Kohana::$log->add(Kohana::INFO, "New exchange rates were downloaded.");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Loads exchange xml
     */
    private function load()
    {
        $xml = simplexml_load_file($this->config['rate_list']);
        $this->date = new DateTime((string) $xml->Cube->Cube['time']);
        $this->xml = $xml->Cube->Cube;
    }
}