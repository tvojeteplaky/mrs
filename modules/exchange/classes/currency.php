<?php

class Currency
{
    private $rate;
    private $text_code;
    private $code;
    private $name;
    private $country;

    function __construct($text_code, $rate = null)
    {
        $this->text_code = $text_code;
        $this->rate = $rate;
        $this->load();
    }

    public function get_text_code()
    {
        return $this->text_code;
    }

    public function get_code()
    {
        return $this->code;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function get_country()
    {
        return $this->country;
    }

    public function get_rate()
    {
        return $this->rate;
    }

    /**
     * Calculates price from a currency to this
     * @param $from
     * @param $price
     * @return float
     */
    public function calculate_price($from, $price)
    {
        return ((float) $price / (float) $from->get_rate()) * (float) $this->get_rate();
    }

    /**
     * Loads currency's informations
     */
    private function load()
    {
        $config = Kohana::config('exchange');
        $xml = simplexml_load_file($config['currencies']);
        $currency = $xml->xpath("/ISO_4217/CcyTbl/CcyNtry[Ccy = '{$this->text_code}']");
        if (is_null($this->rate))
            $this->load_rate();

        if (!empty($currency)) {
            $this->code = $currency[0]->CcyNbr;
            $this->name = $currency[0]->CcyNm;
            $this->country = $currency[0]->CtryNm;
        } else {
            throw new Exception("Currency not found.");
        }
    }

    /**
     * Loads currency's rate in case you didn't get it from list
     */
    private function load_rate()
    {
        $config = Kohana::config('exchange');
        if ($this->text_code == $config['default']) {
            $this->rate = 1.0;
            return;
        }
        $xml = simplexml_load_file($config['rate_list']);
        $elements = $xml->Cube->Cube->Cube;
        foreach ($elements as $element)
            if ($element['currency'] == $this->text_code) {
                $this->rate = $element['rate'];
                break;
            }
    }
}