<?php

return array(
    'url' => 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml',
    'rate_list' => str_replace('\\', '/',DOCROOT).'modules/exchange/files/exchange.xml',
    'currencies' => str_replace('\\', '/',DOCROOT).'modules/exchange/files/currencies.xml',
    'default' => 'EUR'
);