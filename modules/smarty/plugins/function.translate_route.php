<?php defined('SYSPATH') or die('No direct script access.');


function smarty_function_translate_route($params, &$smarty)
{
    $nazev_seo = $params["route"];
    $route_actual = ORM::factory('route')->where('nazev_seo', '=', (Service_Route::get_actual_nazev_seo() != '') ? Service_Route::get_actual_nazev_seo() : 'index')->find();
    $actual_language_id = $route_actual->language_id;

    $route_cz = ORM::factory('route')->where('nazev_seo', "=", $nazev_seo)->find();
    $page = ORM::factory('page')->where('route_id', "=", $route_cz->id)->find();
    $page_new = ORM::factory('page')->where('page_id', "=", $page->id)->where('language_id', "=", $actual_language_id)->find();
    $route = ORM::factory('route')->where('id', '=', $page_new->route_id)->find();
    return $route->nazev_seo;
}
?>
