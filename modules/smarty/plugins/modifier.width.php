<?php
/**
 * @param string $url
 * @return int
 */
function smarty_modifier_width($url)
{
    $full = DOCROOT .trim($url);
    if (!file_exists($full))
        return false;
    $img = Image::factory($full);
    return $img->width;
}