<?php defined('SYSPATH') or die('No direct script access.');

class Service_Feed_Heureka extends Service_Feed
{

    public static function make_feed($items, $path)
    {
        self::init();

        $template = new View(self::$template_folder."heureka");
        $template->items = $items;
        $template->website_address = self::$default_address;
        $content = $template->render();

        self::save_file($content, $path);
    }

}