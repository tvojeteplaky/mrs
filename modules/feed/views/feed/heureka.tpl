<?xml version="1.0" encoding="utf-8"?>
<SHOP>
    {foreach from=$items item=item name=feed}
        <SHOPITEM>
            <ITEM_ID>{$item.id}</ITEM_ID>
            <PRODUCT>{$item.nazev|htmlspecialchars}</PRODUCT>
            <DESCRIPTION>{if isset($item.description)}{$item.description|htmlspecialchars}{else}{$item.popis|htmlspecialchars}{/if}</DESCRIPTION>
            <DELIVERY_DATE>{if $item.pocet_na_sklade <= 0}-1{else}4{/if}</DELIVERY_DATE>
            <URL>{$item.full_address}</URL>
            {if $item.youtube_code}
                <VIDEO_URL>{$item.youtube_code}</VIDEO_URL>
            {/if}
            <IMGURL>{$item.full_address_photo}</IMGURL>
            {foreach $item.fotogalerie as $photo}
                <IMGURL_ALTERNATIVE>{$website_address}media/photos/product/item/gallery/images-{$item.id}/{$photo.photo_seo}-t1.jpg</IMGURL_ALTERNATIVE>
            {/foreach}
            <PRICE_VAT>{$item.cena_s_dph}</PRICE_VAT>
            <MANUFACTURER>{$item.vyrobce|htmlspecialchars}</MANUFACTURER>
            <CATEGORYTEXT>
                {foreach $item.categories as $cat name=cat}
                    {if $smarty.foreach.cat.iteration > 1} | {/if}{$cat}
                {/foreach}
            </CATEGORYTEXT>
        </SHOPITEM>
    {/foreach}
</SHOP>