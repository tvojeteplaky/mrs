<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zaky
 * Date: 19.9.13
 * Time: 9:51
 * To change this template use File | Settings | File Templates.
 */

class PayUTest extends Unittest_TestCase  {
    public function test_createPayUFormAfterOrderSend() {
        $payU = new PayU();
        // pozor castka v halerich
        $payInfo = new PayUInfo(100000, "Platba z eshopu noventis.cz", 1, "Jarmil", "Bezouska", "spam@vzak.cz", "123.123.123.123");
        // vygeneruje unikatni session  id, ktere je potreba ulozit do objednavky
        $session_id = $payInfo->getSessionId();
        $head_template = $payU->createHead();
        $form_template = $payU->createForm($payInfo);

        $head_text = $head_template->render();
        $form_text = $form_template->render();
    }

    public function test_checkStatus() {
        $post_params = array();
        $post_params["pos_id"] = PayU::PAYU_POS_ID;
        $post_params["ts"] = time();
        $post_params["session_id"] = "1-".$post_params["ts"];
        $post_params["sig"] = md5($post_params["pos_id"].$post_params["session_id"].$post_params["ts"].PayU::PAYU_KEY2);
        $payU = new PayU();
        $payU->checkStatus($post_params);
    }
}