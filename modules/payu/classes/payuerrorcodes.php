<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zaky
 * Date: 19.9.13
 * Time: 13:48
 * To change this template use File | Settings | File Templates.
 */

class PayUErrorCodes {
    const ERROR_MISSING_POS_ID = 100;
    const ERROR_MISSING_SESSION_ID = 101;
    const ERROR_MISSING_TIMESTAMP = 102;
    const ERROR_MISSING_OR_BAD_SIG = 103;
    const ERROR_MISSING_DESCRIPTION = 104;
    const ERROR_MISSING_CLIENT_IP = 105;
    const ERROR_MISSING_FIRST_NAME = 106;
    const ERROR_MISSING_LAST_NAME = 107;
    const ERROR_MISSING_STREET = 108;
    const ERROR_MISSING_CITY = 109;
    const ERROR_MISSING_POST_CODE = 110;
    const ERROR_MISSING_AMOUNT = 111;
    const ERROR_BAD_BANK_ACCOUNT = 112;
    const ERROR_MISSING_EMAIL = 113;
    const ERROR_MISSING_PHONE = 114;

    const ERROR_TEMPORARILY = 200;
    const ERROR_TEMPORARILY_DB = 201;
    const ERROR_BLOCK_POS_ID = 202;
    const ERROR_INVALID_PAY_TYPE = 203;
    const ERROR_BLOCK_PAY_TYPE = 204;
    const ERROR_MIN_AMOUNT_REACHED = 205;
    const ERROR_MAX_AMOUNT_REACHED = 206;
    const ERROR_MAX_AMOUNT_OF_ALL_CUSTOMER_TRANSACTION_REACHED = 207;
    const ERROR_INVALID_POS_ID_OR_POS_AUTH_KEY = 209;
    const ERROR_INVALID_HELLER_AMOUNT = 210;
    const ERROR_INVALID_TRANSACTION_CURRENCY = 211;

    const ERROR_NON_EXISTING_TRANSACTION = 500;
    const ERROR_MISSING_ATUHORIZATITON_FOR_TRANSACTION = 501;
    const ERROR_TRANSACTION_ALREADY_STARTED = 502;
    const ERROR_TRANSACTION_ALREADY_ATUHORIZED = 503;
    const ERROR_TRANSACTION_ALREADY_CANCELLED = 504;
    const ERROR_TRANSACTION_ALREADY_SENT_TO_ACCEPTATION = 505;
    const ERROR_TRANSACTION_ALREADY_ACCEPTED = 506;
    const ERROR_PAYMENT_RETURN = 507;
    const ERROR_CUSTOMER_REJECT_PAYMENT = 508;
    const ERROR_TRANSACTION_STATUS = 599;
    const ERROR_CRITICAL = 999;

    /**
     * @param int $error_code
     * @throws Kohana_Exception
     * @return string
     */
    public static function getErrorMessage($error_code){
        if (!is_int($error_code)) {
            throw new Kohana_Exception(__("Error code musí být typu int: ").$error_code);
        } else {
            $error_code = (int) $error_code;
        }
        switch($error_code) {
            case PayUErrorCodes::ERROR_MISSING_POS_ID: return __("chybí parametr pos_id");
            case PayUErrorCodes::ERROR_MISSING_SESSION_ID: return __("chybí parametr session_id");
            case PayUErrorCodes::ERROR_MISSING_TIMESTAMP: return __("chybí parametr ts");
            case PayUErrorCodes::ERROR_MISSING_OR_BAD_SIG: return __("chybí parametr sig anebo nesprávná hodnota parametru sig");
            case PayUErrorCodes::ERROR_MISSING_DESCRIPTION: return __("chybí parametr desc");
            case PayUErrorCodes::ERROR_MISSING_CLIENT_IP: return __("chybí parametr client_ip");
            case PayUErrorCodes::ERROR_MISSING_FIRST_NAME: return __("chybí parametr first_name");
            case PayUErrorCodes::ERROR_MISSING_LAST_NAME: return __("chybí parametr last_name");
            case PayUErrorCodes::ERROR_MISSING_STREET: return __("chybí parametr street");
            case PayUErrorCodes::ERROR_MISSING_CITY: return __("chybí parametr city");
            case PayUErrorCodes::ERROR_MISSING_POST_CODE: return __("chybí parametr post_code");
            case PayUErrorCodes::ERROR_MISSING_AMOUNT: return __("chybí parametr amount");
            case PayUErrorCodes::ERROR_BAD_BANK_ACCOUNT: return __("nesprávné číslo bankovního účtu");
            case PayUErrorCodes::ERROR_MISSING_EMAIL: return __("chybí parametr email");
            case PayUErrorCodes::ERROR_MISSING_PHONE: return __("chybí parametr tel. číslo (phone)");
            case PayUErrorCodes::ERROR_TEMPORARILY: return __("jiná přechodná chyba");
            case PayUErrorCodes::ERROR_TEMPORARILY_DB: return __("jiná přechodná chyba databáze");
            case PayUErrorCodes::ERROR_BLOCK_POS_ID: return __("POS tohoto ID je blokován");
            case PayUErrorCodes::ERROR_INVALID_PAY_TYPE: return __("neplatná hodnota pay_type pro dané pos_id");
            case PayUErrorCodes::ERROR_BLOCK_PAY_TYPE: return __("zvolený typ platby (pay_type) je dočasně zablokován pro dané pos_id, např. z důvodu servisní odstávky platební brány");
            case PayUErrorCodes::ERROR_MIN_AMOUNT_REACHED: return __("částka transakce je nižší než minimální hodnota");
            case PayUErrorCodes::ERROR_MAX_AMOUNT_REACHED: return __("částka transakce je vyšší než maximální hodnota");
            case PayUErrorCodes::ERROR_MAX_AMOUNT_OF_ALL_CUSTOMER_TRANSACTION_REACHED: return __("překročena hodnota všech transakcí pro jednoho zákazníka za poslední období");
            case PayUErrorCodes::ERROR_INVALID_POS_ID_OR_POS_AUTH_KEY: return __("neplatný pos_id nebo pos_auth_key");
            case PayUErrorCodes::ERROR_INVALID_HELLER_AMOUNT: return __("částka transakce obsahuje nepovolené haléřové položky");
            case PayUErrorCodes::ERROR_INVALID_TRANSACTION_CURRENCY: return __("chybná měna transakce");
            case PayUErrorCodes::ERROR_NON_EXISTING_TRANSACTION: return __("neexistující transakce");
            case PayUErrorCodes::ERROR_MISSING_ATUHORIZATITON_FOR_TRANSACTION: return __("chybí autorizace pro tuto transakci");
            case PayUErrorCodes::ERROR_TRANSACTION_ALREADY_STARTED: return __("transakce začala dříve");
            case PayUErrorCodes::ERROR_TRANSACTION_ALREADY_ATUHORIZED: return __("autorizace transakce již byla vykonána");
            case PayUErrorCodes::ERROR_TRANSACTION_ALREADY_CANCELLED: return __("transakce byla dříve zrušena");
            case PayUErrorCodes::ERROR_TRANSACTION_ALREADY_SENT_TO_ACCEPTATION: return __("transakce již byla odeslána k přijetí");
            case PayUErrorCodes::ERROR_TRANSACTION_ALREADY_ACCEPTED: return __("transakce byla přijata");
            case PayUErrorCodes::ERROR_PAYMENT_RETURN: return __("chyba při převodu prostředků zpět zákazníkovi");
            case PayUErrorCodes::ERROR_CUSTOMER_REJECT_PAYMENT: return __("zákazník odstoupil od provedení platby");
            case PayUErrorCodes::ERROR_TRANSACTION_STATUS: return __("nesprávný status transakce, např. není možné přijmout transakci několikrát a jiné");
            case PayUErrorCodes::ERROR_CRITICAL: return __("jiná kritická chyba");


        }

        return __("neočekávaný status code: ").$error_code;
    }
}


