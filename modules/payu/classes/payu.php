<?php defined('SYSPATH') or die('No direct script access.');

class PayU {
    const PAYU_POS_ID = '146172';
    const PAYU_KEY1 = '55d623b9cf9c084bd4f0f389a228e11c';
    const PAYU_KEY2 = '38614116aa65990a4ddaaf7c4318965a';
    const PAYU_POS_AUTH_KEY = 'KPhovJu';
    const PAYU_SERVER_BASE_URL = 'secure.payu.com';
    const PAYU_BASE_CALL = '/paygw/UTF/';
    const PAYU_CALL_NEW_PAYMENT = 'NewPayment';
    const PAYU_CALL_PAYMENT_STATUS = 'Payment/get';
    const PAYU_CALL_CONFIRM_PAYMENT = 'Payment/confirm';
    const PAYU_CALL_CANCEL_PAYMENT = 'Payment/cancel';

    public function __construct() {

    }

    /**
     * @return View
     */
    public function createHead() {
        $template=new View("head");
        $this->setDefaultTemplateProperties($template);
        return $template;
    }

    /**
     * @param IPayUInfo $payUInfo
     * @return View
     */
    public function createForm(IPayUInfo $payUInfo) {
        // check data if all required data was specified
        $template=new View("form");
        $this->setDefaultTemplateProperties($template);
        $payUInfo->validate();
        $template->payUInfo = $payUInfo;
        $template->payu_call_new_payment = PayU::PAYU_CALL_NEW_PAYMENT;
        return $template;
    }

    /**
     * @param View $template
     */
    private function setDefaultTemplateProperties(&$template) {
        if($template == null) return;
        $template->payu_pos_id = PayU::PAYU_POS_ID;
        $template->payu_server_base_url = PayU::PAYU_SERVER_BASE_URL;
        $template->payu_base_call = PayU::PAYU_BASE_CALL;
        $template->payu_key1 = PayU::PAYU_KEY1;
        $template->payu_key2 = PayU::PAYU_KEY2;
        $template->payu_pos_auth_key = PayU::PAYU_POS_AUTH_KEY;
    }
    
    /**
     * @param string $session_id
     * @return PayUStatus
     */
    public function checkStatusBySessionId($session_id) {
        $arr["session_id"] = $session_id;
        $arr["pos_id"] = PayU::PAYU_POS_ID;
        return $this->checkStatus($arr);
    }

    /**
     * @param array $post_params
     * @param bool $check_sig
     * @throws Kohana_Exception
     * @return PayUStatus
     */
    public function checkStatus($post_params, $check_sig = false) {
        // nektere parametry chybeji
        // some parameters are missing
        if(!isset($post_params["pos_id"]) || !isset($post_params["session_id"]) || ( !(($check_sig && isset($post_params["ts"]) && isset($post_params["sig"])) || !$check_sig)))
            throw new Kohana_Exception(__("Nekteré parametry pro zpracovaní požadavku chybí: ").Util_Text::getArrayAsKeyValueText($post_params));

        // obdrzene cislo POS ID je jine, nez bylo ocekavano
        // received POS ID is different than expected
        if($post_params["pos_id"] !== PayU::PAYU_POS_ID)
            throw new Kohana_Exception(__("POS_ID se neshoduje s přidělneným POS_ID: ").Util_Text::getArrayAsKeyValueText($post_params));

        if($check_sig) {
            // verifikace obdrzeneho podpisu
            // verification of received signature
            $sig = md5($post_params["pos_id"].$post_params["session_id"].$post_params["ts"].PayU::PAYU_KEY2);

            // chybny podpis
            // incorrect signature
            if($post_params["sig"] !== $sig)
                throw new Kohana_Exception(__("Chybný podpis požadavku: ").Util_Text::getArrayAsKeyValueText($post_params));
        }

        // podpis, ktery bude odeslan do PayU spolu s pozadavkem
        // signature that will be sent to PayU with request
        $ts = time();
        $request_sig = md5(PayU::PAYU_POS_ID.$post_params["session_id"].$ts.PayU::PAYU_KEY1);

        // priprava retezce (string) parametru k odeslani do PayU
        // preparing parameters string to be sent to PayU
        $parameters = "pos_id=".PayU::PAYU_POS_ID."&session_id=".$post_params["session_id"]."&ts=".$ts."&sig=".$request_sig;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://".PayU::PAYU_SERVER_BASE_URL.PayU::PAYU_BASE_CALL.PayU::PAYU_CALL_PAYMENT_STATUS);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $payu_response = curl_exec($ch);
        curl_close($ch);

        $payUStatus = new PayUStatus();
        if(preg_match("/<response>\s*<status>([a-zA-Z]*)<\/status>.*<\/response>/is", $payu_response, $status)) {

            if ($status[1] === "OK" && preg_match("/<trans>.*<pos_id>([0-9]*)<\/pos_id>.*<session_id>(.*)<\/session_id>.*<order_id>(.*)<\/order_id>.*".
            "<amount>([0-9]*)<\/amount>.*<status>([0-9]*)<\/status>.*<desc>(.*)<\/desc>.*<ts>([0-9]*)<\/ts>.*<sig>([a-z0-9]*)".
            "<\/sig>.*<\/trans>/is", $payu_response, $parts)) {
                $payUStatus->setPosId($parts[1]);
                $payUStatus->setSessionId($parts[2]);
                $payUStatus->setOrderId($parts[3]);
                $payUStatus->setAmountHeller($parts[4]);
                $payUStatus->setStatusCode((int)$parts[5]);
                $payUStatus->setDesc($parts[6]);
                $payUStatus->setTs($parts[7]);
                $payUStatus->setSig($parts[8]);
                return $payUStatus;
            } else if ($status[1] === "ERROR" && preg_match("/<error>.*<nr>([0-9]*)<\/nr>.*<message>(.*)<\/message>.*/is", $payu_response, $error_parts)) {
                $payUStatus->setStatusCode(intval($error_parts[1]));
                return $payUStatus;
            }
        }
        $payUStatus->setStatusCode(PayUStatus::STATUS_UNKNOWN_ERROR, $payu_response);
        return $payUStatus;
    }

}