<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zaky
 * Date: 18.9.13
 * Time: 22:12
 * To change this template use File | Settings | File Templates.
 */

class PayUStatus {
    // „nová“ se objeví ve chvíli, kdy aplikace Obchodu úspěšně vyvolá proceduru NewPayment.
    const STATUS_NEW = 1;
    // “zrušena” se objeví automaticky po určitém počtu dnů (10-14 dni) od vytvoření nebo
    // zahájení transakce (status 1 nebo 4), nedojde-li v tomto termínu k uhrazení platby (prostředky nejsou
    // převedeny na účet PayU).
    // „zrušena“ se objeví také v případě, kdy je transakce se statusem 1 - „nová“ nebo 5 - „pro
    // přijetí“ zrušena aplikací obchodu nebo uživatelem.
    const STATUS_CANCELLED = 2;
    // ”odmítnuta” se objeví v případě, že je “zrušená” transakce (status 2) dodatečně uhrazena
    // (prostředky jsou převedeny na účet PayU).
    // ”odmítnuta” se objeví také v případě, když je transakce se statusem 5 - ”pro přijetí” zrušena
    // a vybraná platební metoda neumožňuje automatické vrácení prostředků Zákazníkovi. Pokud je trans-
    // akce se statusem 3 přijata a automatické přijímání plateb je vypnuto, získává transakce status 5 – „pro
    // přijetí”. Pro dokončení transakce a změnu jejího statusu na 99 – „skončena” je nutné transakci ještě
    // jednou přijmout.
    const STATUS_REJECTED = 3;
    // „zahájena“ je přechodný stav a nemusí se objevit. Transakce může změnit status na „pro
    // přijetí” nebo „skončena” (v případě, že je automatické přijímání plateb zapnuto) přímo ze statusu 1 -
    // „nová”.
    const STATUS_STARTED = 4;
    // „pro přijetí” se objeví pouze tehdy, je-li možnost automatického přijímání plateb vypnuta.
    // Obchod by měl přijmout platbu do tolika dnů (přesněji do uplynutí tolikrát 24 hodin), kolik trvá auto-
    // matické zrušení transakce. Není-li platba přijata do této doby, je automaticky zrušena.
    const STATUS_AWAITING_RECEIPT = 5;
    const STATUS_NO_AUTHORIZATION = 6;
    // „vrácena” se objeví, pokud je transakce se statusem 3 zrušena.
    const STATUS_PAYMENT_REJECTED = 7;
    // „skončena“ označuje úspěšně skončenou transakci. Jde o konečný, neměnný status trans-
    // akce. V okamžiku, kdy je transakci přidělen status 99, může Obchod informovat Zákazníka o tom, že je
    // jeho platba uhrazena (doporučujeme).
    const STATUS_PAYMENT_RECEIVED = 99;
    const STATUS_INCORRECT = 888;
    const STATUS_UNKNOWN_ERROR = 0;

    /** @var int */
    private $status_code;
    /** @var string */
    private $pos_id;
    /** @var string */
    private $session_id;
    /** @var  string */
    private $order_id;
    /** @var  int,  v haléřích */
    private $amount_heller;
    /** @var  string */
    private $desc;
    /** @var  string timestamp */
    private $ts;
    /** @var  string */
    private $sig;
    /** @var  string */
    private $error_message;
    /** @var  string */
    private $status_message;



    public function getStatusMessage(){
        return $this->status_message;
    }

    private function setMessage() {
        $this->status_message = null;
        $this->error_message = null;
        switch(intval($this->status_code)) {
            case PayUStatus::STATUS_NEW:
                $this->status_message = __("nová transakce");
                return;
            case PayUStatus::STATUS_CANCELLED:
                $this->status_message = __("zrušená transakce");
                return;
            case PayUStatus::STATUS_REJECTED:
                $this->status_message = __("odmítnutá transakce");
                return;
            case PayUStatus::STATUS_STARTED:
                $this->status_message = __("zahájena transakce");
                return;
            case PayUStatus::STATUS_AWAITING_RECEIPT:
                $this->status_message = __("platba čeká na přijetí");
                return;
            case PayUStatus::STATUS_NO_AUTHORIZATION:
                $this->status_message = __("neauthorizováno");
                return;
            case PayUStatus::STATUS_PAYMENT_REJECTED:
                $this->status_message = __("platba vrácena");
                return;
            case PayUStatus::STATUS_PAYMENT_RECEIVED:
                $this->status_message = __("platba přijata");
                return;
            case PayUStatus::STATUS_INCORRECT:
                $this->error_message = __("špatný stav platby, zkontrolujte platbu v administraci PayU");
                return;
            case PayUStatus::STATUS_UNKNOWN_ERROR:
                $this->error_message = __("nepodařilo se zjistit stav platby");
                return;
        }

        $this->error_message = PayUErrorCodes::getErrorMessage($this->status_code);
    }

    /**
     * @param int $amount_heller
     */
    public function setAmountHeller($amount_heller)
    {
        $this->amount_heller = $amount_heller;
    }

    /**
     * @return int
     */
    public function getAmountHeller()
    {
        return $this->amount_heller;
    }

    /**
     * @param string $desc
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;
    }

    /**
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param string $order_id
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param string $pos_id
     */
    public function setPosId($pos_id)
    {
        $this->pos_id = $pos_id;
    }

    /**
     * @return string
     */
    public function getPosId()
    {
        return $this->pos_id;
    }

    /**
     * @param string $session_id
     */
    public function setSessionId($session_id)
    {
        $this->session_id = $session_id;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->session_id;
    }

    /**
     * @param string $sig
     */
    public function setSig($sig)
    {
        $this->sig = $sig;
    }

    /**
     * @return string
     */
    public function getSig()
    {
        return $this->sig;
    }

    /**
     * @param int $status_code
     * @param string $error_message_add
     */
    public function setStatusCode($status_code, $error_message_add = null)
    {
        $this->status_code = $status_code;
        $this->setMessage();
        if($this->error_message == null) $this->error_message = "";
        if($error_message_add != null) $this->error_message = $this->error_message."\n".$error_message_add;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @param string $ts
     */
    public function setTs($ts)
    {
        $this->ts = $ts;
    }

    /**
     * @return string
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }


}