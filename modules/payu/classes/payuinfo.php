<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zaky
 * Date: 19.9.13
 * Time: 8:14
 * To change this template use File | Settings | File Templates.
 */

class PayUInfo implements IPayUInfo {
    /** @var  string */
    private $session_id;
    /** @var  int */
    private $amount_heller;
    /** @var  string */
    private $desc;
    /** @var  string */
    private $desc2;
    /** @var  string */
    private $order_id;
    /** @var  string */
    private $first_name;
    /** @var  string */
    private $last_name;
    /** @var  string */
    private $street;
    /** @var  string */
    private $street_number;
    /** @var  string */
    private $city;
    /** @var  string */
    private $postal_code;
    /** @var  string */
    private $country;
    /** @var  string */
    private $email;
    /** @var  string */
    private $phone;
    /** @var string */
    private $language;
    /** @var string */
    private $ip_address;
    /** @var string */
    private $sig;
    /** @var int */
    private $ts;

    /**
     * @param int $amount_heller povinné, částka v haléřích
     * @param string $desc povinné, krátký popis – objevuje se Obchodu ve výpisech transakcí
     * @param string $order_id povinné číslo objednávky, je potžeba pro tvorbu session_id
     * @param string $first_name povinné, jménu uživatele
     * @param string $last_name povinné, příjmení uživatele
     * @param string $email povinné, email uživatele
     * @param string $ip_address povinné, ip adresa uživatele
     */
    function __construct($amount_heller, $desc, $order_id, $first_name, $last_name, $email, $ip_address)
    {
        $this->amount_heller = $amount_heller;
        $this->desc = $desc;
        $this->order_id = $order_id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->ip_address = $ip_address;
    }


    /** @return int povinné, částka v haléřích */
    public function getAmountHeller()
    {
        return $this->amount_heller;
    }

    /**
     * @param int $amount_heller
     */
    public function setAmountHeller($amount_heller)
    {
        $this->amount_heller = $amount_heller;
    }

    /** @return string povinné, krátký popis – objevuje se Obchodu ve výpisech transakcí */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @param string $desc
     */
    public function setDesc($desc)
    {
        $this->desc = $desc;
    }

    /** @return string nepovinné libovolný upřesňující popis */
    public function getDesc2()
    {
        return $this->desc2;
    }

    /**
     * @param string $desc2
     */
    public function setDesc2($desc2)
    {
        $this->desc2 = $desc2;
    }

    /** @return string povinné číslo objednávky, je potžeba pro tvorbu session_id */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param string $order_id
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
    }

    /** @return string povinné jméno */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /** @return string povinné příjmení */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /** @return string neovinné ulice */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /** @return string nepovinné číslo popisné */
    public function getStreetNumber()
    {
        return $this->street_number;
    }

    /**
     * @param string $street_number
     */
    public function setStreetNumber($street_number)
    {
        $this->street_number = $street_number;
    }

    /** @return string nepovinné obec */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /** @return string nepovinné PSČ */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param string $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /** @return string nepovinné země - CZ */
    public function getCountry()
    {
        return  empty($this->country) ? $this->country = "CZ" : $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /** @return string povinné email */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /** @return string nepovinné telefon */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /** @return string nepovinné jazyk aktuálně je možné uvádět buďto kód "cs" anebo "en" */
    public function getLanguage()
    {
        return empty($this->language) ? $this->language = "cs" : $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /** @return string povinné, ip adresa klienta D{1,3}.D{1,3}.D{1,3}.D{1,3} */
    public function getIpAddress()
    {
        return empty($this->ip_address) ? $this->ip_address = Request::$client_ip : $this->ip_address;
    }

    /**
     * @param string $ip_address
     */
    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
    }

    /**
     * @param string $pos_id
     * @param string $pos_auth_key
     * @param string $key1
     * @return string povinné, kontrolní součet parametrů odesílaných ve formuláři
     */
    public function getSig($pos_id, $pos_auth_key, $key1)
    {
        $result = $pos_id . $this->getSessionId() . $pos_auth_key;
        if(($this->getAmountHeller() != null) && $this->getAmountHeller() > 0) $result .= $this->getAmountHeller();
        if(strlen($this->getDesc()) > 0) $result .= $this->getDesc();
        if(strlen($this->getDesc2()) > 0) $result .= $this->getDesc2();
        if(strlen($this->getOrderId()) > 0) $result .= $this->getOrderId();
        if(strlen($this->getFirstName()) > 0) $result .= $this->getFirstName();
        if(strlen($this->getLastName()) > 0) $result .= $this->getLastName();
        if(strlen($this->getStreet()) > 0) $result .= $this->getStreet();
        if(strlen($this->getStreetNumber()) > 0) $result .= $this->getStreetNumber();
        if(strlen($this->getCity()) > 0) $result .= $this->getCity();
        if(strlen($this->getPostalCode()) > 0) $result .= $this->getPostalCode();
        if(strlen($this->getCountry()) > 0) $result .= $this->getCountry();
        if(strlen($this->getEmail()) > 0) $result .= $this->getEmail();
        if(strlen($this->getPhone()) > 0) $result .= $this->getPhone();
        if(strlen($this->getLanguage()) > 0) $result .= $this->getLanguage();
        if(strlen($this->getIpAddress()) > 0) $result .= $this->getIpAddress();
        $result .= $this->getTs() . $key1;

        // md5(pos_id + session_id + values ... + ts + key1)
        return md5($result);
    }

    /** @return string povinné, časová známka použitá na výpočet hodnoty parametru sig */
    public function getTs()
    {
        if (empty($this->ts)) $this->ts=time();
        return $this->ts;
    }

    /**
     * @throws Exception_Hana_Base pokud neni specifikovana order id
     * @return string povinné, unikátní session id pro každou platbu, i jejího opakování
     */
    public function getSessionId()
    {
        if(empty($this->session_id)) {
            if(empty($this->order_id)) {
                throw new Exception_Hana_Base(__("Pro vygenerování session id nejdříve nadefinujte číslo objednávky."));
            } else {
                $this->session_id = $this->order_id . "-" . $this->getTs();
            }
        }
        return $this->session_id;
    }

    /**
     * @throws Exception pokud nejsou vyplněné všechny povinné položky
     * @return void
     */
    public function validate()
    {
        $result = array();
        if(($this->getAmountHeller() == null) || $this->getAmountHeller() < 0) $result[] = "amount";
        if(strlen($this->getDesc()) == 0) $result[] = "desc";
        if(strlen($this->getOrderId()) == 0) $result[] = "order_id";
        if(strlen($this->getFirstName()) == 0) $result[] = "first_name";
        if(strlen($this->getLastName()) == 0) $result[] = "last_name";
        if(strlen($this->getEmail()) == 0) $result[] = "email";
        if(strlen($this->getIpAddress()) == 0) $result[] = "client_ip";
        if(sizeof($result) > 0) {
            throw new Exception_Hana_Base(__("Chybí tyto povinné položky pro vytvoření platby: ") . implode(", ", $result));
        }
    }
}