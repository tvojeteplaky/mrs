<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zaky
 * Date: 18.9.13
 * Time: 21:06
 * To change this template use File | Settings | File Templates.
 */

interface IPayUInfo {
    /** @return int povinné, částka v haléřích */
    public function getAmountHeller();
    /** @return string povinné, krátký popis – objevuje se Obchodu ve výpisech transakcí */
    public function getDesc();
    /** @return string nepovinné libovolný upřesňující popis */
    public function getDesc2();
    /** @return string nepovinné číslo objednávky */
    public function getOrderId();
    /** @return string povinné jméno */
    public function getFirstName();
    /** @return string povinné příjmení */
    public function getLastName();
    /** @return string neovinné ulice */
    public function getStreet();
    /** @return string nepovinné číslo popisné */
    public function getStreetNumber();
    /** @return string nepovinné obec */
    public function getCity();
    /** @return string nepovinné PSČ */
    public function getPostalCode();
    /** @return string nepovinné země - CZ */
    public function getCountry();
    /** @return string povinné email */
    public function getEmail();
    /** @return string nepovinné telefon */
    public function getPhone();
    /** @return string nepovinné jazyk aktuálně je možné uvádět buďto kód "cs" anebo "en" */
    public function getLanguage();
    /** @return string povinné, ip adresa klienta D{1,3}.D{1,3}.D{1,3}.D{1,3} */
    public function getIpAddress();

    /**
     * @param string $pos_id
     * @param $pos_auth_key
     * @param string $key1
     * @return string povinné, kontrolní součet parametrů odesílaných ve formuláři
     */
    public function getSig($pos_id, $pos_auth_key, $key1);
    /** @return int povinné, časová známka použitá na výpočet hodnoty parametru sig */
    public function getTs();
    /**
     * @return string povinné, unikátní session id pro každou platbu, i jejího opakování
     */
    public function getSessionId();

    /**
     * @throws Exception pokud nejsou vyplněné všechny povinné položky
     * @return void
     */
    public function validate();
}