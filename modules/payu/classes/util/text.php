<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zaky
 * Date: 19.9.13
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */

class Util_Text {
    /**
     * @param array $array
     * @param string $keyValueSeparator
     * @param string $newLineSeparator
     * @return string
     */
    public static function getArrayAsKeyValueText($array, $keyValueSeparator = " => ", $newLineSeparator = "\n") {
        $new_arr = array();
        if(is_array($array)) {
            foreach($array as $key => $value) {
                $new_arr[] = $key.$keyValueSeparator.$value;
            }
        }

        return implode($newLineSeparator, $new_arr);
    }
}