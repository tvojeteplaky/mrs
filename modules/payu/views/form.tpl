<form action="https://{$payu_server_base_url}{$payu_base_call}{$payu_call_new_payment}" method="POST" name="payform">
  <input type="hidden" name="pos_id" value="{$payu_pos_id}">
  <input type="hidden" name="pos_auth_key" value="{$payu_pos_auth_key}">
  <input type="hidden" name="session_id" value="{$payUInfo->getSessionId()}">
  <input type="hidden" name="amount" value="{$payUInfo->getAmountHeller()}">
  <input type="hidden" name="desc" value="{$payUInfo->getDesc()}">
  <input type="hidden" name="order_id" value="{$payUInfo->getOrderId()}">
  {if strlen($payUInfo->getDesc2())}<input type="hidden" name="desc2" value="{$payUInfo->getDesc2()}">{/if}
  <input type="hidden" name="first_name" value="{$payUInfo->getFirstName()}">
  <input type="hidden" name="last_name" value="{$payUInfo->getLastName()}">
  {if strlen($payUInfo->getStreet())}<input type="hidden" name="street" value="{$payUInfo->getStreet()}">{/if}
  {if strlen($payUInfo->getStreetNumber())}<input type="hidden" name="street_hn" value="{$payUInfo->getStreetNumber()}">{/if}
  {if strlen($payUInfo->getCity())}<input type="hidden" name="city" value="{$payUInfo->getCity()}">{/if}
  {if strlen($payUInfo->getPostalCode())}<input type="hidden" name="post_code" value="{$payUInfo->getPostalCode()}">{/if}
  {if strlen($payUInfo->getCountry())}<input type="hidden" name="country" value="{$payUInfo->getCountry()}">{/if}
  <input type="hidden" name="email" value="{$payUInfo->getEmail()}">
  {if strlen($payUInfo->getPhone())}<input type="hidden" name="phone" value="{$payUInfo->getPhone()}">{/if}
  {if strlen($payUInfo->getLanguage())}<input type="hidden" name="language" value="{$payUInfo->getLanguage()}">{/if}
  <input type="hidden" name="client_ip" value="{$payUInfo->getIpAddress()}">
  <input type="hidden" name="sig" value="{$payUInfo->getSig($payu_pos_id, $payu_pos_auth_key, $payu_key1)}">
  <input type="hidden" name="ts" value="{$payUInfo->getTs()}">

  <script language='javascript' type='text/javascript'>
    PlnPrintTemplate();
  </script>
  <input type="submit" value="Pay with PayU.cz">
</form>