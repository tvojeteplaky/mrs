<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 14.9.14
 * Time: 13:21
 */

class Gpwebpay_Order_State {
    const CODE_REQUESTED = 1;
    const CODE_PENDING = 2;
    const CODE_CREATED = 3;
    const CODE_CANCELLED = 20;
    const CODE_APPROVED = 4;
    const CODE_APPROVE_REVERSED = 5;
    const CODE_UNAPPROVED = 6;
    const CODE_DEPOSITED_BATCH_OPENED = 7;
    const CODE_DEPOSITED_BATCH_CLOSED = 8;
    const CODE_ORDER_CLOSED = 9;
    const CODE_DELETED = 10;
    const CODE_CREDITED_BATCH_OPENED = 11;
    const CODE_CREDITED_BATCH_CLOSED = 12;
    const CODE_DECLINED = 13;

    /**
     * @param int $code
     * @throws Kohana_Exception
     * @return string
     */
    public static function getMessage($code){
        if (!is_int($code)) {
            throw new Kohana_Exception(__("Code musí být typu int: ").$code);
        } else {
            $code = (int) $code;
        }
        switch($code) {
            case Gpwebpay_Order_State::CODE_REQUESTED: return __("Objednávka byla úspěšně přijata do GP webpay – čeká se na výsledek vyplnění citlivých informací držitele karty.Pokud držitel karty vyplnil citlivé informace, je zaslán dotaz do 3D systému, zda je požadována autentikace držitele karty. Čeká se na výsledek z 3D systému. Jestliže držitel karty přeruší zadávání údajů karty, je to finální stav objednávky.");
            case Gpwebpay_Order_State::CODE_PENDING: return __("Objednávka byla úspěšně přijata do GP webpay – čeká se na výsledek vyplnění citlivých informací držitele karty.Pokud držitel karty vyplnil citlivé informace, je zaslán dotaz do 3D systému, zda je požadována autentikace držitele karty. Čeká se na výsledek z 3D systému. Jestliže držitel karty přeruší zadávání údajů karty, je to finální stav objednávky.");
            case Gpwebpay_Order_State::CODE_CREATED: return __("Objednávka byla úspěšně přijata do GP webpay – čeká se na výsledek vyplnění citlivých informací držitele karty.Pokud držitel karty vyplnil citlivé informace, je zaslán dotaz do 3D systému, zda je požadována autentikace držitele karty. Čeká se na výsledek z 3D systému. Jestliže držitel karty přeruší zadávání údajů karty, je to finální stav objednávky.");
            case Gpwebpay_Order_State::CODE_CANCELLED: return __("Držitel karty zrušil platbu (na platební bráne zvolil možnost \"Zpět do e-shopu\")");
            case Gpwebpay_Order_State::CODE_APPROVED: return __("Výsledek autentikace držitele karty v 3D systému byl úspěšný, byl poslán požadavek na autorizaci do autorizačního centra. Výsledek autorizace objednávky je úspěšný.");
            case Gpwebpay_Order_State::CODE_APPROVE_REVERSED: return __("Autorizace objednávky byla zneplatněna. Na straně držitele karty došlo k odblokování původně autorizovaných finančních prostředků.");
            case Gpwebpay_Order_State::CODE_UNAPPROVED: return __("Výsledek autorizace objednávky je neúspěšný, objednávku není možné uhradit. Není možné pokračovat.");
            case Gpwebpay_Order_State::CODE_DEPOSITED_BATCH_OPENED: return __("Objednávka byla označena pro uhrazení během následného dávkového zpracování. Je možné zneplatnit úhradu objednávky do okamžiku, než proběhne uzavření dávky, ve které se daná úhrada nachází.");
            case Gpwebpay_Order_State::CODE_DEPOSITED_BATCH_CLOSED: return __("Proběhl automatický proces uzavírání dávek a přenos dat do finančních systémů.");
            case Gpwebpay_Order_State::CODE_ORDER_CLOSED: return __("Objednávka byla uzavřena. Jediná přípustná operace je vymazání.");
            case Gpwebpay_Order_State::CODE_DELETED: return __("Objednávka byla odstraněna.");
            case Gpwebpay_Order_State::CODE_CREDITED_BATCH_OPENED: return __("Objednávka byla označena pro návrat během následného dávkového zpracování. Je možné zneplatnit návrat objednávky do okamžiku, než proběhne uzavření dávky, ve které se daná objednávka nachází. Po uzavření dávky zůstává v tomto stavu. Pro objednávku je možné vytvořit více kreditů.");
            case Gpwebpay_Order_State::CODE_CREDITED_BATCH_CLOSED: return __("Objednávka byla označena pro návrat během následného dávkového zpracování. Je možné zneplatnit návrat objednávky do okamžiku, než proběhne uzavření dávky, ve které se daná objednávka nachází. Po uzavření dávky zůstává v tomto stavu. Pro objednávku je možné vytvořit více kreditů.");
            case Gpwebpay_Order_State::CODE_DECLINED: return __("Výsledek autentikace držitele karty v 3D systému je neúspěšný. Držitel karty není autentikován – není možné pokračovat. Objednávku je možné odstranit.");
        }

        return __("neočekávaný status code: ").$code;
    }

    /**
     * @param int $code
     * @throws Kohana_Exception
     * @return string
     */
    public static function getShortMessage($code){
        if (!is_int($code)) {
            throw new Kohana_Exception(__("Code musí být typu int: ").$code);
        } else {
            $code = (int) $code;
        }
        switch($code) {
            case Gpwebpay_Order_State::CODE_REQUESTED: return __("neukončena");
            case Gpwebpay_Order_State::CODE_PENDING: return __("neukončena");
            case Gpwebpay_Order_State::CODE_CREATED: return __("neukončena");
            case Gpwebpay_Order_State::CODE_CANCELLED: return __("zrušena");
            case Gpwebpay_Order_State::CODE_APPROVED: return __("autorizována");
            case Gpwebpay_Order_State::CODE_APPROVE_REVERSED: return __("reverzována");
            case Gpwebpay_Order_State::CODE_UNAPPROVED: return __("neautorizována");
            case Gpwebpay_Order_State::CODE_DEPOSITED_BATCH_OPENED: return __("uhrazena");
            case Gpwebpay_Order_State::CODE_DEPOSITED_BATCH_CLOSED: return __("zpracována");
            case Gpwebpay_Order_State::CODE_ORDER_CLOSED: return __("uzavřena");
            case Gpwebpay_Order_State::CODE_DELETED: return __("vymazána");
            case Gpwebpay_Order_State::CODE_CREDITED_BATCH_OPENED: return __("kreditována");
            case Gpwebpay_Order_State::CODE_CREDITED_BATCH_CLOSED: return __("kreditována");
            case Gpwebpay_Order_State::CODE_DECLINED: return __("zamítnuta");
        }

        return __("neočekávaný status code: ").$code;
    }
}