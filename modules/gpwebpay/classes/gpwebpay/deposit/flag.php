<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 19.9.2014
 * Time: 11:31
 */

class Gpwebpay_Deposit_Flag {
    /** only block amount, then administrator manually accept charge of blocked amount, f.e. we charge amount after items successfully dispatched */
    /** only block amount, then administrator manually accept charge of blocked amount, f.e. we charge amount after items successfully dispatched */
    const ONLY_BLOCK_AMOUNT = 0;
    /** amount is immediately charged from customer account */
    const IMMEDIATE_PAYMENT= 1;
} 