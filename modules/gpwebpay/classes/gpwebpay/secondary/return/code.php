<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 14.9.14
 * Time: 13:22
 */

class Gpwebpay_Secondary_Return_Code {
    const CODE_OK = 0;
    const CODE_ORDERNUMBER = 1;
    const CODE_MERCHANTNUMBER = 2;
    const CODE_AMOUNT = 6;
    const CODE_CURRENCY = 7;
    const CODE_DEPOSITFLAG = 8;
    const CODE_MERORDERNUM = 10;
    const CODE_CREDITNUMBER = 11;
    const CODE_OPERATION = 12;
    const CODE_BATCH = 18;
    const CODE_ORDER = 22;
    const CODE_URL = 24;
    const CODE_MD = 25;
    const CODE_DESC = 26;
    const CODE_DIGEST = 34;
    const CODE_CARDHOLDER_NOT_AUTHENTICATED = 3000;
    const CODE_AUTHENTICATED = 3001;
    const CODE_ISSUER_CARDHOLDER_NOT_3D = 3002;
    const CODE_ISSUER_CARDHOLDER_NOT_ENROLLED = 3004;
    const CODE_TECHNICAL_PROBLEM_DURING_AUTHENTICATION_3005 = 3005;
    const CODE_TECHNICAL_PROBLEM_DURING_AUTHENTICATION_3006 = 3006;
    const CODE_ACQUIRER_TECHNICAL_PROBLEM = 3007;
    const CODE_UNSUPPORTED_CARD_PRODUCT = 3008;
    const CODE_BLOCKED_CARD = 1001;
    const CODE_AUTHORIZATION_DECLINED = 1002;
    const CODE_CARD_PROBLEM = 1003;
    const CODE_TECHNICAL_PROBLEM_AUTHORIZATION_PROCESS = 1004;
    const CODE_ACCOUNT_PROBLEM = 1005;

    /**
     * @param int $code
     * @throws Kohana_Exception
     * @return string
     */
    public static function getMessage($code){
        if (!is_int($code)) {
            throw new Kohana_Exception(__("Code musí být typu int: ").$code);
        } else {
            $code = (int) $code;
        }
        switch($code) {
            case Gpwebpay_Secondary_Return_Code::CODE_OK: return __("OK");
            case Gpwebpay_Secondary_Return_Code::CODE_ORDERNUMBER: return __("ORDERNUMBER");
            case Gpwebpay_Secondary_Return_Code::CODE_MERCHANTNUMBER: return __("MERCHANTNUMBER");
            case Gpwebpay_Secondary_Return_Code::CODE_AMOUNT: return __("AMOUNT");
            case Gpwebpay_Secondary_Return_Code::CODE_CURRENCY: return __("CURRENCY");
            case Gpwebpay_Secondary_Return_Code::CODE_DEPOSITFLAG: return __("DEPOSITFLAG");
            case Gpwebpay_Secondary_Return_Code::CODE_MERORDERNUM: return __("MERORDERNUM");
            case Gpwebpay_Secondary_Return_Code::CODE_CREDITNUMBER: return __("CREDITNUMBER");
            case Gpwebpay_Secondary_Return_Code::CODE_OPERATION: return __("OPERATION");
            case Gpwebpay_Secondary_Return_Code::CODE_BATCH: return __("BATCH");
            case Gpwebpay_Secondary_Return_Code::CODE_ORDER: return __("ORDER");
            case Gpwebpay_Secondary_Return_Code::CODE_URL: return __("URL");
            case Gpwebpay_Secondary_Return_Code::CODE_MD: return __("MD");
            case Gpwebpay_Secondary_Return_Code::CODE_DESC: return __("DESC");
            case Gpwebpay_Secondary_Return_Code::CODE_DIGEST: return __("DIGEST");
            case Gpwebpay_Secondary_Return_Code::CODE_CARDHOLDER_NOT_AUTHENTICATED: return __("Neúspěšné ověření držitele karty");
            case Gpwebpay_Secondary_Return_Code::CODE_AUTHENTICATED: return __("Držitel karty ověřen");
            case Gpwebpay_Secondary_Return_Code::CODE_ISSUER_CARDHOLDER_NOT_3D: return __("Vydavatel karty nebo karta není zapojena do 3D");
            case Gpwebpay_Secondary_Return_Code::CODE_ISSUER_CARDHOLDER_NOT_ENROLLED: return __("Vydavatel karty není zapojen do 3D nebo karta nebyla aktivována");
            case Gpwebpay_Secondary_Return_Code::CODE_TECHNICAL_PROBLEM_DURING_AUTHENTICATION_3005: return __("Technický problém při ověření držitele karty");
            case Gpwebpay_Secondary_Return_Code::CODE_TECHNICAL_PROBLEM_DURING_AUTHENTICATION_3006: return __("Technický problém při ověření držitele karty");
            case Gpwebpay_Secondary_Return_Code::CODE_ACQUIRER_TECHNICAL_PROBLEM: return __("Technický problém v systému zúčtující banky. Kontaktujte obchodníka.");
            case Gpwebpay_Secondary_Return_Code::CODE_UNSUPPORTED_CARD_PRODUCT: return __("Použit nepodporovaný karetní produkt");
            case Gpwebpay_Secondary_Return_Code::CODE_BLOCKED_CARD: return __("Neúspěšná autorizace – karta blokovaná");
            case Gpwebpay_Secondary_Return_Code::CODE_AUTHORIZATION_DECLINED: return __("Autorizace zamítnuta");
            case Gpwebpay_Secondary_Return_Code::CODE_CARD_PROBLEM: return __("Neúspěšná autorizace – problém karty");
            case Gpwebpay_Secondary_Return_Code::CODE_TECHNICAL_PROBLEM_AUTHORIZATION_PROCESS: return __("Neúspěšná autorizace – technický problém v autorizačním centru");
            case Gpwebpay_Secondary_Return_Code::CODE_ACCOUNT_PROBLEM: return __("Neúspěšná autorizace – problém účtu");
        }

        return __("neočekávaný status code: ").$code;
    }

    /**
     * @param int $prCode
     * @param int $srCode
     * @throws Kohana_Exception
     * @return string
     */
    public static function getFullMessage($prCode, $srCode){
        if (!is_int($prCode) || !is_int($srCode)) {
            throw new Kohana_Exception(__("Kódy musí být typu int: ").$prCode.", ".$srCode);
        } else {
            $prCode = (int) $prCode;
            $srCode = (int) $srCode;
        }

        return Gpwebpay_Primary_Return_Code::getMessage($prCode) . " - ".Gpwebpay_Secondary_Return_Code::getMessage($srCode);
    }
} 