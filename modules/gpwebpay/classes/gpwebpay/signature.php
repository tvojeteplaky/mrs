<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 14.9.14
 * Time: 12:36
 */

class Gpwebpay_Signature {
    var $privatni, $heslo, $verejny;
// parametry: jmeno souboru soukromeho klice, heslo k soukromemu klici, jmeno souboru s verejnym klicem
//	function Gpwebpay_Signature($privatni="test_key.pem", $heslo="changeit", $verejny="test_cert.pem"){
    function Gpwebpay_Signature($privatni, $heslo, $verejny){
        $fp = fopen($privatni, "r");
        $this->privatni = fread($fp, filesize($privatni));
        fclose($fp);
        $this->heslo=$heslo;

        $fp = fopen($verejny, "r");
        $this->verejny = fread($fp, filesize($verejny));
        fclose($fp);
    }

    /**
     * @param $text
     * @return string base64 encoded digest
     */
    function sign($text){
        $pkeyid = openssl_get_privatekey($this->privatni, $this->heslo);
        openssl_sign($text, $signature, $pkeyid);
        $signature = base64_encode($signature);
        openssl_free_key($pkeyid);
        return $signature;
    }

    /**
     * @param string $text
     * @param string $signature base64 encoded digest
     * @return bool verify response from gpwebpay
     */
    function verify($text, $signature){
        $pubkeyid = openssl_get_publickey($this->verejny);
        $signature = base64_decode($signature);
        $vysledek = openssl_verify($text, $signature, $pubkeyid);
        openssl_free_key($pubkeyid);
        return (($vysledek==1) ? true : false);
    }
} 