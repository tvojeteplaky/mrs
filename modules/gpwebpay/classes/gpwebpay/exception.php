<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 25.5.14
 * Time: 19:44
 */

class Gpwebpay_Exception extends Kohana_Exception {

    /**
     * @var SoapFault
     */
    private $error;

    /**
     * @param SoapFault $error
     */
    function __construct($error)
    {
        $this->error = $error;
    }
}