<?php
/**
 * Created by PhpStorm.
 * User: zaky
 * Date: 19.9.2014
 * Time: 11:50
 */

class Gpwebpay_Create_Order {
    /**
     * Přidělené číslo obchodníka.
     * povinné
     * max length 10
     * @var string
     */
    private $merchantNumber;
    /**
     * Hodnota CREATE_ORDER.
     * povinné
     * max length 20
     * @var string
     */
    private $operation = "CREATE_ORDER";
    /**
     * Pořadové číslo objednávky, číslo musí být v každém požadavku od obchodníka unikátní.
     * povinné
     * max length 15
     * @var int
     */
    private $orderNumber;
    /**
     * Částka v nejmenších jednotkách dané měny pro Kč = v haléřích, pro EUR = v centech.
     * povinné
     * max length 15
     * @var int
     */
    private $amount;
    /**
     * Identifikátor měny dle ISO 4217.
     * Pro CZK = 203.
     * povinné
     * max length 3
     * @var int
     */
    private $currency;
    /**
     * Udává, zda má být objednávka uhrazena automaticky.
     * Povolené hodnoty: 0 = není požadována okamžitá úhrada 1 = je požadována úhrada
     * viz Gpwebpay_Deposit_Flag
     * povinné
     * max length 1
     * @var int
     */
    private $depositFlag;
    /**
     * Identifikace objednávky pro obchodníka.
     * V případě, že není zadáno, použije se hodnota ORDERNUMBER.
     * Zobrazí se na výpisu z banky.
     * nepovinné
     * max length 30
     * @var int
     */
    private $merOrderNum;
    /**
     * Plná URL adresa obchodníka. Na tuto adresu bude odeslán výsledek požadavku.
     * Výsledek je přeposlán přes prohlížeč zákazníka – tj. je použit redirect (metoda GET).
     * (včetně specifikace protokolu – např. https://)
     * povinné
     * max length 300
     * @var string
     */
    private $url;
    /**
     * Popis nákupu.
     * Obsah pole se přenáší do 3-D systému pro možnost následné kontroly držitelem karty během autentikace u Access Control Serveru vydavatelské banky.
     * Pole musí obsahovat pouze ASCII znaky v rozsahu 0x20 – 0x7E.
     * nepovinné
     * max length 255
     * @var string
     */
    private $description;
    /**
     * Libovolná data obchodníka, která jsou vrácena obchodníkovi v odpovědi v nezměněné podobě – pouze očištěna o „whitespace“ znaky na obou stranách.
     * Pole se používá pro uspokojení rozdílných požadavků jednotlivých e-shopů.
     * Pole musí obsahovat pouze ASCII znaky v rozsahu 0x20 – 0x7E.
     * Pokud je nezbytné přenášet jiná data, potom je zapotřebí použít BASE64 kódování (viz Dodatek Base64).
     * Pole nesmí obsahovat osobní údaje.
     * Výsledná délka dat může být maximálně 255.
     * nepovinné
     * max length 255
     * @var string
     */
    private $md;

    /**
     * @var int
     */
    private $prCode;

    /**
     * @var int
     */
    private $srCode;

    /**
     * @var string
     */
    private $resultText;

    /**
     * @param int $orderNumber
     * @param int $amountInCrowns
     * @param string $url
     * @param int $currency
     * @param int $depositFlag
     */
    function __construct($orderNumber = null, $amountInCrowns = null, $url = null, $currency = null, $depositFlag = null)
    {
        $this->orderNumber = $orderNumber;
        $this->currency = $currency;
        $this->depositFlag = $depositFlag;
        if($amountInCrowns !== null && $amountInCrowns > 0) {
            $this->amount = round($amountInCrowns * 100);
            if($this->currency !== null) $this->currency = 203;
            if($this->depositFlag !== null) $this->depositFlag = Gpwebpay_Deposit_Flag::IMMEDIATE_PAYMENT;
        }
        $this->url = $url;
    }


    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmountInHallers($amount)
    {
        $this->amount = round($amount);
    }

    /**
     * @param int $amount
     */
    public function setAmountInCrowns($amount)
    {
        $this->amount = round($amount * 100);
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getDepositFlag()
    {
        return $this->depositFlag;
    }

    /**
     * @param int $depositFlag
     */
    public function setDepositFlag($depositFlag)
    {
        $this->depositFlag = $depositFlag;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getMd()
    {
        return $this->md;
    }

    /**
     * @param string $md
     */
    public function setMd($md)
    {
        $this->md = $md;
    }

    /**
     * @return int
     */
    public function getMerOrderNum()
    {
        return $this->merOrderNum;
    }

    /**
     * @param int $merOrderNum
     */
    public function setMerOrderNum($merOrderNum)
    {
        $this->merOrderNum = $merOrderNum;
    }

    /**
     * @return string
     */
    public function getMerchantNumber()
    {
        return $this->merchantNumber;
    }

    /**
     * @param string $merchantNumber
     */
    public function setMerchantNumber($merchantNumber)
    {
        $this->merchantNumber = $merchantNumber;
    }

    /**
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param string $operation
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return int
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param int $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array asscociative array with param key, value
     */
    private function getParams() {
        $array = array();
        if(!empty($this->merchantNumber)) $array["MERCHANTNUMBER"] = $this->merchantNumber;
        if(!empty($this->operation)) $array["OPERATION"] = $this->operation;
        if(!empty($this->orderNumber)) $array["ORDERNUMBER"] = $this->orderNumber;
        if(!empty($this->amount)) $array["AMOUNT"] = $this->amount;
        if(!empty($this->currency)) $array["CURRENCY"] = $this->currency;
        if(!empty($this->depositFlag)) $array["DEPOSITFLAG"] = $this->depositFlag;
        if(!empty($this->merOrderNum)) $array["MERORDERNUM"] = $this->merOrderNum;
        if(!empty($this->url)) $array["URL"] = $this->url;
        if(!empty($this->md)) $array["MD"] = $this->md;
        if(!empty($this->prCode)) $array["PRCOCE"] = $this->prCode;
        if(!empty($this->srCode)) $array["SRCODE"] = $this->srCode;
        if(!empty($this->resultText)) $array["RESULTTEXT"] = $this->resultText;

        return $array;
    }

    /**
     * @return string for creating digest for signing
     */
    public function getParamsTextForDigest() {
        return implode("|", $this->getParams());
    }

    /**
     * @param string $base_url
     * @param string $digest
     * @return string for creating digest for signing
     */
    public function getParamsTextForRequestUrl($base_url, $digest) {
        $params = $this->getParams();
        $params["DIGEST"] = $digest;
        $arr = array();
        foreach ($params as $key => $value) {
            $arr[] = $key . "=". urlencode($value);
        }

        return $base_url . "?". implode("&", $arr);
    }

    /**
     * @param int $prCode
     */
    public function setPrCode($prCode)
    {
        $this->prCode = $prCode;
    }

    /**
     * @param string $resultText
     */
    public function setResultText($resultText)
    {
        $this->resultText = $resultText;
    }

    /**
     * @param int $srCode
     */
    public function setSrCode($srCode)
    {
        $this->srCode = $srCode;
    }

    /**
     * @return int
     */
    public function getPrCode()
    {
        return $this->prCode;
    }

    /**
     * @return string
     */
    public function getResultText()
    {
        return $this->resultText;
    }

    /**
     * @return int
     */
    public function getSrCode()
    {
        return $this->srCode;
    }


    public function getFullMessage() {
        if($this->getPrCode() !== null && $this->getSrCode() !== null) {
            return Gpwebpay_Primary_Return_Code::getFullMessage($this->getPrCode(), $this->getSrCode());
        }

        return null;
    }
} 