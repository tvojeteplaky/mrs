<?php
/**
 * File for class GpwebpayStructResponse
 * @package Gpwebpay
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * This class stands for GpwebpayStructResponse originally named Response
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/2561928c4ff14fe5ca974ac32f1de5d5/wsdl.xml
 * @package Gpwebpay
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
class GpwebpayStructResponse extends GpwebpayWsdlClass
{
    /**
     * The digest
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $digest;
    /**
     * The ok
     * @var boolean
     */
    public $ok;
    /**
     * The primaryReturnCode
     * @var int
     */
    public $primaryReturnCode;
    /**
     * The secondaryReturnCode
     * @var int
     */
    public $secondaryReturnCode;
    /**
     * The requestId
     * @var long
     */
    public $requestId;
    /**
     * Constructor method for Response
     * @see parent::__construct()
     * @param string $_digest
     * @param boolean $_ok
     * @param int $_primaryReturnCode
     * @param int $_secondaryReturnCode
     * @param long $_requestId
     * @return GpwebpayStructResponse
     */
    public function __construct($_digest = NULL,$_ok = NULL,$_primaryReturnCode = NULL,$_secondaryReturnCode = NULL,$_requestId = NULL)
    {
        parent::__construct(array('digest'=>$_digest,'ok'=>$_ok,'primaryReturnCode'=>$_primaryReturnCode,'secondaryReturnCode'=>$_secondaryReturnCode,'requestId'=>$_requestId),false);
    }
    /**
     * Get digest value
     * @return string|null
     */
    public function getDigest()
    {
        return $this->digest;
    }
    /**
     * Set digest value
     * @param string $_digest the digest
     * @return string
     */
    public function setDigest($_digest)
    {
        return ($this->digest = $_digest);
    }
    /**
     * Get ok value
     * @return boolean|null
     */
    public function getOk()
    {
        return $this->ok;
    }
    /**
     * Set ok value
     * @param boolean $_ok the ok
     * @return boolean
     */
    public function setOk($_ok)
    {
        return ($this->ok = $_ok);
    }
    /**
     * Get primaryReturnCode value
     * @return int|null
     */
    public function getPrimaryReturnCode()
    {
        return $this->primaryReturnCode;
    }
    /**
     * Set primaryReturnCode value
     * @param int $_primaryReturnCode the primaryReturnCode
     * @return int
     */
    public function setPrimaryReturnCode($_primaryReturnCode)
    {
        return ($this->primaryReturnCode = $_primaryReturnCode);
    }
    /**
     * Get secondaryReturnCode value
     * @return int|null
     */
    public function getSecondaryReturnCode()
    {
        return $this->secondaryReturnCode;
    }
    /**
     * Set secondaryReturnCode value
     * @param int $_secondaryReturnCode the secondaryReturnCode
     * @return int
     */
    public function setSecondaryReturnCode($_secondaryReturnCode)
    {
        return ($this->secondaryReturnCode = $_secondaryReturnCode);
    }
    /**
     * Get requestId value
     * @return long|null
     */
    public function getRequestId()
    {
        return $this->requestId;
    }
    /**
     * Set requestId value
     * @param long $_requestId the requestId
     * @return long
     */
    public function setRequestId($_requestId)
    {
        return ($this->requestId = $_requestId);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see GpwebpayWsdlClass::__set_state()
     * @uses GpwebpayWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return GpwebpayStructResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
