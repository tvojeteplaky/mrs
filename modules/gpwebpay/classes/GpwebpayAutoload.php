<?php
/**
 * File to load generated classes once at once time
 * @package Gpwebpay
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * Includes for all generated classes files
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
require_once dirname(__FILE__) . '/GpwebpayWsdlClass.php';
require_once dirname(__FILE__) . '/Response/GpwebpayStructResponse.php';
require_once dirname(__FILE__) . '/Order/Response/GpwebpayStructOrderResponse.php';
require_once dirname(__FILE__) . '/Order/Response/GpwebpayStructOrderStateResponse.php';
require_once dirname(__FILE__) . '/Delete/GpwebpayServiceDelete.php';
require_once dirname(__FILE__) . '/Approve/GpwebpayServiceApprove.php';
require_once dirname(__FILE__) . '/Batch/GpwebpayServiceBatch.php';
require_once dirname(__FILE__) . '/Credit/GpwebpayServiceCredit.php';
require_once dirname(__FILE__) . '/Deposit/GpwebpayServiceDeposit.php';
require_once dirname(__FILE__) . '/Order/GpwebpayServiceOrder.php';
require_once dirname(__FILE__) . '/Query/GpwebpayServiceQuery.php';
require_once dirname(__FILE__) . '/GpwebpayClassMap.php';
