<?php
/**
 * File for the class which returns the class map definition
 * @package Gpwebpay
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * Class which returns the class map definition by the static method GpwebpayClassMap::classMap()
 * @package Gpwebpay
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
class GpwebpayClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS
     * @return array
     */
    final public static function classMap()
    {
        return array (
  'OrderResponse' => 'GpwebpayStructOrderResponse',
  'OrderStateResponse' => 'GpwebpayStructOrderStateResponse',
  'Response' => 'GpwebpayStructResponse',
);
    }
}
