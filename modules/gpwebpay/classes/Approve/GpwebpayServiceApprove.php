<?php
/**
 * File for class GpwebpayServiceApprove
 * @package Gpwebpay
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * This class stands for GpwebpayServiceApprove originally named Approve
 * @package Gpwebpay
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
class GpwebpayServiceApprove extends GpwebpayWsdlClass
{
    /**
     * Method to call the operation originally named approveReversal
     * @uses GpwebpayWsdlClass::getSoapClient()
     * @uses GpwebpayWsdlClass::setResult()
     * @uses GpwebpayWsdlClass::saveLastError()
     * @param string $_merchantNumber
     * @param string $_orderNumber
     * @param string $_digest
     * @return GpwebpayStructOrderResponse
     */
    public function approveReversal($_merchantNumber,$_orderNumber,$_digest)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->approveReversal($_merchantNumber,$_orderNumber,$_digest));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see GpwebpayWsdlClass::getResult()
     * @return GpwebpayStructOrderResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
