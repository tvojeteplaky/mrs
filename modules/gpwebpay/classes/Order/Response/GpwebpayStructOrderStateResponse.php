<?php
/**
 * File for class GpwebpayStructOrderStateResponse
 * @package Gpwebpay
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * This class stands for GpwebpayStructOrderStateResponse originally named OrderStateResponse
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/2561928c4ff14fe5ca974ac32f1de5d5/wsdl.xml
 * @package Gpwebpay
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
class GpwebpayStructOrderStateResponse extends GpwebpayStructOrderResponse
{
    /**
     * The state
     * @var int
     */
    public $state;
    /**
     * Constructor method for OrderStateResponse
     * @see parent::__construct()
     * @param int $_state
     * @return GpwebpayStructOrderStateResponse
     */
    public function __construct($_state = NULL)
    {
        GpwebpayWsdlClass::__construct(array('state'=>$_state),false);
    }
    /**
     * Get state value
     * @return int|null
     */
    public function getState()
    {
        return $this->state;
    }
    /**
     * Set state value
     * @param int $_state the state
     * @return int
     */
    public function setState($_state)
    {
        return ($this->state = $_state);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see GpwebpayWsdlClass::__set_state()
     * @uses GpwebpayWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return GpwebpayStructOrderStateResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
