<?php
/**
 * File for class GpwebpayStructOrderResponse
 * @package Gpwebpay
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * This class stands for GpwebpayStructOrderResponse originally named OrderResponse
 * Meta informations extracted from the WSDL
 * - from schema : var/wsdltophp.com/storage/wsdls/2561928c4ff14fe5ca974ac32f1de5d5/wsdl.xml
 * @package Gpwebpay
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
class GpwebpayStructOrderResponse extends GpwebpayStructResponse
{
    /**
     * The orderNumber
     * Meta informations extracted from the WSDL
     * - nillable : true
     * @var string
     */
    public $orderNumber;
    /**
     * Constructor method for OrderResponse
     * @see parent::__construct()
     * @param string $_orderNumber
     * @return GpwebpayStructOrderResponse
     */
    public function __construct($_orderNumber = NULL)
    {
        GpwebpayWsdlClass::__construct(array('orderNumber'=>$_orderNumber),false);
    }
    /**
     * Get orderNumber value
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }
    /**
     * Set orderNumber value
     * @param string $_orderNumber the orderNumber
     * @return string
     */
    public function setOrderNumber($_orderNumber)
    {
        return ($this->orderNumber = $_orderNumber);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see GpwebpayWsdlClass::__set_state()
     * @uses GpwebpayWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return GpwebpayStructOrderResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
