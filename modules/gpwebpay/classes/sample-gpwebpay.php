<?php
/**
 * Test with Gpwebpay for 'var/wsdltophp.com/storage/wsdls/2561928c4ff14fe5ca974ac32f1de5d5/wsdl.xml'
 * @package Gpwebpay
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
ini_set('memory_limit','512M');
ini_set('display_errors',true);
error_reporting(-1);
/**
 * Load autoload
 */
require_once dirname(__FILE__) . '/GpwebpayAutoload.php';
/**
 * Wsdl instanciation infos. By default, nothing has to be set.
 * If you wish to override the SoapClient's options, please refer to the sample below.
 * 
 * This is an associative array as:
 * - the key must be a GpwebpayWsdlClass constant beginning with WSDL_
 * - the value must be the corresponding key value
 * Each option matches the {@link http://www.php.net/manual/en/soapclient.soapclient.php} options
 * 
 * Here is below an example of how you can set the array:
 * $wsdl = array();
 * $wsdl[GpwebpayWsdlClass::WSDL_URL] = 'var/wsdltophp.com/storage/wsdls/2561928c4ff14fe5ca974ac32f1de5d5/wsdl.xml';
 * $wsdl[GpwebpayWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
 * $wsdl[GpwebpayWsdlClass::WSDL_TRACE] = true;
 * $wsdl[GpwebpayWsdlClass::WSDL_LOGIN] = 'myLogin';
 * $wsdl[GpwebpayWsdlClass::WSDL_PASSWD] = '**********';
 * etc....
 * Then instantiate the Service class as: 
 * - $wsdlObject = new GpwebpayWsdlClass($wsdl);
 */
/**
 * Examples
 */


/***********************************
 * Example for GpwebpayServiceDelete
 */
$gpwebpayServiceDelete = new GpwebpayServiceDelete();
// sample call for GpwebpayServiceDelete::delete()
if($gpwebpayServiceDelete->delete($_merchantNumber,$_orderNumber,$_digest))
    print_r($gpwebpayServiceDelete->getResult());
else
    print_r($gpwebpayServiceDelete->getLastError());

/************************************
 * Example for GpwebpayServiceApprove
 */
$gpwebpayServiceApprove = new GpwebpayServiceApprove();
// sample call for GpwebpayServiceApprove::approveReversal()
if($gpwebpayServiceApprove->approveReversal($_merchantNumber,$_orderNumber,$_digest))
    print_r($gpwebpayServiceApprove->getResult());
else
    print_r($gpwebpayServiceApprove->getLastError());

/**********************************
 * Example for GpwebpayServiceBatch
 */
$gpwebpayServiceBatch = new GpwebpayServiceBatch();
// sample call for GpwebpayServiceBatch::batchClose()
if($gpwebpayServiceBatch->batchClose($_merchantNumber,$_digest))
    print_r($gpwebpayServiceBatch->getResult());
else
    print_r($gpwebpayServiceBatch->getLastError());

/***********************************
 * Example for GpwebpayServiceCredit
 */
$gpwebpayServiceCredit = new GpwebpayServiceCredit();
// sample call for GpwebpayServiceCredit::credit()
if($gpwebpayServiceCredit->credit($_merchantNumber,$_orderNumber,$_amount,$_digest))
    print_r($gpwebpayServiceCredit->getResult());
else
    print_r($gpwebpayServiceCredit->getLastError());
// sample call for GpwebpayServiceCredit::creditReversal()
if($gpwebpayServiceCredit->creditReversal($_merchantNumber,$_orderNumber,$_creditNumber,$_digest))
    print_r($gpwebpayServiceCredit->getResult());
else
    print_r($gpwebpayServiceCredit->getLastError());

/************************************
 * Example for GpwebpayServiceDeposit
 */
$gpwebpayServiceDeposit = new GpwebpayServiceDeposit();
// sample call for GpwebpayServiceDeposit::deposit()
if($gpwebpayServiceDeposit->deposit($_merchantNumber,$_orderNumber,$_amount,$_digest))
    print_r($gpwebpayServiceDeposit->getResult());
else
    print_r($gpwebpayServiceDeposit->getLastError());
// sample call for GpwebpayServiceDeposit::depositReversal()
if($gpwebpayServiceDeposit->depositReversal($_merchantNumber,$_orderNumber,$_digest))
    print_r($gpwebpayServiceDeposit->getResult());
else
    print_r($gpwebpayServiceDeposit->getLastError());

/**********************************
 * Example for GpwebpayServiceOrder
 */
$gpwebpayServiceOrder = new GpwebpayServiceOrder();
// sample call for GpwebpayServiceOrder::orderClose()
if($gpwebpayServiceOrder->orderClose($_merchantNumber,$_orderNumber,$_digest))
    print_r($gpwebpayServiceOrder->getResult());
else
    print_r($gpwebpayServiceOrder->getLastError());

/**********************************
 * Example for GpwebpayServiceQuery
 */
$gpwebpayServiceQuery = new GpwebpayServiceQuery();
// sample call for GpwebpayServiceQuery::queryOrderState()
if($gpwebpayServiceQuery->queryOrderState($_merchantNumber,$_orderNumber,$_digest))
    print_r($gpwebpayServiceQuery->getResult());
else
    print_r($gpwebpayServiceQuery->getLastError());
