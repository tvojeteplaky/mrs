<?php
/**
 * File for class GpwebpayServiceQuery
 * @package Gpwebpay
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
/**
 * This class stands for GpwebpayServiceQuery originally named Query
 * @package Gpwebpay
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2014-09-24
 */
class GpwebpayServiceQuery extends GpwebpayWsdlClass
{
    /**
     * Method to call the operation originally named queryOrderState
     * @uses GpwebpayWsdlClass::getSoapClient()
     * @uses GpwebpayWsdlClass::setResult()
     * @uses GpwebpayWsdlClass::saveLastError()
     * @param string $_merchantNumber
     * @param string $_orderNumber
     * @param string $_digest
     * @return GpwebpayStructOrderStateResponse
     */
    public function queryOrderState($_merchantNumber,$_orderNumber,$_digest)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->queryOrderState($_merchantNumber,$_orderNumber,$_digest));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see GpwebpayWsdlClass::getResult()
     * @return GpwebpayStructOrderStateResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
