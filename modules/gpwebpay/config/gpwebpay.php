<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

    'BASE_GPWEBPAY_URL'       => 'https://test.3dsecure.gpwebpay.com/webservices/services/pgw',
    'GPWEBPAY_WSDL'       => str_replace('\\', '/',DOCROOT).'modules/gpwebpay/wsdl/pgw-standard_test.wsdl',
    'BASE_URL_CREATE_ORDER_REQUEST'   =>'https://test.3dsecure.gpwebpay.com/rb/order.do',
    'BASE_URL_CREATE_ORDER_RESPONSE'  =>'',
    'MERCHANTNUMBER'=>'',
    'PRIVATE_KEY_WEB' => '',
    'PRIVATE_KEY_WEB_PASSWORD'=> '',
    'PUBLIC_KEY_GPWEBPAY'=> str_replace('\\', '/',DOCROOT).'modules/gpwebpay/files/muzo.signing_test.pem',

);