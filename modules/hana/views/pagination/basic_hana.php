<div class="pagination-centered">
    <ul class="pagination">
        <li class="arrow"><a href="<?php echo $current_page ?>">&laquo;</a></li>
        <?php for ($i = 1; $i <= $total_pages; $i++): ?>
        <?php if ($i == $current_page): ?>
        <li class="current"><a href="#"><?php echo $i ?></a></li>
        <?php else: ?>
        <li><a href="<?php echo HTML::chars($page->url($i)) ?>"><?php echo $i ?></a></li>
        <?php endif ?>

        <?php endfor ?>
        <li class="arrow"><a href="<?php echo HTML::chars($page->url($current_page+1)) ?>">&raquo;</a></li>
    </ul><!-- .pagination -->
</div>