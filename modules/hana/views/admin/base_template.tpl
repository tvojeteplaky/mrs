{* zakladni sablona systemu Hana 2 - copyright 2011 Pavel Herink *}
<!DOCTYPE html>
<html lang=cs>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="Pavel Herink"/>
    <meta name="copyright" content="2011"/>
    <meta name="description" content="Systém pro správu obsahu Hana verze 2"/>
    <meta name="keywords" content=""/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{$media_path}admin/css/style.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{$media_path}admin/css/style-print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="{$media_path}admin/css/jquery-ui-1.8.6.custom.css" type="text/css"/>
    <link rel="stylesheet" href="{$media_path}admin/css/showLoading.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{$media_path}admin/css/agile-uploader.css" type="text/css" media="screen"/>


    <link rel="stylesheet" href="{$media_path}admin/css/jquery.fancybox.css" type="text/css"/>
    <link rel="stylesheet" href="{$media_path}admin/css/{$admin_theme.nazev_css}.css" type="text/css"/>
    <link rel="stylesheet" href="{$media_path}admin/css/bootstrap-datetimepicker.min.css" type="text/css"/>
    <link rel="stylesheet" href="{$media_path}admin/css/bootstrap-colorpicker.min.css" type="text/css"/>
    <link rel="stylesheet" href="{$media_path}admin/css/select2.min.css" type="text/css"/>
    <link rel="stylesheet" href="{$media_path}admin/css/jquery.smartmenus.bootstrap.css" type="text/css"/>

    <script type="text/javascript" src="{$media_path}admin/js/jquery.old.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery-ui-1.8.18.custom.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/locales/bootstrap-datetimepicker.cs.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/daterangepicker.jQuery.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.showLoading.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.tristateCheckbox.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.tablednd.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.uploadify.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.sortable.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/select2.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.smartmenus.min.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/jquery.smartmenus.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="{$media_path}js/hana.js"></script>
    <script type="text/javascript" src="{$media_path}admin/js/admin.js"></script>

    <title>{$title} - administrační rozhranní</title>
    {literal}
        <script type="text/javascript">

            // Load the Visualization API and the piechart package.
            google.load('visualization', '1.0', {'packages': ['corechart', 'geochart']});
        </script>
    {/literal}
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <!-- hlavicka -->
            <header>
                <!--  hlavni menu L1 -->
                {if !empty($admin_menu)}
                    <nav class="navbar navbar-{if $admin_theme.inverse}inverse{else}default{/if}" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            {if $web_owner.default_title}
                                <a class="navbar-brand" href="{$url_base}" data-toggle="popover" data-trigger="hover" data-content="{translate str="Budete přesměrován na frontend"}" target="_blank">{$web_owner.default_title}</a>
                            {/if}
                        </div>
                        <div class="collapse navbar-collapse" id="main-navbar-collapse-1">
                            <ul class="navbar-nav nav">
                                {foreach key=k item=link name=nav from=$admin_menu}
                                    <li class="{if in_array($link.id, $link_path)}active{/if}">
                                        <a href="{if !empty($link.children)}#{else}{$link.href}{/if}">{$link.nazev} {if !empty($link.children)}<b class="caret"></b>{/if}</a>
                                        {if !empty($link.children)}
                                            <ul class="dropdown-menu">
                                                <li><a href="{$link.href}">{$link.nazev}</a></li>
                                                <li class="divider"></li>
                                                {foreach from=$link.children item=child}
                                                    <li class="{if in_array($child.id, $link_path)}active{/if}">
                                                        <a href="{if !empty($child.children)}#{else}{$child.href}{/if}">{$child.nazev} {if !empty($child.children)}<b class="caret"></b>{/if}</a>
                                                        {if !empty($child.children)}
                                                            <ul class="dropdown-menu">
                                                                <li><a href="{$child.href}">{$child.nazev}</a></li>
                                                                <li class="divider"></li>
                                                                {foreach $child.children as $child2}
                                                                    <li class="{if in_array($child2.id, $link_path)}active{/if}">
                                                                        <a href="{$child2.href}">{$child2.nazev}</a>
                                                                    </li>
                                                                {/foreach}
                                                            </ul>
                                                        {/if}
                                                    </li>
                                                {/foreach}
                                            </ul>
                                        {/if}
                                    </li>
                                {/foreach}
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                {* odkaz na odhlaseni - staticky *}
                                <li><a href="{$url_base}admin/logout">Odhlásit</a></li>
                            </ul>
                        </div>
                    </nav>
                {/if}
            </header>
        </div>
    </div>

    <!-- stredni cast -->
    <div class="row">
        {if !empty($center_section)}{$center_section}{/if}
        <!-- hlavni obsah -->
        <div class="col-md-12" id="ContentSection">
            {if !empty($admin_content)}{$admin_content}{/if}
        </div>
    </div>
    <footer>
        <div class="row">
            <div class="col-sm-12 text-right">
                <p><a href="http://www.dgstudio.cz"><img src="{$media_path}admin/img/dg_logo{if $admin_theme.id == 2}_light{/if}.png" alt="DG Studio logo"></a></p>
            </div>
        </div>
    </footer>
</div>
</body>
</html>