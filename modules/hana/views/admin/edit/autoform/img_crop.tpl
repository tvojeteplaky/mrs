<a href="#" data-toggle="modal" data-target="#modal-{$id}">Oříznout obrázek</a>
<!-- Modal -->
<div class="modal fade" id="modal-{$id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ořezání obrázku <small>{if isset($title)}{$title}{/if}</small></h4>
            </div>
            <div class="modal-body">
                <script type="text/javascript" src="{$media_path}admin/js/autoform/canvas.js"></script>

                <style type="text/css">
                    * {
                        font-family: "Roboto", "Helvetica", sans-serif;
                    }

                    canvas {
                        box-shadow: 0 0 15px 0 rgba(0, 0, 0, .2);
                        background: url('{$media_path}admin/img/transparent.png');
                        cursor: move;
                        display: block;
                        margin: 15px auto;
                    }

                    button {
                        border: 1px solid;
                        text-transform: uppercase;
                        cursor: pointer;
                        transition: all .3s;
                    }

                    button:hover {
                        background: #2f75ab;
                        color: white;
                    }

                    input {
                        border: 0;
                        border-bottom: 1px solid;
                        background: rgba(0, 0, 0, .1);
                    }

                    button, input {
                        border-color: #2f75ab;
                        background: transparent;
                        font-size: 1rem;
                        padding: 5px 10px;
                        color: #2f75ab;
                    }

                    button + button {
                        margin-left: -1px;
                    }
                </style>
                <canvas id="image-canvas-{$id}"></canvas>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{translate str="Zavřít"}</button>
                <button type="button" class="btn btn-warning" id="finish-button-{$id}">{translate str="Ořezat"}</button>
                <button type="button" class="btn btn-primary" disabled="disabled" aria-disabled="true" id="save-button-{$id}">{translate str="Uložit"}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#modal-{$id}').on('shown.bs.modal', function (e) {
        Canvas.init("{$from}", "{$id}", {$settings.width}, {$settings.height}, "?cropped_img_save", "{$ext}");
    })
</script>
