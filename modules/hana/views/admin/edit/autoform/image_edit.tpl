<div class="row" class="autoform image-edit">
    <div class="col-xs-12">
        {if !empty($slides)}
            <div class="row">
                <div class="col-xs-12">
                    <ul class="sortable grid items">
                        {foreach $slides as $slide}
                            <li data-id="{$slide.id}" class="item">
                                {if $slide.photo_src}
                                    <a href="{$media_path}{$path}{$slide.photo_src}-ad.{$slide.ext}" class="fancybox" rel="gallery">
                                        <img src="{$media_path}{$path}{$slide.photo_src}-at.{$slide.ext}" alt="{$slide.nazev} photo" class="img-responsive">
                                    </a>
                                {/if}
                                <div class="btn-group btn-group-sm" role="group">
                                    <button data-id="{$slide.id}" type="button" class="btn btn-primary width-50 edit">Upravit</button>
                                    <button data-id="{$slide.id}" type="button" class="btn btn-warning width-50 delete">Smazat</button>
                                </div>
                            </li>
                        {/foreach}
                        <div class="clearfix"></div>
                    </ul>
                </div>
            </div>
        {/if}
        <div class="row add">
            <div class="col-xs-12">
                <a href="#" class="btn btn-default" data-toggle="modal" data-target="#image-edit-modal">{translate str="Přidat"}</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="image-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Slide</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal" id="image-edit-form" data-action="?" data-method="post">
                    <div class="form-group">
                        <label for="nazev" class="col-sm-2 control-label">Název</label>
                        <div class="col-sm-10">
                            <input type="text" id="nazev" value="" class="form-control" name="form[nazev]" placeholder="Název">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="link" class="col-sm-2 control-label">Odkaz</label>
                        <div class="col-sm-10">
                            <input type="text" value="" class="form-control" id="odkaz" name="form[odkaz]" placeholder="Odkaz">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="popis" class="col-sm-2 control-label">Text</label>
                        <div class="col-sm-10">
                            <textarea type="text" value="" class="form-control" id="popis" name="form[popis]" placeholder="Text"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label form="zobrazit" class="col-sm-2 control-label">Zobrazit</label>
                        <div class="col-sm-10">
                            <input type="checkbox" value="1" checked name="form[zobrazit]" id="zobrazit">
                        </div>
                    </div>
                    <div class="form-group">
                        <label form="image_edit_file" class="col-sm-2 control-label">Obrázek</label>
                        <div class="col-sm-10">
                            <input type="file" name="image_edit_file" id="image_edit_file" class="form-control">
                        </div>
                    </div>
                    <input type="hidden" name="form[slider_id]" value="{$slider.id}">
                    <input type="hidden" name="form[id]" value="0">
                    <input type="hidden" name="form[action]" value="new">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{translate str="Zavřit"}</button>
                <button type="submit" form="image-edit-form" class="btn btn-primary" id="image-edit-action">{translate str="Odelat"}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{$media_path}admin/js/autoform/image-edit.js"></script>
<script type="text/javascript">
    $(function (){
        ImageEdit.init($('#image-edit-form'), $('#image-edit-action'), $('.item'));
    });
</script>