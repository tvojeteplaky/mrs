<?php defined('SYSPATH') or die('No direct script access.');


class AutoForm_Item_imageedit extends AutoForm_Item {
    private $orm = null;
    private $table;
    private $file;

    public function pregenerate($data_orm) {

        $result_data = array();
        $this->orm = $data_orm;
        if (isset($_POST['form']) && isset($_POST['form']['action'])) {
            switch ($_POST['form']['action']) {
                case 'new' : $result_data = $this->add_new_image($_POST['form']);
                    break;
                case 'edit': $result_data = $this->edit_image($_POST['form']);
                    break;
                case 'reorder': $result_data = $this->reorder_images($_POST['form']);
                    break;
                case 'delete': $result_data = $this->delete_image($_POST['form']);
                    break;
                case 'info': $result_data = $this->info_image($_POST['form']);
                    break;
            };
        }

        return $result_data;
    }

    public function generate($data, $template=false) {
        $template = new View('admin/edit/autoform/image_edit');
        if (is_null($this->orm))
            $this->orm = $this->settings['orm'];
        $this->table = $this->settings['table'];

        $order = false;
        if (array_key_exists("poradi", ORM::factory($this->settings['table'])->table_columns()))
            $order = true;

        $slides = $this->orm->slider_slide->where('language_id', '=',$this->orm->get_selected_language_id());
        if ($order)
            $slides->order_by('poradi');
        $slides = $slides->find_all();
        $template->order = $order;
        $template->slides = $slides;
        $template->slider = $this->orm;
        $template->path = 'photos/'.$this->settings['object']->get_module_key().'/'.$this->settings['object']->get_submodule_key().'/images-'.$this->orm->id.'/';


        return $template->render();
    }

    /**
     * Add new image to slider
     * @param $data
     */
    private function add_new_image($data)
    {
        if (isset($_FILES['image_edit_file']) && $_FILES["image_edit_file"]["name"]) {
            $edit_object = $this->settings['object'];
            $file = $_FILES['image_edit_file'];
            $orm = ORM::factory($this->settings['table'])
                ->language($this->orm->get_selected_language_id());

            foreach ($data as $key => $item) {
                if ($key != 'action' && $key != 'id')
                    $orm->$key = $item;
            }

            if(array_key_exists("poradi", $orm->table_columns()))
            {
                $order = $edit_object->get_module_service()->get_new_order_position($orm);
                $orm->poradi = $order;
            }

            $orm->save();

            $this->upload_file($orm, $file);
        }
    }

    /**
     * Uploads file (image) and saves
     * @param $image
     * @param $file
     */
    private function upload_file($image, $file)
    {
        $name = explode('.', $file['name']);
        $ext = array_pop($name);
        $name = seo::uprav_fyzicky_nazev_souboru(implode('.', $name));
        $setting = isset($this->settings['setting']) ? $this->settings['setting'] : 'photo';

        if (($count = count(ORM::factory($this->settings['table'])->where('photo_src', 'like', $name.'%')->where($this->settings['object']->get_module_key() . '_id','=', $this->orm->id)->find_all())) > 0) {
            $name .= "_" . ($count + 1);
        }

        $languages = Kohana::config("languages")->get("mapping");

        $name .= '_'.$languages[$this->orm->language_id];

        if ($image->id) {
            // nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
            $image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->settings['object']->get_module_key(), $this->settings['object']->get_submodule_key(), $setting);
            $this->settings['object']->get_module_service()->insert_image("image_edit_file", $this->settings['object']->get_subject_dir(), $image_settings, $name, true, $ext, '', false);
        }

        $image->ext = $ext;
        $image->photo_src = $name;
        $image->save();

    }

    /**
     * Reorders items
     * @param $data
     */
    private function reorder_images($data)
    {
        $json = array('code' => 0, 'message' => '');
        if (isset($_POST['newOrder'])) {
            $order = 1;
            foreach ($_POST['newOrder'] as $id) {
                $slide = ORM::factory($this->settings['table'], $id);
                $slide->poradi = $order;
                $slide->save();
                $order++;
            }
            $json['code'] = 1;
            $json['message'] = I18n::get('Úspěšně vykonáno');
        } else {
            $json['code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }

        echo json_encode($json);
        exit;
    }

    /**
     * Deletes image
     * @param $data
     */
    private function delete_image($data)
    {
        $json = array('code' => 0, 'message' => '');
        if (isset($data['id'])) {
            $id = $data['id'];
            $image = ORM::factory($this->settings['table'])
                ->where(Inflector::plural($this->settings['table']) . '.id', '=', $id)
                ->language($this->orm->get_selected_language_id())
                ->find();

            if ($image->loaded()) {
                $setting = isset($this->settings['setting']) ? $this->settings['setting'] : 'photo';
                Service_Hana_File::delete_simple_photo($this->settings['object']->get_subject_dir()."images-".$this->orm->id."/", $image->photo_src, $setting, $this->settings['object']);
                $image->delete();
                $json = 'deleted';
            } else {
                $json['code'] = 3;
                $json['message'] = I18n::get('Obrázek neexistuje') . ' ID ' . $id;
            }
        } else {
            $json['code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }
        echo json_encode($json);
        exit;
    }

    /**
     * Edites image
     * @param $data
     */
    private function edit_image($data)
    {
        $json = array('code' => 0, 'message' => '');
        $is_file = false;
        if (isset($data['id'])) {
            $id = $data['id'];
            $image = ORM::factory($this->settings['table'])
                ->where(Inflector::plural($this->settings['table']) . '.id', '=', $id)
                ->language($this->orm->get_selected_language_id())
                ->find();

            if ($image->loaded()) {
                foreach ($data as $key=>$value) {
                    if ($key != 'action' && $key != 'id')
                        $image->$key = $value;
                }
                if (!isset($data['zobrazit']))
                    $image->zobrazit = 0;
                $image->save();

                if (isset($_FILES['image_edit_file']) && $_FILES["image_edit_file"]["name"]) {
                    $is_file = true;
                    $setting = isset($this->settings['setting']) ? $this->settings['setting'] : 'photo';
                    Service_Hana_File::delete_simple_photo($this->settings['object']->get_subject_dir()."images-".$this->orm->id."/", $image->photo_src, $setting, $this->settings['object']);
                    $this->upload_file($image, $_FILES['image_edit_file']);
                }

                $json = 'updated';
            } else {
                $json['code'] = 3;
                $json['message'] = I18n::get('Obrázek neexistuje') . ' ID ' . $id;
            }
        } else {
            $json['code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }

        if (!$is_file) {
            echo json_encode($json);
            exit;
        }
    }

    /**
     * Prints info about image
     * @param $data
     */
    private function info_image($data)
    {
        $json = array('code' => 0, 'message' => '');

        if (isset($data['id'])) {
            $id = $data['id'];
            $image = ORM::factory($this->settings['table'])
                ->where(Inflector::plural($this->settings['table']) . '.id', '=', $id)
                ->language($this->orm->get_selected_language_id())
                ->find();

            if ($image->loaded()) {
                $json = $image->as_array();
            } else {
                $json['code'] = 3;
                $json['message'] = I18n::get('Obrázek neexistuje') . ' ID ' . $id;
            }
        } else {
            $json['code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }

        echo json_encode($json);
        exit;
    }
}