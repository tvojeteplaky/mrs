<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Trida reprezentujici odkaz
 * specificka nastaveni:
 * HTML - HTML atributy prvku
 * href - pouze staticka url adresa
 * hrefid - url adresa s poslednim automatickym segmentem /id - pro admin
 * title - titulek odkazu
 * orm_tree_level_indicator - vlozi obrazek s odrazkou pred nazev kategorie podle urovne zanoreni
 *
 * @package    Hana/AutoForm
 * @author     Pavel Herink
 * @copyright  (c) 2010 Pavel Herink
 */
class AutoForm_Item_Tabbody extends AutoForm_Item_Text
{

    public function generate($data_orm, $template = false)
    {
        $tab = '
        </tbody>
    </table>
</div>
<div role="tabpanel" class="tab-pane fade" id="'.$this->settings['id'].'">
<table summary="editační formulář" class="table table-striped table-edit">
    <tbody>
    ';
        return array('type' => 'tab', 'text' => $tab);
    }

}