<?php defined('SYSPATH') or die('No direct script access.');


class AutoForm_Item_Youtube extends AutoForm_Item
{
    private $orm = null;
    private $table;

    public function pregenerate($data_orm)
    {

        $result_data = array();
        $this->orm = $data_orm;
        if (isset($_POST['form']) && isset($_POST['form']['action'])) {
            switch ($_POST['form']['action']) {
                case 'new' :
                    $result_data = $this->add_new_video($_POST['form']);
                    break;
                case 'edit':
                    $result_data = $this->edit_video($_POST['form']);
                    break;
                case 'reorder':
                    $result_data = $this->reorder_videos($_POST['form']);
                    break;
                case 'delete':
                    $result_data = $this->delete_video($_POST['form']);
                    break;
                case 'info':
                    $result_data = $this->info_video($_POST['form']);
                    break;
            };
        }

        return $result_data;
    }

    public function generate($data, $template = false)
    {
        $template = new View('admin/edit/autoform/youtube_edit');
        if (is_null($this->orm))
            $this->orm = $this->settings['orm'];
        $this->table = $this->settings['table'];
        $table = $this->table;

        $order = false;
        if (array_key_exists("poradi", ORM::factory($this->settings['table'])->table_columns()))
            $order = true;

        $videos = $this->orm->$table->where('language_id', '=', $this->orm->get_selected_language_id());
        if ($order)
            $videos->order_by('poradi');
        $videos = $videos->find_all();
        $template->order = $order;
        $template->youtubes = $videos;
        $template->product = $this->orm;
        $template->path = 'photos/' . $this->settings['object']->get_module_key() . '/' . $this->settings['object']->get_submodule_key() . '/videos-' . $this->orm->id . '/';


        return $template->render();
    }

    /**
     * Add new video to youtubes
     * @param $data
     */
    private function add_new_video($data)
    {
        $edit_object = $this->settings['object'];
        $orm = ORM::factory($this->settings['table'])
            ->language($this->orm->get_selected_language_id());

        foreach ($data as $key => $item) {
            if ($key != 'action' && $key != 'id')
                $orm->$key = $item;
        }

        if (array_key_exists("poradi", $orm->table_columns())) {
            $order = $edit_object->get_module_service()->get_new_order_position($orm);
            $orm->poradi = $order;
        }

        $orm->save();
    }

    /**
     * Reorders items
     * @param $data
     */
    private function reorder_videos($data)
    {
        $json = array('error_code' => 0, 'message' => '');
        if (isset($_POST['newOrder'])) {
            $order = 1;
            foreach ($_POST['newOrder'] as $id) {
                $slide = ORM::factory($this->settings['table'], $id);
                $slide->poradi = $order;
                $slide->save();
                $order++;
            }
            $json['error_code'] = 1;
            $json['message'] = I18n::get('Úspěšně vykonáno');
        } else {
            $json['error_code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }

        echo json_encode($json);
        exit;
    }

    /**
     * Deletes video
     * @param $data
     */
    private function delete_video($data)
    {
        $json = array('error_code' => 0, 'message' => '');
        if (isset($data['id'])) {
            $id = $data['id'];
            $video = ORM::factory($this->settings['table'])
                ->where(Inflector::plural($this->settings['table']) . '.id', '=', $id)
                ->language($this->orm->get_selected_language_id())
                ->find();

            if ($video->loaded()) {
                $video->delete();
                $json = 'deleted';
            } else {
                $json['error_code'] = 3;
                $json['message'] = I18n::get('Video neexistuje') . ' ID ' . $id;
            }
        } else {
            $json['error_code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }
        echo json_encode($json);
        exit;
    }

    /**
     * Edites video
     * @param $data
     */
    private function edit_video($data)
    {
        $json = array('error_code' => 0, 'message' => '');
        $is_file = false;
        if (isset($data['id'])) {
            $id = $data['id'];
            $video = ORM::factory($this->settings['table'])
                ->where(Inflector::plural($this->settings['table']) . '.id', '=', $id)
                ->language($this->orm->get_selected_language_id())
                ->find();

            if ($video->loaded()) {
                foreach ($data as $key => $value) {
                    if ($key != 'action' && $key != 'id')
                        $video->$key = $value;
                }
                if (!isset($data['zobrazit']))
                    $video->zobrazit = 0;
                $video->save();

                if (isset($_FILES['youtube_edit_file']) && $_FILES["youtube_edit_file"]["name"]) {
                    $is_file = true;
                    $setting = isset($this->settings['setting']) ? $this->settings['setting'] : 'photo';
                    Service_Hana_File::delete_simple_photo($this->settings['object']->get_subject_dir() . "videos-" . $this->orm->id . "/", $video->photo_src, $setting, $this->settings['object']);
                    $this->upload_file($video, $_FILES['youtube_edit_file']);
                }

                $json = 'updated';
            } else {
                $json['error_code'] = 3;
                $json['message'] = I18n::get('Video neexistuje') . ' ID ' . $id;
            }
        } else {
            $json['error_code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }

        if (!$is_file) {
            echo json_encode($json);
            exit;
        }
    }

    /**
     * Prints info about video
     * @param $data
     */
    private function info_video($data)
    {
        $json = array('error_code' => 0, 'message' => '');

        if (isset($data['id'])) {
            $id = $data['id'];
            $video = ORM::factory($this->settings['table'])
                ->where(Inflector::plural($this->settings['table']) . '.id', '=', $id)
                ->language($this->orm->get_selected_language_id())
                ->find();

            if ($video->loaded()) {
                $json = $video->as_array();
            } else {
                $json['error_code'] = 3;
                $json['message'] = I18n::get('Video neexistuje') . ' ID ' . $id;
            }
        } else {
            $json['error_code'] = 2;
            $json['message'] = I18n::get('Chybějící parametry');
        }

        echo json_encode($json);
        exit;
    }
}