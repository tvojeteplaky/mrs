<?php defined('SYSPATH') or die('No direct script access.');

class Controller_External extends Controller {

    protected $allow_external_request = true;
    private $output;

    public function action_index()
    {

    }

    public function action_i18n()
    {
        if (isset($_GET['translate'])) {
            $this->output = i18n::get($_GET['translate']);
            if (isset($_GET['lang']))
                $this->output = i18n::get($_GET['translate'], $_GET['lang']);
        } else {
            $this->output = i18n::$lang;
        }
    }

    public function after()
    {
        echo json_encode($this->output);
        exit;
    }

}
