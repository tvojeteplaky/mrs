<?php defined('SYSPATH') or die('No direct script access.');

class Model_Email_Form_Contact extends Model
{
    protected $validate_object;
    protected $form_errors;
    
    public function check($form_data)
    {
        $this->validate_object = Validate::factory($form_data)
            ->filters(TRUE,array('strip_tags' => NULL))
            ->rules('email', array('not_empty'=>NULL))
            ->rule('jmeno', 'not_empty')
            ->rule('zprava', 'not_empty')
            ->rule('email','regex',array('([a-zA-Z0-9_\.\+-]+\@[a-zA-Z0-9-]+\.[a-zA-Z0-9-\.]+|\+?\(?\d{2,4}\)?[\d\s-]{3,})'));
            //->rule('tel', 'not_empty');

        if($this->validate_object->check()){
            return true;
        }else{
            return false;
        }
    }
    
    public function validate()
    {
       return $this->validate_object;
    }


 
}
?>

