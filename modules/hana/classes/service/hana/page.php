<?php defined('SYSPATH') or die('No direct script access.');

/**
 *
 * Servisa pro obsluhu statickych stranek.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Service_Hana_Page extends Service_Hana_Module_Base
{
    public static $photos_resources_dir = "media/photos/";
    public static $files_resources_dir="media/files/";
    public static $navigation_module = "page";
    protected static $chainable = true;
    protected static $thumbs = array('small' => 't1', 'big' => 'ad');
    protected static $thumb_banners = array('large' => 't1', "medium" => "t2", "small" => "t3");

    /**
     * Nacte stranku dle route_id
     * @param int $id
     * @return array
     */
    public static function get_page_by_route_id($id, $redirect = false)
    {
        $page_orm = orm::factory("page")->where("route_id", "=", $id)->find();

        if ($page_orm->direct_to_sublink && $redirect) {

            $language_id = Hana_Application::instance()->get_actual_language_id();
            //throw new Kohana_Exception("Požadovaná stránka nebyla nalezena", array(), "404");
            $first_subpage_seo = DB::select("routes.nazev_seo")
                ->from("pages")
                ->join("page_data")->on("pages.id", "=", "page_data.page_id")
                ->join("routes")->on("page_data.route_id", "=", "routes.id")
                ->where("pages.parent_id", "=", db::expr($page_orm->id))
                ->where("routes.language_id", "=", DB::expr($language_id))
                ->where("routes.zobrazit", "=", DB::expr(1))
                ->order_by("poradi")
                ->limit(1)
                ->execute()->get("nazev_seo");
            Request::instance()->redirect(url::base() . $first_subpage_seo);
        }

        $result_data = $page_orm->as_array();
        $result_data['nazev_seo'] = $page_orm->route->nazev_seo;

        // cesta k obrazku
        if ($page_orm->page_category_id == 3) {
            $dir = "item";
        } else {
            $dir = "unrelated";
        }
        $dirname = self::$photos_resources_dir . "page/" . $dir . "/images-" . $page_orm->id . "/";

        if ($page_orm->photo_src && file_exists(str_replace('\\', '/', DOCROOT) . $dirname . $page_orm->photo_src . "-ad.jpg")) {
            $result_data["photo"] = url::base() . $dirname . $page_orm->photo_src . "-ad.jpg";
        } else {
            $result_data["photo"] = "";
        }


        $result_data['photos'] = array();
        $photos_orm = $page_orm->page_photos
            ->where('zobrazit', '=', 1)
            ->where('language_id', '=', $page_orm->language_id)
            ->order_by('poradi', self::$order_direction)
            ->find_all();

        foreach ($photos_orm as $photo) {
            $result_data['photos'][$photo->id] = $photo->as_array();
            foreach (self::$thumbs as $key => $thumb) {

                $dirname = self::$photos_resources_dir . self::$navigation_module . "/item/gallery/images-" . $page_orm->id . '/' . $photo->photo_src . '-' . $thumb . '.jpg';

                if ($photo->photo_src && file_exists(str_replace('\\', '/', DOCROOT) . $dirname)) {

                    $result_data['photos'][$photo->id][$key] = $dirname;
                }
            }
        }

         // vylistovani souburu
       $files=$page_orm->page_files
            ->where("page_file_data.language_id","=",$page_orm->language_id)
            ->where("page_files.zobrazit","=",1)
            ->find_all();

       $filedirname=self::$files_resources_dir."page/item/files-".$page_orm->id."/";
       $files_array=array();
       $x=1;
       foreach($files as $file)
       {
           if($file->file_src && file_exists(str_replace('\\', '/',DOCROOT).$filedirname.$file->file_src.".".$file->ext))
           {
               $files_array[$x]["file"]=url::base().$filedirname.$file->file_src.".".$file->ext;
               $files_array[$x]["nazev"]=$file->nazev;
               $files_array[$x]["ext"]=$file->ext;
               $files_array[$x]["file_thumb"]=url::base()."media/admin/img/icons/".$file->ext.".png";
               $x++;
           }
       }
     
       $result_data["files"]=$files_array;

        return $result_data;
    }

    /**
     * Returns top page from parent
     * @param array $page
     * @return array
     */
    public static function get_top_parent_page($page)
    {
        if ($page['parent_id'] > 0) {
            $page_parent = ORM::factory('page')
                ->where('pages.id', '=', $page['parent_id'])
                ->language($page['language_id'])
                ->find();

            return self::get_page_by_route_id($page_parent->route_id);
        } else {
            return $page;
        }
    }

    /**
     * Najde podřízené stránky.
     * @param type $page_id
     * @return orm
     */
    public static function get_page_index($page_id)
    {
        $language_id = Hana_Application::instance()->get_actual_language_id();
        $index_pages = DB::select("pages.photo_src")->select("pages.id")->select("page_data.nazev")->select("page_data.uvodni_popis")->select("page_data.akce_text")->select("routes.nazev_seo")
            ->from("pages")
            ->join("page_data")->on("pages.id", "=", "page_data.page_id")
            ->join("routes")->on("page_data.route_id", "=", "routes.id")
            ->where("pages.parent_id", "=", db::expr($page_id))
            ->where("routes.language_id", "=", DB::expr($language_id))
            ->where("routes.zobrazit", "=", DB::expr(1))
            ->order_by("poradi")
            ->execute();

        $index_pages = $index_pages->as_array();
        $result_data = array();

        foreach ($index_pages as $page) {
            $result_data[$page["nazev"]]["nazev"] = $page["nazev"];
            $result_data[$page["nazev"]]["nazev_seo"] = $page["nazev_seo"];
            $result_data[$page["nazev"]]["uvodni_popis"] = $page["uvodni_popis"];
            $result_data[$page["nazev"]]["akce_text"] = $page["akce_text"];

            $dirname = self::$photos_resources_dir . "page/item/images-" . $page["id"] . "/";
            if ($page["photo_src"] && file_exists(str_replace('\\', '/', DOCROOT) . $dirname . $page["photo_src"] . "-t2.jpg")) {
                $result_data[$page["nazev"]]["photo_detail"] = url::base() . $dirname . $page["photo_src"] . "-t2.jpg";
            }
        }


        return $result_data;
    }


    /**
     *  Tato funkce za pomoci parametrů zkontroluje dostupnost dané fotky a nebo vrátí prázdný string.
     * 
     * @var string $photo Název fotky
     * @var string $dirname Cesta k adresáři s fotkami
     * @var string[] $thumbs 
     * 
     * @return string[] 
     */
    public static function _photo_way_generator($photo,$dirname="media/photos/page/item/images-1",$thumbs = array("ad"=>"jpg","at"=>"jpg")) {
            $result_data = array();

            foreach ($thumbs as $thumb => $ext) {
                $way = $dirname . $photo . "-" . $thumb . ".".$ext;
                if ($photo && file_exists(str_replace('\\', '/', DOCROOT) . $way)) {
                    $result_data[$thumb] = url::base() . $way;
                } else {
                    $result_data[$thumb] = "";
                }
                unset($way);
            }
            return $result_data;       
    }

    /**
     *
     * @param type $code
     * @param type $language_id
     * @return type
     */
    public static function get_static_content_by_code($code, $language_id = 1)
    {
        return orm::factory("static_content")->where("kod", "=", $code)->where("language_id", "=", $language_id)->find();
    }

    public static function search_config()
    {
        return array(

            "title" => "Stránky",
            "display_title" => "page_data.nazev",
            "display_text" => "page_data.uvodni_popis",
            "search_columns" => array("page_data.nazev", "page_data.popis", "page_data.uvodni_popis"),
//                  "display_category_title"=>"product_category_data.nazev",
//                  "display_category_text"=>"product_category_data.uvodni_popis",
//                  "search_category_columns"=>array("product_category_data.nazev", "product_category_data.uvodni_popis", "product_category_data.popis")

        );
    }

}

?>
