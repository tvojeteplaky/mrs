<?php defined('SYSPATH') or die('No direct script access.');


class Service_Hana_Filemanager
{

    public static function process()
    {
        function access($attr, $path, $data, $volume) {
            return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
                ? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
                :  null;                                    // else elFinder decide it itself
        }

        if (isset($_SERVER["CONTEXT_DOCUMENT_ROOT"]))
            $path = $_SERVER["CONTEXT_DOCUMENT_ROOT"];
        else
        {
            $separator = DIRECTORY_SEPARATOR;
            if (strpos( $_SERVER["SCRIPT_FILENAME"], $separator) === FALSE)
                $separator = '/';
            $exploded = explode($separator, $_SERVER["SCRIPT_FILENAME"]);
            array_pop($exploded);
            $path = implode(DIRECTORY_SEPARATOR, $exploded);
        }

        $opts = array(
            //'debug' => true,
            'roots' => array(
                array(
                    'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
                    'path'          => $path . '/media/userfiles/', // path to files (REQUIRED)
                    'URL'           => '/media/userfiles/', // URL to files (REQUIRED)
                    'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
                )
            )
        );

        // run elFinder
        $connector = new Filemanager_Elfinderconnector(new Filemanager_ElFinder($opts));
        $connector->run();
    }

}