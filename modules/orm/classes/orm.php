<?php defined('SYSPATH') or die('No direct script access.');

class ORM extends Kohana_ORM {
    public static function apply_settings($model, $settings)
    {
        foreach ($settings as $setup => $value) {
            if (empty($value) || (strpos($setup, 'where') !== FALSE && !is_array($value)))
                continue;

            switch (preg_replace('/[0-9]/i', '', $setup)) {
                case 'limit' :
                    if ($value != 0) $model->limit($value);
                    break;
                case 'offset' :
                    if ($value != 0) $model->offset($value);
                    break;
                case 'order_by' :
                    if (is_array($value))
                        $model->order_by($value[0], $value[1]);
                    else
                        $model->order_by($value);
                    break;
                case 'where' :
                    $model->where($value[0], $value[1], $value[2]);
                    break;
                case 'where_more' :
                    $model->where($value[0], '>', $value[1]);
                    break;
                case 'where_less' :
                    $model->where($value[0], '<', $value[1]);
                    break;
                case 'where_more_eq' :
                    $model->where($value[0], '>=', $value[1]);
                    break;
                case 'where_less_eq' :
                    $model->where($value[0], '<=', $value[1]);
                    break;
                case 'where_eq' :
                    $model->where($value[0], '=', $value[1]);
                    break;
                case 'join_on' :
                    $model->join($value[0])->on($value[1], $value[2], $value[3]);
                    break;
            }
        }

        return $model;
    }
}